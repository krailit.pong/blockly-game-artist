var _ = require('lodash');
// import { randomValue } from '../utils';

/**
 * An instantiable Artist API logic. The methods on this object are called by
 * generated user code. As they are called, they insert commands into this.log.
 * NOTE: this.log is also modified in some cases externally (both accessed and
 * I think cleared).
 */
var ArtistAPI = function () {
  this.log = [];
};

module.exports = ArtistAPI;

// ArtistAPI.prototype.random = function (values) {
//   return randomValue(values);
// };

// ArtistAPI.prototype.drawCircle = function (size, id) {
//   for (var i = 0; i < 36; i++) {
//     this.moveForward(size, id);
//     this.turnRight(10, id);
//   }
// };

// ArtistAPI.prototype.drawSnowflake = function (type, id) {
//   var i, j, k;

//   // mirors Blockly.JavaScript.colour_random.
//   var random_colour = function () {
//     var colors = Blockly.FieldColour.COLOURS;
//     return colors[Math.floor(Math.random()*colors.length)];
//   };

//   if (type === 'random') {
//     type = _.sample(['fractal', 'flower', 'spiral', 'line', 'parallelogram', 'square']);
//   }

//   switch (type) {
//     case 'fractal':
//       for (i = 0; i < 8; i++) {
//         this.jumpForward(45, id);
//         this.turnLeft(45, id);
//         for (j = 0; j < 3; j++) {
//           for (k = 0; k < 3; k++) {
//             this.moveForward(15, id);
//             this.moveBackward(15, id);
//             this.turnRight(45, id);
//           }
//           this.turnLeft(90, id);
//           this.moveBackward(15, id);
//           this.turnLeft(45, id);
//         }
//         this.turnRight(90, id);
//       }
//       break;

//     case 'flower':
//       for (i = 0; i < 5; i++) {
//         this.drawCircle(2, id);
//         this.drawCircle(4, id);
//         this.turnRight(72, id);
//       }
//       break;

//     case 'spiral':
//       for (i = 0; i < 20; i++) {
//         this.drawCircle(3, id);
//         this.moveForward(20, id);
//         this.turnRight(18, id);
//       }
//       break;

//     case 'line':
//       for (i = 0; i < 90; i++) {
//         this.penColour(random_colour());
//         this.moveForward(50, id);
//         this.moveBackward(50, id);
//         this.turnRight(4, id);
//       }
//       this.penColour("#FFFFFF", id);
//       break;

//     case 'parallelogram':
//       for (i = 0; i < 10; i++) {
//         for (j = 0; j < 2; j++) {
//           this.moveForward(50, id);
//           this.turnRight(60, id);
//           this.moveForward(50, id);
//           this.turnRight(120, id);
//         }
//         this.turnRight(36, id);
//       }
//       break;

//     case 'square':
//       for (i = 0; i < 10; i++) {
//         for (j = 0; j < 4; j++) {
//           this.moveForward(50, id);
//           this.turnRight(90, id);
//         }
//         this.turnRight(36, id);
//       }
//       break;
//   }
// };


ArtistAPI.prototype.moveForward = function (distance, id) {
  this.log.push(['FD', distance, id]);
};

ArtistAPI.prototype.moveBackward = function (distance, id) {
  this.log.push(['FD', -distance, id]);
};

ArtistAPI.prototype.moveUp = function (distance, id) {
  this.log.push(['MV', distance, 0, id]);
};

ArtistAPI.prototype.moveDown = function (distance, id) {
  this.log.push(['MV', distance, 180, id]);
};

ArtistAPI.prototype.moveLeft = function (distance, id) {
  this.log.push(['MV', distance, 270, id]);
};

ArtistAPI.prototype.moveRight = function (distance, id) {
  this.log.push(['MV', distance, 90, id]);
};

ArtistAPI.prototype.jumpTo = function (position, id) {
  this.log.push(['JT', position, id]);
};

ArtistAPI.prototype.jumpToXY = function (x, y, id) {
  this.log.push(['JT', [x, y], id]);
};

ArtistAPI.prototype.moveUpRight = function (distance, id) {
  this.log.push(['MD', distance, 45, id]);
};

ArtistAPI.prototype.moveDownRight = function (distance, id) {
  this.log.push(['MD', distance, 135, id]);
};

ArtistAPI.prototype.moveDownLeft = function (distance, id) {
  this.log.push(['MD', distance, 225, id]);
};

ArtistAPI.prototype.moveUpLeft = function (distance, id) {
  this.log.push(['MD', distance, 315, id]);
};

ArtistAPI.prototype.jumpUp = function (distance, id) {
  this.log.push(['JD', distance, 0, id]);
};

ArtistAPI.prototype.jumpDown = function (distance, id) {
  this.log.push(['JD', distance, 180, id]);
};

ArtistAPI.prototype.jumpLeft = function (distance, id) {
  this.log.push(['JD', distance, 270, id]);
};

ArtistAPI.prototype.jumpRight = function (distance, id) {
  this.log.push(['JD', distance, 90, id]);
};

ArtistAPI.prototype.jumpUpRight = function (distance, id) {
  this.log.push(['JD', distance, 45, id]);
};

ArtistAPI.prototype.jumpDownRight = function (distance, id) {
  this.log.push(['JD', distance, 135, id]);
};

ArtistAPI.prototype.jumpDownLeft = function (distance, id) {
  this.log.push(['JD', distance, 225, id]);
};

ArtistAPI.prototype.jumpUpLeft = function (distance, id) {
  this.log.push(['JD', distance, 315, id]);
};

ArtistAPI.prototype.jumpForward = function (distance, id) {
  this.log.push(['JF', distance, id]);
};

ArtistAPI.prototype.jumpBackward = function (distance, id) {
  this.log.push(['JF', -distance, id]);
};

ArtistAPI.prototype.turnRight = function (angle, id) {
  this.log.push(['RT', angle, id]);
};

ArtistAPI.prototype.turnLeft = function (angle, id) {
  this.log.push(['RT', -angle, id]);
};

ArtistAPI.prototype.pointTo = function (angle, id) {
  this.log.push(['PT', angle, id]);
};

ArtistAPI.prototype.globalAlpha = function (alpha, id) {
  this.log.push(['GA', alpha, id]);
};

ArtistAPI.prototype.penUp = function (id) {
  this.log.push(['PU', id]);
};

ArtistAPI.prototype.penDown = function (id) {
  this.log.push(['PD', id]);
};

ArtistAPI.prototype.penWidth = function (width, id) {
  this.log.push(['PW', Math.max(width, 0), id]);
};

ArtistAPI.prototype.penColour = function (colour, id) {
  this.log.push(['PC', colour, id]);
};

ArtistAPI.prototype.penPattern = function (pattern, id) {
  this.log.push(['PS', pattern, id]);
};

ArtistAPI.prototype.hideTurtle = function (id) {
  this.log.push(['HT', id]);
};

ArtistAPI.prototype.showTurtle = function (id) {
  this.log.push(['ST', id]);
};

ArtistAPI.prototype.drawSticker = function (sticker, size, id) {
  this.log.push(['sticker', sticker, size, id]);
};

ArtistAPI.prototype.setArtist = function (artist, id) {
  this.log.push(['setArtist', artist, id]);
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy91dGlscy9hcGkuanMiXSwibmFtZXMiOlsiXyIsInJlcXVpcmUiLCJBcnRpc3RBUEkiLCJsb2ciLCJtb2R1bGUiLCJleHBvcnRzIiwicHJvdG90eXBlIiwibW92ZUZvcndhcmQiLCJkaXN0YW5jZSIsImlkIiwicHVzaCIsIm1vdmVCYWNrd2FyZCIsIm1vdmVVcCIsIm1vdmVEb3duIiwibW92ZUxlZnQiLCJtb3ZlUmlnaHQiLCJqdW1wVG8iLCJwb3NpdGlvbiIsImp1bXBUb1hZIiwieCIsInkiLCJtb3ZlVXBSaWdodCIsIm1vdmVEb3duUmlnaHQiLCJtb3ZlRG93bkxlZnQiLCJtb3ZlVXBMZWZ0IiwianVtcFVwIiwianVtcERvd24iLCJqdW1wTGVmdCIsImp1bXBSaWdodCIsImp1bXBVcFJpZ2h0IiwianVtcERvd25SaWdodCIsImp1bXBEb3duTGVmdCIsImp1bXBVcExlZnQiLCJqdW1wRm9yd2FyZCIsImp1bXBCYWNrd2FyZCIsInR1cm5SaWdodCIsImFuZ2xlIiwidHVybkxlZnQiLCJwb2ludFRvIiwiZ2xvYmFsQWxwaGEiLCJhbHBoYSIsInBlblVwIiwicGVuRG93biIsInBlbldpZHRoIiwid2lkdGgiLCJNYXRoIiwibWF4IiwicGVuQ29sb3VyIiwiY29sb3VyIiwicGVuUGF0dGVybiIsInBhdHRlcm4iLCJoaWRlVHVydGxlIiwic2hvd1R1cnRsZSIsImRyYXdTdGlja2VyIiwic3RpY2tlciIsInNpemUiLCJzZXRBcnRpc3QiLCJhcnRpc3QiXSwibWFwcGluZ3MiOiJBQUFBLElBQUlBLElBQUlDLFFBQVEsUUFBUixDQUFSO0FBQ0E7O0FBRUE7Ozs7OztBQU1BLElBQUlDLFlBQVksWUFBWTtBQUMxQixPQUFLQyxHQUFMLEdBQVcsRUFBWDtBQUNELENBRkQ7O0FBSUFDLE9BQU9DLE9BQVAsR0FBaUJILFNBQWpCOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0FBLFVBQVVJLFNBQVYsQ0FBb0JDLFdBQXBCLEdBQWtDLFVBQVVDLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ3hELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCQyxFQUFqQixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQkssWUFBcEIsR0FBbUMsVUFBVUgsUUFBVixFQUFvQkMsRUFBcEIsRUFBd0I7QUFDekQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU8sQ0FBQ0YsUUFBUixFQUFrQkMsRUFBbEIsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0JNLE1BQXBCLEdBQTZCLFVBQVVKLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ25ELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLENBQWpCLEVBQW9CQyxFQUFwQixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQk8sUUFBcEIsR0FBK0IsVUFBVUwsUUFBVixFQUFvQkMsRUFBcEIsRUFBd0I7QUFDckQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU9GLFFBQVAsRUFBaUIsR0FBakIsRUFBc0JDLEVBQXRCLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CUSxRQUFwQixHQUErQixVQUFVTixRQUFWLEVBQW9CQyxFQUFwQixFQUF3QjtBQUNyRCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT0YsUUFBUCxFQUFpQixHQUFqQixFQUFzQkMsRUFBdEIsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0JTLFNBQXBCLEdBQWdDLFVBQVVQLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ3RELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLEVBQWpCLEVBQXFCQyxFQUFyQixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQlUsTUFBcEIsR0FBNkIsVUFBVUMsUUFBVixFQUFvQlIsRUFBcEIsRUFBd0I7QUFDbkQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU9PLFFBQVAsRUFBaUJSLEVBQWpCLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CWSxRQUFwQixHQUErQixVQUFVQyxDQUFWLEVBQWFDLENBQWIsRUFBZ0JYLEVBQWhCLEVBQW9CO0FBQ2pELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPLENBQUNTLENBQUQsRUFBSUMsQ0FBSixDQUFQLEVBQWVYLEVBQWYsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0JlLFdBQXBCLEdBQWtDLFVBQVViLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ3hELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLEVBQWpCLEVBQXFCQyxFQUFyQixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQmdCLGFBQXBCLEdBQW9DLFVBQVVkLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQzFELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLEdBQWpCLEVBQXNCQyxFQUF0QixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQmlCLFlBQXBCLEdBQW1DLFVBQVVmLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ3pELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLEdBQWpCLEVBQXNCQyxFQUF0QixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQmtCLFVBQXBCLEdBQWlDLFVBQVVoQixRQUFWLEVBQW9CQyxFQUFwQixFQUF3QjtBQUN2RCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT0YsUUFBUCxFQUFpQixHQUFqQixFQUFzQkMsRUFBdEIsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0JtQixNQUFwQixHQUE2QixVQUFVakIsUUFBVixFQUFvQkMsRUFBcEIsRUFBd0I7QUFDbkQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU9GLFFBQVAsRUFBaUIsQ0FBakIsRUFBb0JDLEVBQXBCLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9Cb0IsUUFBcEIsR0FBK0IsVUFBVWxCLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ3JELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLEdBQWpCLEVBQXNCQyxFQUF0QixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQnFCLFFBQXBCLEdBQStCLFVBQVVuQixRQUFWLEVBQW9CQyxFQUFwQixFQUF3QjtBQUNyRCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT0YsUUFBUCxFQUFpQixHQUFqQixFQUFzQkMsRUFBdEIsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0JzQixTQUFwQixHQUFnQyxVQUFVcEIsUUFBVixFQUFvQkMsRUFBcEIsRUFBd0I7QUFDdEQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU9GLFFBQVAsRUFBaUIsRUFBakIsRUFBcUJDLEVBQXJCLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CdUIsV0FBcEIsR0FBa0MsVUFBVXJCLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ3hELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLEVBQWpCLEVBQXFCQyxFQUFyQixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQndCLGFBQXBCLEdBQW9DLFVBQVV0QixRQUFWLEVBQW9CQyxFQUFwQixFQUF3QjtBQUMxRCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT0YsUUFBUCxFQUFpQixHQUFqQixFQUFzQkMsRUFBdEIsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0J5QixZQUFwQixHQUFtQyxVQUFVdkIsUUFBVixFQUFvQkMsRUFBcEIsRUFBd0I7QUFDekQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU9GLFFBQVAsRUFBaUIsR0FBakIsRUFBc0JDLEVBQXRCLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CMEIsVUFBcEIsR0FBaUMsVUFBVXhCLFFBQVYsRUFBb0JDLEVBQXBCLEVBQXdCO0FBQ3ZELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRixRQUFQLEVBQWlCLEdBQWpCLEVBQXNCQyxFQUF0QixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQjJCLFdBQXBCLEdBQWtDLFVBQVV6QixRQUFWLEVBQW9CQyxFQUFwQixFQUF3QjtBQUN4RCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT0YsUUFBUCxFQUFpQkMsRUFBakIsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0I0QixZQUFwQixHQUFtQyxVQUFVMUIsUUFBVixFQUFvQkMsRUFBcEIsRUFBd0I7QUFDekQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU8sQ0FBQ0YsUUFBUixFQUFrQkMsRUFBbEIsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0I2QixTQUFwQixHQUFnQyxVQUFVQyxLQUFWLEVBQWlCM0IsRUFBakIsRUFBcUI7QUFDbkQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU8wQixLQUFQLEVBQWMzQixFQUFkLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CK0IsUUFBcEIsR0FBK0IsVUFBVUQsS0FBVixFQUFpQjNCLEVBQWpCLEVBQXFCO0FBQ2xELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPLENBQUMwQixLQUFSLEVBQWUzQixFQUFmLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CZ0MsT0FBcEIsR0FBOEIsVUFBVUYsS0FBVixFQUFpQjNCLEVBQWpCLEVBQXFCO0FBQ2pELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPMEIsS0FBUCxFQUFjM0IsRUFBZCxDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQmlDLFdBQXBCLEdBQWtDLFVBQVVDLEtBQVYsRUFBaUIvQixFQUFqQixFQUFxQjtBQUNyRCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBTzhCLEtBQVAsRUFBYy9CLEVBQWQsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0JtQyxLQUFwQixHQUE0QixVQUFVaEMsRUFBVixFQUFjO0FBQ3hDLE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPRCxFQUFQLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9Cb0MsT0FBcEIsR0FBOEIsVUFBVWpDLEVBQVYsRUFBYztBQUMxQyxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT0QsRUFBUCxDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQnFDLFFBQXBCLEdBQStCLFVBQVVDLEtBQVYsRUFBaUJuQyxFQUFqQixFQUFxQjtBQUNsRCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT21DLEtBQUtDLEdBQUwsQ0FBU0YsS0FBVCxFQUFnQixDQUFoQixDQUFQLEVBQTJCbkMsRUFBM0IsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0J5QyxTQUFwQixHQUFnQyxVQUFVQyxNQUFWLEVBQWtCdkMsRUFBbEIsRUFBc0I7QUFDcEQsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU9zQyxNQUFQLEVBQWV2QyxFQUFmLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CMkMsVUFBcEIsR0FBaUMsVUFBVUMsT0FBVixFQUFtQnpDLEVBQW5CLEVBQXVCO0FBQ3RELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsSUFBRCxFQUFPd0MsT0FBUCxFQUFnQnpDLEVBQWhCLENBQWQ7QUFDRCxDQUZEOztBQUlBUCxVQUFVSSxTQUFWLENBQW9CNkMsVUFBcEIsR0FBaUMsVUFBVTFDLEVBQVYsRUFBYztBQUM3QyxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLElBQUQsRUFBT0QsRUFBUCxDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQjhDLFVBQXBCLEdBQWlDLFVBQVUzQyxFQUFWLEVBQWM7QUFDN0MsT0FBS04sR0FBTCxDQUFTTyxJQUFULENBQWMsQ0FBQyxJQUFELEVBQU9ELEVBQVAsQ0FBZDtBQUNELENBRkQ7O0FBSUFQLFVBQVVJLFNBQVYsQ0FBb0IrQyxXQUFwQixHQUFrQyxVQUFVQyxPQUFWLEVBQW1CQyxJQUFuQixFQUF5QjlDLEVBQXpCLEVBQTZCO0FBQzdELE9BQUtOLEdBQUwsQ0FBU08sSUFBVCxDQUFjLENBQUMsU0FBRCxFQUFZNEMsT0FBWixFQUFxQkMsSUFBckIsRUFBMkI5QyxFQUEzQixDQUFkO0FBQ0QsQ0FGRDs7QUFJQVAsVUFBVUksU0FBVixDQUFvQmtELFNBQXBCLEdBQWdDLFVBQVVDLE1BQVYsRUFBa0JoRCxFQUFsQixFQUFzQjtBQUNwRCxPQUFLTixHQUFMLENBQVNPLElBQVQsQ0FBYyxDQUFDLFdBQUQsRUFBYytDLE1BQWQsRUFBc0JoRCxFQUF0QixDQUFkO0FBQ0QsQ0FGRCIsImZpbGUiOiJhcGkuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgXyA9IHJlcXVpcmUoJ2xvZGFzaCcpO1xuLy8gaW1wb3J0IHsgcmFuZG9tVmFsdWUgfSBmcm9tICcuLi91dGlscyc7XG5cbi8qKlxuICogQW4gaW5zdGFudGlhYmxlIEFydGlzdCBBUEkgbG9naWMuIFRoZSBtZXRob2RzIG9uIHRoaXMgb2JqZWN0IGFyZSBjYWxsZWQgYnlcbiAqIGdlbmVyYXRlZCB1c2VyIGNvZGUuIEFzIHRoZXkgYXJlIGNhbGxlZCwgdGhleSBpbnNlcnQgY29tbWFuZHMgaW50byB0aGlzLmxvZy5cbiAqIE5PVEU6IHRoaXMubG9nIGlzIGFsc28gbW9kaWZpZWQgaW4gc29tZSBjYXNlcyBleHRlcm5hbGx5IChib3RoIGFjY2Vzc2VkIGFuZFxuICogSSB0aGluayBjbGVhcmVkKS5cbiAqL1xudmFyIEFydGlzdEFQSSA9IGZ1bmN0aW9uICgpIHtcbiAgdGhpcy5sb2cgPSBbXTtcbn07XG5cbm1vZHVsZS5leHBvcnRzID0gQXJ0aXN0QVBJO1xuXG4vLyBBcnRpc3RBUEkucHJvdG90eXBlLnJhbmRvbSA9IGZ1bmN0aW9uICh2YWx1ZXMpIHtcbi8vICAgcmV0dXJuIHJhbmRvbVZhbHVlKHZhbHVlcyk7XG4vLyB9O1xuXG4vLyBBcnRpc3RBUEkucHJvdG90eXBlLmRyYXdDaXJjbGUgPSBmdW5jdGlvbiAoc2l6ZSwgaWQpIHtcbi8vICAgZm9yICh2YXIgaSA9IDA7IGkgPCAzNjsgaSsrKSB7XG4vLyAgICAgdGhpcy5tb3ZlRm9yd2FyZChzaXplLCBpZCk7XG4vLyAgICAgdGhpcy50dXJuUmlnaHQoMTAsIGlkKTtcbi8vICAgfVxuLy8gfTtcblxuLy8gQXJ0aXN0QVBJLnByb3RvdHlwZS5kcmF3U25vd2ZsYWtlID0gZnVuY3Rpb24gKHR5cGUsIGlkKSB7XG4vLyAgIHZhciBpLCBqLCBrO1xuXG4vLyAgIC8vIG1pcm9ycyBCbG9ja2x5LkphdmFTY3JpcHQuY29sb3VyX3JhbmRvbS5cbi8vICAgdmFyIHJhbmRvbV9jb2xvdXIgPSBmdW5jdGlvbiAoKSB7XG4vLyAgICAgdmFyIGNvbG9ycyA9IEJsb2NrbHkuRmllbGRDb2xvdXIuQ09MT1VSUztcbi8vICAgICByZXR1cm4gY29sb3JzW01hdGguZmxvb3IoTWF0aC5yYW5kb20oKSpjb2xvcnMubGVuZ3RoKV07XG4vLyAgIH07XG5cbi8vICAgaWYgKHR5cGUgPT09ICdyYW5kb20nKSB7XG4vLyAgICAgdHlwZSA9IF8uc2FtcGxlKFsnZnJhY3RhbCcsICdmbG93ZXInLCAnc3BpcmFsJywgJ2xpbmUnLCAncGFyYWxsZWxvZ3JhbScsICdzcXVhcmUnXSk7XG4vLyAgIH1cblxuLy8gICBzd2l0Y2ggKHR5cGUpIHtcbi8vICAgICBjYXNlICdmcmFjdGFsJzpcbi8vICAgICAgIGZvciAoaSA9IDA7IGkgPCA4OyBpKyspIHtcbi8vICAgICAgICAgdGhpcy5qdW1wRm9yd2FyZCg0NSwgaWQpO1xuLy8gICAgICAgICB0aGlzLnR1cm5MZWZ0KDQ1LCBpZCk7XG4vLyAgICAgICAgIGZvciAoaiA9IDA7IGogPCAzOyBqKyspIHtcbi8vICAgICAgICAgICBmb3IgKGsgPSAwOyBrIDwgMzsgaysrKSB7XG4vLyAgICAgICAgICAgICB0aGlzLm1vdmVGb3J3YXJkKDE1LCBpZCk7XG4vLyAgICAgICAgICAgICB0aGlzLm1vdmVCYWNrd2FyZCgxNSwgaWQpO1xuLy8gICAgICAgICAgICAgdGhpcy50dXJuUmlnaHQoNDUsIGlkKTtcbi8vICAgICAgICAgICB9XG4vLyAgICAgICAgICAgdGhpcy50dXJuTGVmdCg5MCwgaWQpO1xuLy8gICAgICAgICAgIHRoaXMubW92ZUJhY2t3YXJkKDE1LCBpZCk7XG4vLyAgICAgICAgICAgdGhpcy50dXJuTGVmdCg0NSwgaWQpO1xuLy8gICAgICAgICB9XG4vLyAgICAgICAgIHRoaXMudHVyblJpZ2h0KDkwLCBpZCk7XG4vLyAgICAgICB9XG4vLyAgICAgICBicmVhaztcblxuLy8gICAgIGNhc2UgJ2Zsb3dlcic6XG4vLyAgICAgICBmb3IgKGkgPSAwOyBpIDwgNTsgaSsrKSB7XG4vLyAgICAgICAgIHRoaXMuZHJhd0NpcmNsZSgyLCBpZCk7XG4vLyAgICAgICAgIHRoaXMuZHJhd0NpcmNsZSg0LCBpZCk7XG4vLyAgICAgICAgIHRoaXMudHVyblJpZ2h0KDcyLCBpZCk7XG4vLyAgICAgICB9XG4vLyAgICAgICBicmVhaztcblxuLy8gICAgIGNhc2UgJ3NwaXJhbCc6XG4vLyAgICAgICBmb3IgKGkgPSAwOyBpIDwgMjA7IGkrKykge1xuLy8gICAgICAgICB0aGlzLmRyYXdDaXJjbGUoMywgaWQpO1xuLy8gICAgICAgICB0aGlzLm1vdmVGb3J3YXJkKDIwLCBpZCk7XG4vLyAgICAgICAgIHRoaXMudHVyblJpZ2h0KDE4LCBpZCk7XG4vLyAgICAgICB9XG4vLyAgICAgICBicmVhaztcblxuLy8gICAgIGNhc2UgJ2xpbmUnOlxuLy8gICAgICAgZm9yIChpID0gMDsgaSA8IDkwOyBpKyspIHtcbi8vICAgICAgICAgdGhpcy5wZW5Db2xvdXIocmFuZG9tX2NvbG91cigpKTtcbi8vICAgICAgICAgdGhpcy5tb3ZlRm9yd2FyZCg1MCwgaWQpO1xuLy8gICAgICAgICB0aGlzLm1vdmVCYWNrd2FyZCg1MCwgaWQpO1xuLy8gICAgICAgICB0aGlzLnR1cm5SaWdodCg0LCBpZCk7XG4vLyAgICAgICB9XG4vLyAgICAgICB0aGlzLnBlbkNvbG91cihcIiNGRkZGRkZcIiwgaWQpO1xuLy8gICAgICAgYnJlYWs7XG5cbi8vICAgICBjYXNlICdwYXJhbGxlbG9ncmFtJzpcbi8vICAgICAgIGZvciAoaSA9IDA7IGkgPCAxMDsgaSsrKSB7XG4vLyAgICAgICAgIGZvciAoaiA9IDA7IGogPCAyOyBqKyspIHtcbi8vICAgICAgICAgICB0aGlzLm1vdmVGb3J3YXJkKDUwLCBpZCk7XG4vLyAgICAgICAgICAgdGhpcy50dXJuUmlnaHQoNjAsIGlkKTtcbi8vICAgICAgICAgICB0aGlzLm1vdmVGb3J3YXJkKDUwLCBpZCk7XG4vLyAgICAgICAgICAgdGhpcy50dXJuUmlnaHQoMTIwLCBpZCk7XG4vLyAgICAgICAgIH1cbi8vICAgICAgICAgdGhpcy50dXJuUmlnaHQoMzYsIGlkKTtcbi8vICAgICAgIH1cbi8vICAgICAgIGJyZWFrO1xuXG4vLyAgICAgY2FzZSAnc3F1YXJlJzpcbi8vICAgICAgIGZvciAoaSA9IDA7IGkgPCAxMDsgaSsrKSB7XG4vLyAgICAgICAgIGZvciAoaiA9IDA7IGogPCA0OyBqKyspIHtcbi8vICAgICAgICAgICB0aGlzLm1vdmVGb3J3YXJkKDUwLCBpZCk7XG4vLyAgICAgICAgICAgdGhpcy50dXJuUmlnaHQoOTAsIGlkKTtcbi8vICAgICAgICAgfVxuLy8gICAgICAgICB0aGlzLnR1cm5SaWdodCgzNiwgaWQpO1xuLy8gICAgICAgfVxuLy8gICAgICAgYnJlYWs7XG4vLyAgIH1cbi8vIH07XG5cblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5tb3ZlRm9yd2FyZCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ0ZEJywgZGlzdGFuY2UsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLm1vdmVCYWNrd2FyZCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ0ZEJywgLWRpc3RhbmNlLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5tb3ZlVXAgPSBmdW5jdGlvbiAoZGlzdGFuY2UsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydNVicsIGRpc3RhbmNlLCAwLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5tb3ZlRG93biA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ01WJywgZGlzdGFuY2UsIDE4MCwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUubW92ZUxlZnQgPSBmdW5jdGlvbiAoZGlzdGFuY2UsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydNVicsIGRpc3RhbmNlLCAyNzAsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLm1vdmVSaWdodCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ01WJywgZGlzdGFuY2UsIDkwLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5qdW1wVG8gPSBmdW5jdGlvbiAocG9zaXRpb24sIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydKVCcsIHBvc2l0aW9uLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5qdW1wVG9YWSA9IGZ1bmN0aW9uICh4LCB5LCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnSlQnLCBbeCwgeV0sIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLm1vdmVVcFJpZ2h0ID0gZnVuY3Rpb24gKGRpc3RhbmNlLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnTUQnLCBkaXN0YW5jZSwgNDUsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLm1vdmVEb3duUmlnaHQgPSBmdW5jdGlvbiAoZGlzdGFuY2UsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydNRCcsIGRpc3RhbmNlLCAxMzUsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLm1vdmVEb3duTGVmdCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ01EJywgZGlzdGFuY2UsIDIyNSwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUubW92ZVVwTGVmdCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ01EJywgZGlzdGFuY2UsIDMxNSwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUuanVtcFVwID0gZnVuY3Rpb24gKGRpc3RhbmNlLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnSkQnLCBkaXN0YW5jZSwgMCwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUuanVtcERvd24gPSBmdW5jdGlvbiAoZGlzdGFuY2UsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydKRCcsIGRpc3RhbmNlLCAxODAsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLmp1bXBMZWZ0ID0gZnVuY3Rpb24gKGRpc3RhbmNlLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnSkQnLCBkaXN0YW5jZSwgMjcwLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5qdW1wUmlnaHQgPSBmdW5jdGlvbiAoZGlzdGFuY2UsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydKRCcsIGRpc3RhbmNlLCA5MCwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUuanVtcFVwUmlnaHQgPSBmdW5jdGlvbiAoZGlzdGFuY2UsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydKRCcsIGRpc3RhbmNlLCA0NSwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUuanVtcERvd25SaWdodCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ0pEJywgZGlzdGFuY2UsIDEzNSwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUuanVtcERvd25MZWZ0ID0gZnVuY3Rpb24gKGRpc3RhbmNlLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnSkQnLCBkaXN0YW5jZSwgMjI1LCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5qdW1wVXBMZWZ0ID0gZnVuY3Rpb24gKGRpc3RhbmNlLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnSkQnLCBkaXN0YW5jZSwgMzE1LCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5qdW1wRm9yd2FyZCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ0pGJywgZGlzdGFuY2UsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLmp1bXBCYWNrd2FyZCA9IGZ1bmN0aW9uIChkaXN0YW5jZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ0pGJywgLWRpc3RhbmNlLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS50dXJuUmlnaHQgPSBmdW5jdGlvbiAoYW5nbGUsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydSVCcsIGFuZ2xlLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS50dXJuTGVmdCA9IGZ1bmN0aW9uIChhbmdsZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ1JUJywgLWFuZ2xlLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5wb2ludFRvID0gZnVuY3Rpb24gKGFuZ2xlLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnUFQnLCBhbmdsZSwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUuZ2xvYmFsQWxwaGEgPSBmdW5jdGlvbiAoYWxwaGEsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydHQScsIGFscGhhLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5wZW5VcCA9IGZ1bmN0aW9uIChpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnUFUnLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5wZW5Eb3duID0gZnVuY3Rpb24gKGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydQRCcsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLnBlbldpZHRoID0gZnVuY3Rpb24gKHdpZHRoLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnUFcnLCBNYXRoLm1heCh3aWR0aCwgMCksIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLnBlbkNvbG91ciA9IGZ1bmN0aW9uIChjb2xvdXIsIGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydQQycsIGNvbG91ciwgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUucGVuUGF0dGVybiA9IGZ1bmN0aW9uIChwYXR0ZXJuLCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnUFMnLCBwYXR0ZXJuLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5oaWRlVHVydGxlID0gZnVuY3Rpb24gKGlkKSB7XG4gIHRoaXMubG9nLnB1c2goWydIVCcsIGlkXSk7XG59O1xuXG5BcnRpc3RBUEkucHJvdG90eXBlLnNob3dUdXJ0bGUgPSBmdW5jdGlvbiAoaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ1NUJywgaWRdKTtcbn07XG5cbkFydGlzdEFQSS5wcm90b3R5cGUuZHJhd1N0aWNrZXIgPSBmdW5jdGlvbiAoc3RpY2tlciwgc2l6ZSwgaWQpIHtcbiAgdGhpcy5sb2cucHVzaChbJ3N0aWNrZXInLCBzdGlja2VyLCBzaXplLCBpZF0pO1xufTtcblxuQXJ0aXN0QVBJLnByb3RvdHlwZS5zZXRBcnRpc3QgPSBmdW5jdGlvbiAoYXJ0aXN0LCBpZCkge1xuICB0aGlzLmxvZy5wdXNoKFsnc2V0QXJ0aXN0JywgYXJ0aXN0LCBpZF0pO1xufTtcbiJdfQ==