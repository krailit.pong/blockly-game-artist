/**
 * @fileoverview Constants used in production code and tests.
 */

/**
 * Enumeration of user program execution outcomes.
 * These are determined by each app.
 */
export const ResultType = {
  UNSET: 0, // The result has not yet been computed.
  SUCCESS: 1, // The program completed successfully, achieving the goal.
  FAILURE: -1, // The program ran without error but did not achieve goal.
  TIMEOUT: 2, // The program did not complete (likely infinite loop).
  ERROR: -2 // The program generated an error.
};

/**
 * @typedef {number} TestResult
 */

/**
 * Enumeration of test results.
 * EMPTY_BLOCK_FAIL and EMPTY_FUNCTION_BLOCK_FAIL can only occur if
 * StudioApp.checkForEmptyBlocks_ is true.
 * A number of these results are enumerated on the dashboard side in
 * activity_constants.rb, and it's important that these two files are kept in
 * sync.
 * NOTE: We store the results for user attempts in our db, so changing these
 * values would necessitate a migration
 *
 * @enum {number}
 */
export const TestResults = {
  // Default value before any tests are run.
  NO_TESTS_RUN: -1,

  // The level was not solved.
  GENERIC_FAIL: 0, // Used by DSL defined levels.
  EMPTY_BLOCK_FAIL: 1, // An "if" or "repeat" block was empty.
  TOO_FEW_BLOCKS_FAIL: 2, // Fewer than the ideal number of blocks used.
  LEVEL_INCOMPLETE_FAIL: 3, // Default failure to complete a level.
  MISSING_BLOCK_UNFINISHED: 4, // A required block was not used.
  EXTRA_TOP_BLOCKS_FAIL: 5, // There was more than one top-level block.
  RUNTIME_ERROR_FAIL: 6, // There was a runtime error in the program.
  SYNTAX_ERROR_FAIL: 7, // There was a syntax error in the program.
  MISSING_BLOCK_FINISHED: 10, // The level was solved without required block.
  APP_SPECIFIC_FAIL: 11, // Application-specific failure.
  EMPTY_FUNCTION_BLOCK_FAIL: 12, // A "function" block was empty
  UNUSED_PARAM: 13, // Param declared but not used in function.
  UNUSED_FUNCTION: 14, // Function declared but not used in workspace.
  PARAM_INPUT_UNATTACHED: 15, // Function not called with enough params.
  INCOMPLETE_BLOCK_IN_FUNCTION: 16, // Incomplete block inside a function.
  QUESTION_MARKS_IN_NUMBER_FIELD: 17, // Block has ??? instead of a value.
  EMPTY_FUNCTIONAL_BLOCK: 18, // There's a functional block with an open input
  EXAMPLE_FAILED: 19, // One of our examples didn't match the definition

  // start using negative values, since we consider >= 20 to be "solved"
  NESTED_FOR_SAME_VARIABLE: -2, // We have nested for loops each using the same counter variable
  // NOTE: for smoe period of time, this was -1 and conflicted with NO_TESTS_RUN
  EMPTY_FUNCTION_NAME: -3, // We have a variable or function with the name ""
  MISSING_RECOMMENDED_BLOCK_UNFINISHED: -4, // The level was attempted but not solved without a recommended block
  EXTRA_FUNCTION_FAIL: -5, // The program contains a JavaScript function when it should not
  LOCAL_FUNCTION_FAIL: -6, // The program contains an unexpected JavaScript local function
  GENERIC_LINT_FAIL: -7, // The program contains a lint error
  LOG_CONDITION_FAIL: -8, // The program execution log did not pass a required condition
  BLOCK_LIMIT_FAIL: -9, // Puzzle was solved using more than the toolbox limit of a block

  // Codes for unvalidated levels.
  UNSUBMITTED_ATTEMPT: -50, // Progress was saved without submitting for review, or was unsubmitted.

  SKIPPED: -100, // Skipped, e.g. they used the skip button on a challenge level

  // Numbers below 20 are generally considered some form of failure.
  // Numbers >= 20 generally indicate some form of success (although again there
  // are values like REVIEW_REJECTED_RESULT that don't seem to quite meet that restriction.
  MINIMUM_PASS_RESULT: 20,

  // The level was solved in a non-optimal way.  User may advance or retry.
  TOO_MANY_BLOCKS_FAIL: 20, // More than the ideal number of blocks were used.
  APP_SPECIFIC_ACCEPTABLE_FAIL: 21, // Application-specific acceptable failure.
  MISSING_RECOMMENDED_BLOCK_FINISHED: 22, // The level was solved without a recommended block

  // Numbers >= 30, are considered to be "perfectly" solved, i.e. those in the range
  // of 20-30 have correct but not optimal solutions
  MINIMUM_OPTIMAL_RESULT: 30,

  // The level was solved in an optimal way.
  FREE_PLAY: 30, // The user is in free-play mode.
  PASS_WITH_EXTRA_TOP_BLOCKS: 31, // There was more than one top-level block.
  APP_SPECIFIC_IMPERFECT_PASS: 32, // The level was passed in some optimal but not necessarily perfect way
  EDIT_BLOCKS: 70, // The user is creating/editing a new level.
  MANUAL_PASS: 90, // The level was manually set as perfected internally.

  // The level was solved in the ideal manner.
  ALL_PASS: 100,

  // Contained level result. Not validated, but should be treated as a success
  CONTAINED_LEVEL_RESULT: 101,

  // The level was solved with fewer blocks than the recommended number of blocks.
  BETTER_THAN_IDEAL: 102,

  SUBMITTED_RESULT: 1000,
  LOCKED_RESULT: 1001,
  READONLY_SUBMISSION_RESULT: 1002,
  REVIEW_REJECTED_RESULT: 1500,
  REVIEW_ACCEPTED_RESULT: 2000
};

export const BeeTerminationValue = {
  FAILURE: false,
  SUCCESS: true,
  INFINITE_LOOP: Infinity,
  NOT_AT_FLOWER: 1, // Tried to get nectar when not at flower.
  FLOWER_EMPTY: 2, // Tried to get nectar when flower empty.
  NOT_AT_HONEYCOMB: 3, // Tried to make honey when not at honeycomb.
  HONEYCOMB_FULL: 4, // Tried to make honey, but no room at honeycomb.
  UNCHECKED_CLOUD: 5, // Finished puzzle, but didn't check every clouded item
  UNCHECKED_PURPLE: 6, // Finished puzzle, but didn't check every purple flower
  INSUFFICIENT_NECTAR: 7, // Didn't collect all nectar by finish
  INSUFFICIENT_HONEY: 8, // Didn't make all honey by finish
  DID_NOT_COLLECT_EVERYTHING: 9 // For quantum levels, didn't try to collect all available honey/nectar
};

export const HarvesterTerminationValue = {
  WRONG_CROP: 1,
  EMPTY_CROP: 2,
  DID_NOT_COLLECT_EVERYTHING: 3
};

export const KeyCodes = {
  BACKSPACE: 8,
  ENTER: 13,
  SPACE: 32,
  LEFT: 37,
  UP: 38,
  RIGHT: 39,
  DOWN: 40,
  COPY: 67,
  PASTE: 86,
  DELETE: 127
};

export const Position = {
  OUTTOPOUTLEFT: 1,
  OUTTOPLEFT: 2,
  OUTTOPCENTER: 3,
  OUTTOPRIGHT: 4,
  OUTTOPOUTRIGHT: 5,
  TOPOUTLEFT: 6,
  TOPLEFT: 7,
  TOPCENTER: 8,
  TOPRIGHT: 9,
  TOPOUTRIGHT: 10,
  MIDDLEOUTLEFT: 11,
  MIDDLELEFT: 12,
  MIDDLECENTER: 13,
  MIDDLERIGHT: 14,
  MIDDLEOUTRIGHT: 15,
  BOTTOMOUTLEFT: 16,
  BOTTOMLEFT: 17,
  BOTTOMCENTER: 18,
  BOTTOMRIGHT: 19,
  BOTTOMOUTRIGHT: 20,
  OUTBOTTOMOUTLEFT: 21,
  OUTBOTTOMLEFT: 22,
  OUTBOTTOMCENTER: 23,
  OUTBOTTOMRIGHT: 24,
  OUTBOTTOMOUTRIGHT: 25
};

/** @const {string} SVG element namespace */
export const SVG_NS = 'http://www.w3.org/2000/svg';

export const ALPHABET = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
export const CIPHER = 'Iq61F8kiaUHPGcsY7DgX4yAu3LwtWhnCmeR5pVrJoKfQZMx0BSdlOjEv2TbN9z';
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9jb25zdGFudHMuanMiXSwibmFtZXMiOlsiUmVzdWx0VHlwZSIsIlVOU0VUIiwiU1VDQ0VTUyIsIkZBSUxVUkUiLCJUSU1FT1VUIiwiRVJST1IiLCJUZXN0UmVzdWx0cyIsIk5PX1RFU1RTX1JVTiIsIkdFTkVSSUNfRkFJTCIsIkVNUFRZX0JMT0NLX0ZBSUwiLCJUT09fRkVXX0JMT0NLU19GQUlMIiwiTEVWRUxfSU5DT01QTEVURV9GQUlMIiwiTUlTU0lOR19CTE9DS19VTkZJTklTSEVEIiwiRVhUUkFfVE9QX0JMT0NLU19GQUlMIiwiUlVOVElNRV9FUlJPUl9GQUlMIiwiU1lOVEFYX0VSUk9SX0ZBSUwiLCJNSVNTSU5HX0JMT0NLX0ZJTklTSEVEIiwiQVBQX1NQRUNJRklDX0ZBSUwiLCJFTVBUWV9GVU5DVElPTl9CTE9DS19GQUlMIiwiVU5VU0VEX1BBUkFNIiwiVU5VU0VEX0ZVTkNUSU9OIiwiUEFSQU1fSU5QVVRfVU5BVFRBQ0hFRCIsIklOQ09NUExFVEVfQkxPQ0tfSU5fRlVOQ1RJT04iLCJRVUVTVElPTl9NQVJLU19JTl9OVU1CRVJfRklFTEQiLCJFTVBUWV9GVU5DVElPTkFMX0JMT0NLIiwiRVhBTVBMRV9GQUlMRUQiLCJORVNURURfRk9SX1NBTUVfVkFSSUFCTEUiLCJFTVBUWV9GVU5DVElPTl9OQU1FIiwiTUlTU0lOR19SRUNPTU1FTkRFRF9CTE9DS19VTkZJTklTSEVEIiwiRVhUUkFfRlVOQ1RJT05fRkFJTCIsIkxPQ0FMX0ZVTkNUSU9OX0ZBSUwiLCJHRU5FUklDX0xJTlRfRkFJTCIsIkxPR19DT05ESVRJT05fRkFJTCIsIkJMT0NLX0xJTUlUX0ZBSUwiLCJVTlNVQk1JVFRFRF9BVFRFTVBUIiwiU0tJUFBFRCIsIk1JTklNVU1fUEFTU19SRVNVTFQiLCJUT09fTUFOWV9CTE9DS1NfRkFJTCIsIkFQUF9TUEVDSUZJQ19BQ0NFUFRBQkxFX0ZBSUwiLCJNSVNTSU5HX1JFQ09NTUVOREVEX0JMT0NLX0ZJTklTSEVEIiwiTUlOSU1VTV9PUFRJTUFMX1JFU1VMVCIsIkZSRUVfUExBWSIsIlBBU1NfV0lUSF9FWFRSQV9UT1BfQkxPQ0tTIiwiQVBQX1NQRUNJRklDX0lNUEVSRkVDVF9QQVNTIiwiRURJVF9CTE9DS1MiLCJNQU5VQUxfUEFTUyIsIkFMTF9QQVNTIiwiQ09OVEFJTkVEX0xFVkVMX1JFU1VMVCIsIkJFVFRFUl9USEFOX0lERUFMIiwiU1VCTUlUVEVEX1JFU1VMVCIsIkxPQ0tFRF9SRVNVTFQiLCJSRUFET05MWV9TVUJNSVNTSU9OX1JFU1VMVCIsIlJFVklFV19SRUpFQ1RFRF9SRVNVTFQiLCJSRVZJRVdfQUNDRVBURURfUkVTVUxUIiwiQmVlVGVybWluYXRpb25WYWx1ZSIsIklORklOSVRFX0xPT1AiLCJJbmZpbml0eSIsIk5PVF9BVF9GTE9XRVIiLCJGTE9XRVJfRU1QVFkiLCJOT1RfQVRfSE9ORVlDT01CIiwiSE9ORVlDT01CX0ZVTEwiLCJVTkNIRUNLRURfQ0xPVUQiLCJVTkNIRUNLRURfUFVSUExFIiwiSU5TVUZGSUNJRU5UX05FQ1RBUiIsIklOU1VGRklDSUVOVF9IT05FWSIsIkRJRF9OT1RfQ09MTEVDVF9FVkVSWVRISU5HIiwiSGFydmVzdGVyVGVybWluYXRpb25WYWx1ZSIsIldST05HX0NST1AiLCJFTVBUWV9DUk9QIiwiS2V5Q29kZXMiLCJCQUNLU1BBQ0UiLCJFTlRFUiIsIlNQQUNFIiwiTEVGVCIsIlVQIiwiUklHSFQiLCJET1dOIiwiQ09QWSIsIlBBU1RFIiwiREVMRVRFIiwiUG9zaXRpb24iLCJPVVRUT1BPVVRMRUZUIiwiT1VUVE9QTEVGVCIsIk9VVFRPUENFTlRFUiIsIk9VVFRPUFJJR0hUIiwiT1VUVE9QT1VUUklHSFQiLCJUT1BPVVRMRUZUIiwiVE9QTEVGVCIsIlRPUENFTlRFUiIsIlRPUFJJR0hUIiwiVE9QT1VUUklHSFQiLCJNSURETEVPVVRMRUZUIiwiTUlERExFTEVGVCIsIk1JRERMRUNFTlRFUiIsIk1JRERMRVJJR0hUIiwiTUlERExFT1VUUklHSFQiLCJCT1RUT01PVVRMRUZUIiwiQk9UVE9NTEVGVCIsIkJPVFRPTUNFTlRFUiIsIkJPVFRPTVJJR0hUIiwiQk9UVE9NT1VUUklHSFQiLCJPVVRCT1RUT01PVVRMRUZUIiwiT1VUQk9UVE9NTEVGVCIsIk9VVEJPVFRPTUNFTlRFUiIsIk9VVEJPVFRPTVJJR0hUIiwiT1VUQk9UVE9NT1VUUklHSFQiLCJTVkdfTlMiLCJBTFBIQUJFVCIsIkNJUEhFUiJdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7QUFJQTs7OztBQUlBLE9BQU8sTUFBTUEsYUFBYTtBQUN4QkMsU0FBTyxDQURpQixFQUNSO0FBQ2hCQyxXQUFTLENBRmUsRUFFUjtBQUNoQkMsV0FBUyxDQUFDLENBSGMsRUFHUjtBQUNoQkMsV0FBUyxDQUplLEVBSVI7QUFDaEJDLFNBQU8sQ0FBQyxDQUxnQixDQUtSO0FBTFEsQ0FBbkI7O0FBUVA7Ozs7QUFJQTs7Ozs7Ozs7Ozs7O0FBWUEsT0FBTyxNQUFNQyxjQUFjO0FBQ3pCO0FBQ0FDLGdCQUFjLENBQUMsQ0FGVTs7QUFJekI7QUFDQUMsZ0JBQWMsQ0FMVyxFQUtXO0FBQ3BDQyxvQkFBa0IsQ0FOTyxFQU1XO0FBQ3BDQyx1QkFBcUIsQ0FQSSxFQU9XO0FBQ3BDQyx5QkFBdUIsQ0FSRSxFQVFXO0FBQ3BDQyw0QkFBMEIsQ0FURCxFQVNXO0FBQ3BDQyx5QkFBdUIsQ0FWRSxFQVVXO0FBQ3BDQyxzQkFBb0IsQ0FYSyxFQVdXO0FBQ3BDQyxxQkFBbUIsQ0FaTSxFQVlXO0FBQ3BDQywwQkFBd0IsRUFiQyxFQWFXO0FBQ3BDQyxxQkFBbUIsRUFkTSxFQWNXO0FBQ3BDQyw2QkFBMkIsRUFmRixFQWVXO0FBQ3BDQyxnQkFBYyxFQWhCVyxFQWdCVztBQUNwQ0MsbUJBQWlCLEVBakJRLEVBaUJXO0FBQ3BDQywwQkFBd0IsRUFsQkMsRUFrQlc7QUFDcENDLGdDQUE4QixFQW5CTCxFQW1CVztBQUNwQ0Msa0NBQWdDLEVBcEJQLEVBb0JXO0FBQ3BDQywwQkFBd0IsRUFyQkMsRUFxQlc7QUFDcENDLGtCQUFnQixFQXRCUyxFQXNCVzs7QUFFcEM7QUFDQUMsNEJBQTBCLENBQUMsQ0F6QkYsRUF5Qlc7QUFDcEM7QUFDQUMsdUJBQXFCLENBQUMsQ0EzQkcsRUEyQlc7QUFDcENDLHdDQUFzQyxDQUFDLENBNUJkLEVBNEJpQjtBQUMxQ0MsdUJBQXFCLENBQUMsQ0E3QkcsRUE2Qlc7QUFDcENDLHVCQUFxQixDQUFDLENBOUJHLEVBOEJXO0FBQ3BDQyxxQkFBbUIsQ0FBQyxDQS9CSyxFQStCVztBQUNwQ0Msc0JBQW9CLENBQUMsQ0FoQ0ksRUFnQ1c7QUFDcENDLG9CQUFrQixDQUFDLENBakNNLEVBaUNXOztBQUVwQztBQUNBQyx1QkFBcUIsQ0FBQyxFQXBDRyxFQW9DVzs7QUFFcENDLFdBQVMsQ0FBQyxHQXRDZSxFQXNDYTs7QUFFdEM7QUFDQTtBQUNBO0FBQ0FDLHVCQUFxQixFQTNDSTs7QUE2Q3pCO0FBQ0FDLHdCQUFzQixFQTlDRyxFQThDZTtBQUN4Q0MsZ0NBQThCLEVBL0NMLEVBK0NlO0FBQ3hDQyxzQ0FBb0MsRUFoRFgsRUFnRGU7O0FBRXhDO0FBQ0E7QUFDQUMsMEJBQXdCLEVBcERDOztBQXNEekI7QUFDQUMsYUFBVyxFQXZEYyxFQXVEUjtBQUNqQkMsOEJBQTRCLEVBeERILEVBd0RRO0FBQ2pDQywrQkFBNkIsRUF6REosRUF5RFE7QUFDakNDLGVBQWEsRUExRFksRUEwRFI7QUFDakJDLGVBQWEsRUEzRFksRUEyRFI7O0FBRWpCO0FBQ0FDLFlBQVUsR0E5RGU7O0FBZ0V6QjtBQUNBQywwQkFBd0IsR0FqRUM7O0FBbUV6QjtBQUNBQyxxQkFBbUIsR0FwRU07O0FBc0V6QkMsb0JBQWtCLElBdEVPO0FBdUV6QkMsaUJBQWUsSUF2RVU7QUF3RXpCQyw4QkFBNEIsSUF4RUg7QUF5RXpCQywwQkFBd0IsSUF6RUM7QUEwRXpCQywwQkFBd0I7QUExRUMsQ0FBcEI7O0FBNkVQLE9BQU8sTUFBTUMsc0JBQXNCO0FBQ2pDbkQsV0FBUyxLQUR3QjtBQUVqQ0QsV0FBUyxJQUZ3QjtBQUdqQ3FELGlCQUFlQyxRQUhrQjtBQUlqQ0MsaUJBQWUsQ0FKa0IsRUFJWDtBQUN0QkMsZ0JBQWMsQ0FMbUIsRUFLWDtBQUN0QkMsb0JBQWtCLENBTmUsRUFNWDtBQUN0QkMsa0JBQWdCLENBUGlCLEVBT1g7QUFDdEJDLG1CQUFpQixDQVJnQixFQVFWO0FBQ3ZCQyxvQkFBa0IsQ0FUZSxFQVNWO0FBQ3ZCQyx1QkFBcUIsQ0FWWSxFQVVWO0FBQ3ZCQyxzQkFBb0IsQ0FYYSxFQVdWO0FBQ3ZCQyw4QkFBNEIsQ0FaSyxDQVlIO0FBWkcsQ0FBNUI7O0FBZVAsT0FBTyxNQUFNQyw0QkFBNEI7QUFDdkNDLGNBQWEsQ0FEMEI7QUFFdkNDLGNBQWEsQ0FGMEI7QUFHdkNILDhCQUE0QjtBQUhXLENBQWxDOztBQU1QLE9BQU8sTUFBTUksV0FBVztBQUN0QkMsYUFBVyxDQURXO0FBRXRCQyxTQUFPLEVBRmU7QUFHdEJDLFNBQU8sRUFIZTtBQUl0QkMsUUFBTSxFQUpnQjtBQUt0QkMsTUFBSSxFQUxrQjtBQU10QkMsU0FBTyxFQU5lO0FBT3RCQyxRQUFNLEVBUGdCO0FBUXRCQyxRQUFNLEVBUmdCO0FBU3RCQyxTQUFPLEVBVGU7QUFVdEJDLFVBQVE7QUFWYyxDQUFqQjs7QUFhUCxPQUFPLE1BQU1DLFdBQVc7QUFDdEJDLGlCQUFrQixDQURJO0FBRXRCQyxjQUFrQixDQUZJO0FBR3RCQyxnQkFBa0IsQ0FISTtBQUl0QkMsZUFBa0IsQ0FKSTtBQUt0QkMsa0JBQWtCLENBTEk7QUFNdEJDLGNBQWtCLENBTkk7QUFPdEJDLFdBQWtCLENBUEk7QUFRdEJDLGFBQWtCLENBUkk7QUFTdEJDLFlBQWtCLENBVEk7QUFVdEJDLGVBQWtCLEVBVkk7QUFXdEJDLGlCQUFrQixFQVhJO0FBWXRCQyxjQUFrQixFQVpJO0FBYXRCQyxnQkFBa0IsRUFiSTtBQWN0QkMsZUFBa0IsRUFkSTtBQWV0QkMsa0JBQWtCLEVBZkk7QUFnQnRCQyxpQkFBa0IsRUFoQkk7QUFpQnRCQyxjQUFrQixFQWpCSTtBQWtCdEJDLGdCQUFrQixFQWxCSTtBQW1CdEJDLGVBQWtCLEVBbkJJO0FBb0J0QkMsa0JBQWtCLEVBcEJJO0FBcUJ0QkMsb0JBQWtCLEVBckJJO0FBc0J0QkMsaUJBQWtCLEVBdEJJO0FBdUJ0QkMsbUJBQWtCLEVBdkJJO0FBd0J0QkMsa0JBQWtCLEVBeEJJO0FBeUJ0QkMscUJBQWtCO0FBekJJLENBQWpCOztBQTRCUDtBQUNBLE9BQU8sTUFBTUMsU0FBUyw0QkFBZjs7QUFFUCxPQUFPLE1BQU1DLFdBQVcsZ0VBQWpCO0FBQ1AsT0FBTyxNQUFNQyxTQUFTLGdFQUFmIiwiZmlsZSI6ImNvbnN0YW50cy5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQGZpbGVvdmVydmlldyBDb25zdGFudHMgdXNlZCBpbiBwcm9kdWN0aW9uIGNvZGUgYW5kIHRlc3RzLlxuICovXG5cbi8qKlxuICogRW51bWVyYXRpb24gb2YgdXNlciBwcm9ncmFtIGV4ZWN1dGlvbiBvdXRjb21lcy5cbiAqIFRoZXNlIGFyZSBkZXRlcm1pbmVkIGJ5IGVhY2ggYXBwLlxuICovXG5leHBvcnQgY29uc3QgUmVzdWx0VHlwZSA9IHtcbiAgVU5TRVQ6IDAsICAgICAgIC8vIFRoZSByZXN1bHQgaGFzIG5vdCB5ZXQgYmVlbiBjb21wdXRlZC5cbiAgU1VDQ0VTUzogMSwgICAgIC8vIFRoZSBwcm9ncmFtIGNvbXBsZXRlZCBzdWNjZXNzZnVsbHksIGFjaGlldmluZyB0aGUgZ29hbC5cbiAgRkFJTFVSRTogLTEsICAgIC8vIFRoZSBwcm9ncmFtIHJhbiB3aXRob3V0IGVycm9yIGJ1dCBkaWQgbm90IGFjaGlldmUgZ29hbC5cbiAgVElNRU9VVDogMiwgICAgIC8vIFRoZSBwcm9ncmFtIGRpZCBub3QgY29tcGxldGUgKGxpa2VseSBpbmZpbml0ZSBsb29wKS5cbiAgRVJST1I6IC0yICAgICAgIC8vIFRoZSBwcm9ncmFtIGdlbmVyYXRlZCBhbiBlcnJvci5cbn07XG5cbi8qKlxuICogQHR5cGVkZWYge251bWJlcn0gVGVzdFJlc3VsdFxuICovXG5cbi8qKlxuICogRW51bWVyYXRpb24gb2YgdGVzdCByZXN1bHRzLlxuICogRU1QVFlfQkxPQ0tfRkFJTCBhbmQgRU1QVFlfRlVOQ1RJT05fQkxPQ0tfRkFJTCBjYW4gb25seSBvY2N1ciBpZlxuICogU3R1ZGlvQXBwLmNoZWNrRm9yRW1wdHlCbG9ja3NfIGlzIHRydWUuXG4gKiBBIG51bWJlciBvZiB0aGVzZSByZXN1bHRzIGFyZSBlbnVtZXJhdGVkIG9uIHRoZSBkYXNoYm9hcmQgc2lkZSBpblxuICogYWN0aXZpdHlfY29uc3RhbnRzLnJiLCBhbmQgaXQncyBpbXBvcnRhbnQgdGhhdCB0aGVzZSB0d28gZmlsZXMgYXJlIGtlcHQgaW5cbiAqIHN5bmMuXG4gKiBOT1RFOiBXZSBzdG9yZSB0aGUgcmVzdWx0cyBmb3IgdXNlciBhdHRlbXB0cyBpbiBvdXIgZGIsIHNvIGNoYW5naW5nIHRoZXNlXG4gKiB2YWx1ZXMgd291bGQgbmVjZXNzaXRhdGUgYSBtaWdyYXRpb25cbiAqXG4gKiBAZW51bSB7bnVtYmVyfVxuICovXG5leHBvcnQgY29uc3QgVGVzdFJlc3VsdHMgPSB7XG4gIC8vIERlZmF1bHQgdmFsdWUgYmVmb3JlIGFueSB0ZXN0cyBhcmUgcnVuLlxuICBOT19URVNUU19SVU46IC0xLFxuXG4gIC8vIFRoZSBsZXZlbCB3YXMgbm90IHNvbHZlZC5cbiAgR0VORVJJQ19GQUlMOiAwLCAgICAgICAgICAgICAgICAgICAgLy8gVXNlZCBieSBEU0wgZGVmaW5lZCBsZXZlbHMuXG4gIEVNUFRZX0JMT0NLX0ZBSUw6IDEsICAgICAgICAgICAgICAgIC8vIEFuIFwiaWZcIiBvciBcInJlcGVhdFwiIGJsb2NrIHdhcyBlbXB0eS5cbiAgVE9PX0ZFV19CTE9DS1NfRkFJTDogMiwgICAgICAgICAgICAgLy8gRmV3ZXIgdGhhbiB0aGUgaWRlYWwgbnVtYmVyIG9mIGJsb2NrcyB1c2VkLlxuICBMRVZFTF9JTkNPTVBMRVRFX0ZBSUw6IDMsICAgICAgICAgICAvLyBEZWZhdWx0IGZhaWx1cmUgdG8gY29tcGxldGUgYSBsZXZlbC5cbiAgTUlTU0lOR19CTE9DS19VTkZJTklTSEVEOiA0LCAgICAgICAgLy8gQSByZXF1aXJlZCBibG9jayB3YXMgbm90IHVzZWQuXG4gIEVYVFJBX1RPUF9CTE9DS1NfRkFJTDogNSwgICAgICAgICAgIC8vIFRoZXJlIHdhcyBtb3JlIHRoYW4gb25lIHRvcC1sZXZlbCBibG9jay5cbiAgUlVOVElNRV9FUlJPUl9GQUlMOiA2LCAgICAgICAgICAgICAgLy8gVGhlcmUgd2FzIGEgcnVudGltZSBlcnJvciBpbiB0aGUgcHJvZ3JhbS5cbiAgU1lOVEFYX0VSUk9SX0ZBSUw6IDcsICAgICAgICAgICAgICAgLy8gVGhlcmUgd2FzIGEgc3ludGF4IGVycm9yIGluIHRoZSBwcm9ncmFtLlxuICBNSVNTSU5HX0JMT0NLX0ZJTklTSEVEOiAxMCwgICAgICAgICAvLyBUaGUgbGV2ZWwgd2FzIHNvbHZlZCB3aXRob3V0IHJlcXVpcmVkIGJsb2NrLlxuICBBUFBfU1BFQ0lGSUNfRkFJTDogMTEsICAgICAgICAgICAgICAvLyBBcHBsaWNhdGlvbi1zcGVjaWZpYyBmYWlsdXJlLlxuICBFTVBUWV9GVU5DVElPTl9CTE9DS19GQUlMOiAxMiwgICAgICAvLyBBIFwiZnVuY3Rpb25cIiBibG9jayB3YXMgZW1wdHlcbiAgVU5VU0VEX1BBUkFNOiAxMywgICAgICAgICAgICAgICAgICAgLy8gUGFyYW0gZGVjbGFyZWQgYnV0IG5vdCB1c2VkIGluIGZ1bmN0aW9uLlxuICBVTlVTRURfRlVOQ1RJT046IDE0LCAgICAgICAgICAgICAgICAvLyBGdW5jdGlvbiBkZWNsYXJlZCBidXQgbm90IHVzZWQgaW4gd29ya3NwYWNlLlxuICBQQVJBTV9JTlBVVF9VTkFUVEFDSEVEOiAxNSwgICAgICAgICAvLyBGdW5jdGlvbiBub3QgY2FsbGVkIHdpdGggZW5vdWdoIHBhcmFtcy5cbiAgSU5DT01QTEVURV9CTE9DS19JTl9GVU5DVElPTjogMTYsICAgLy8gSW5jb21wbGV0ZSBibG9jayBpbnNpZGUgYSBmdW5jdGlvbi5cbiAgUVVFU1RJT05fTUFSS1NfSU5fTlVNQkVSX0ZJRUxEOiAxNywgLy8gQmxvY2sgaGFzID8/PyBpbnN0ZWFkIG9mIGEgdmFsdWUuXG4gIEVNUFRZX0ZVTkNUSU9OQUxfQkxPQ0s6IDE4LCAgICAgICAgIC8vIFRoZXJlJ3MgYSBmdW5jdGlvbmFsIGJsb2NrIHdpdGggYW4gb3BlbiBpbnB1dFxuICBFWEFNUExFX0ZBSUxFRDogMTksICAgICAgICAgICAgICAgICAvLyBPbmUgb2Ygb3VyIGV4YW1wbGVzIGRpZG4ndCBtYXRjaCB0aGUgZGVmaW5pdGlvblxuXG4gIC8vIHN0YXJ0IHVzaW5nIG5lZ2F0aXZlIHZhbHVlcywgc2luY2Ugd2UgY29uc2lkZXIgPj0gMjAgdG8gYmUgXCJzb2x2ZWRcIlxuICBORVNURURfRk9SX1NBTUVfVkFSSUFCTEU6IC0yLCAgICAgICAvLyBXZSBoYXZlIG5lc3RlZCBmb3IgbG9vcHMgZWFjaCB1c2luZyB0aGUgc2FtZSBjb3VudGVyIHZhcmlhYmxlXG4gIC8vIE5PVEU6IGZvciBzbW9lIHBlcmlvZCBvZiB0aW1lLCB0aGlzIHdhcyAtMSBhbmQgY29uZmxpY3RlZCB3aXRoIE5PX1RFU1RTX1JVTlxuICBFTVBUWV9GVU5DVElPTl9OQU1FOiAtMywgICAgICAgICAgICAvLyBXZSBoYXZlIGEgdmFyaWFibGUgb3IgZnVuY3Rpb24gd2l0aCB0aGUgbmFtZSBcIlwiXG4gIE1JU1NJTkdfUkVDT01NRU5ERURfQkxPQ0tfVU5GSU5JU0hFRDogLTQsIC8vIFRoZSBsZXZlbCB3YXMgYXR0ZW1wdGVkIGJ1dCBub3Qgc29sdmVkIHdpdGhvdXQgYSByZWNvbW1lbmRlZCBibG9ja1xuICBFWFRSQV9GVU5DVElPTl9GQUlMOiAtNSwgICAgICAgICAgICAvLyBUaGUgcHJvZ3JhbSBjb250YWlucyBhIEphdmFTY3JpcHQgZnVuY3Rpb24gd2hlbiBpdCBzaG91bGQgbm90XG4gIExPQ0FMX0ZVTkNUSU9OX0ZBSUw6IC02LCAgICAgICAgICAgIC8vIFRoZSBwcm9ncmFtIGNvbnRhaW5zIGFuIHVuZXhwZWN0ZWQgSmF2YVNjcmlwdCBsb2NhbCBmdW5jdGlvblxuICBHRU5FUklDX0xJTlRfRkFJTDogLTcsICAgICAgICAgICAgICAvLyBUaGUgcHJvZ3JhbSBjb250YWlucyBhIGxpbnQgZXJyb3JcbiAgTE9HX0NPTkRJVElPTl9GQUlMOiAtOCwgICAgICAgICAgICAgLy8gVGhlIHByb2dyYW0gZXhlY3V0aW9uIGxvZyBkaWQgbm90IHBhc3MgYSByZXF1aXJlZCBjb25kaXRpb25cbiAgQkxPQ0tfTElNSVRfRkFJTDogLTksICAgICAgICAgICAgICAgLy8gUHV6emxlIHdhcyBzb2x2ZWQgdXNpbmcgbW9yZSB0aGFuIHRoZSB0b29sYm94IGxpbWl0IG9mIGEgYmxvY2tcblxuICAvLyBDb2RlcyBmb3IgdW52YWxpZGF0ZWQgbGV2ZWxzLlxuICBVTlNVQk1JVFRFRF9BVFRFTVBUOiAtNTAsICAgICAgICAgICAvLyBQcm9ncmVzcyB3YXMgc2F2ZWQgd2l0aG91dCBzdWJtaXR0aW5nIGZvciByZXZpZXcsIG9yIHdhcyB1bnN1Ym1pdHRlZC5cblxuICBTS0lQUEVEOiAtMTAwLCAgICAgICAgICAgICAgICAgICAgICAgIC8vIFNraXBwZWQsIGUuZy4gdGhleSB1c2VkIHRoZSBza2lwIGJ1dHRvbiBvbiBhIGNoYWxsZW5nZSBsZXZlbFxuXG4gIC8vIE51bWJlcnMgYmVsb3cgMjAgYXJlIGdlbmVyYWxseSBjb25zaWRlcmVkIHNvbWUgZm9ybSBvZiBmYWlsdXJlLlxuICAvLyBOdW1iZXJzID49IDIwIGdlbmVyYWxseSBpbmRpY2F0ZSBzb21lIGZvcm0gb2Ygc3VjY2VzcyAoYWx0aG91Z2ggYWdhaW4gdGhlcmVcbiAgLy8gYXJlIHZhbHVlcyBsaWtlIFJFVklFV19SRUpFQ1RFRF9SRVNVTFQgdGhhdCBkb24ndCBzZWVtIHRvIHF1aXRlIG1lZXQgdGhhdCByZXN0cmljdGlvbi5cbiAgTUlOSU1VTV9QQVNTX1JFU1VMVDogMjAsXG5cbiAgLy8gVGhlIGxldmVsIHdhcyBzb2x2ZWQgaW4gYSBub24tb3B0aW1hbCB3YXkuICBVc2VyIG1heSBhZHZhbmNlIG9yIHJldHJ5LlxuICBUT09fTUFOWV9CTE9DS1NfRkFJTDogMjAsICAgICAgICAgICAgICAgLy8gTW9yZSB0aGFuIHRoZSBpZGVhbCBudW1iZXIgb2YgYmxvY2tzIHdlcmUgdXNlZC5cbiAgQVBQX1NQRUNJRklDX0FDQ0VQVEFCTEVfRkFJTDogMjEsICAgICAgIC8vIEFwcGxpY2F0aW9uLXNwZWNpZmljIGFjY2VwdGFibGUgZmFpbHVyZS5cbiAgTUlTU0lOR19SRUNPTU1FTkRFRF9CTE9DS19GSU5JU0hFRDogMjIsIC8vIFRoZSBsZXZlbCB3YXMgc29sdmVkIHdpdGhvdXQgYSByZWNvbW1lbmRlZCBibG9ja1xuXG4gIC8vIE51bWJlcnMgPj0gMzAsIGFyZSBjb25zaWRlcmVkIHRvIGJlIFwicGVyZmVjdGx5XCIgc29sdmVkLCBpLmUuIHRob3NlIGluIHRoZSByYW5nZVxuICAvLyBvZiAyMC0zMCBoYXZlIGNvcnJlY3QgYnV0IG5vdCBvcHRpbWFsIHNvbHV0aW9uc1xuICBNSU5JTVVNX09QVElNQUxfUkVTVUxUOiAzMCxcblxuICAvLyBUaGUgbGV2ZWwgd2FzIHNvbHZlZCBpbiBhbiBvcHRpbWFsIHdheS5cbiAgRlJFRV9QTEFZOiAzMCwgICAvLyBUaGUgdXNlciBpcyBpbiBmcmVlLXBsYXkgbW9kZS5cbiAgUEFTU19XSVRIX0VYVFJBX1RPUF9CTE9DS1M6IDMxLCAgLy8gVGhlcmUgd2FzIG1vcmUgdGhhbiBvbmUgdG9wLWxldmVsIGJsb2NrLlxuICBBUFBfU1BFQ0lGSUNfSU1QRVJGRUNUX1BBU1M6IDMyLCAvLyBUaGUgbGV2ZWwgd2FzIHBhc3NlZCBpbiBzb21lIG9wdGltYWwgYnV0IG5vdCBuZWNlc3NhcmlseSBwZXJmZWN0IHdheVxuICBFRElUX0JMT0NLUzogNzAsIC8vIFRoZSB1c2VyIGlzIGNyZWF0aW5nL2VkaXRpbmcgYSBuZXcgbGV2ZWwuXG4gIE1BTlVBTF9QQVNTOiA5MCwgLy8gVGhlIGxldmVsIHdhcyBtYW51YWxseSBzZXQgYXMgcGVyZmVjdGVkIGludGVybmFsbHkuXG5cbiAgLy8gVGhlIGxldmVsIHdhcyBzb2x2ZWQgaW4gdGhlIGlkZWFsIG1hbm5lci5cbiAgQUxMX1BBU1M6IDEwMCxcblxuICAvLyBDb250YWluZWQgbGV2ZWwgcmVzdWx0LiBOb3QgdmFsaWRhdGVkLCBidXQgc2hvdWxkIGJlIHRyZWF0ZWQgYXMgYSBzdWNjZXNzXG4gIENPTlRBSU5FRF9MRVZFTF9SRVNVTFQ6IDEwMSxcblxuICAvLyBUaGUgbGV2ZWwgd2FzIHNvbHZlZCB3aXRoIGZld2VyIGJsb2NrcyB0aGFuIHRoZSByZWNvbW1lbmRlZCBudW1iZXIgb2YgYmxvY2tzLlxuICBCRVRURVJfVEhBTl9JREVBTDogMTAyLFxuXG4gIFNVQk1JVFRFRF9SRVNVTFQ6IDEwMDAsXG4gIExPQ0tFRF9SRVNVTFQ6IDEwMDEsXG4gIFJFQURPTkxZX1NVQk1JU1NJT05fUkVTVUxUOiAxMDAyLFxuICBSRVZJRVdfUkVKRUNURURfUkVTVUxUOiAxNTAwLFxuICBSRVZJRVdfQUNDRVBURURfUkVTVUxUOiAyMDAwLFxufTtcblxuZXhwb3J0IGNvbnN0IEJlZVRlcm1pbmF0aW9uVmFsdWUgPSB7XG4gIEZBSUxVUkU6IGZhbHNlLFxuICBTVUNDRVNTOiB0cnVlLFxuICBJTkZJTklURV9MT09QOiBJbmZpbml0eSxcbiAgTk9UX0FUX0ZMT1dFUjogMSwgICAgIC8vIFRyaWVkIHRvIGdldCBuZWN0YXIgd2hlbiBub3QgYXQgZmxvd2VyLlxuICBGTE9XRVJfRU1QVFk6IDIsICAgICAgLy8gVHJpZWQgdG8gZ2V0IG5lY3RhciB3aGVuIGZsb3dlciBlbXB0eS5cbiAgTk9UX0FUX0hPTkVZQ09NQjogMywgIC8vIFRyaWVkIHRvIG1ha2UgaG9uZXkgd2hlbiBub3QgYXQgaG9uZXljb21iLlxuICBIT05FWUNPTUJfRlVMTDogNCwgICAgLy8gVHJpZWQgdG8gbWFrZSBob25leSwgYnV0IG5vIHJvb20gYXQgaG9uZXljb21iLlxuICBVTkNIRUNLRURfQ0xPVUQ6IDUsICAgIC8vIEZpbmlzaGVkIHB1enpsZSwgYnV0IGRpZG4ndCBjaGVjayBldmVyeSBjbG91ZGVkIGl0ZW1cbiAgVU5DSEVDS0VEX1BVUlBMRTogNiwgICAvLyBGaW5pc2hlZCBwdXp6bGUsIGJ1dCBkaWRuJ3QgY2hlY2sgZXZlcnkgcHVycGxlIGZsb3dlclxuICBJTlNVRkZJQ0lFTlRfTkVDVEFSOiA3LC8vIERpZG4ndCBjb2xsZWN0IGFsbCBuZWN0YXIgYnkgZmluaXNoXG4gIElOU1VGRklDSUVOVF9IT05FWTogOCwgLy8gRGlkbid0IG1ha2UgYWxsIGhvbmV5IGJ5IGZpbmlzaFxuICBESURfTk9UX0NPTExFQ1RfRVZFUllUSElORzogOSAvLyBGb3IgcXVhbnR1bSBsZXZlbHMsIGRpZG4ndCB0cnkgdG8gY29sbGVjdCBhbGwgYXZhaWxhYmxlIGhvbmV5L25lY3RhclxufTtcblxuZXhwb3J0IGNvbnN0IEhhcnZlc3RlclRlcm1pbmF0aW9uVmFsdWUgPSB7XG4gIFdST05HX0NST1AgOiAxLFxuICBFTVBUWV9DUk9QIDogMixcbiAgRElEX05PVF9DT0xMRUNUX0VWRVJZVEhJTkc6IDNcbn07XG5cbmV4cG9ydCBjb25zdCBLZXlDb2RlcyA9IHtcbiAgQkFDS1NQQUNFOiA4LFxuICBFTlRFUjogMTMsXG4gIFNQQUNFOiAzMixcbiAgTEVGVDogMzcsXG4gIFVQOiAzOCxcbiAgUklHSFQ6IDM5LFxuICBET1dOOiA0MCxcbiAgQ09QWTogNjcsXG4gIFBBU1RFOiA4NixcbiAgREVMRVRFOiAxMjdcbn07XG5cbmV4cG9ydCBjb25zdCBQb3NpdGlvbiA9IHtcbiAgT1VUVE9QT1VUTEVGVDogICAgMSxcbiAgT1VUVE9QTEVGVDogICAgICAgMixcbiAgT1VUVE9QQ0VOVEVSOiAgICAgMyxcbiAgT1VUVE9QUklHSFQ6ICAgICAgNCxcbiAgT1VUVE9QT1VUUklHSFQ6ICAgNSxcbiAgVE9QT1VUTEVGVDogICAgICAgNixcbiAgVE9QTEVGVDogICAgICAgICAgNyxcbiAgVE9QQ0VOVEVSOiAgICAgICAgOCxcbiAgVE9QUklHSFQ6ICAgICAgICAgOSxcbiAgVE9QT1VUUklHSFQ6ICAgICAgMTAsXG4gIE1JRERMRU9VVExFRlQ6ICAgIDExLFxuICBNSURETEVMRUZUOiAgICAgICAxMixcbiAgTUlERExFQ0VOVEVSOiAgICAgMTMsXG4gIE1JRERMRVJJR0hUOiAgICAgIDE0LFxuICBNSURETEVPVVRSSUdIVDogICAxNSxcbiAgQk9UVE9NT1VUTEVGVDogICAgMTYsXG4gIEJPVFRPTUxFRlQ6ICAgICAgIDE3LFxuICBCT1RUT01DRU5URVI6ICAgICAxOCxcbiAgQk9UVE9NUklHSFQ6ICAgICAgMTksXG4gIEJPVFRPTU9VVFJJR0hUOiAgIDIwLFxuICBPVVRCT1RUT01PVVRMRUZUOiAyMSxcbiAgT1VUQk9UVE9NTEVGVDogICAgMjIsXG4gIE9VVEJPVFRPTUNFTlRFUjogIDIzLFxuICBPVVRCT1RUT01SSUdIVDogICAyNCxcbiAgT1VUQk9UVE9NT1VUUklHSFQ6MjVcbn07XG5cbi8qKiBAY29uc3Qge3N0cmluZ30gU1ZHIGVsZW1lbnQgbmFtZXNwYWNlICovXG5leHBvcnQgY29uc3QgU1ZHX05TID0gJ2h0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnJztcblxuZXhwb3J0IGNvbnN0IEFMUEhBQkVUID0gJ2FiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6QUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVowMTIzNDU2Nzg5JztcbmV4cG9ydCBjb25zdCBDSVBIRVIgPSAnSXE2MUY4a2lhVUhQR2NzWTdEZ1g0eUF1M0x3dFdobkNtZVI1cFZySm9LZlFaTXgwQlNkbE9qRXYyVGJOOXonO1xuIl19