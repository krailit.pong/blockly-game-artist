var Colours = require('./colours');
//require('@cdo/locale');
var msg = require('./locale');
// var customLevelBlocks = require('./customLevelBlocks');
import { Position } from '../constants';
const RANDOM_VALUE = 'RAND';

// Install extensions to blockly's language and JavaScript generator.
export const install = function (blockly) {
  var generator = blockly.JavaScript;

  // Create a smaller palette.
  blockly.FieldColour.COLOURS = [
  // Row 1.
  Colours.BLACK, Colours.GREY, Colours.KHAKI, Colours.WHITE,
  // Row 2.
  Colours.RED, Colours.PINK, Colours.ORANGE, Colours.YELLOW,
  // Row 3.
  Colours.GREEN, Colours.BLUE, Colours.AQUAMARINE, Colours.PLUM];
  blockly.FieldColour.COLUMNS = 3;
  // }

  blockly.Blocks.starter = {
    init: function () {
      this.appendDummyInput().appendField("เริ่มต้นที่นี่");
      this.setColour(184);
      this.setInputsInline(true);
      this.setNextStatement(true);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  generator.starter = function (block) {
    // var text_0 = block.getFieldValue('FD');
    // var code = 'FD' + text_0 + '|'
    return "/**/";
  };

  blockly.Blocks.draw_circle = {
    init: function () {
      this.appendDummyInput().appendField(new blockly.FieldDropdown([["วงกลม", "f"]]), "go_step").appendField(new blockly.FieldNumber("???"), "rad").appendField("พิกเซล");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript.draw_circle = function (block) {
    var dropdown_go_step = block.getFieldValue('go_step');
    var text_pixel = block.getFieldValue('rad');
    return 'this.drawerCircle(' + text_pixel + '); ';
  };

  ////////////////////////////////////////// ดำเนินการ ////////////////////////////////////////////////////////////
  // Move Forward and Backward //
  blockly.Blocks.go_option = {
    init: function () {
      this.appendDummyInput().appendField(new blockly.FieldDropdown([["ไปข้างหน้า", "f"], ["ถอยหลังไป", "b"]]), "go_step").appendField(new blockly.FieldNumber("???"), "pixel").appendField("พิกเซล");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript.go_option = function (block) {
    var dropdown_go_step = block.getFieldValue('go_step');
    var text_pixel = block.getFieldValue('pixel');
    // TODO: Assemble JavaScript into code variable.
    // return "DNG_GO_OPTION" + dropdown_go_step + "-" + text_pixel + "|";
    text_pixel = isNaN(text_pixel) ? 0 : parseInt(text_pixel);
    if (dropdown_go_step == 'f') {
      text_pixel = 0 + text_pixel;
    } else {
      text_pixel = 0 - text_pixel;
    }
    return 'this.drawLine(' + text_pixel + '); ';
  };
  // Move Forward and Backward //

  // Turn Left and Right //
  blockly.Blocks.turn_option = {
    init: function () {
      this.appendDummyInput().appendField(new blockly.FieldDropdown([["หันไปขวา", "right"], ["หันไปซ้าย", "left"]]), "turn_step").appendField(new blockly.FieldAngle("???"), "degree").appendField("องศา");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript.turn_option = function (block) {
    var dropdown_turn_step = block.getFieldValue('turn_step');
    var text_degree = block.getFieldValue('degree');
    text_degree = isNaN(text_degree) ? 0 : parseInt(text_degree);

    if (dropdown_turn_step === 'f') {
      return 'this.rotateDrawerRight(' + text_degree + '); ';
    } else {
      return 'this.rotateDrawerLeft(' + text_degree + '); ';
    }
  };

  // Turn Left and Right //

  // Jump forward and backward //
  blockly.Blocks.jump_option = {
    init: function () {
      this.appendDummyInput().appendField(new blockly.FieldDropdown([["กระโดดไปข้างหน้า", "j_forward"], ["กระโดดไปข้างหลัง", "b_forward"]]), "turn_step").appendField(new blockly.FieldNumber("0"), "pixel").appendField("พิกเซล");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript.jump_option = function (block) {
    var dropdown_turn_step = block.getFieldValue('turn_step');
    var text_pixel = block.getFieldValue('pixel');
    text_pixel = isNaN(text_pixel) ? 0 : text_pixel;
    if (dropdown_turn_step === "j_forward") {
      return 'this.drawerJumpforward(' + text_pixel + '); ';
    } else {
      return 'this.drawerJumpbackward(' + text_pixel + '); ';
    }
    // return "DNG_JUMP_OPTION" + dropdown_turn_step + "-" + text_pixel + "|";
  };

  // Jump forward and backward //


  blockly.Blocks.jump_axis = {
    init: function () {
      this.appendValueInput("Jump").setCheck("Number").appendField("กระโดด").appendField("แนวนอนที่").appendField(new blockly.FieldTextInput("0"), "X-axis").appendField("แนวตั้งที่").appendField(new blockly.FieldTextInput("0"), "Y-axis").appendField("ของพื้นที่ทั้งหมด");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript.jump_axis = function (block) {
    var text_x_axis = block.getFieldValue('X-axis');
    var text_y_axis = block.getFieldValue('Y-axis');
    var value_jump = blockly.JavaScript.valueToCode(block, 'Jump', blockly.JavaScript.ORDER_ATOMIC);
    return "DNG_JUMP_AXIS" + text_x_axis + "-" + text_y_axis + "|";
  };

  blockly.Blocks.point_to = {
    init: function () {
      this.appendDummyInput().appendField("หันหน้าไปที่").appendField(new blockly.FieldAngle("???"), "degree");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(230);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript.point_to = function (block) {
    var degree = block.getFieldValue('degree');
    // console.log('degree = ' + degree)
    return 'this.drawerPointTo(' + degree + '); ';
    // var value_jump = blockly.JavaScript.valueToCode(block, 'Jump', blockly.JavaScript.ORDER_ATOMIC);
    // return "DNG_JUMP_AXIS" + text_x_axis + "-" + text_y_axis + "|";
  };

  ////////////////////////////////////////// ดำเนินการ ////////////////////////////////////////////////////////////

  ////////////////////////////////////////// พู่กัน ////////////////////////////////////////////////////////////

  // Color //

  blockly.Blocks.dng_cl_static = {
    init: function () {
      this.appendDummyInput().appendField("ตั้งค่าสี").appendField(new blockly.FieldColour("#ff0000"), "color");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(65);
      this.setTooltip("");
      this.setHelpUrl("");
      // this.appendValueInput("main")
      //   .setCheck(null)
      //   .appendField("ตั้งค่าสี")
      //   .appendField(new blockly.FieldColour("#ff0000"), "color");
      // this.setInputsInline(false);
      // this.setPreviousStatement(true, null);
      // this.setNextStatement(true, null);
      // this.setColour(65);
      // this.setTooltip("");
      // this.setHelpUrl("");
    }
  };

  blockly.JavaScript.dng_cl_static = function (block) {
    var selColor = block.getFieldValue('color');
    return 'this.drawerLineColor("' + selColor + '"); ';
    // return "DNG_CL_STATIC" + colour_color + "-|";
  };

  // Color //

  // Random Color //

  blockly.Blocks['dgn_cl_random'] = {
    init: function () {
      this.appendDummyInput().appendField("ตั้งค่าสี").appendField("สุ่มสี");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(65);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript['dgn_cl_random'] = function (block) {
    var num = Math.floor(Math.random() * Math.pow(2, 24));
    var color = '#' + ('00000' + num.toString(16)).substr(-6);
    return 'this.drawerLineColor("' + color + '"); ';
    // return "DNG_CL_RANDOM" + color + "-|";
  };

  // Random Color //

  // Line Height //

  blockly.Blocks.dng_line_height = {
    init: function () {
      this.appendDummyInput().appendField("ขนาดดินสอ").appendField(new blockly.FieldNumber("???"), "pixel").appendField("พิกเซล");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(65);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript.dng_line_height = function (block) {
    var text_pixel = block.getFieldValue('pixel');
    return 'this.drawerLineHeight(' + text_pixel + '); ';
    // return "DNG_LN_HEIGHT" + text_pixel + "-|";
  };
  // Line Height //

  blockly.Blocks.alpha = {
    init: function () {
      this.appendDummyInput().appendField("ความจางของสี").appendField(new blockly.FieldNumber("10"), "alpha");
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(65);
      this.setTooltip('');
    }
  };
  blockly.JavaScript.alpha = function (block) {
    let text_pixel = block.getFieldValue('alpha');
    // var alphaIndex = Colours.alphaCode()
    return 'this.drawerLineAlpha(' + text_pixel + '); ';
  };

  ////////////////////////////////////////// พู่กัน ////////////////////////////////////////////////////////////

  ////////////////////////////////////////// ลูป ////////////////////////////////////////////////////////////

  // Loop For //

  blockly.Blocks.loop_for = {
    init: function () {
      this.appendDummyInput().appendField("สำหรับ").appendField(new blockly.FieldDropdown([["i", "i"], ["length", "length"], ["repeat", "repeat"]]), "loop_option").appendField("จาก").appendField(new blockly.FieldNumber("0"), "start").appendField("จนถึง").appendField(new blockly.FieldNumber("0"), "stop").appendField("นับทีละ").appendField(new blockly.FieldNumber("0"), "plus");
      this.appendStatementInput("for").setCheck(null);
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(0);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript['loop_for'] = function (block) {
    var dropdown_loop_option = block.getFieldValue('loop_option');
    var text_start = block.getFieldValue('start');
    var text_stop = block.getFieldValue('stop');
    var text_plus = block.getFieldValue('plus');
    var statements_for = blockly.JavaScript.statementToCode(block, 'for');
    // statements_for = statements_for.replace(" ", "")
    // statements_for = statements_for.split('|').join('^')
    if (statements_for != "") {
      let start = parseInt(text_start);
      let end = parseInt(text_stop);
      let adding = parseInt(text_plus);
      // var statementArray = statements_for.split('^')
      // statementArray = Answer.removeA(statementArray, "")
      // console.log(" statementArray " + statementArray);
      if (adding > 0 && start < end) {
        let codeString = 'for (let ' + dropdown_loop_option + ' = ' + start + '; ' + dropdown_loop_option + ' < ' + end + '; ' + dropdown_loop_option + '++) {' + statements_for + '}; ';
        return codeString;
      }

      return '';
    }

    return '';
  };
  // Loop For //

  // Loop While //
  blockly.Blocks.loop_while = {
    init: function () {
      this.appendDummyInput().appendField("ทำซ้ำ").appendField(new blockly.FieldNumber("???"), "start").appendField("รอบ");
      this.appendStatementInput("while").setCheck(null).appendField("ทำ");
      this.setInputsInline(true);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(0);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript['loop_while'] = function (block) {
    var text_start = block.getFieldValue('start');
    var statements_while = blockly.JavaScript.statementToCode(block, 'while');
    // statements_while = statements_while.replace(" ", "")
    // statements_while = statements_while.split('|').join('^')
    // return "LOOP_WHILE," + text_start + "," + statements_while + "|";

    if (statements_while != "") {
      let start = parseInt(text_start);

      if (start > 0) {
        let codeString = 'for (let i = 0; i < ' + start + '; i++) {' + statements_while + '}; ';
        return codeString;
      }

      return '';
    }

    return '';
    // var statements_for = blockly.JavaScript.statementToCode(block, 'for');
    // statements_for = statements_for.replace(" ", "")
    // statements_for = statements_for.split('|').join('^')
    // return "LOOP_FOR" + dropdown_loop_option + "," + text_start + "," + text_stop + "," + text_plus + "," + statements_for + "|";
  };

  // Loop While //

  ////////////////////////////////////////// ลูป ////////////////////////////////////////////////////////////

  ////////////////////////////////////////// คำนวณ ////////////////////////////////////////////////////////////

  // Calculate variable //

  blockly.Blocks.cal_variable = {
    init: function () {
      this.appendDummyInput().appendField(new blockly.FieldNumber("0"), "variable");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour(240);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript['cal_variable'] = function (block) {
    var text_variable = block.getFieldValue('variable');
    // TODO: Assemble JavaScript into code variable.
    var code = "CAL_VARIABLE" + text_variable + "-|";
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, blockly.JavaScript.ORDER_NONE];
  };

  // Calculate variable //

  // Calculate Function //

  blockly.Blocks.cal_function = {
    init: function () {
      this.appendDummyInput().appendField(new blockly.FieldNumber("0"), "first_var").appendField(new blockly.FieldDropdown([["+", "+"], ["-", "-"], ["x", "x"], ["÷", "÷"], ["^", "^"]]), "cal_function_option").appendField(new blockly.FieldNumber("0"), "second_var");
      this.setOutput(true, null);
      this.setColour(240);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript['cal_function'] = function (block) {
    var text_first_var = block.getFieldValue('first_var');
    var dropdown_cal_function_option = block.getFieldValue('cal_function_option');
    var text_second_var = block.getFieldValue('second_var');
    // TODO: Assemble JavaScript into code variable.
    var code = "CAL_FUNCTION" + text_first_var + "-" + dropdown_cal_function_option + "-" + text_second_var + "|";
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, blockly.JavaScript.ORDER_NONE];
  };

  // Calculate Function //

  ////////////////////////////////////////// คำนวณ ////////////////////////////////////////////////////////////


  ////////////////////////////////////////// ตัวแปร ////////////////////////////////////////////////////////////

  // First //

  blockly.Blocks.variable_first = {
    init: function () {
      this.appendDummyInput().appendField(new blockly.FieldDropdown([["circ", "circ"], ["counter", "counter"], ["i", "i"], ["j", "j"], ["length", "length"], ["points", "points"], ["radius", "radius"], ["repeat", "repeat"], ["sides", "sides"]]), "option");
      this.setInputsInline(true);
      this.setOutput(true, null);
      this.setColour(330);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript['variable_first'] = function (block) {
    var dropdown_option = block.getFieldValue('option');
    // TODO: Assemble JavaScript into code variable.
    var code = "VARIABLE_FIRST" + dropdown_option + "|";
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, blockly.JavaScript.ORDER_NONE];
  };

  // First //

  // Second //

  blockly.Blocks.variable_second = {
    init: function () {
      this.appendValueInput("second").setCheck(null).appendField("ชุด").appendField(new blockly.FieldDropdown([["circ", "circ"], ["counter", "counter"], ["i", "i"], ["j", "j"], ["length", "length"], ["points", "points"], ["radius", "radius"], ["repeat", "repeat"], ["sides", "sides"]]), "option").appendField("ถึง");
      this.setInputsInline(false);
      this.setPreviousStatement(true, null);
      this.setNextStatement(true, null);
      this.setColour(330);
      this.setTooltip("");
      this.setHelpUrl("");
    }
  };

  blockly.JavaScript['variable_second'] = function (block) {
    var dropdown_option = block.getFieldValue('option');
    var value_name = blockly.JavaScript.valueToCode(block, 'second', blockly.JavaScript.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    var code = "VARIABLE_SECOND" + dropdown_option + "-" + value_name + "|";
    return code;
  };

  // Second //

  ////////////////////////////////////////// ตัวแปร ////////////////////////////////////////////////////////////

  // Calculate Function //

  // function createPointToBlocks(onInit) {
  //   return {
  //     helpUrl: '',
  // init: function () {
  //   this.setColour(184);
  //   this.setPreviousStatement(true);
  //   this.setInputsInline(true);
  //   this.setNextStatement(true);
  //   this.setTooltip(0);
  //   this.appendDummyInput()
  //       .appendTitle('xx');
  //   if (onInit) {
  //     onInit(this);
  //   }
  // }
  //   };
  // }


  // blockly.Blocks.point_to = createPointToBlocks(function (block) {
  //   // Block for pointing to a specified direction
  // block.appendDummyInput()
  //     .appendTitle(new blockly.FieldAngleTextInput('0', {
  //       direction: 'turnRight'
  //     }), 'DIRECTION')
  //     .appendTitle('point to');
  // });

  // blockly.Blocks.point_to = {
  //   init: function () {
  //     this.setColour(184);
  //     this.setPreviousStatement(true);
  //     this.setInputsInline(true);
  //     this.setNextStatement(true);
  //     this.setTooltip(0);
  //     this.appendDummyInput()
  //       .appendField('xx');
  //     this.appendDummyInput()
  //       .appendTitle(new blockly.FieldAngleTextInput('0', {
  //         direction: 'turnRight'
  //       }), 'DIRECTION');
  //     this.appendField('point to');
  //   }
  // }


  // [['FD', 'block_id_UO^)ury]p+7iN@^2]S0}', 100], ['FD', 'block_id_IakCR6jv8g$rh(t4Mp4I', 100]]

  /*
    
    generator.draw_move_by_constant = function () {
      // Generate JavaScript for moving forward or backward the internal number of
      // pixels.
      var value = window.parseFloat(this.getTitleValue('VALUE')) || 0;
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
    generator.draw_move_by_constant_dropdown = generator.draw_move_by_constant;
  
    blockly.Blocks.draw_turn_by_constant_restricted = {
      // Block for turning either left or right from among a fixed set of angles.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.draw_turn.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(new blockly.FieldAngleDropdown({
              directionTitleName: 'DIR',
              menuGenerator: this.VALUE
            }), 'VALUE')
            .appendTitle(msg.degrees());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.turnTooltip());
      }
    };
  
    blockly.Blocks.draw_turn_by_constant_restricted.VALUE =
        [30, 45, 60, 90, 120, 135, 150, 180].map(function (t) {
          return [String(t), String(t)];
        });
  
    generator.draw_turn_by_constant_restricted = function () {
      // Generate JavaScript for turning either left or right from among a fixed
      // set of angles.
      var value = window.parseFloat(this.getTitleValue('VALUE'));
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.draw_turn_by_constant = {
      // Block for turning left or right any number of degrees.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
          .appendTitle(new blockly.FieldDropdown(
            blockly.Blocks.draw_turn.DIRECTIONS), 'DIR');
        this.appendDummyInput()
          .appendTitle(new blockly.FieldAngleTextInput('90', {
            directionTitle: 'DIR'
          }), 'VALUE')
          .appendTitle(msg.degrees());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.turnTooltip());
      }
    };
  
    blockly.Blocks.draw_turn_by_constant_dropdown = {
      // Block for turning left or right any number of degrees.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
          .appendTitle(new blockly.FieldDropdown(
            blockly.Blocks.draw_turn.DIRECTIONS), 'DIR');
        this.appendDummyInput()
          .appendTitle(new blockly.FieldAngleDropdown({
            directionTitleName: 'DIR'
          }), 'VALUE')
          .appendTitle(msg.degrees());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.turnTooltip());
      }
    };
  
    generator.draw_turn_by_constant = function () {
      // Generate JavaScript for turning left or right.
      var value = window.parseFloat(this.getTitleValue('VALUE')) || 0;
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
    generator.draw_turn_by_constant_dropdown = generator.draw_turn_by_constant;
  
    generator.draw_move_inline = function () {
      // Generate JavaScript for moving forward or backward the internal number of
      // pixels.
      var value = window.parseFloat(this.getTitleValue('VALUE'));
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
  
  
    blockly.Blocks.draw_turn_inline_restricted = {
      // Block for turning either left or right from among a fixed set of angles.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.draw_turn.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(new blockly.FieldAngleDropdown({
              directionTitleName: 'DIR',
              menuGenerator: this.VALUE
            }), 'VALUE')
            .appendTitle(msg.degrees());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.turnTooltip());
      }
    };
  
    blockly.Blocks.draw_turn_inline_restricted.VALUE =
        [30, 45, 60, 90, 120, 135, 150, 180].map(function (t) {
          return [String(t), String(t)];
        });
  
    generator.draw_turn_inline_restricted = function () {
      // Generate JavaScript for turning either left or right from among a fixed
      // set of angles.
      var value = window.parseFloat(this.getTitleValue('VALUE'));
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.draw_turn_inline = {
      // Block for turning left or right any number of degrees.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.draw_turn.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(new blockly.FieldAngleTextInput('90', {
              directionTitle: 'DIR'
            }), 'VALUE')
            .appendTitle(msg.degrees());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.turnTooltip());
      }
    };
  
    function createPointToBlocks(onInit) {
      return {
        helpUrl: '',
        init: function () {
          this.setHSV(184, 1.00, 0.74);
          this.setPreviousStatement(true);
          this.setInputsInline(true);
          this.setNextStatement(true);
          this.setTooltip(msg.pointTo());
          this.appendDummyInput()
              .appendTitle(msg.pointTo());
          if (onInit) {
            onInit(this);
          }
        }
      };
    }
  
    blockly.Blocks.point_to = createPointToBlocks(function (block) {
      // Block for pointing to a specified direction
      block.appendDummyInput()
          .appendTitle(new blockly.FieldAngleTextInput('0', {
            direction: 'turnRight'
          }), 'DIRECTION')
          .appendTitle(msg.degrees());
    });
  
    generator.point_to = function () {
      let value = window.parseFloat(this.getTitleValue('DIRECTION')) || 0;
      return `Turtle.pointTo(${value}, 'block_id_${this.id}');\n`;
    };
  
    blockly.Blocks.point_to_param = createPointToBlocks(function (block) {
    // Block for pointing to a specified direction
      block.appendValueInput('VALUE')
          .setCheck(blockly.BlockValueType.NUMBER)
          .addFieldHelper(blockly.BlockFieldHelper.ANGLE_HELPER, {
            block,
            direction: 'turnRight',
          });
      block.appendDummyInput()
          .appendTitle(msg.degrees());
    });
  
    generator.point_to_param = function () {
      let value = generator.valueToCode(this, 'VALUE',
          blockly.JavaScript.ORDER_NONE);
      return `Turtle.pointTo(${value}, 'block_id_${this.id}');\n`;
    };
  
    blockly.Blocks.point_to_by_constant_restricted =
        createPointToBlocks(function (block) {
      block.appendDummyInput()
          .appendTitle(new blockly.FieldAngleDropdown({
            direction: 'turnRight',
            menuGenerator: block.VALUE
          }), 'VALUE')
          .appendTitle(msg.degrees());
    });
  
    blockly.Blocks.point_to_by_constant_restricted.VALUE =
        [0, 30, 45, 60, 90, 120, 135, 150, 180].map(function (t) {
          return [String(t), String(t)];
        });
  
    generator.point_to_by_constant_restricted = function () {
      let value = window.parseFloat(this.getTitleValue('VALUE'));
      return `Turtle.pointTo(${value}, 'block_id_${this.id}');\n`;
    };
  
    generator.draw_turn_inline = function () {
      // Generate JavaScript for turning left or right.
      var value = window.parseFloat(this.getTitleValue('VALUE'));
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.variables_get_counter = {
      // Variable getter.
      category: null,  // Variables are handled specially.
      helpUrl: blockly.Msg.VARIABLES_GET_HELPURL,
      init: function () {
        this.setHSV(312, 0.32, 0.62);
        this.appendDummyInput()
            .appendTitle(blockly.Msg.VARIABLES_GET_TITLE)
            .appendTitle(new blockly.FieldLabel(msg.loopVariable()), 'VAR');
        this.setOutput(true);
        this.setTooltip(blockly.Msg.VARIABLES_GET_TOOLTIP);
      },
      getVars: blockly.Variables.getVars,
    };
  
    generator.variables_get_counter = generator.variables_get;
  
    blockly.Blocks.variables_get_length = {
      // Variable getter.
      category: null,  // Variables are handled specially.
      helpUrl: blockly.Msg.VARIABLES_GET_HELPURL,
      init: function () {
        this.setHSV(312, 0.32, 0.62);
        this.appendDummyInput()
            .appendTitle(blockly.Msg.VARIABLES_GET_TITLE)
            .appendTitle(new blockly.FieldLabel(msg.lengthParameter()), 'VAR');
        this.setOutput(true);
        this.setTooltip(blockly.Msg.VARIABLES_GET_TOOLTIP);
      },
      getVars: blockly.Variables.getVars,
    };
  
    generator.variables_get_length = generator.variables_get;
  
    blockly.Blocks.variables_get_sides = {
      // Variable getter.
      category: null,  // Variables are handled specially.
      helpUrl: blockly.Msg.VARIABLES_GET_HELPURL,
      init: function () {
        this.setHSV(312, 0.32, 0.62);
        this.appendDummyInput()
            .appendTitle(blockly.Msg.VARIABLES_GET_TITLE)
            .appendTitle(new blockly.FieldLabel('sides'), 'VAR');
        this.setOutput(true);
        this.setTooltip(blockly.Msg.VARIABLES_GET_TOOLTIP);
      },
      getVars: blockly.Variables.getVars,
    };
  
    generator.variables_get_sides = generator.variables_get;
  
    // Create a fake "draw a square" function so it can be made available to users
    // without being shown in the workspace.
    blockly.Blocks.draw_a_square = {
      // Draw a square.
      init: function () {
        this.setHSV(94, 0.84, 0.60);
        this.appendDummyInput()
            .appendTitle(msg.drawASquare());
        this.appendValueInput('VALUE')
            .setAlign(blockly.ALIGN_RIGHT)
            .setCheck(blockly.BlockValueType.NUMBER)
                .appendTitle(msg.lengthParameter() + ':');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setInputsInline(true);
        this.setTooltip('');
      }
    };
  
    generator.draw_a_square = function () {
      // Generate JavaScript for drawing a square.
      var value_length = generator.valueToCode(
          this, 'VALUE', generator.ORDER_ATOMIC) || 0;
      var loopVar = gensym('count');
      return [
          // The generated comment helps detect required blocks.
          // Don't change it without changing requiredBlocks_.
          '// draw_a_square',
          'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 4; ' +
                loopVar + '++) {',
          '  Turtle.moveForward(' + value_length + ');',
          '  Turtle.turnRight(90);',
          '}\n'].join('\n');
    };
  
    // Create a fake "draw a snowman" function so it can be made available to
    // users without being shown in the workspace.
    blockly.Blocks.draw_a_snowman = {
      // Draw a circle in front of the turtle, ending up on the opposite side.
      init: function () {
        this.setHSV(94, 0.84, 0.60);
        this.appendDummyInput()
            .appendTitle(msg.drawASnowman());
        this.appendValueInput('VALUE')
            .setAlign(blockly.ALIGN_RIGHT)
            .setCheck(blockly.BlockValueType.NUMBER)
            .appendTitle(msg.lengthParameter() + ':');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip('');
      }
    };
  
    generator.draw_a_snowman = function () {
      // Generate JavaScript for drawing a snowman in front of the turtle.
      var value = generator.valueToCode(
          this, 'VALUE', generator.ORDER_ATOMIC);
      var distancesVar = gensym('distances');
      var loopVar = gensym('counter');
      var degreeVar = gensym('degree');
      var distanceVar = gensym('distance');
      return [
        // The generated comment helps detect required blocks.
        // Don't change it without changing requiredBlocks_.
        '// draw_a_snowman',
        'Turtle.turnLeft(90);',
        'var ' + distancesVar + ' = [' + value + ' * 0.5, ' + value + ' * 0.3,' +
            value + ' * 0.2];',
        'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 6; ' +
            loopVar + '++) {\n',
        '  var ' + distanceVar + ' = ' + distancesVar + '[' + loopVar +
            ' < 3 ? ' + loopVar + ': 5 - ' + loopVar + '] / 57.5;',
        '  for (var ' + degreeVar + ' = 0; ' + degreeVar + ' < 90; ' +
            degreeVar + '++) {',
        '    Turtle.moveForward(' + distanceVar + ');',
        '    Turtle.turnRight(2);',
        '  }',
        '  if (' + loopVar + ' !== 2) {',
        '    Turtle.turnLeft(180);',
        '  }',
        '}',
        'Turtle.turnLeft(90);\n'].join('\n');
    };
  
    // This is a modified copy of blockly.Blocks.controls_for with the
    // variable named "counter" hardcoded.
    blockly.Blocks.controls_for_counter = {
      // For loop with hardcoded loop variable.
      helpUrl: blockly.Msg.CONTROLS_FOR_HELPURL,
      init: function () {
        this.setHSV(322, 0.90, 0.95);
        this.appendDummyInput()
            .appendTitle(blockly.Msg.CONTROLS_FOR_INPUT_WITH)
            .appendTitle(new blockly.FieldLabel(msg.loopVariable()),
                         'VAR');
        this.interpolateMsg(blockly.Msg.CONTROLS_FOR_INPUT_FROM_TO_BY,
                          ['FROM', 'Number', blockly.ALIGN_RIGHT],
                          ['TO', 'Number', blockly.ALIGN_RIGHT],
                          ['BY', 'Number', blockly.ALIGN_RIGHT],
                          blockly.ALIGN_RIGHT);
        this.appendStatementInput('DO')
            .appendTitle(blockly.Msg.CONTROLS_FOR_INPUT_DO);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setInputsInline(true);
        this.setTooltip(blockly.Msg.CONTROLS_FOR_TOOLTIP.replace(
            '%1', this.getTitleValue('VAR')));
      },
      getVars: blockly.Variables.getVars,
      // serialize the counter variable name to xml so that it can be used across
      // different locales
      mutationToDom: function () {
        var container = document.createElement('mutation');
        var counter = this.getTitleValue('VAR');
        container.setAttribute('counter', counter);
        return container;
      },
      // deserialize the counter variable name
      domToMutation: function (xmlElement) {
        var counter = xmlElement.getAttribute('counter');
        this.setTitleValue(counter, 'VAR');
      }
    };
  
    generator.controls_for_counter = generator.controls_for;
  
    // Delete these standard blocks.
    delete blockly.Blocks.procedures_defreturn;
    delete blockly.Blocks.procedures_ifreturn;
  
    // General blocks.
  
    blockly.Blocks.draw_move = {
      // Block for moving forward or backwards.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendValueInput('VALUE')
            .setCheck(blockly.BlockValueType.NUMBER)
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.draw_move.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(msg.dots());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.moveTooltip());
      }
    };
  
    blockly.Blocks.draw_move.DIRECTIONS =
        [[msg.moveForward(), 'moveForward'],
         [msg.moveBackward(), 'moveBackward']];
  
    generator.draw_move = function () {
      // Generate JavaScript for moving forward or backwards.
      var value = generator.valueToCode(this, 'VALUE',
          generator.ORDER_NONE) || '0';
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.jump = {
      // Block for moving forward or backwards.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendValueInput('VALUE')
            .setCheck(blockly.BlockValueType.NUMBER)
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.jump.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(msg.dots());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.jumpTooltip());
      }
    };
  
    var longMoveLengthDropdownValue = "LONG_MOVE_LENGTH";
    var shortMoveLengthDropdownValue = "SHORT_MOVE_LENGTH";
    var longDiagonalMoveLengthDropdownValue = "LONG_DIAGONAL_MOVE_LENGTH";
    var shortDiagonalMoveLengthDropdownValue = "SHORT_DIAGONAL_MOVE_LENGTH";
    var defaultMoveLength = 50;
    var defaultDiagonalMoveLength = defaultMoveLength * Math.sqrt(2);
    var simpleLengthChoices = [
      [skin.longLineDraw, longMoveLengthDropdownValue],
      [skin.shortLineDraw, shortMoveLengthDropdownValue]
    ];
    var simpleDiagonalLengthChoices = [
      [skin.longLineDraw, longDiagonalMoveLengthDropdownValue],
      [skin.shortLineDraw, shortDiagonalMoveLengthDropdownValue]
    ];
    var simpleLengthRightChoices = [
      [skin.longLineDrawRight, longMoveLengthDropdownValue],
      [skin.shortLineDrawRight, shortMoveLengthDropdownValue]
    ];
  
    var SimpleMove = {
      SHORT_MOVE_LENGTH: 50,
      LONG_MOVE_LENGTH: 100,
      SHORT_DIAGONAL_MOVE_LENGTH: 50 * Math.sqrt(2),
      LONG_DIAGONAL_MOVE_LENGTH: 100 * Math.sqrt(2),
      DIRECTION_CONFIGS: {
        left: {
          title: commonMsg.directionWestLetter(),
          moveFunction: 'moveLeft',
          tooltip: msg.moveWestTooltip(),
          image: skin.westLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleLengthChoices,
          defaultLength: defaultMoveLength,
          defaultDropdownValue: longMoveLengthDropdownValue
        },
        right: {
          title: commonMsg.directionEastLetter(),
          moveFunction: 'moveRight',
          tooltip: msg.moveEastTooltip(),
          image: skin.eastLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleLengthRightChoices,
          defaultLength: defaultMoveLength,
          defaultDropdownValue: longMoveLengthDropdownValue
        },
        up: {
          title: commonMsg.directionNorthLetter(),
          moveFunction: 'moveUp',
          tooltip: msg.moveNorthTooltip(),
          image: skin.northLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleLengthChoices,
          defaultLength: defaultMoveLength,
          defaultDropdownValue: longMoveLengthDropdownValue
        },
        down: {
          title: commonMsg.directionSouthLetter(),
          moveFunction: 'moveDown',
          tooltip: msg.moveSouthTooltip(),
          image: skin.southLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleLengthChoices,
          defaultLength: defaultMoveLength,
          defaultDropdownValue: longMoveLengthDropdownValue
        },
        up_left: {
          title: commonMsg.directionNorthwestLetter(),
          moveFunction: 'moveUpLeft',
          tooltip: msg.moveNorthwestTooltip(),
          image: skin.northwestLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleDiagonalLengthChoices,
          defaultLength: defaultDiagonalMoveLength,
          defaultDropdownValue: longDiagonalMoveLengthDropdownValue
        },
        up_right: {
          title: commonMsg.directionNortheastLetter(),
          moveFunction: 'moveUpRight',
          tooltip: msg.moveNortheastTooltip(),
          image: skin.northeastLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleDiagonalLengthChoices,
          defaultLength: defaultDiagonalMoveLength,
          defaultDropdownValue: longDiagonalMoveLengthDropdownValue
        },
        down_left: {
          title: commonMsg.directionSouthwestLetter(),
          moveFunction: 'moveDownLeft',
          tooltip: msg.moveSouthwestTooltip(),
          image: skin.southwestLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleDiagonalLengthChoices,
          defaultLength: defaultDiagonalMoveLength,
          defaultDropdownValue: longDiagonalMoveLengthDropdownValue
        },
        down_right: {
          title: commonMsg.directionSoutheastLetter(),
          moveFunction: 'moveDownRight',
          tooltip: msg.moveSoutheastTooltip(),
          image: skin.southeastLineDraw,
          imageDimensions: {width: 72, height: 56},
          lengths: simpleDiagonalLengthChoices,
          defaultLength: defaultDiagonalMoveLength,
          defaultDropdownValue: longDiagonalMoveLengthDropdownValue
        },
        jump_left: {
          isJump: true,
          title: commonMsg.directionWestLetter(),
          moveFunction: 'jumpLeft',
          image: skin.leftJumpArrow,
          tooltip: msg.jumpWestTooltip(),
          defaultLength: defaultMoveLength
        },
        jump_right: {
          isJump: true,
          title: commonMsg.directionEastLetter(),
          moveFunction: 'jumpRight',
          image: skin.rightJumpArrow,
          tooltip: msg.jumpEastTooltip(),
          defaultLength: defaultMoveLength
        },
        jump_up: {
          isJump: true,
          title: commonMsg.directionNorthLetter(),
          moveFunction: 'jumpUp',
          image: skin.upJumpArrow,
          tooltip: msg.jumpNorthTooltip(),
          defaultLength: defaultMoveLength
        },
        jump_down: {
          isJump: true,
          title: commonMsg.directionSouthLetter(),
          moveFunction: 'jumpDown',
          image: skin.downJumpArrow,
          tooltip: msg.jumpSouthTooltip(),
          defaultLength: defaultMoveLength
        },
        jump_up_left: {
          isJump: true,
          title: commonMsg.directionNorthwestLetter(),
          moveFunction: 'jumpUpLeft',
          image: skin.upLeftJumpArrow,
          tooltip: msg.jumpNorthwestTooltip(),
          defaultLength: defaultDiagonalMoveLength
        },
        jump_up_right: {
          isJump: true,
          title: commonMsg.directionNortheastLetter(),
          moveFunction: 'jumpUpRight',
          image: skin.upRightJumpArrow,
          tooltip: msg.jumpNortheastTooltip(),
          defaultLength: defaultDiagonalMoveLength
        },
        jump_down_left: {
          isJump: true,
          title: commonMsg.directionSouthwestLetter(),
          moveFunction: 'jumpDownLeft',
          image: skin.downLeftJumpArrow,
          tooltip: msg.jumpSouthwestTooltip(),
          defaultLength: defaultDiagonalMoveLength
        },
        jump_down_right: {
          isJump: true,
          title: commonMsg.directionSoutheastLetter(),
          moveFunction: 'jumpDownRight',
          image: skin.downRightJumpArrow,
          tooltip: msg.jumpSoutheastTooltip(),
          defaultLength: defaultDiagonalMoveLength
        }
      },
      generateBlocksForAllDirections: function () {
        SimpleMove.generateBlocksForDirection('up');
        SimpleMove.generateBlocksForDirection('down');
        SimpleMove.generateBlocksForDirection('left');
        SimpleMove.generateBlocksForDirection('right');
        SimpleMove.generateBlocksForDirection('up_left');
        SimpleMove.generateBlocksForDirection('up_right');
        SimpleMove.generateBlocksForDirection('down_left');
        SimpleMove.generateBlocksForDirection('down_right');
      },
      generateBlocksForDirection: function (direction) {
        generator['simple_move_' + direction] = SimpleMove.generateCodeGenerator(direction);
        generator['simple_jump_' + direction] = SimpleMove.generateCodeGenerator('jump_' + direction);
        generator['simple_move_' + direction + '_length'] = SimpleMove.generateCodeGenerator(direction, true);
        blockly.Blocks['simple_move_' + direction + '_length'] = SimpleMove.generateMoveBlock(direction, true);
        blockly.Blocks['simple_move_' + direction] = SimpleMove.generateMoveBlock(direction);
        blockly.Blocks['simple_jump_' + direction] = SimpleMove.generateMoveBlock('jump_' + direction);
      },
      generateMoveBlock: function (direction, hasLengthInput) {
        var directionConfig = SimpleMove.DIRECTION_CONFIGS[direction];
        var directionLetterWidth = 12;
        return {
          helpUrl: '',
          init: function () {
            this.setHSV(184, 1.00, 0.74);
            var input = this.appendDummyInput();
            if (directionConfig.isJump) {
              input.appendTitle(commonMsg.jump());
            }
            input.appendTitle(new blockly.FieldLabel(directionConfig.title, {fixedSize: {width: directionLetterWidth, height: 18}}));
  
            if (directionConfig.imageDimensions) {
              input.appendTitle(new blockly.FieldImage(directionConfig.image,
                directionConfig.imageDimensions.width,
                directionConfig.imageDimensions.height));
            } else {
              input.appendTitle(new blockly.FieldImage(directionConfig.image));
            }
            this.setPreviousStatement(true);
            this.setNextStatement(true);
            this.setTooltip(directionConfig.tooltip);
            if (hasLengthInput) {
              var dropdown = new blockly.FieldImageDropdown(directionConfig.lengths);
              dropdown.setValue(directionConfig.defaultDropdownValue);
              input.appendTitle(dropdown, 'length');
            }
          }
        };
      },
      generateCodeGenerator: function (direction, hasLengthInput) {
        return function () {
          var directionConfig = SimpleMove.DIRECTION_CONFIGS[direction];
          var length = directionConfig.defaultLength;
  
          if (hasLengthInput) {
            length = SimpleMove[this.getTitleValue("length")];
          }
          return 'Turtle.' + directionConfig.moveFunction + '(' + length + ',' + '\'block_id_' + this.id + '\');\n';
        };
      }
    };
  
    SimpleMove.generateBlocksForAllDirections();
  
    blockly.Blocks.jump.DIRECTIONS =
        [[msg.jumpForward(), 'jumpForward'],
         [msg.jumpBackward(), 'jumpBackward']];
  
    generator.jump = function () {
      // Generate JavaScript for jumping forward or backwards.
      var value = generator.valueToCode(this, 'VALUE',
          generator.ORDER_NONE) || '0';
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.jump_by_constant = {
      // Block for moving forward or backward the internal number of pixels
      // without drawing.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.jump.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(new blockly.FieldTextInput('100',
                blockly.FieldTextInput.numberValidator), 'VALUE')
            .appendTitle(msg.dots());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.jumpTooltip());
      }
    };
  
    blockly.Blocks.jump_by_constant_dropdown = {
      // Block for moving forward or backward the internal number of pixels
      // without drawing.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.jump.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(), 'VALUE')
            .appendTitle(msg.dots());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.jumpTooltip());
      }
    };
  
    generator.jump_by_constant = function () {
      // Generate JavaScript for moving forward or backward the internal number
      // of pixels without drawing.
      var value = window.parseFloat(this.getTitleValue('VALUE')) || 0;
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
    generator.jump_by_constant_dropdown = generator.jump_by_constant;
  
    blockly.Blocks.jump_to = {
      // Block for jumping to a specified position
      helpUrl: '',
      init: function () {
        var dropdown = new blockly.FieldDropdown(this.VALUES);
        dropdown.setValue(this.VALUES[1][1]); // default to top-left
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
          .appendTitle(msg.jump());
        this.appendDummyInput()
          .appendTitle(dropdown, 'VALUE');
        this.setPreviousStatement(true);
        this.setInputsInline(true);
        this.setNextStatement(true);
        this.setTooltip(msg.jumpTooltip());
      }
    };
  
    generator.jump_to = function () {
      let value = this.getTitleValue('VALUE');
      if (value === RANDOM_VALUE) {
        let possibleValues = this.VALUES.map(item => item[1])
            .filter(item => item !== RANDOM_VALUE);
        value = `Turtle.random([${possibleValues}])`;
      }
      return `Turtle.jumpTo(${value}, 'block_id_${this.id}');\n`;
    };
  
    blockly.Blocks.jump_to_xy = {
      // Block for jumping to specified XY location.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
          .appendTitle(msg.jumpTo());
        this.appendDummyInput()
            .appendTitle(new blockly.FieldTextInput('0',
              blockly.FieldTextInput.numberValidator), 'XPOS')
            .appendTitle(commonMsg.positionAbsoluteOver());
        this.appendDummyInput()
            .appendTitle(new blockly.FieldTextInput('0',
              blockly.FieldTextInput.numberValidator), 'YPOS')
            .appendTitle(commonMsg.positionAbsoluteDown());
        this.setPreviousStatement(true);
        this.setInputsInline(true);
        this.setNextStatement(true);
        this.setTooltip(msg.jumpTooltip());
      }
    };
  
    generator.jump_to_xy = function () {
      const xParam = window.parseFloat(this.getTitleValue('XPOS')) || 0;
      const yParam = window.parseFloat(this.getTitleValue('YPOS')) || 0;
      return `Turtle.jumpToXY(${xParam}, ${yParam}, 'block_id_${this.id}');\n`;
    };
  
    blockly.Blocks.draw_turn = {
      // Block for turning left or right.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendValueInput('VALUE')
            .setCheck(blockly.BlockValueType.NUMBER)
            .addFieldHelper(blockly.BlockFieldHelper.ANGLE_HELPER, {
              block: this,
              directionTitle: 'DIR',
            })
            .appendTitle(new blockly.FieldDropdown(
                blockly.Blocks.draw_turn.DIRECTIONS), 'DIR');
        this.appendDummyInput()
            .appendTitle(msg.degrees());
        this.setInputsInline(true);
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.turnTooltip());
      }
    };
  
    blockly.Blocks.draw_turn.DIRECTIONS =
        [[msg.turnRight(), 'turnRight'],
         [msg.turnLeft(), 'turnLeft']];
  
    generator.draw_turn = function () {
      // Generate JavaScript for turning left or right.
      var value = generator.valueToCode(this, 'VALUE',
          generator.ORDER_NONE) || '0';
      return 'Turtle.' + this.getTitleValue('DIR') +
          '(' + value + ', \'block_id_' + this.id + '\');\n';
    };
  
    // this is the old version of this block, that should only still be used in
    // old shared levels
    blockly.Blocks.draw_width = {
      // Block for setting the pen width.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendValueInput('WIDTH')
            .setCheck(blockly.BlockValueType.NUMBER)
            .appendTitle(msg.setWidth());
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.widthTooltip());
      }
    };
  
    generator.draw_width = function () {
      // Generate JavaScript for setting the pen width.
      var width = generator.valueToCode(this, 'WIDTH',
          generator.ORDER_NONE) || '1';
      return 'Turtle.penWidth(' + width + ', \'block_id_' + this.id + '\');\n';
    };
  
    // inlined version of draw_width
    blockly.Blocks.draw_width_inline = {
      // Block for setting the pen width.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.setInputsInline(true);
        this.appendDummyInput()
            .appendTitle(msg.setWidth());
        this.appendDummyInput()
            .appendTitle(new blockly.FieldTextInput('1',
              blockly.FieldTextInput.numberValidator), 'WIDTH');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.widthTooltip());
      }
    };
  
    generator.draw_width_inline = function () {
      // Generate JavaScript for setting the pen width.
      var width = this.getTitleValue('WIDTH');
      return 'Turtle.penWidth(' + width + ', \'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.draw_pen = {
      // Block for pen up/down.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(this.STATE), 'PEN');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.penTooltip());
      }
    };
  
    blockly.Blocks.draw_pen.STATE =
        [[msg.penUp(), 'penUp'],
         [msg.penDown(), 'penDown']];
  
    generator.draw_pen = function () {
      // Generate JavaScript for pen up/down.
      return 'Turtle.' + this.getTitleValue('PEN') +
          '(\'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.draw_colour = {
      // Block for setting the colour.
      helpUrl: '',
      init: function () {
        this.setHSV(196, 1.0, 0.79);
        this.appendValueInput('COLOUR')
            .setCheck(blockly.BlockValueType.COLOUR)
            .appendTitle(msg.setColour());
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.colourTooltip());
      }
    };
  
    blockly.Blocks.alpha = {
      // TODO:
      // - Add alpha to a group
      // - Make sure it doesn't count against correct solutions
      //
      init: function () {
        this.appendValueInput("VALUE")
            .setCheck("Number")
            .appendTitle(msg.setAlpha());
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.setHSV(196, 1.0, 0.79);
        this.setTooltip('');
      }
    };
  
    generator.alpha = function () {
      var alpha = generator.valueToCode(this, 'VALUE', blockly.JavaScript.ORDER_NONE);
      return 'Turtle.globalAlpha(' + alpha + ', \'block_id_' +
          this.id + '\');\n';
    };
  
    generator.draw_colour = function () {
      // Generate JavaScript for setting the colour.
      var colour = generator.valueToCode(this, 'COLOUR',
          generator.ORDER_NONE) || '\'#000000\'';
      return 'Turtle.penColour(' + colour + ', \'block_id_' +
          this.id + '\');\n';
    };
  
    blockly.Blocks.draw_colour_simple = {
      // Simplified dropdown block for setting the colour.
      init: function () {
        var colours = [Colours.RED, Colours.BLACK, Colours.PINK, Colours.ORANGE,
          Colours.YELLOW, Colours.GREEN, Colours.BLUE, Colours.AQUAMARINE, Colours.PLUM];
        this.setHSV(196, 1.0, 0.79);
        var colourField = new blockly.FieldColourDropdown(colours, 45, 35);
        this.appendDummyInput()
            .appendTitle(msg.setColour())
            .appendTitle(colourField, 'COLOUR');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
        this.setTooltip(msg.colourTooltip());
      }
    };
  
    generator.draw_colour_simple = function () {
      // Generate JavaScript for setting the colour.
      var colour = this.getTitleValue('COLOUR') || '\'#000000\'';
      return 'Turtle.penColour("' + colour + '", \'block_id_' +
          this.id + '\');\n';
    };
  
    blockly.Blocks.draw_line_style_pattern = {
      // Block to handle event when an arrow button is pressed.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.appendDummyInput()
             .appendTitle(msg.setPattern())
             .appendTitle( new blockly.FieldImageDropdown(
                skin.lineStylePatternOptions, 150, 20 ), 'VALUE' );
        this.setTooltip(msg.setPattern());
      }
    };
  
    generator.draw_line_style_pattern = function () {
      // Generate JavaScript for setting the image for a patterned line.
      var pattern = this.getTitleValue('VALUE') || '\'DEFAULT\'';
      return 'Turtle.penPattern("' + pattern + '", \'block_id_' +
          this.id + '\');\n';
    };
  
    blockly.Blocks.up_big = {
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.appendDummyInput()
          .appendTitle(new blockly.FieldDropdown(this.STATE), 'VISIBILITY');
        this.setTooltip(msg.turtleVisibilityTooltip());
      }
    };
  
    generator.up_big = function () {
      // Generate JavaScript for setting the colour.
      var colour = generator.valueToCode(this, 'COLOUR',
        generator.ORDER_NONE) || '\'#000000\'';
      return 'Turtle.penColour(' + colour + ', \'block_id_' +
        this.id + '\');\n';
    };
  
    blockly.Blocks.turtle_visibility = {
      // Block for changing turtle visiblity.
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        this.setPreviousStatement(true, null);
        this.setNextStatement(true, null);
        this.appendDummyInput()
            .appendTitle(new blockly.FieldDropdown(this.STATE), 'VISIBILITY');
        this.setTooltip(msg.turtleVisibilityTooltip());
      }
    };
  
    blockly.Blocks.turtle_visibility.STATE =
        [[msg.hideTurtle(), 'hideTurtle'],
         [msg.showTurtle(), 'showTurtle']];
  
    generator.turtle_visibility = function () {
      // Generate JavaScript for changing turtle visibility.
      return 'Turtle.' + this.getTitleValue('VISIBILITY') +
          '(\'block_id_' + this.id + '\');\n';
    };
  
    function createDrawStickerBlock(blockName)  {
      return {
        helpUrl: '',
        init: function () {
          this.setHSV(184, 1.00, 0.74);
          var dropdown;
          var input = this.appendDummyInput();
          input.appendTitle(msg.drawSticker());
          this.setInputsInline(true);
          this.setPreviousStatement(true);
          this.setNextStatement(true);
  
          // Generates a list of pairs of the form [[url, name]]
          var values = [];
          for (var name in skin.stickers) {
            var url = skin.stickers[name];
            values.push([url, name]);
          }
          dropdown = new blockly.FieldImageDropdown(values, 40, 40);
  
          input.appendTitle(dropdown, 'VALUE');
  
          appendToDrawStickerBlock(blockName, this);
        }
      };
    }
  
    // Add size input to the draw sticker block (text input & socket)
    function appendToDrawStickerBlock(blockName, block) {
      if (blockName === 'turtle_sticker_with_size') {
        block.appendDummyInput().appendTitle(msg.withSize());
        block.appendValueInput('SIZE')
            .setCheck(blockly.BlockValueType.NUMBER);
        block.appendDummyInput()
            .appendTitle(msg.pixels());
        block.setTooltip(msg.drawStickerWithSize());
      } else if (blockName === 'turtle_sticker_with_size_non_param') {
        block.appendDummyInput()
            .appendTitle(msg.withSize());
        block.appendDummyInput().appendTitle(new blockly.FieldTextInput('0',
            blockly.FieldTextInput.numberValidator), 'SIZE')
            .appendTitle(msg.pixels());
        block.setTooltip(msg.drawStickerWithSize());
      } else {
        block.setTooltip(msg.drawSticker());
      }
    }
  
    // We alias 'turtle_stamp' to be the same as the 'sticker' block for
    // backwards compatibility.
    blockly.Blocks.sticker = blockly.Blocks.turtle_stamp =
        createDrawStickerBlock();
  
    generator.sticker = generator.turtle_stamp = function () {
      return 'Turtle.drawSticker("' + this.getTitleValue('VALUE') +
          '", null, \'block_id_' + this.id + '\');\n';
    };
  
    blockly.Blocks.turtle_sticker_with_size =
        createDrawStickerBlock('turtle_sticker_with_size');
  
    generator.turtle_sticker_with_size = function () {
      let size = generator.valueToCode(this, 'SIZE',
          blockly.JavaScript.ORDER_NONE);
      return `Turtle.drawSticker('${this.getTitleValue('VALUE')}',${size},
          'block_id_${this.id}');\n`;
    };
  
    blockly.Blocks.turtle_sticker_with_size_non_param =
        createDrawStickerBlock('turtle_sticker_with_size_non_param');
  
    generator.turtle_sticker_with_size_non_param = function () {
      let size = window.parseFloat(this.getTitleValue('SIZE')) || 0;
      return `Turtle.drawSticker('${this.getTitleValue('VALUE')}',${size},
          'block_id_${this.id}');\n`;
    };
  
    blockly.Blocks.turtle_setArtist = {
      helpUrl: '',
      init: function () {
        this.setHSV(184, 1.00, 0.74);
        var values = (skin.artistOptions || ['default'])
          .map(artist => [
            msg.setCharacter({character: artist.charAt(0).toUpperCase() + artist.slice(1)}),
            artist
          ]);
        this.appendDummyInput()
          .appendTitle(new blockly.FieldDropdown(values), 'VALUE');
        this.setPreviousStatement(true);
        this.setNextStatement(true);
      },
    };
  
    generator.turtle_setArtist = function () {
      return `Turtle.setArtist('${this.getTitleValue('VALUE')}',
        'block_id_${this.id}');\n`;
    };
  
    //customLevelBlocks.install(blockly, generator, gensym);
    */
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9pbml0Q29udGVudC9ibG9ja3NfYXJ0aXN0LmpzIl0sIm5hbWVzIjpbIkNvbG91cnMiLCJyZXF1aXJlIiwibXNnIiwiUG9zaXRpb24iLCJSQU5ET01fVkFMVUUiLCJpbnN0YWxsIiwiYmxvY2tseSIsImdlbmVyYXRvciIsIkphdmFTY3JpcHQiLCJGaWVsZENvbG91ciIsIkNPTE9VUlMiLCJCTEFDSyIsIkdSRVkiLCJLSEFLSSIsIldISVRFIiwiUkVEIiwiUElOSyIsIk9SQU5HRSIsIllFTExPVyIsIkdSRUVOIiwiQkxVRSIsIkFRVUFNQVJJTkUiLCJQTFVNIiwiQ09MVU1OUyIsIkJsb2NrcyIsInN0YXJ0ZXIiLCJpbml0IiwiYXBwZW5kRHVtbXlJbnB1dCIsImFwcGVuZEZpZWxkIiwic2V0Q29sb3VyIiwic2V0SW5wdXRzSW5saW5lIiwic2V0TmV4dFN0YXRlbWVudCIsInNldFRvb2x0aXAiLCJzZXRIZWxwVXJsIiwiYmxvY2siLCJkcmF3X2NpcmNsZSIsIkZpZWxkRHJvcGRvd24iLCJGaWVsZE51bWJlciIsInNldFByZXZpb3VzU3RhdGVtZW50IiwiZHJvcGRvd25fZ29fc3RlcCIsImdldEZpZWxkVmFsdWUiLCJ0ZXh0X3BpeGVsIiwiZ29fb3B0aW9uIiwiaXNOYU4iLCJwYXJzZUludCIsInR1cm5fb3B0aW9uIiwiRmllbGRBbmdsZSIsImRyb3Bkb3duX3R1cm5fc3RlcCIsInRleHRfZGVncmVlIiwianVtcF9vcHRpb24iLCJqdW1wX2F4aXMiLCJhcHBlbmRWYWx1ZUlucHV0Iiwic2V0Q2hlY2siLCJGaWVsZFRleHRJbnB1dCIsInRleHRfeF9heGlzIiwidGV4dF95X2F4aXMiLCJ2YWx1ZV9qdW1wIiwidmFsdWVUb0NvZGUiLCJPUkRFUl9BVE9NSUMiLCJwb2ludF90byIsImRlZ3JlZSIsImRuZ19jbF9zdGF0aWMiLCJzZWxDb2xvciIsIm51bSIsIk1hdGgiLCJmbG9vciIsInJhbmRvbSIsInBvdyIsImNvbG9yIiwidG9TdHJpbmciLCJzdWJzdHIiLCJkbmdfbGluZV9oZWlnaHQiLCJhbHBoYSIsImxvb3BfZm9yIiwiYXBwZW5kU3RhdGVtZW50SW5wdXQiLCJkcm9wZG93bl9sb29wX29wdGlvbiIsInRleHRfc3RhcnQiLCJ0ZXh0X3N0b3AiLCJ0ZXh0X3BsdXMiLCJzdGF0ZW1lbnRzX2ZvciIsInN0YXRlbWVudFRvQ29kZSIsInN0YXJ0IiwiZW5kIiwiYWRkaW5nIiwiY29kZVN0cmluZyIsImxvb3Bfd2hpbGUiLCJzdGF0ZW1lbnRzX3doaWxlIiwiY2FsX3ZhcmlhYmxlIiwic2V0T3V0cHV0IiwidGV4dF92YXJpYWJsZSIsImNvZGUiLCJPUkRFUl9OT05FIiwiY2FsX2Z1bmN0aW9uIiwidGV4dF9maXJzdF92YXIiLCJkcm9wZG93bl9jYWxfZnVuY3Rpb25fb3B0aW9uIiwidGV4dF9zZWNvbmRfdmFyIiwidmFyaWFibGVfZmlyc3QiLCJkcm9wZG93bl9vcHRpb24iLCJ2YXJpYWJsZV9zZWNvbmQiLCJ2YWx1ZV9uYW1lIl0sIm1hcHBpbmdzIjoiQUFBQSxJQUFJQSxVQUFVQyxRQUFRLFdBQVIsQ0FBZDtBQUNBO0FBQ0EsSUFBSUMsTUFBTUQsUUFBUSxVQUFSLENBQVY7QUFDQTtBQUNBLFNBQ0VFLFFBREYsUUFFTyxjQUZQO0FBR0EsTUFBTUMsZUFBZSxNQUFyQjs7QUFFQTtBQUNBLE9BQU8sTUFBTUMsVUFBVSxVQUFVQyxPQUFWLEVBQW1CO0FBQ3hDLE1BQUlDLFlBQVlELFFBQVFFLFVBQXhCOztBQUVBO0FBQ0FGLFVBQVFHLFdBQVIsQ0FBb0JDLE9BQXBCLEdBQThCO0FBQzVCO0FBQ0FWLFVBQVFXLEtBRm9CLEVBRWJYLFFBQVFZLElBRkssRUFHNUJaLFFBQVFhLEtBSG9CLEVBR2JiLFFBQVFjLEtBSEs7QUFJNUI7QUFDQWQsVUFBUWUsR0FMb0IsRUFLZmYsUUFBUWdCLElBTE8sRUFNNUJoQixRQUFRaUIsTUFOb0IsRUFNWmpCLFFBQVFrQixNQU5JO0FBTzVCO0FBQ0FsQixVQUFRbUIsS0FSb0IsRUFRYm5CLFFBQVFvQixJQVJLLEVBUzVCcEIsUUFBUXFCLFVBVG9CLEVBU1JyQixRQUFRc0IsSUFUQSxDQUE5QjtBQVdBaEIsVUFBUUcsV0FBUixDQUFvQmMsT0FBcEIsR0FBOEIsQ0FBOUI7QUFDQTs7QUFFQWpCLFVBQVFrQixNQUFSLENBQWVDLE9BQWYsR0FBeUI7QUFDdkJDLFVBQU0sWUFBWTtBQUNoQixXQUFLQyxnQkFBTCxHQUNHQyxXQURILENBQ2UsZ0JBRGY7QUFFQSxXQUFLQyxTQUFMLENBQWUsR0FBZjtBQUNBLFdBQUtDLGVBQUwsQ0FBcUIsSUFBckI7QUFDQSxXQUFLQyxnQkFBTCxDQUFzQixJQUF0QjtBQUNBLFdBQUtDLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFUc0IsR0FBekI7O0FBWUExQixZQUFVa0IsT0FBVixHQUFvQixVQUFVUyxLQUFWLEVBQWlCO0FBQ25DO0FBQ0E7QUFDQSxXQUFPLE1BQVA7QUFDRCxHQUpEOztBQU1BNUIsVUFBUWtCLE1BQVIsQ0FBZVcsV0FBZixHQUE2QjtBQUMzQlQsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxJQUFJdEIsUUFBUThCLGFBQVosQ0FBMEIsQ0FDckMsQ0FBQyxPQUFELEVBQVUsR0FBVixDQURxQyxDQUExQixDQURmLEVBR00sU0FITixFQUlHUixXQUpILENBSWUsSUFBSXRCLFFBQVErQixXQUFaLENBQXdCLEtBQXhCLENBSmYsRUFJK0MsS0FKL0MsRUFLR1QsV0FMSCxDQUtlLFFBTGY7QUFNQSxXQUFLVSxvQkFBTCxDQUEwQixJQUExQixFQUFnQyxJQUFoQztBQUNBLFdBQUtQLGdCQUFMLENBQXNCLElBQXRCLEVBQTRCLElBQTVCO0FBQ0EsV0FBS0YsU0FBTCxDQUFlLEdBQWY7QUFDQSxXQUFLRyxVQUFMLENBQWdCLEVBQWhCO0FBQ0EsV0FBS0MsVUFBTCxDQUFnQixFQUFoQjtBQUNEO0FBYjBCLEdBQTdCOztBQWdCQTNCLFVBQVFFLFVBQVIsQ0FBbUIyQixXQUFuQixHQUFpQyxVQUFVRCxLQUFWLEVBQWlCO0FBQ2hELFFBQUlLLG1CQUFtQkwsTUFBTU0sYUFBTixDQUFvQixTQUFwQixDQUF2QjtBQUNBLFFBQUlDLGFBQWFQLE1BQU1NLGFBQU4sQ0FBb0IsS0FBcEIsQ0FBakI7QUFDQSxXQUFPLHVCQUF1QkMsVUFBdkIsR0FBb0MsS0FBM0M7QUFFRCxHQUxEOztBQU9BO0FBQ0E7QUFDQW5DLFVBQVFrQixNQUFSLENBQWVrQixTQUFmLEdBQTJCO0FBQ3pCaEIsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxJQUFJdEIsUUFBUThCLGFBQVosQ0FBMEIsQ0FDckMsQ0FBQyxZQUFELEVBQWUsR0FBZixDQURxQyxFQUVyQyxDQUFDLFdBQUQsRUFBYyxHQUFkLENBRnFDLENBQTFCLENBRGYsRUFJTSxTQUpOLEVBS0dSLFdBTEgsQ0FLZSxJQUFJdEIsUUFBUStCLFdBQVosQ0FBd0IsS0FBeEIsQ0FMZixFQUsrQyxPQUwvQyxFQU1HVCxXQU5ILENBTWUsUUFOZjtBQU9BLFdBQUtVLG9CQUFMLENBQTBCLElBQTFCLEVBQWdDLElBQWhDO0FBQ0EsV0FBS1AsZ0JBQUwsQ0FBc0IsSUFBdEIsRUFBNEIsSUFBNUI7QUFDQSxXQUFLRixTQUFMLENBQWUsR0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFkd0IsR0FBM0I7O0FBaUJBM0IsVUFBUUUsVUFBUixDQUFtQmtDLFNBQW5CLEdBQStCLFVBQVVSLEtBQVYsRUFBaUI7QUFDOUMsUUFBSUssbUJBQW1CTCxNQUFNTSxhQUFOLENBQW9CLFNBQXBCLENBQXZCO0FBQ0EsUUFBSUMsYUFBYVAsTUFBTU0sYUFBTixDQUFvQixPQUFwQixDQUFqQjtBQUNBO0FBQ0E7QUFDQUMsaUJBQWNFLE1BQU1GLFVBQU4sQ0FBRCxHQUFzQixDQUF0QixHQUEwQkcsU0FBU0gsVUFBVCxDQUF2QztBQUNBLFFBQUlGLG9CQUFvQixHQUF4QixFQUE2QjtBQUMzQkUsbUJBQWEsSUFBSUEsVUFBakI7QUFDRCxLQUZELE1BRU87QUFDTEEsbUJBQWEsSUFBSUEsVUFBakI7QUFDRDtBQUNELFdBQU8sbUJBQW1CQSxVQUFuQixHQUFnQyxLQUF2QztBQUVELEdBYkQ7QUFjQTs7QUFFQTtBQUNBbkMsVUFBUWtCLE1BQVIsQ0FBZXFCLFdBQWYsR0FBNkI7QUFDM0JuQixVQUFNLFlBQVk7QUFDaEIsV0FBS0MsZ0JBQUwsR0FDR0MsV0FESCxDQUNlLElBQUl0QixRQUFROEIsYUFBWixDQUEwQixDQUNyQyxDQUFDLFVBQUQsRUFBYSxPQUFiLENBRHFDLEVBRXJDLENBQUMsV0FBRCxFQUFjLE1BQWQsQ0FGcUMsQ0FBMUIsQ0FEZixFQUlNLFdBSk4sRUFLR1IsV0FMSCxDQUtlLElBQUl0QixRQUFRd0MsVUFBWixDQUF1QixLQUF2QixDQUxmLEVBSzhDLFFBTDlDLEVBTUdsQixXQU5ILENBTWUsTUFOZjtBQU9BLFdBQUtVLG9CQUFMLENBQTBCLElBQTFCLEVBQWdDLElBQWhDO0FBQ0EsV0FBS1AsZ0JBQUwsQ0FBc0IsSUFBdEIsRUFBNEIsSUFBNUI7QUFDQSxXQUFLRixTQUFMLENBQWUsR0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFkMEIsR0FBN0I7O0FBaUJBM0IsVUFBUUUsVUFBUixDQUFtQnFDLFdBQW5CLEdBQWlDLFVBQVVYLEtBQVYsRUFBaUI7QUFDaEQsUUFBSWEscUJBQXFCYixNQUFNTSxhQUFOLENBQW9CLFdBQXBCLENBQXpCO0FBQ0EsUUFBSVEsY0FBY2QsTUFBTU0sYUFBTixDQUFvQixRQUFwQixDQUFsQjtBQUNBUSxrQkFBZUwsTUFBTUssV0FBTixDQUFELEdBQXVCLENBQXZCLEdBQTJCSixTQUFTSSxXQUFULENBQXpDOztBQUVBLFFBQUlELHVCQUF1QixHQUEzQixFQUFnQztBQUM5QixhQUFPLDRCQUE0QkMsV0FBNUIsR0FBMEMsS0FBakQ7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPLDJCQUEyQkEsV0FBM0IsR0FBeUMsS0FBaEQ7QUFDRDtBQUNGLEdBVkQ7O0FBWUE7O0FBRUE7QUFDQTFDLFVBQVFrQixNQUFSLENBQWV5QixXQUFmLEdBQTZCO0FBQzNCdkIsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxJQUFJdEIsUUFBUThCLGFBQVosQ0FBMEIsQ0FDckMsQ0FBQyxrQkFBRCxFQUFxQixXQUFyQixDQURxQyxFQUVyQyxDQUFDLGtCQUFELEVBQXFCLFdBQXJCLENBRnFDLENBQTFCLENBRGYsRUFJTSxXQUpOLEVBS0dSLFdBTEgsQ0FLZSxJQUFJdEIsUUFBUStCLFdBQVosQ0FBd0IsR0FBeEIsQ0FMZixFQUs2QyxPQUw3QyxFQU1HVCxXQU5ILENBTWUsUUFOZjtBQU9BLFdBQUtVLG9CQUFMLENBQTBCLElBQTFCLEVBQWdDLElBQWhDO0FBQ0EsV0FBS1AsZ0JBQUwsQ0FBc0IsSUFBdEIsRUFBNEIsSUFBNUI7QUFDQSxXQUFLRixTQUFMLENBQWUsR0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFkMEIsR0FBN0I7O0FBaUJBM0IsVUFBUUUsVUFBUixDQUFtQnlDLFdBQW5CLEdBQWlDLFVBQVVmLEtBQVYsRUFBaUI7QUFDaEQsUUFBSWEscUJBQXFCYixNQUFNTSxhQUFOLENBQW9CLFdBQXBCLENBQXpCO0FBQ0EsUUFBSUMsYUFBYVAsTUFBTU0sYUFBTixDQUFvQixPQUFwQixDQUFqQjtBQUNBQyxpQkFBY0UsTUFBTUYsVUFBTixDQUFELEdBQXNCLENBQXRCLEdBQTBCQSxVQUF2QztBQUNBLFFBQUlNLHVCQUF1QixXQUEzQixFQUF3QztBQUN0QyxhQUFPLDRCQUE0Qk4sVUFBNUIsR0FBeUMsS0FBaEQ7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPLDZCQUE2QkEsVUFBN0IsR0FBMEMsS0FBakQ7QUFDRDtBQUNEO0FBQ0QsR0FWRDs7QUFZQTs7O0FBR0FuQyxVQUFRa0IsTUFBUixDQUFlMEIsU0FBZixHQUEyQjtBQUN6QnhCLFVBQU0sWUFBWTtBQUNoQixXQUFLeUIsZ0JBQUwsQ0FBc0IsTUFBdEIsRUFDR0MsUUFESCxDQUNZLFFBRFosRUFFR3hCLFdBRkgsQ0FFZSxRQUZmLEVBR0dBLFdBSEgsQ0FHZSxXQUhmLEVBSUdBLFdBSkgsQ0FJZSxJQUFJdEIsUUFBUStDLGNBQVosQ0FBMkIsR0FBM0IsQ0FKZixFQUlnRCxRQUpoRCxFQUtHekIsV0FMSCxDQUtlLFlBTGYsRUFNR0EsV0FOSCxDQU1lLElBQUl0QixRQUFRK0MsY0FBWixDQUEyQixHQUEzQixDQU5mLEVBTWdELFFBTmhELEVBT0d6QixXQVBILENBT2UsbUJBUGY7QUFRQSxXQUFLVSxvQkFBTCxDQUEwQixJQUExQixFQUFnQyxJQUFoQztBQUNBLFdBQUtQLGdCQUFMLENBQXNCLElBQXRCLEVBQTRCLElBQTVCO0FBQ0EsV0FBS0YsU0FBTCxDQUFlLEdBQWY7QUFDQSxXQUFLRyxVQUFMLENBQWdCLEVBQWhCO0FBQ0EsV0FBS0MsVUFBTCxDQUFnQixFQUFoQjtBQUNEO0FBZndCLEdBQTNCOztBQWtCQTNCLFVBQVFFLFVBQVIsQ0FBbUIwQyxTQUFuQixHQUErQixVQUFVaEIsS0FBVixFQUFpQjtBQUM5QyxRQUFJb0IsY0FBY3BCLE1BQU1NLGFBQU4sQ0FBb0IsUUFBcEIsQ0FBbEI7QUFDQSxRQUFJZSxjQUFjckIsTUFBTU0sYUFBTixDQUFvQixRQUFwQixDQUFsQjtBQUNBLFFBQUlnQixhQUFhbEQsUUFBUUUsVUFBUixDQUFtQmlELFdBQW5CLENBQStCdkIsS0FBL0IsRUFBc0MsTUFBdEMsRUFBOEM1QixRQUFRRSxVQUFSLENBQW1Ca0QsWUFBakUsQ0FBakI7QUFDQSxXQUFPLGtCQUFrQkosV0FBbEIsR0FBZ0MsR0FBaEMsR0FBc0NDLFdBQXRDLEdBQW9ELEdBQTNEO0FBQ0QsR0FMRDs7QUFRQWpELFVBQVFrQixNQUFSLENBQWVtQyxRQUFmLEdBQTBCO0FBQ3hCakMsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxjQURmLEVBRUdBLFdBRkgsQ0FFZSxJQUFJdEIsUUFBUXdDLFVBQVosQ0FBdUIsS0FBdkIsQ0FGZixFQUU4QyxRQUY5QztBQUdBLFdBQUtSLG9CQUFMLENBQTBCLElBQTFCLEVBQWdDLElBQWhDO0FBQ0EsV0FBS1AsZ0JBQUwsQ0FBc0IsSUFBdEIsRUFBNEIsSUFBNUI7QUFDQSxXQUFLRixTQUFMLENBQWUsR0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFWdUIsR0FBMUI7O0FBYUEzQixVQUFRRSxVQUFSLENBQW1CbUQsUUFBbkIsR0FBOEIsVUFBVXpCLEtBQVYsRUFBaUI7QUFDN0MsUUFBSTBCLFNBQVMxQixNQUFNTSxhQUFOLENBQW9CLFFBQXBCLENBQWI7QUFDQTtBQUNBLFdBQU8sd0JBQXdCb0IsTUFBeEIsR0FBaUMsS0FBeEM7QUFDQTtBQUNBO0FBQ0QsR0FORDs7QUFTQTs7QUFFQTs7QUFFQTs7QUFFQXRELFVBQVFrQixNQUFSLENBQWVxQyxhQUFmLEdBQStCO0FBQzdCbkMsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxXQURmLEVBRUdBLFdBRkgsQ0FFZSxJQUFJdEIsUUFBUUcsV0FBWixDQUF3QixTQUF4QixDQUZmLEVBRW1ELE9BRm5EO0FBR0EsV0FBSzZCLG9CQUFMLENBQTBCLElBQTFCLEVBQWdDLElBQWhDO0FBQ0EsV0FBS1AsZ0JBQUwsQ0FBc0IsSUFBdEIsRUFBNEIsSUFBNUI7QUFDQSxXQUFLRixTQUFMLENBQWUsRUFBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRDtBQXBCNEIsR0FBL0I7O0FBdUJBM0IsVUFBUUUsVUFBUixDQUFtQnFELGFBQW5CLEdBQW1DLFVBQVUzQixLQUFWLEVBQWlCO0FBQ2xELFFBQUk0QixXQUFXNUIsTUFBTU0sYUFBTixDQUFvQixPQUFwQixDQUFmO0FBQ0EsV0FBTywyQkFBMkJzQixRQUEzQixHQUFzQyxNQUE3QztBQUNBO0FBQ0QsR0FKRDs7QUFNQTs7QUFFQTs7QUFFQXhELFVBQVFrQixNQUFSLENBQWUsZUFBZixJQUFrQztBQUNoQ0UsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxXQURmLEVBRUdBLFdBRkgsQ0FFZSxRQUZmO0FBR0EsV0FBS1Usb0JBQUwsQ0FBMEIsSUFBMUIsRUFBZ0MsSUFBaEM7QUFDQSxXQUFLUCxnQkFBTCxDQUFzQixJQUF0QixFQUE0QixJQUE1QjtBQUNBLFdBQUtGLFNBQUwsQ0FBZSxFQUFmO0FBQ0EsV0FBS0csVUFBTCxDQUFnQixFQUFoQjtBQUNBLFdBQUtDLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDRDtBQVYrQixHQUFsQzs7QUFhQTNCLFVBQVFFLFVBQVIsQ0FBbUIsZUFBbkIsSUFBc0MsVUFBVTBCLEtBQVYsRUFBaUI7QUFDckQsUUFBSTZCLE1BQU1DLEtBQUtDLEtBQUwsQ0FBV0QsS0FBS0UsTUFBTCxLQUFnQkYsS0FBS0csR0FBTCxDQUFTLENBQVQsRUFBWSxFQUFaLENBQTNCLENBQVY7QUFDQSxRQUFJQyxRQUFRLE1BQU0sQ0FBQyxVQUFVTCxJQUFJTSxRQUFKLENBQWEsRUFBYixDQUFYLEVBQTZCQyxNQUE3QixDQUFvQyxDQUFDLENBQXJDLENBQWxCO0FBQ0EsV0FBTywyQkFBMkJGLEtBQTNCLEdBQW1DLE1BQTFDO0FBQ0E7QUFDRCxHQUxEOztBQU9BOztBQUVBOztBQUVBOUQsVUFBUWtCLE1BQVIsQ0FBZStDLGVBQWYsR0FBaUM7QUFDL0I3QyxVQUFNLFlBQVk7QUFDaEIsV0FBS0MsZ0JBQUwsR0FDR0MsV0FESCxDQUNlLFdBRGYsRUFFR0EsV0FGSCxDQUVlLElBQUl0QixRQUFRK0IsV0FBWixDQUF3QixLQUF4QixDQUZmLEVBRStDLE9BRi9DLEVBR0dULFdBSEgsQ0FHZSxRQUhmO0FBSUEsV0FBS1Usb0JBQUwsQ0FBMEIsSUFBMUIsRUFBZ0MsSUFBaEM7QUFDQSxXQUFLUCxnQkFBTCxDQUFzQixJQUF0QixFQUE0QixJQUE1QjtBQUNBLFdBQUtGLFNBQUwsQ0FBZSxFQUFmO0FBQ0EsV0FBS0csVUFBTCxDQUFnQixFQUFoQjtBQUNBLFdBQUtDLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDRDtBQVg4QixHQUFqQzs7QUFjQTNCLFVBQVFFLFVBQVIsQ0FBbUIrRCxlQUFuQixHQUFxQyxVQUFVckMsS0FBVixFQUFpQjtBQUNwRCxRQUFJTyxhQUFhUCxNQUFNTSxhQUFOLENBQW9CLE9BQXBCLENBQWpCO0FBQ0EsV0FBTywyQkFBMkJDLFVBQTNCLEdBQXdDLEtBQS9DO0FBQ0E7QUFDRCxHQUpEO0FBS0E7O0FBRUFuQyxVQUFRa0IsTUFBUixDQUFlZ0QsS0FBZixHQUF1QjtBQUNyQjlDLFVBQU0sWUFBWTtBQUNoQixXQUFLQyxnQkFBTCxHQUNHQyxXQURILENBQ2UsY0FEZixFQUVHQSxXQUZILENBRWUsSUFBSXRCLFFBQVErQixXQUFaLENBQXdCLElBQXhCLENBRmYsRUFFOEMsT0FGOUM7QUFHQSxXQUFLQyxvQkFBTCxDQUEwQixJQUExQixFQUFnQyxJQUFoQztBQUNBLFdBQUtQLGdCQUFMLENBQXNCLElBQXRCLEVBQTRCLElBQTVCO0FBQ0EsV0FBS0YsU0FBTCxDQUFlLEVBQWY7QUFDQSxXQUFLRyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFUb0IsR0FBdkI7QUFXQTFCLFVBQVFFLFVBQVIsQ0FBbUJnRSxLQUFuQixHQUEyQixVQUFVdEMsS0FBVixFQUFpQjtBQUMxQyxRQUFJTyxhQUFhUCxNQUFNTSxhQUFOLENBQW9CLE9BQXBCLENBQWpCO0FBQ0E7QUFDQSxXQUFPLDBCQUEwQkMsVUFBMUIsR0FBdUMsS0FBOUM7QUFDRCxHQUpEOztBQU1BOztBQUVBOztBQUVBOztBQUVBbkMsVUFBUWtCLE1BQVIsQ0FBZWlELFFBQWYsR0FBMEI7QUFDeEIvQyxVQUFNLFlBQVk7QUFDaEIsV0FBS0MsZ0JBQUwsR0FDR0MsV0FESCxDQUNlLFFBRGYsRUFFR0EsV0FGSCxDQUVlLElBQUl0QixRQUFROEIsYUFBWixDQUEwQixDQUNyQyxDQUFDLEdBQUQsRUFBTSxHQUFOLENBRHFDLEVBRXJDLENBQUMsUUFBRCxFQUFXLFFBQVgsQ0FGcUMsRUFHckMsQ0FBQyxRQUFELEVBQVcsUUFBWCxDQUhxQyxDQUExQixDQUZmLEVBTU0sYUFOTixFQU9HUixXQVBILENBT2UsS0FQZixFQVFHQSxXQVJILENBUWUsSUFBSXRCLFFBQVErQixXQUFaLENBQXdCLEdBQXhCLENBUmYsRUFRNkMsT0FSN0MsRUFTR1QsV0FUSCxDQVNlLE9BVGYsRUFVR0EsV0FWSCxDQVVlLElBQUl0QixRQUFRK0IsV0FBWixDQUF3QixHQUF4QixDQVZmLEVBVTZDLE1BVjdDLEVBV0dULFdBWEgsQ0FXZSxTQVhmLEVBWUdBLFdBWkgsQ0FZZSxJQUFJdEIsUUFBUStCLFdBQVosQ0FBd0IsR0FBeEIsQ0FaZixFQVk2QyxNQVo3QztBQWFBLFdBQUtxQyxvQkFBTCxDQUEwQixLQUExQixFQUNHdEIsUUFESCxDQUNZLElBRFo7QUFFQSxXQUFLdEIsZUFBTCxDQUFxQixJQUFyQjtBQUNBLFdBQUtRLG9CQUFMLENBQTBCLElBQTFCLEVBQWdDLElBQWhDO0FBQ0EsV0FBS1AsZ0JBQUwsQ0FBc0IsSUFBdEIsRUFBNEIsSUFBNUI7QUFDQSxXQUFLRixTQUFMLENBQWUsQ0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUF2QnVCLEdBQTFCOztBQTBCQTNCLFVBQVFFLFVBQVIsQ0FBbUIsVUFBbkIsSUFBaUMsVUFBVTBCLEtBQVYsRUFBaUI7QUFDaEQsUUFBSXlDLHVCQUF1QnpDLE1BQU1NLGFBQU4sQ0FBb0IsYUFBcEIsQ0FBM0I7QUFDQSxRQUFJb0MsYUFBYTFDLE1BQU1NLGFBQU4sQ0FBb0IsT0FBcEIsQ0FBakI7QUFDQSxRQUFJcUMsWUFBWTNDLE1BQU1NLGFBQU4sQ0FBb0IsTUFBcEIsQ0FBaEI7QUFDQSxRQUFJc0MsWUFBWTVDLE1BQU1NLGFBQU4sQ0FBb0IsTUFBcEIsQ0FBaEI7QUFDQSxRQUFJdUMsaUJBQWlCekUsUUFBUUUsVUFBUixDQUFtQndFLGVBQW5CLENBQW1DOUMsS0FBbkMsRUFBMEMsS0FBMUMsQ0FBckI7QUFDQTtBQUNBO0FBQ0EsUUFBSTZDLGtCQUFrQixFQUF0QixFQUEwQjtBQUN4QixVQUFJRSxRQUFRckMsU0FBU2dDLFVBQVQsQ0FBWjtBQUNBLFVBQUlNLE1BQU10QyxTQUFTaUMsU0FBVCxDQUFWO0FBQ0EsVUFBSU0sU0FBU3ZDLFNBQVNrQyxTQUFULENBQWI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxVQUFJSyxTQUFTLENBQVQsSUFBY0YsUUFBUUMsR0FBMUIsRUFBK0I7QUFDN0IsWUFBSUUsYUFBYSxjQUFjVCxvQkFBZCxHQUFxQyxLQUFyQyxHQUE2Q00sS0FBN0MsR0FBcUQsSUFBckQsR0FBNEROLG9CQUE1RCxHQUFtRixLQUFuRixHQUEyRk8sR0FBM0YsR0FBaUcsSUFBakcsR0FBd0dQLG9CQUF4RyxHQUErSCxPQUEvSCxHQUF5SUksY0FBekksR0FBMEosS0FBM0s7QUFDQSxlQUFPSyxVQUFQO0FBQ0Q7O0FBRUQsYUFBTyxFQUFQO0FBRUQ7O0FBRUQsV0FBTyxFQUFQO0FBRUQsR0ExQkQ7QUEyQkE7O0FBRUE7QUFDQTlFLFVBQVFrQixNQUFSLENBQWU2RCxVQUFmLEdBQTRCO0FBQzFCM0QsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxPQURmLEVBRUdBLFdBRkgsQ0FFZSxJQUFJdEIsUUFBUStCLFdBQVosQ0FBd0IsS0FBeEIsQ0FGZixFQUUrQyxPQUYvQyxFQUdHVCxXQUhILENBR2UsS0FIZjtBQUlBLFdBQUs4QyxvQkFBTCxDQUEwQixPQUExQixFQUNHdEIsUUFESCxDQUNZLElBRFosRUFFR3hCLFdBRkgsQ0FFZSxJQUZmO0FBR0EsV0FBS0UsZUFBTCxDQUFxQixJQUFyQjtBQUNBLFdBQUtRLG9CQUFMLENBQTBCLElBQTFCLEVBQWdDLElBQWhDO0FBQ0EsV0FBS1AsZ0JBQUwsQ0FBc0IsSUFBdEIsRUFBNEIsSUFBNUI7QUFDQSxXQUFLRixTQUFMLENBQWUsQ0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFmeUIsR0FBNUI7O0FBa0JBM0IsVUFBUUUsVUFBUixDQUFtQixZQUFuQixJQUFtQyxVQUFVMEIsS0FBVixFQUFpQjtBQUNsRCxRQUFJMEMsYUFBYTFDLE1BQU1NLGFBQU4sQ0FBb0IsT0FBcEIsQ0FBakI7QUFDQSxRQUFJOEMsbUJBQW1CaEYsUUFBUUUsVUFBUixDQUFtQndFLGVBQW5CLENBQW1DOUMsS0FBbkMsRUFBMEMsT0FBMUMsQ0FBdkI7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsUUFBSW9ELG9CQUFvQixFQUF4QixFQUE0QjtBQUMxQixVQUFJTCxRQUFRckMsU0FBU2dDLFVBQVQsQ0FBWjs7QUFFQSxVQUFJSyxRQUFRLENBQVosRUFBZTtBQUNiLFlBQUlHLGFBQWEseUJBQXlCSCxLQUF6QixHQUFpQyxVQUFqQyxHQUE4Q0ssZ0JBQTlDLEdBQWlFLEtBQWxGO0FBQ0EsZUFBT0YsVUFBUDtBQUNEOztBQUVELGFBQU8sRUFBUDtBQUVEOztBQUVELFdBQU8sRUFBUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0QsR0F4QkQ7O0FBMEJBOztBQUVBOztBQUVBOztBQUVBOztBQUVBOUUsVUFBUWtCLE1BQVIsQ0FBZStELFlBQWYsR0FBOEI7QUFDNUI3RCxVQUFNLFlBQVk7QUFDaEIsV0FBS0MsZ0JBQUwsR0FDR0MsV0FESCxDQUNlLElBQUl0QixRQUFRK0IsV0FBWixDQUF3QixHQUF4QixDQURmLEVBQzZDLFVBRDdDO0FBRUEsV0FBS1AsZUFBTCxDQUFxQixJQUFyQjtBQUNBLFdBQUswRCxTQUFMLENBQWUsSUFBZixFQUFxQixJQUFyQjtBQUNBLFdBQUszRCxTQUFMLENBQWUsR0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFUMkIsR0FBOUI7O0FBWUEzQixVQUFRRSxVQUFSLENBQW1CLGNBQW5CLElBQXFDLFVBQVUwQixLQUFWLEVBQWlCO0FBQ3BELFFBQUl1RCxnQkFBZ0J2RCxNQUFNTSxhQUFOLENBQW9CLFVBQXBCLENBQXBCO0FBQ0E7QUFDQSxRQUFJa0QsT0FBTyxpQkFBaUJELGFBQWpCLEdBQWlDLElBQTVDO0FBQ0E7QUFDQSxXQUFPLENBQUNDLElBQUQsRUFBT3BGLFFBQVFFLFVBQVIsQ0FBbUJtRixVQUExQixDQUFQO0FBQ0QsR0FORDs7QUFRQTs7QUFFQTs7QUFFQXJGLFVBQVFrQixNQUFSLENBQWVvRSxZQUFmLEdBQThCO0FBQzVCbEUsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLGdCQUFMLEdBQ0dDLFdBREgsQ0FDZSxJQUFJdEIsUUFBUStCLFdBQVosQ0FBd0IsR0FBeEIsQ0FEZixFQUM2QyxXQUQ3QyxFQUVHVCxXQUZILENBRWUsSUFBSXRCLFFBQVE4QixhQUFaLENBQTBCLENBQ3JDLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FEcUMsRUFFckMsQ0FBQyxHQUFELEVBQU0sR0FBTixDQUZxQyxFQUdyQyxDQUFDLEdBQUQsRUFBTSxHQUFOLENBSHFDLEVBSXJDLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FKcUMsRUFLckMsQ0FBQyxHQUFELEVBQU0sR0FBTixDQUxxQyxDQUExQixDQUZmLEVBUU0scUJBUk4sRUFTR1IsV0FUSCxDQVNlLElBQUl0QixRQUFRK0IsV0FBWixDQUF3QixHQUF4QixDQVRmLEVBUzZDLFlBVDdDO0FBVUEsV0FBS21ELFNBQUwsQ0FBZSxJQUFmLEVBQXFCLElBQXJCO0FBQ0EsV0FBSzNELFNBQUwsQ0FBZSxHQUFmO0FBQ0EsV0FBS0csVUFBTCxDQUFnQixFQUFoQjtBQUNBLFdBQUtDLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDRDtBQWhCMkIsR0FBOUI7O0FBbUJBM0IsVUFBUUUsVUFBUixDQUFtQixjQUFuQixJQUFxQyxVQUFVMEIsS0FBVixFQUFpQjtBQUNwRCxRQUFJMkQsaUJBQWlCM0QsTUFBTU0sYUFBTixDQUFvQixXQUFwQixDQUFyQjtBQUNBLFFBQUlzRCwrQkFBK0I1RCxNQUFNTSxhQUFOLENBQW9CLHFCQUFwQixDQUFuQztBQUNBLFFBQUl1RCxrQkFBa0I3RCxNQUFNTSxhQUFOLENBQW9CLFlBQXBCLENBQXRCO0FBQ0E7QUFDQSxRQUFJa0QsT0FBTyxpQkFBaUJHLGNBQWpCLEdBQWtDLEdBQWxDLEdBQXdDQyw0QkFBeEMsR0FBdUUsR0FBdkUsR0FBNkVDLGVBQTdFLEdBQStGLEdBQTFHO0FBQ0E7QUFDQSxXQUFPLENBQUNMLElBQUQsRUFBT3BGLFFBQVFFLFVBQVIsQ0FBbUJtRixVQUExQixDQUFQO0FBQ0QsR0FSRDs7QUFVQTs7QUFFQTs7O0FBSUE7O0FBRUE7O0FBRUFyRixVQUFRa0IsTUFBUixDQUFld0UsY0FBZixHQUFnQztBQUM5QnRFLFVBQU0sWUFBWTtBQUNoQixXQUFLQyxnQkFBTCxHQUNHQyxXQURILENBQ2UsSUFBSXRCLFFBQVE4QixhQUFaLENBQTBCLENBQ3JDLENBQUMsTUFBRCxFQUFTLE1BQVQsQ0FEcUMsRUFFckMsQ0FBQyxTQUFELEVBQVksU0FBWixDQUZxQyxFQUdyQyxDQUFDLEdBQUQsRUFBTSxHQUFOLENBSHFDLEVBSXJDLENBQUMsR0FBRCxFQUFNLEdBQU4sQ0FKcUMsRUFLckMsQ0FBQyxRQUFELEVBQVcsUUFBWCxDQUxxQyxFQU1yQyxDQUFDLFFBQUQsRUFBVyxRQUFYLENBTnFDLEVBT3JDLENBQUMsUUFBRCxFQUFXLFFBQVgsQ0FQcUMsRUFRckMsQ0FBQyxRQUFELEVBQVcsUUFBWCxDQVJxQyxFQVNyQyxDQUFDLE9BQUQsRUFBVSxPQUFWLENBVHFDLENBQTFCLENBRGYsRUFXTSxRQVhOO0FBWUEsV0FBS04sZUFBTCxDQUFxQixJQUFyQjtBQUNBLFdBQUswRCxTQUFMLENBQWUsSUFBZixFQUFxQixJQUFyQjtBQUNBLFdBQUszRCxTQUFMLENBQWUsR0FBZjtBQUNBLFdBQUtHLFVBQUwsQ0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLENBQWdCLEVBQWhCO0FBQ0Q7QUFuQjZCLEdBQWhDOztBQXNCQTNCLFVBQVFFLFVBQVIsQ0FBbUIsZ0JBQW5CLElBQXVDLFVBQVUwQixLQUFWLEVBQWlCO0FBQ3RELFFBQUkrRCxrQkFBa0IvRCxNQUFNTSxhQUFOLENBQW9CLFFBQXBCLENBQXRCO0FBQ0E7QUFDQSxRQUFJa0QsT0FBTyxtQkFBbUJPLGVBQW5CLEdBQXFDLEdBQWhEO0FBQ0E7QUFDQSxXQUFPLENBQUNQLElBQUQsRUFBT3BGLFFBQVFFLFVBQVIsQ0FBbUJtRixVQUExQixDQUFQO0FBQ0QsR0FORDs7QUFRQTs7QUFFQTs7QUFFQXJGLFVBQVFrQixNQUFSLENBQWUwRSxlQUFmLEdBQWlDO0FBQy9CeEUsVUFBTSxZQUFZO0FBQ2hCLFdBQUt5QixnQkFBTCxDQUFzQixRQUF0QixFQUNHQyxRQURILENBQ1ksSUFEWixFQUVHeEIsV0FGSCxDQUVlLEtBRmYsRUFHR0EsV0FISCxDQUdlLElBQUl0QixRQUFROEIsYUFBWixDQUEwQixDQUNyQyxDQUFDLE1BQUQsRUFBUyxNQUFULENBRHFDLEVBRXJDLENBQUMsU0FBRCxFQUFZLFNBQVosQ0FGcUMsRUFHckMsQ0FBQyxHQUFELEVBQU0sR0FBTixDQUhxQyxFQUlyQyxDQUFDLEdBQUQsRUFBTSxHQUFOLENBSnFDLEVBS3JDLENBQUMsUUFBRCxFQUFXLFFBQVgsQ0FMcUMsRUFNckMsQ0FBQyxRQUFELEVBQVcsUUFBWCxDQU5xQyxFQU9yQyxDQUFDLFFBQUQsRUFBVyxRQUFYLENBUHFDLEVBUXJDLENBQUMsUUFBRCxFQUFXLFFBQVgsQ0FScUMsRUFTckMsQ0FBQyxPQUFELEVBQVUsT0FBVixDQVRxQyxDQUExQixDQUhmLEVBYU0sUUFiTixFQWNHUixXQWRILENBY2UsS0FkZjtBQWVBLFdBQUtFLGVBQUwsQ0FBcUIsS0FBckI7QUFDQSxXQUFLUSxvQkFBTCxDQUEwQixJQUExQixFQUFnQyxJQUFoQztBQUNBLFdBQUtQLGdCQUFMLENBQXNCLElBQXRCLEVBQTRCLElBQTVCO0FBQ0EsV0FBS0YsU0FBTCxDQUFlLEdBQWY7QUFDQSxXQUFLRyxVQUFMLENBQWdCLEVBQWhCO0FBQ0EsV0FBS0MsVUFBTCxDQUFnQixFQUFoQjtBQUNEO0FBdkI4QixHQUFqQzs7QUEwQkEzQixVQUFRRSxVQUFSLENBQW1CLGlCQUFuQixJQUF3QyxVQUFVMEIsS0FBVixFQUFpQjtBQUN2RCxRQUFJK0Qsa0JBQWtCL0QsTUFBTU0sYUFBTixDQUFvQixRQUFwQixDQUF0QjtBQUNBLFFBQUkyRCxhQUFhN0YsUUFBUUUsVUFBUixDQUFtQmlELFdBQW5CLENBQStCdkIsS0FBL0IsRUFBc0MsUUFBdEMsRUFBZ0Q1QixRQUFRRSxVQUFSLENBQW1Ca0QsWUFBbkUsQ0FBakI7QUFDQTtBQUNBLFFBQUlnQyxPQUFPLG9CQUFvQk8sZUFBcEIsR0FBc0MsR0FBdEMsR0FBNENFLFVBQTVDLEdBQXlELEdBQXBFO0FBQ0EsV0FBT1QsSUFBUDtBQUNELEdBTkQ7O0FBUUE7O0FBRUE7O0FBRUE7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7O0FBRUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUErbkNELENBOXRETSIsImZpbGUiOiJibG9ja3NfYXJ0aXN0LmpzIiwic291cmNlc0NvbnRlbnQiOlsidmFyIENvbG91cnMgPSByZXF1aXJlKCcuL2NvbG91cnMnKTtcbi8vcmVxdWlyZSgnQGNkby9sb2NhbGUnKTtcbnZhciBtc2cgPSByZXF1aXJlKCcuL2xvY2FsZScpO1xuLy8gdmFyIGN1c3RvbUxldmVsQmxvY2tzID0gcmVxdWlyZSgnLi9jdXN0b21MZXZlbEJsb2NrcycpO1xuaW1wb3J0IHtcbiAgUG9zaXRpb25cbn0gZnJvbSAnLi4vY29uc3RhbnRzJztcbmNvbnN0IFJBTkRPTV9WQUxVRSA9ICdSQU5EJztcblxuLy8gSW5zdGFsbCBleHRlbnNpb25zIHRvIGJsb2NrbHkncyBsYW5ndWFnZSBhbmQgSmF2YVNjcmlwdCBnZW5lcmF0b3IuXG5leHBvcnQgY29uc3QgaW5zdGFsbCA9IGZ1bmN0aW9uIChibG9ja2x5KSB7XG4gIHZhciBnZW5lcmF0b3IgPSBibG9ja2x5LkphdmFTY3JpcHQ7XG5cbiAgLy8gQ3JlYXRlIGEgc21hbGxlciBwYWxldHRlLlxuICBibG9ja2x5LkZpZWxkQ29sb3VyLkNPTE9VUlMgPSBbXG4gICAgLy8gUm93IDEuXG4gICAgQ29sb3Vycy5CTEFDSywgQ29sb3Vycy5HUkVZLFxuICAgIENvbG91cnMuS0hBS0ksIENvbG91cnMuV0hJVEUsXG4gICAgLy8gUm93IDIuXG4gICAgQ29sb3Vycy5SRUQsIENvbG91cnMuUElOSyxcbiAgICBDb2xvdXJzLk9SQU5HRSwgQ29sb3Vycy5ZRUxMT1csXG4gICAgLy8gUm93IDMuXG4gICAgQ29sb3Vycy5HUkVFTiwgQ29sb3Vycy5CTFVFLFxuICAgIENvbG91cnMuQVFVQU1BUklORSwgQ29sb3Vycy5QTFVNXG4gIF07XG4gIGJsb2NrbHkuRmllbGRDb2xvdXIuQ09MVU1OUyA9IDM7XG4gIC8vIH1cblxuICBibG9ja2x5LkJsb2Nrcy5zdGFydGVyID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC5gOC4o+C4tOC5iOC4oeC4leC5ieC4meC4l+C4teC5iOC4meC4teC5iFwiKVxuICAgICAgdGhpcy5zZXRDb2xvdXIoMTg0KTtcbiAgICAgIHRoaXMuc2V0SW5wdXRzSW5saW5lKHRydWUpO1xuICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgdGhpcy5zZXRUb29sdGlwKFwiXCIpO1xuICAgICAgdGhpcy5zZXRIZWxwVXJsKFwiXCIpO1xuICAgIH1cbiAgfTtcblxuICBnZW5lcmF0b3Iuc3RhcnRlciA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIC8vIHZhciB0ZXh0XzAgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdGRCcpO1xuICAgIC8vIHZhciBjb2RlID0gJ0ZEJyArIHRleHRfMCArICd8J1xuICAgIHJldHVybiBcIi8qKi9cIjtcbiAgfTtcblxuICBibG9ja2x5LkJsb2Nrcy5kcmF3X2NpcmNsZSA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGREcm9wZG93bihbXG4gICAgICAgICAgW1wi4Lin4LiH4LiB4Lil4LihXCIsIFwiZlwiXVxuICAgICAgICBdKSwgXCJnb19zdGVwXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZE51bWJlcihcIj8/P1wiKSwgXCJyYWRcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4Lie4Li04LiB4LmA4LiL4LilXCIpO1xuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDIzMCk7XG4gICAgICB0aGlzLnNldFRvb2x0aXAoXCJcIik7XG4gICAgICB0aGlzLnNldEhlbHBVcmwoXCJcIik7XG4gICAgfVxuICB9O1xuXG4gIGJsb2NrbHkuSmF2YVNjcmlwdC5kcmF3X2NpcmNsZSA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciBkcm9wZG93bl9nb19zdGVwID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgnZ29fc3RlcCcpO1xuICAgIHZhciB0ZXh0X3BpeGVsID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgncmFkJyk7XG4gICAgcmV0dXJuICd0aGlzLmRyYXdlckNpcmNsZSgnICsgdGV4dF9waXhlbCArICcpOyAnXG5cbiAgfTtcblxuICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8g4LiU4Liz4LmA4LiZ4Li04LiZ4LiB4Liy4LijIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuICAvLyBNb3ZlIEZvcndhcmQgYW5kIEJhY2t3YXJkIC8vXG4gIGJsb2NrbHkuQmxvY2tzLmdvX29wdGlvbiA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGREcm9wZG93bihbXG4gICAgICAgICAgW1wi4LmE4Lib4LiC4LmJ4Liy4LiH4Lir4LiZ4LmJ4LiyXCIsIFwiZlwiXSxcbiAgICAgICAgICBbXCLguJbguK3guKLguKvguKXguLHguIfguYTguJtcIiwgXCJiXCJdXG4gICAgICAgIF0pLCBcImdvX3N0ZXBcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKG5ldyBibG9ja2x5LkZpZWxkTnVtYmVyKFwiPz8/XCIpLCBcInBpeGVsXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC4nuC4tOC4geC5gOC4i+C4pVwiKTtcbiAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldENvbG91cigyMzApO1xuICAgICAgdGhpcy5zZXRUb29sdGlwKFwiXCIpO1xuICAgICAgdGhpcy5zZXRIZWxwVXJsKFwiXCIpO1xuICAgIH1cbiAgfTtcblxuICBibG9ja2x5LkphdmFTY3JpcHQuZ29fb3B0aW9uID0gZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgdmFyIGRyb3Bkb3duX2dvX3N0ZXAgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdnb19zdGVwJyk7XG4gICAgdmFyIHRleHRfcGl4ZWwgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdwaXhlbCcpO1xuICAgIC8vIFRPRE86IEFzc2VtYmxlIEphdmFTY3JpcHQgaW50byBjb2RlIHZhcmlhYmxlLlxuICAgIC8vIHJldHVybiBcIkROR19HT19PUFRJT05cIiArIGRyb3Bkb3duX2dvX3N0ZXAgKyBcIi1cIiArIHRleHRfcGl4ZWwgKyBcInxcIjtcbiAgICB0ZXh0X3BpeGVsID0gKGlzTmFOKHRleHRfcGl4ZWwpKSA/IDAgOiBwYXJzZUludCh0ZXh0X3BpeGVsKTtcbiAgICBpZiAoZHJvcGRvd25fZ29fc3RlcCA9PSAnZicpIHtcbiAgICAgIHRleHRfcGl4ZWwgPSAwICsgdGV4dF9waXhlbDtcbiAgICB9IGVsc2Uge1xuICAgICAgdGV4dF9waXhlbCA9IDAgLSB0ZXh0X3BpeGVsO1xuICAgIH1cbiAgICByZXR1cm4gJ3RoaXMuZHJhd0xpbmUoJyArIHRleHRfcGl4ZWwgKyAnKTsgJ1xuXG4gIH07XG4gIC8vIE1vdmUgRm9yd2FyZCBhbmQgQmFja3dhcmQgLy9cblxuICAvLyBUdXJuIExlZnQgYW5kIFJpZ2h0IC8vXG4gIGJsb2NrbHkuQmxvY2tzLnR1cm5fb3B0aW9uID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKFtcbiAgICAgICAgICBbXCLguKvguLHguJnguYTguJvguILguKfguLJcIiwgXCJyaWdodFwiXSxcbiAgICAgICAgICBbXCLguKvguLHguJnguYTguJvguIvguYnguLLguKJcIiwgXCJsZWZ0XCJdXG4gICAgICAgIF0pLCBcInR1cm5fc3RlcFwiKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGRBbmdsZShcIj8/P1wiKSwgXCJkZWdyZWVcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4Lit4LiH4Lio4LiyXCIpO1xuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDIzMCk7XG4gICAgICB0aGlzLnNldFRvb2x0aXAoXCJcIik7XG4gICAgICB0aGlzLnNldEhlbHBVcmwoXCJcIik7XG4gICAgfVxuICB9O1xuXG4gIGJsb2NrbHkuSmF2YVNjcmlwdC50dXJuX29wdGlvbiA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciBkcm9wZG93bl90dXJuX3N0ZXAgPSBibG9jay5nZXRGaWVsZFZhbHVlKCd0dXJuX3N0ZXAnKTtcbiAgICB2YXIgdGV4dF9kZWdyZWUgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdkZWdyZWUnKTtcbiAgICB0ZXh0X2RlZ3JlZSA9IChpc05hTih0ZXh0X2RlZ3JlZSkpID8gMCA6IHBhcnNlSW50KHRleHRfZGVncmVlKTtcblxuICAgIGlmIChkcm9wZG93bl90dXJuX3N0ZXAgPT09ICdmJykge1xuICAgICAgcmV0dXJuICd0aGlzLnJvdGF0ZURyYXdlclJpZ2h0KCcgKyB0ZXh0X2RlZ3JlZSArICcpOyAnXG4gICAgfSBlbHNlIHtcbiAgICAgIHJldHVybiAndGhpcy5yb3RhdGVEcmF3ZXJMZWZ0KCcgKyB0ZXh0X2RlZ3JlZSArICcpOyAnXG4gICAgfVxuICB9O1xuXG4gIC8vIFR1cm4gTGVmdCBhbmQgUmlnaHQgLy9cblxuICAvLyBKdW1wIGZvcndhcmQgYW5kIGJhY2t3YXJkIC8vXG4gIGJsb2NrbHkuQmxvY2tzLmp1bXBfb3B0aW9uID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKFtcbiAgICAgICAgICBbXCLguIHguKPguLDguYLguJTguJTguYTguJvguILguYnguLLguIfguKvguJnguYnguLJcIiwgXCJqX2ZvcndhcmRcIl0sXG4gICAgICAgICAgW1wi4LiB4Lij4Liw4LmC4LiU4LiU4LmE4Lib4LiC4LmJ4Liy4LiH4Lir4Lil4Lix4LiHXCIsIFwiYl9mb3J3YXJkXCJdXG4gICAgICAgIF0pLCBcInR1cm5fc3RlcFwiKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGROdW1iZXIoXCIwXCIpLCBcInBpeGVsXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC4nuC4tOC4geC5gOC4i+C4pVwiKTtcbiAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldENvbG91cigyMzApO1xuICAgICAgdGhpcy5zZXRUb29sdGlwKFwiXCIpO1xuICAgICAgdGhpcy5zZXRIZWxwVXJsKFwiXCIpO1xuICAgIH1cbiAgfTtcblxuICBibG9ja2x5LkphdmFTY3JpcHQuanVtcF9vcHRpb24gPSBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICB2YXIgZHJvcGRvd25fdHVybl9zdGVwID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgndHVybl9zdGVwJyk7XG4gICAgdmFyIHRleHRfcGl4ZWwgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdwaXhlbCcpO1xuICAgIHRleHRfcGl4ZWwgPSAoaXNOYU4odGV4dF9waXhlbCkpID8gMCA6IHRleHRfcGl4ZWxcbiAgICBpZiAoZHJvcGRvd25fdHVybl9zdGVwID09PSBcImpfZm9yd2FyZFwiKSB7XG4gICAgICByZXR1cm4gJ3RoaXMuZHJhd2VySnVtcGZvcndhcmQoJyArIHRleHRfcGl4ZWwgKyAnKTsgJ1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gJ3RoaXMuZHJhd2VySnVtcGJhY2t3YXJkKCcgKyB0ZXh0X3BpeGVsICsgJyk7ICdcbiAgICB9XG4gICAgLy8gcmV0dXJuIFwiRE5HX0pVTVBfT1BUSU9OXCIgKyBkcm9wZG93bl90dXJuX3N0ZXAgKyBcIi1cIiArIHRleHRfcGl4ZWwgKyBcInxcIjtcbiAgfTtcblxuICAvLyBKdW1wIGZvcndhcmQgYW5kIGJhY2t3YXJkIC8vXG5cblxuICBibG9ja2x5LkJsb2Nrcy5qdW1wX2F4aXMgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5hcHBlbmRWYWx1ZUlucHV0KFwiSnVtcFwiKVxuICAgICAgICAuc2V0Q2hlY2soXCJOdW1iZXJcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiB4Lij4Liw4LmC4LiU4LiUXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC5geC4meC4p+C4meC4reC4meC4l+C4teC5iFwiKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGRUZXh0SW5wdXQoXCIwXCIpLCBcIlgtYXhpc1wiKVxuICAgICAgICAuYXBwZW5kRmllbGQoXCLguYHguJnguKfguJXguLHguYnguIfguJfguLXguYhcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKG5ldyBibG9ja2x5LkZpZWxkVGV4dElucHV0KFwiMFwiKSwgXCJZLWF4aXNcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiC4Lit4LiH4Lie4Li34LmJ4LiZ4LiX4Li14LmI4LiX4Lix4LmJ4LiH4Lir4Lih4LiUXCIpO1xuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDIzMCk7XG4gICAgICB0aGlzLnNldFRvb2x0aXAoXCJcIik7XG4gICAgICB0aGlzLnNldEhlbHBVcmwoXCJcIik7XG4gICAgfVxuICB9O1xuXG4gIGJsb2NrbHkuSmF2YVNjcmlwdC5qdW1wX2F4aXMgPSBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICB2YXIgdGV4dF94X2F4aXMgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdYLWF4aXMnKTtcbiAgICB2YXIgdGV4dF95X2F4aXMgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdZLWF4aXMnKTtcbiAgICB2YXIgdmFsdWVfanVtcCA9IGJsb2NrbHkuSmF2YVNjcmlwdC52YWx1ZVRvQ29kZShibG9jaywgJ0p1bXAnLCBibG9ja2x5LkphdmFTY3JpcHQuT1JERVJfQVRPTUlDKTtcbiAgICByZXR1cm4gXCJETkdfSlVNUF9BWElTXCIgKyB0ZXh0X3hfYXhpcyArIFwiLVwiICsgdGV4dF95X2F4aXMgKyBcInxcIjtcbiAgfTtcblxuXG4gIGJsb2NrbHkuQmxvY2tzLnBvaW50X3RvID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC4q+C4seC4meC4q+C4meC5ieC4suC5hOC4m+C4l+C4teC5iFwiKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGRBbmdsZShcIj8/P1wiKSwgXCJkZWdyZWVcIilcbiAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldENvbG91cigyMzApO1xuICAgICAgdGhpcy5zZXRUb29sdGlwKFwiXCIpO1xuICAgICAgdGhpcy5zZXRIZWxwVXJsKFwiXCIpO1xuICAgIH1cbiAgfTtcblxuICBibG9ja2x5LkphdmFTY3JpcHQucG9pbnRfdG8gPSBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICB2YXIgZGVncmVlID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgnZGVncmVlJyk7XG4gICAgLy8gY29uc29sZS5sb2coJ2RlZ3JlZSA9ICcgKyBkZWdyZWUpXG4gICAgcmV0dXJuICd0aGlzLmRyYXdlclBvaW50VG8oJyArIGRlZ3JlZSArICcpOyAnXG4gICAgLy8gdmFyIHZhbHVlX2p1bXAgPSBibG9ja2x5LkphdmFTY3JpcHQudmFsdWVUb0NvZGUoYmxvY2ssICdKdW1wJywgYmxvY2tseS5KYXZhU2NyaXB0Lk9SREVSX0FUT01JQyk7XG4gICAgLy8gcmV0dXJuIFwiRE5HX0pVTVBfQVhJU1wiICsgdGV4dF94X2F4aXMgKyBcIi1cIiArIHRleHRfeV9heGlzICsgXCJ8XCI7XG4gIH07XG5cblxuICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8g4LiU4Liz4LmA4LiZ4Li04LiZ4LiB4Liy4LijIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyDguJ7guLnguYjguIHguLHguJkgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgLy8gQ29sb3IgLy9cblxuICBibG9ja2x5LkJsb2Nrcy5kbmdfY2xfc3RhdGljID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC4leC4seC5ieC4h+C4hOC5iOC4suC4quC4tVwiKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGRDb2xvdXIoXCIjZmYwMDAwXCIpLCBcImNvbG9yXCIpO1xuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDY1KTtcbiAgICAgIHRoaXMuc2V0VG9vbHRpcChcIlwiKTtcbiAgICAgIHRoaXMuc2V0SGVscFVybChcIlwiKTtcbiAgICAgIC8vIHRoaXMuYXBwZW5kVmFsdWVJbnB1dChcIm1haW5cIilcbiAgICAgIC8vICAgLnNldENoZWNrKG51bGwpXG4gICAgICAvLyAgIC5hcHBlbmRGaWVsZChcIuC4leC4seC5ieC4h+C4hOC5iOC4suC4quC4tVwiKVxuICAgICAgLy8gICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGRDb2xvdXIoXCIjZmYwMDAwXCIpLCBcImNvbG9yXCIpO1xuICAgICAgLy8gdGhpcy5zZXRJbnB1dHNJbmxpbmUoZmFsc2UpO1xuICAgICAgLy8gdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIC8vIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIC8vIHRoaXMuc2V0Q29sb3VyKDY1KTtcbiAgICAgIC8vIHRoaXMuc2V0VG9vbHRpcChcIlwiKTtcbiAgICAgIC8vIHRoaXMuc2V0SGVscFVybChcIlwiKTtcbiAgICB9XG4gIH07XG5cbiAgYmxvY2tseS5KYXZhU2NyaXB0LmRuZ19jbF9zdGF0aWMgPSBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICB2YXIgc2VsQ29sb3IgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdjb2xvcicpO1xuICAgIHJldHVybiAndGhpcy5kcmF3ZXJMaW5lQ29sb3IoXCInICsgc2VsQ29sb3IgKyAnXCIpOyAnXG4gICAgLy8gcmV0dXJuIFwiRE5HX0NMX1NUQVRJQ1wiICsgY29sb3VyX2NvbG9yICsgXCItfFwiO1xuICB9O1xuXG4gIC8vIENvbG9yIC8vXG5cbiAgLy8gUmFuZG9tIENvbG9yIC8vXG5cbiAgYmxvY2tseS5CbG9ja3NbJ2Rnbl9jbF9yYW5kb20nXSA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAuYXBwZW5kRmllbGQoXCLguJXguLHguYnguIfguITguYjguLLguKrguLVcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4Liq4Li44LmI4Lih4Liq4Li1XCIpO1xuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDY1KTtcbiAgICAgIHRoaXMuc2V0VG9vbHRpcChcIlwiKTtcbiAgICAgIHRoaXMuc2V0SGVscFVybChcIlwiKTtcbiAgICB9XG4gIH07XG5cbiAgYmxvY2tseS5KYXZhU2NyaXB0WydkZ25fY2xfcmFuZG9tJ10gPSBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICB2YXIgbnVtID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogTWF0aC5wb3coMiwgMjQpKTtcbiAgICB2YXIgY29sb3IgPSAnIycgKyAoJzAwMDAwJyArIG51bS50b1N0cmluZygxNikpLnN1YnN0cigtNik7XG4gICAgcmV0dXJuICd0aGlzLmRyYXdlckxpbmVDb2xvcihcIicgKyBjb2xvciArICdcIik7ICdcbiAgICAvLyByZXR1cm4gXCJETkdfQ0xfUkFORE9NXCIgKyBjb2xvciArIFwiLXxcIjtcbiAgfTtcblxuICAvLyBSYW5kb20gQ29sb3IgLy9cblxuICAvLyBMaW5lIEhlaWdodCAvL1xuXG4gIGJsb2NrbHkuQmxvY2tzLmRuZ19saW5lX2hlaWdodCA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAuYXBwZW5kRmllbGQoXCLguILguJnguLLguJTguJTguLTguJnguKrguK1cIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKG5ldyBibG9ja2x5LkZpZWxkTnVtYmVyKFwiPz8/XCIpLCBcInBpeGVsXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC4nuC4tOC4geC5gOC4i+C4pVwiKTtcbiAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldENvbG91cig2NSk7XG4gICAgICB0aGlzLnNldFRvb2x0aXAoXCJcIik7XG4gICAgICB0aGlzLnNldEhlbHBVcmwoXCJcIik7XG4gICAgfVxuICB9O1xuXG4gIGJsb2NrbHkuSmF2YVNjcmlwdC5kbmdfbGluZV9oZWlnaHQgPSBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICB2YXIgdGV4dF9waXhlbCA9IGJsb2NrLmdldEZpZWxkVmFsdWUoJ3BpeGVsJyk7XG4gICAgcmV0dXJuICd0aGlzLmRyYXdlckxpbmVIZWlnaHQoJyArIHRleHRfcGl4ZWwgKyAnKTsgJ1xuICAgIC8vIHJldHVybiBcIkROR19MTl9IRUlHSFRcIiArIHRleHRfcGl4ZWwgKyBcIi18XCI7XG4gIH07XG4gIC8vIExpbmUgSGVpZ2h0IC8vXG5cbiAgYmxvY2tseS5CbG9ja3MuYWxwaGEgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiE4Lin4Liy4Lih4LiI4Liy4LiH4LiC4Lit4LiH4Liq4Li1XCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZE51bWJlcihcIjEwXCIpLCBcImFscGhhXCIpXG4gICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUsIG51bGwpO1xuICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUsIG51bGwpO1xuICAgICAgdGhpcy5zZXRDb2xvdXIoNjUpO1xuICAgICAgdGhpcy5zZXRUb29sdGlwKCcnKTtcbiAgICB9XG4gIH07XG4gIGJsb2NrbHkuSmF2YVNjcmlwdC5hbHBoYSA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIGxldCB0ZXh0X3BpeGVsID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgnYWxwaGEnKTtcbiAgICAvLyB2YXIgYWxwaGFJbmRleCA9IENvbG91cnMuYWxwaGFDb2RlKClcbiAgICByZXR1cm4gJ3RoaXMuZHJhd2VyTGluZUFscGhhKCcgKyB0ZXh0X3BpeGVsICsgJyk7ICdcbiAgfTtcblxuICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8g4Lie4Li54LmI4LiB4Lix4LiZIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyDguKXguLnguJsgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgLy8gTG9vcCBGb3IgLy9cblxuICBibG9ja2x5LkJsb2Nrcy5sb29wX2ZvciA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAuYXBwZW5kRmllbGQoXCLguKrguLPguKvguKPguLHguJpcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKG5ldyBibG9ja2x5LkZpZWxkRHJvcGRvd24oW1xuICAgICAgICAgIFtcImlcIiwgXCJpXCJdLFxuICAgICAgICAgIFtcImxlbmd0aFwiLCBcImxlbmd0aFwiXSxcbiAgICAgICAgICBbXCJyZXBlYXRcIiwgXCJyZXBlYXRcIl1cbiAgICAgICAgXSksIFwibG9vcF9vcHRpb25cIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiI4Liy4LiBXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZE51bWJlcihcIjBcIiksIFwic3RhcnRcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiI4LiZ4LiW4Li24LiHXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZE51bWJlcihcIjBcIiksIFwic3RvcFwiKVxuICAgICAgICAuYXBwZW5kRmllbGQoXCLguJnguLHguJrguJfguLXguKXguLBcIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKG5ldyBibG9ja2x5LkZpZWxkTnVtYmVyKFwiMFwiKSwgXCJwbHVzXCIpO1xuICAgICAgdGhpcy5hcHBlbmRTdGF0ZW1lbnRJbnB1dChcImZvclwiKVxuICAgICAgICAuc2V0Q2hlY2sobnVsbCk7XG4gICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICB0aGlzLnNldENvbG91cigwKTtcbiAgICAgIHRoaXMuc2V0VG9vbHRpcChcIlwiKTtcbiAgICAgIHRoaXMuc2V0SGVscFVybChcIlwiKTtcbiAgICB9XG4gIH07XG5cbiAgYmxvY2tseS5KYXZhU2NyaXB0Wydsb29wX2ZvciddID0gZnVuY3Rpb24gKGJsb2NrKSB7XG4gICAgdmFyIGRyb3Bkb3duX2xvb3Bfb3B0aW9uID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgnbG9vcF9vcHRpb24nKTtcbiAgICB2YXIgdGV4dF9zdGFydCA9IGJsb2NrLmdldEZpZWxkVmFsdWUoJ3N0YXJ0Jyk7XG4gICAgdmFyIHRleHRfc3RvcCA9IGJsb2NrLmdldEZpZWxkVmFsdWUoJ3N0b3AnKTtcbiAgICB2YXIgdGV4dF9wbHVzID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgncGx1cycpO1xuICAgIHZhciBzdGF0ZW1lbnRzX2ZvciA9IGJsb2NrbHkuSmF2YVNjcmlwdC5zdGF0ZW1lbnRUb0NvZGUoYmxvY2ssICdmb3InKTtcbiAgICAvLyBzdGF0ZW1lbnRzX2ZvciA9IHN0YXRlbWVudHNfZm9yLnJlcGxhY2UoXCIgXCIsIFwiXCIpXG4gICAgLy8gc3RhdGVtZW50c19mb3IgPSBzdGF0ZW1lbnRzX2Zvci5zcGxpdCgnfCcpLmpvaW4oJ14nKVxuICAgIGlmIChzdGF0ZW1lbnRzX2ZvciAhPSBcIlwiKSB7XG4gICAgICBsZXQgc3RhcnQgPSBwYXJzZUludCh0ZXh0X3N0YXJ0KVxuICAgICAgbGV0IGVuZCA9IHBhcnNlSW50KHRleHRfc3RvcClcbiAgICAgIGxldCBhZGRpbmcgPSBwYXJzZUludCh0ZXh0X3BsdXMpXG4gICAgICAvLyB2YXIgc3RhdGVtZW50QXJyYXkgPSBzdGF0ZW1lbnRzX2Zvci5zcGxpdCgnXicpXG4gICAgICAvLyBzdGF0ZW1lbnRBcnJheSA9IEFuc3dlci5yZW1vdmVBKHN0YXRlbWVudEFycmF5LCBcIlwiKVxuICAgICAgLy8gY29uc29sZS5sb2coXCIgc3RhdGVtZW50QXJyYXkgXCIgKyBzdGF0ZW1lbnRBcnJheSk7XG4gICAgICBpZiAoYWRkaW5nID4gMCAmJiBzdGFydCA8IGVuZCkge1xuICAgICAgICBsZXQgY29kZVN0cmluZyA9ICdmb3IgKGxldCAnICsgZHJvcGRvd25fbG9vcF9vcHRpb24gKyAnID0gJyArIHN0YXJ0ICsgJzsgJyArIGRyb3Bkb3duX2xvb3Bfb3B0aW9uICsgJyA8ICcgKyBlbmQgKyAnOyAnICsgZHJvcGRvd25fbG9vcF9vcHRpb24gKyAnKyspIHsnICsgc3RhdGVtZW50c19mb3IgKyAnfTsgJ1xuICAgICAgICByZXR1cm4gY29kZVN0cmluZ1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gJydcblxuICAgIH1cblxuICAgIHJldHVybiAnJ1xuXG4gIH1cbiAgLy8gTG9vcCBGb3IgLy9cblxuICAvLyBMb29wIFdoaWxlIC8vXG4gIGJsb2NrbHkuQmxvY2tzLmxvb3Bfd2hpbGUgPSB7XG4gICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiX4Liz4LiL4LmJ4LizXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZE51bWJlcihcIj8/P1wiKSwgXCJzdGFydFwiKVxuICAgICAgICAuYXBwZW5kRmllbGQoXCLguKPguK3guJpcIik7XG4gICAgICB0aGlzLmFwcGVuZFN0YXRlbWVudElucHV0KFwid2hpbGVcIilcbiAgICAgICAgLnNldENoZWNrKG51bGwpXG4gICAgICAgIC5hcHBlbmRGaWVsZChcIuC4l+C4s1wiKTtcbiAgICAgIHRoaXMuc2V0SW5wdXRzSW5saW5lKHRydWUpO1xuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDApO1xuICAgICAgdGhpcy5zZXRUb29sdGlwKFwiXCIpO1xuICAgICAgdGhpcy5zZXRIZWxwVXJsKFwiXCIpO1xuICAgIH1cbiAgfTtcblxuICBibG9ja2x5LkphdmFTY3JpcHRbJ2xvb3Bfd2hpbGUnXSA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciB0ZXh0X3N0YXJ0ID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgnc3RhcnQnKTtcbiAgICB2YXIgc3RhdGVtZW50c193aGlsZSA9IGJsb2NrbHkuSmF2YVNjcmlwdC5zdGF0ZW1lbnRUb0NvZGUoYmxvY2ssICd3aGlsZScpO1xuICAgIC8vIHN0YXRlbWVudHNfd2hpbGUgPSBzdGF0ZW1lbnRzX3doaWxlLnJlcGxhY2UoXCIgXCIsIFwiXCIpXG4gICAgLy8gc3RhdGVtZW50c193aGlsZSA9IHN0YXRlbWVudHNfd2hpbGUuc3BsaXQoJ3wnKS5qb2luKCdeJylcbiAgICAvLyByZXR1cm4gXCJMT09QX1dISUxFLFwiICsgdGV4dF9zdGFydCArIFwiLFwiICsgc3RhdGVtZW50c193aGlsZSArIFwifFwiO1xuXG4gICAgaWYgKHN0YXRlbWVudHNfd2hpbGUgIT0gXCJcIikge1xuICAgICAgbGV0IHN0YXJ0ID0gcGFyc2VJbnQodGV4dF9zdGFydClcblxuICAgICAgaWYgKHN0YXJ0ID4gMCkge1xuICAgICAgICBsZXQgY29kZVN0cmluZyA9ICdmb3IgKGxldCBpID0gMDsgaSA8ICcgKyBzdGFydCArICc7IGkrKykgeycgKyBzdGF0ZW1lbnRzX3doaWxlICsgJ307ICdcbiAgICAgICAgcmV0dXJuIGNvZGVTdHJpbmdcbiAgICAgIH1cblxuICAgICAgcmV0dXJuICcnXG5cbiAgICB9XG5cbiAgICByZXR1cm4gJydcbiAgICAvLyB2YXIgc3RhdGVtZW50c19mb3IgPSBibG9ja2x5LkphdmFTY3JpcHQuc3RhdGVtZW50VG9Db2RlKGJsb2NrLCAnZm9yJyk7XG4gICAgLy8gc3RhdGVtZW50c19mb3IgPSBzdGF0ZW1lbnRzX2Zvci5yZXBsYWNlKFwiIFwiLCBcIlwiKVxuICAgIC8vIHN0YXRlbWVudHNfZm9yID0gc3RhdGVtZW50c19mb3Iuc3BsaXQoJ3wnKS5qb2luKCdeJylcbiAgICAvLyByZXR1cm4gXCJMT09QX0ZPUlwiICsgZHJvcGRvd25fbG9vcF9vcHRpb24gKyBcIixcIiArIHRleHRfc3RhcnQgKyBcIixcIiArIHRleHRfc3RvcCArIFwiLFwiICsgdGV4dF9wbHVzICsgXCIsXCIgKyBzdGF0ZW1lbnRzX2ZvciArIFwifFwiO1xuICB9O1xuXG4gIC8vIExvb3AgV2hpbGUgLy9cblxuICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8g4Lil4Li54LibIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyDguITguLPguJnguKfguJMgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgLy8gQ2FsY3VsYXRlIHZhcmlhYmxlIC8vXG5cbiAgYmxvY2tseS5CbG9ja3MuY2FsX3ZhcmlhYmxlID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZE51bWJlcihcIjBcIiksIFwidmFyaWFibGVcIik7XG4gICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgIHRoaXMuc2V0T3V0cHV0KHRydWUsIG51bGwpO1xuICAgICAgdGhpcy5zZXRDb2xvdXIoMjQwKTtcbiAgICAgIHRoaXMuc2V0VG9vbHRpcChcIlwiKTtcbiAgICAgIHRoaXMuc2V0SGVscFVybChcIlwiKTtcbiAgICB9XG4gIH07XG5cbiAgYmxvY2tseS5KYXZhU2NyaXB0WydjYWxfdmFyaWFibGUnXSA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciB0ZXh0X3ZhcmlhYmxlID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgndmFyaWFibGUnKTtcbiAgICAvLyBUT0RPOiBBc3NlbWJsZSBKYXZhU2NyaXB0IGludG8gY29kZSB2YXJpYWJsZS5cbiAgICB2YXIgY29kZSA9IFwiQ0FMX1ZBUklBQkxFXCIgKyB0ZXh0X3ZhcmlhYmxlICsgXCItfFwiO1xuICAgIC8vIFRPRE86IENoYW5nZSBPUkRFUl9OT05FIHRvIHRoZSBjb3JyZWN0IHN0cmVuZ3RoLlxuICAgIHJldHVybiBbY29kZSwgYmxvY2tseS5KYXZhU2NyaXB0Lk9SREVSX05PTkVdO1xuICB9O1xuXG4gIC8vIENhbGN1bGF0ZSB2YXJpYWJsZSAvL1xuXG4gIC8vIENhbGN1bGF0ZSBGdW5jdGlvbiAvL1xuXG4gIGJsb2NrbHkuQmxvY2tzLmNhbF9mdW5jdGlvbiA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGROdW1iZXIoXCIwXCIpLCBcImZpcnN0X3ZhclwiKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGREcm9wZG93bihbXG4gICAgICAgICAgW1wiK1wiLCBcIitcIl0sXG4gICAgICAgICAgW1wiLVwiLCBcIi1cIl0sXG4gICAgICAgICAgW1wieFwiLCBcInhcIl0sXG4gICAgICAgICAgW1wiw7dcIiwgXCLDt1wiXSxcbiAgICAgICAgICBbXCJeXCIsIFwiXlwiXVxuICAgICAgICBdKSwgXCJjYWxfZnVuY3Rpb25fb3B0aW9uXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZE51bWJlcihcIjBcIiksIFwic2Vjb25kX3ZhclwiKTtcbiAgICAgIHRoaXMuc2V0T3V0cHV0KHRydWUsIG51bGwpO1xuICAgICAgdGhpcy5zZXRDb2xvdXIoMjQwKTtcbiAgICAgIHRoaXMuc2V0VG9vbHRpcChcIlwiKTtcbiAgICAgIHRoaXMuc2V0SGVscFVybChcIlwiKTtcbiAgICB9XG4gIH07XG5cbiAgYmxvY2tseS5KYXZhU2NyaXB0WydjYWxfZnVuY3Rpb24nXSA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciB0ZXh0X2ZpcnN0X3ZhciA9IGJsb2NrLmdldEZpZWxkVmFsdWUoJ2ZpcnN0X3ZhcicpO1xuICAgIHZhciBkcm9wZG93bl9jYWxfZnVuY3Rpb25fb3B0aW9uID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgnY2FsX2Z1bmN0aW9uX29wdGlvbicpO1xuICAgIHZhciB0ZXh0X3NlY29uZF92YXIgPSBibG9jay5nZXRGaWVsZFZhbHVlKCdzZWNvbmRfdmFyJyk7XG4gICAgLy8gVE9ETzogQXNzZW1ibGUgSmF2YVNjcmlwdCBpbnRvIGNvZGUgdmFyaWFibGUuXG4gICAgdmFyIGNvZGUgPSBcIkNBTF9GVU5DVElPTlwiICsgdGV4dF9maXJzdF92YXIgKyBcIi1cIiArIGRyb3Bkb3duX2NhbF9mdW5jdGlvbl9vcHRpb24gKyBcIi1cIiArIHRleHRfc2Vjb25kX3ZhciArIFwifFwiO1xuICAgIC8vIFRPRE86IENoYW5nZSBPUkRFUl9OT05FIHRvIHRoZSBjb3JyZWN0IHN0cmVuZ3RoLlxuICAgIHJldHVybiBbY29kZSwgYmxvY2tseS5KYXZhU2NyaXB0Lk9SREVSX05PTkVdO1xuICB9O1xuXG4gIC8vIENhbGN1bGF0ZSBGdW5jdGlvbiAvL1xuXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyDguITguLPguJnguKfguJMgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cblxuXG4gIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLyDguJXguLHguKfguYHguJvguKMgLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vXG5cbiAgLy8gRmlyc3QgLy9cblxuICBibG9ja2x5LkJsb2Nrcy52YXJpYWJsZV9maXJzdCA9IHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAuYXBwZW5kRmllbGQobmV3IGJsb2NrbHkuRmllbGREcm9wZG93bihbXG4gICAgICAgICAgW1wiY2lyY1wiLCBcImNpcmNcIl0sXG4gICAgICAgICAgW1wiY291bnRlclwiLCBcImNvdW50ZXJcIl0sXG4gICAgICAgICAgW1wiaVwiLCBcImlcIl0sXG4gICAgICAgICAgW1wialwiLCBcImpcIl0sXG4gICAgICAgICAgW1wibGVuZ3RoXCIsIFwibGVuZ3RoXCJdLFxuICAgICAgICAgIFtcInBvaW50c1wiLCBcInBvaW50c1wiXSxcbiAgICAgICAgICBbXCJyYWRpdXNcIiwgXCJyYWRpdXNcIl0sXG4gICAgICAgICAgW1wicmVwZWF0XCIsIFwicmVwZWF0XCJdLFxuICAgICAgICAgIFtcInNpZGVzXCIsIFwic2lkZXNcIl1cbiAgICAgICAgXSksIFwib3B0aW9uXCIpO1xuICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICB0aGlzLnNldE91dHB1dCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDMzMCk7XG4gICAgICB0aGlzLnNldFRvb2x0aXAoXCJcIik7XG4gICAgICB0aGlzLnNldEhlbHBVcmwoXCJcIik7XG4gICAgfVxuICB9O1xuXG4gIGJsb2NrbHkuSmF2YVNjcmlwdFsndmFyaWFibGVfZmlyc3QnXSA9IGZ1bmN0aW9uIChibG9jaykge1xuICAgIHZhciBkcm9wZG93bl9vcHRpb24gPSBibG9jay5nZXRGaWVsZFZhbHVlKCdvcHRpb24nKTtcbiAgICAvLyBUT0RPOiBBc3NlbWJsZSBKYXZhU2NyaXB0IGludG8gY29kZSB2YXJpYWJsZS5cbiAgICB2YXIgY29kZSA9IFwiVkFSSUFCTEVfRklSU1RcIiArIGRyb3Bkb3duX29wdGlvbiArIFwifFwiO1xuICAgIC8vIFRPRE86IENoYW5nZSBPUkRFUl9OT05FIHRvIHRoZSBjb3JyZWN0IHN0cmVuZ3RoLlxuICAgIHJldHVybiBbY29kZSwgYmxvY2tseS5KYXZhU2NyaXB0Lk9SREVSX05PTkVdO1xuICB9O1xuXG4gIC8vIEZpcnN0IC8vXG5cbiAgLy8gU2Vjb25kIC8vXG5cbiAgYmxvY2tseS5CbG9ja3MudmFyaWFibGVfc2Vjb25kID0ge1xuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dChcInNlY29uZFwiKVxuICAgICAgICAuc2V0Q2hlY2sobnVsbClcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiK4Li44LiUXCIpXG4gICAgICAgIC5hcHBlbmRGaWVsZChuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKFtcbiAgICAgICAgICBbXCJjaXJjXCIsIFwiY2lyY1wiXSxcbiAgICAgICAgICBbXCJjb3VudGVyXCIsIFwiY291bnRlclwiXSxcbiAgICAgICAgICBbXCJpXCIsIFwiaVwiXSxcbiAgICAgICAgICBbXCJqXCIsIFwialwiXSxcbiAgICAgICAgICBbXCJsZW5ndGhcIiwgXCJsZW5ndGhcIl0sXG4gICAgICAgICAgW1wicG9pbnRzXCIsIFwicG9pbnRzXCJdLFxuICAgICAgICAgIFtcInJhZGl1c1wiLCBcInJhZGl1c1wiXSxcbiAgICAgICAgICBbXCJyZXBlYXRcIiwgXCJyZXBlYXRcIl0sXG4gICAgICAgICAgW1wic2lkZXNcIiwgXCJzaWRlc1wiXVxuICAgICAgICBdKSwgXCJvcHRpb25cIilcbiAgICAgICAgLmFwcGVuZEZpZWxkKFwi4LiW4Li24LiHXCIpO1xuICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUoZmFsc2UpO1xuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgIHRoaXMuc2V0Q29sb3VyKDMzMCk7XG4gICAgICB0aGlzLnNldFRvb2x0aXAoXCJcIik7XG4gICAgICB0aGlzLnNldEhlbHBVcmwoXCJcIik7XG4gICAgfVxuICB9O1xuXG4gIGJsb2NrbHkuSmF2YVNjcmlwdFsndmFyaWFibGVfc2Vjb25kJ10gPSBmdW5jdGlvbiAoYmxvY2spIHtcbiAgICB2YXIgZHJvcGRvd25fb3B0aW9uID0gYmxvY2suZ2V0RmllbGRWYWx1ZSgnb3B0aW9uJyk7XG4gICAgdmFyIHZhbHVlX25hbWUgPSBibG9ja2x5LkphdmFTY3JpcHQudmFsdWVUb0NvZGUoYmxvY2ssICdzZWNvbmQnLCBibG9ja2x5LkphdmFTY3JpcHQuT1JERVJfQVRPTUlDKTtcbiAgICAvLyBUT0RPOiBBc3NlbWJsZSBKYXZhU2NyaXB0IGludG8gY29kZSB2YXJpYWJsZS5cbiAgICB2YXIgY29kZSA9IFwiVkFSSUFCTEVfU0VDT05EXCIgKyBkcm9wZG93bl9vcHRpb24gKyBcIi1cIiArIHZhbHVlX25hbWUgKyBcInxcIjtcbiAgICByZXR1cm4gY29kZTtcbiAgfTtcblxuICAvLyBTZWNvbmQgLy9cblxuICAvLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8g4LiV4Lix4Lin4LmB4Lib4LijIC8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vLy8vL1xuXG4gIC8vIENhbGN1bGF0ZSBGdW5jdGlvbiAvL1xuXG4gIC8vIGZ1bmN0aW9uIGNyZWF0ZVBvaW50VG9CbG9ja3Mob25Jbml0KSB7XG4gIC8vICAgcmV0dXJuIHtcbiAgLy8gICAgIGhlbHBVcmw6ICcnLFxuICAvLyBpbml0OiBmdW5jdGlvbiAoKSB7XG4gIC8vICAgdGhpcy5zZXRDb2xvdXIoMTg0KTtcbiAgLy8gICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAvLyAgIHRoaXMuc2V0SW5wdXRzSW5saW5lKHRydWUpO1xuICAvLyAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgLy8gICB0aGlzLnNldFRvb2x0aXAoMCk7XG4gIC8vICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgLy8gICAgICAgLmFwcGVuZFRpdGxlKCd4eCcpO1xuICAvLyAgIGlmIChvbkluaXQpIHtcbiAgLy8gICAgIG9uSW5pdCh0aGlzKTtcbiAgLy8gICB9XG4gIC8vIH1cbiAgLy8gICB9O1xuICAvLyB9XG5cblxuICAvLyBibG9ja2x5LkJsb2Nrcy5wb2ludF90byA9IGNyZWF0ZVBvaW50VG9CbG9ja3MoZnVuY3Rpb24gKGJsb2NrKSB7XG4gIC8vICAgLy8gQmxvY2sgZm9yIHBvaW50aW5nIHRvIGEgc3BlY2lmaWVkIGRpcmVjdGlvblxuICAvLyBibG9jay5hcHBlbmREdW1teUlucHV0KClcbiAgLy8gICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZEFuZ2xlVGV4dElucHV0KCcwJywge1xuICAvLyAgICAgICBkaXJlY3Rpb246ICd0dXJuUmlnaHQnXG4gIC8vICAgICB9KSwgJ0RJUkVDVElPTicpXG4gIC8vICAgICAuYXBwZW5kVGl0bGUoJ3BvaW50IHRvJyk7XG4gIC8vIH0pO1xuXG4gIC8vIGJsb2NrbHkuQmxvY2tzLnBvaW50X3RvID0ge1xuICAvLyAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgLy8gICAgIHRoaXMuc2V0Q29sb3VyKDE4NCk7XG4gIC8vICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAvLyAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gIC8vICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSk7XG4gIC8vICAgICB0aGlzLnNldFRvb2x0aXAoMCk7XG4gIC8vICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAvLyAgICAgICAuYXBwZW5kRmllbGQoJ3h4Jyk7XG4gIC8vICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAvLyAgICAgICAuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGRBbmdsZVRleHRJbnB1dCgnMCcsIHtcbiAgLy8gICAgICAgICBkaXJlY3Rpb246ICd0dXJuUmlnaHQnXG4gIC8vICAgICAgIH0pLCAnRElSRUNUSU9OJyk7XG4gIC8vICAgICB0aGlzLmFwcGVuZEZpZWxkKCdwb2ludCB0bycpO1xuICAvLyAgIH1cbiAgLy8gfVxuXG5cbiAgLy8gW1snRkQnLCAnYmxvY2tfaWRfVU9eKXVyeV1wKzdpTkBeMl1TMH0nLCAxMDBdLCBbJ0ZEJywgJ2Jsb2NrX2lkX0lha0NSNmp2OGckcmgodDRNcDRJJywgMTAwXV1cblxuICAvKlxuIFxuICBcbiAgICBnZW5lcmF0b3IuZHJhd19tb3ZlX2J5X2NvbnN0YW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgLy8gR2VuZXJhdGUgSmF2YVNjcmlwdCBmb3IgbW92aW5nIGZvcndhcmQgb3IgYmFja3dhcmQgdGhlIGludGVybmFsIG51bWJlciBvZlxuICAgICAgLy8gcGl4ZWxzLlxuICAgICAgdmFyIHZhbHVlID0gd2luZG93LnBhcnNlRmxvYXQodGhpcy5nZXRUaXRsZVZhbHVlKCdWQUxVRScpKSB8fCAwO1xuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSJykgK1xuICAgICAgICAgICcoJyArIHZhbHVlICsgJywgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gICAgZ2VuZXJhdG9yLmRyYXdfbW92ZV9ieV9jb25zdGFudF9kcm9wZG93biA9IGdlbmVyYXRvci5kcmF3X21vdmVfYnlfY29uc3RhbnQ7XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybl9ieV9jb25zdGFudF9yZXN0cmljdGVkID0ge1xuICAgICAgLy8gQmxvY2sgZm9yIHR1cm5pbmcgZWl0aGVyIGxlZnQgb3IgcmlnaHQgZnJvbSBhbW9uZyBhIGZpeGVkIHNldCBvZiBhbmdsZXMuXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKFxuICAgICAgICAgICAgICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybi5ESVJFQ1RJT05TKSwgJ0RJUicpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkQW5nbGVEcm9wZG93bih7XG4gICAgICAgICAgICAgIGRpcmVjdGlvblRpdGxlTmFtZTogJ0RJUicsXG4gICAgICAgICAgICAgIG1lbnVHZW5lcmF0b3I6IHRoaXMuVkFMVUVcbiAgICAgICAgICAgIH0pLCAnVkFMVUUnKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG1zZy5kZWdyZWVzKCkpO1xuICAgICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLnR1cm5Ub29sdGlwKCkpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybl9ieV9jb25zdGFudF9yZXN0cmljdGVkLlZBTFVFID1cbiAgICAgICAgWzMwLCA0NSwgNjAsIDkwLCAxMjAsIDEzNSwgMTUwLCAxODBdLm1hcChmdW5jdGlvbiAodCkge1xuICAgICAgICAgIHJldHVybiBbU3RyaW5nKHQpLCBTdHJpbmcodCldO1xuICAgICAgICB9KTtcbiAgXG4gICAgZ2VuZXJhdG9yLmRyYXdfdHVybl9ieV9jb25zdGFudF9yZXN0cmljdGVkID0gZnVuY3Rpb24gKCkge1xuICAgICAgLy8gR2VuZXJhdGUgSmF2YVNjcmlwdCBmb3IgdHVybmluZyBlaXRoZXIgbGVmdCBvciByaWdodCBmcm9tIGFtb25nIGEgZml4ZWRcbiAgICAgIC8vIHNldCBvZiBhbmdsZXMuXG4gICAgICB2YXIgdmFsdWUgPSB3aW5kb3cucGFyc2VGbG9hdCh0aGlzLmdldFRpdGxlVmFsdWUoJ1ZBTFVFJykpO1xuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSJykgK1xuICAgICAgICAgICcoJyArIHZhbHVlICsgJywgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybl9ieV9jb25zdGFudCA9IHtcbiAgICAgIC8vIEJsb2NrIGZvciB0dXJuaW5nIGxlZnQgb3IgcmlnaHQgYW55IG51bWJlciBvZiBkZWdyZWVzLlxuICAgICAgaGVscFVybDogJycsXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkRHJvcGRvd24oXG4gICAgICAgICAgICBibG9ja2x5LkJsb2Nrcy5kcmF3X3R1cm4uRElSRUNUSU9OUyksICdESVInKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGRBbmdsZVRleHRJbnB1dCgnOTAnLCB7XG4gICAgICAgICAgICBkaXJlY3Rpb25UaXRsZTogJ0RJUidcbiAgICAgICAgICB9KSwgJ1ZBTFVFJylcbiAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmRlZ3JlZXMoKSk7XG4gICAgICAgIHRoaXMuc2V0SW5wdXRzSW5saW5lKHRydWUpO1xuICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0VG9vbHRpcChtc2cudHVyblRvb2x0aXAoKSk7XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MuZHJhd190dXJuX2J5X2NvbnN0YW50X2Ryb3Bkb3duID0ge1xuICAgICAgLy8gQmxvY2sgZm9yIHR1cm5pbmcgbGVmdCBvciByaWdodCBhbnkgbnVtYmVyIG9mIGRlZ3JlZXMuXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGREcm9wZG93bihcbiAgICAgICAgICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybi5ESVJFQ1RJT05TKSwgJ0RJUicpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZEFuZ2xlRHJvcGRvd24oe1xuICAgICAgICAgICAgZGlyZWN0aW9uVGl0bGVOYW1lOiAnRElSJ1xuICAgICAgICAgIH0pLCAnVkFMVUUnKVxuICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuZGVncmVlcygpKTtcbiAgICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKG1zZy50dXJuVG9vbHRpcCgpKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IuZHJhd190dXJuX2J5X2NvbnN0YW50ID0gZnVuY3Rpb24gKCkge1xuICAgICAgLy8gR2VuZXJhdGUgSmF2YVNjcmlwdCBmb3IgdHVybmluZyBsZWZ0IG9yIHJpZ2h0LlxuICAgICAgdmFyIHZhbHVlID0gd2luZG93LnBhcnNlRmxvYXQodGhpcy5nZXRUaXRsZVZhbHVlKCdWQUxVRScpKSB8fCAwO1xuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSJykgK1xuICAgICAgICAgICcoJyArIHZhbHVlICsgJywgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gICAgZ2VuZXJhdG9yLmRyYXdfdHVybl9ieV9jb25zdGFudF9kcm9wZG93biA9IGdlbmVyYXRvci5kcmF3X3R1cm5fYnlfY29uc3RhbnQ7XG4gIFxuICAgIGdlbmVyYXRvci5kcmF3X21vdmVfaW5saW5lID0gZnVuY3Rpb24gKCkge1xuICAgICAgLy8gR2VuZXJhdGUgSmF2YVNjcmlwdCBmb3IgbW92aW5nIGZvcndhcmQgb3IgYmFja3dhcmQgdGhlIGludGVybmFsIG51bWJlciBvZlxuICAgICAgLy8gcGl4ZWxzLlxuICAgICAgdmFyIHZhbHVlID0gd2luZG93LnBhcnNlRmxvYXQodGhpcy5nZXRUaXRsZVZhbHVlKCdWQUxVRScpKTtcbiAgICAgIHJldHVybiAnVHVydGxlLicgKyB0aGlzLmdldFRpdGxlVmFsdWUoJ0RJUicpICtcbiAgICAgICAgICAnKCcgKyB2YWx1ZSArICcsIFxcJ2Jsb2NrX2lkXycgKyB0aGlzLmlkICsgJ1xcJyk7XFxuJztcbiAgICB9O1xuICBcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MuZHJhd190dXJuX2lubGluZV9yZXN0cmljdGVkID0ge1xuICAgICAgLy8gQmxvY2sgZm9yIHR1cm5pbmcgZWl0aGVyIGxlZnQgb3IgcmlnaHQgZnJvbSBhbW9uZyBhIGZpeGVkIHNldCBvZiBhbmdsZXMuXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKFxuICAgICAgICAgICAgICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybi5ESVJFQ1RJT05TKSwgJ0RJUicpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkQW5nbGVEcm9wZG93bih7XG4gICAgICAgICAgICAgIGRpcmVjdGlvblRpdGxlTmFtZTogJ0RJUicsXG4gICAgICAgICAgICAgIG1lbnVHZW5lcmF0b3I6IHRoaXMuVkFMVUVcbiAgICAgICAgICAgIH0pLCAnVkFMVUUnKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG1zZy5kZWdyZWVzKCkpO1xuICAgICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLnR1cm5Ub29sdGlwKCkpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybl9pbmxpbmVfcmVzdHJpY3RlZC5WQUxVRSA9XG4gICAgICAgIFszMCwgNDUsIDYwLCA5MCwgMTIwLCAxMzUsIDE1MCwgMTgwXS5tYXAoZnVuY3Rpb24gKHQpIHtcbiAgICAgICAgICByZXR1cm4gW1N0cmluZyh0KSwgU3RyaW5nKHQpXTtcbiAgICAgICAgfSk7XG4gIFxuICAgIGdlbmVyYXRvci5kcmF3X3R1cm5faW5saW5lX3Jlc3RyaWN0ZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBHZW5lcmF0ZSBKYXZhU2NyaXB0IGZvciB0dXJuaW5nIGVpdGhlciBsZWZ0IG9yIHJpZ2h0IGZyb20gYW1vbmcgYSBmaXhlZFxuICAgICAgLy8gc2V0IG9mIGFuZ2xlcy5cbiAgICAgIHZhciB2YWx1ZSA9IHdpbmRvdy5wYXJzZUZsb2F0KHRoaXMuZ2V0VGl0bGVWYWx1ZSgnVkFMVUUnKSk7XG4gICAgICByZXR1cm4gJ1R1cnRsZS4nICsgdGhpcy5nZXRUaXRsZVZhbHVlKCdESVInKSArXG4gICAgICAgICAgJygnICsgdmFsdWUgKyAnLCBcXCdibG9ja19pZF8nICsgdGhpcy5pZCArICdcXCcpO1xcbic7XG4gICAgfTtcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MuZHJhd190dXJuX2lubGluZSA9IHtcbiAgICAgIC8vIEJsb2NrIGZvciB0dXJuaW5nIGxlZnQgb3IgcmlnaHQgYW55IG51bWJlciBvZiBkZWdyZWVzLlxuICAgICAgaGVscFVybDogJycsXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGREcm9wZG93bihcbiAgICAgICAgICAgICAgICBibG9ja2x5LkJsb2Nrcy5kcmF3X3R1cm4uRElSRUNUSU9OUyksICdESVInKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZEFuZ2xlVGV4dElucHV0KCc5MCcsIHtcbiAgICAgICAgICAgICAgZGlyZWN0aW9uVGl0bGU6ICdESVInXG4gICAgICAgICAgICB9KSwgJ1ZBTFVFJylcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuZGVncmVlcygpKTtcbiAgICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKG1zZy50dXJuVG9vbHRpcCgpKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBmdW5jdGlvbiBjcmVhdGVQb2ludFRvQmxvY2tzKG9uSW5pdCkge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgaGVscFVybDogJycsXG4gICAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICB0aGlzLnNldEhTVigxODQsIDEuMDAsIDAuNzQpO1xuICAgICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICAgIHRoaXMuc2V0VG9vbHRpcChtc2cucG9pbnRUbygpKTtcbiAgICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLnBvaW50VG8oKSk7XG4gICAgICAgICAgaWYgKG9uSW5pdCkge1xuICAgICAgICAgICAgb25Jbml0KHRoaXMpO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfTtcbiAgICB9XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnBvaW50X3RvID0gY3JlYXRlUG9pbnRUb0Jsb2NrcyhmdW5jdGlvbiAoYmxvY2spIHtcbiAgICAgIC8vIEJsb2NrIGZvciBwb2ludGluZyB0byBhIHNwZWNpZmllZCBkaXJlY3Rpb25cbiAgICAgIGJsb2NrLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZEFuZ2xlVGV4dElucHV0KCcwJywge1xuICAgICAgICAgICAgZGlyZWN0aW9uOiAndHVyblJpZ2h0J1xuICAgICAgICAgIH0pLCAnRElSRUNUSU9OJylcbiAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmRlZ3JlZXMoKSk7XG4gICAgfSk7XG4gIFxuICAgIGdlbmVyYXRvci5wb2ludF90byA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGxldCB2YWx1ZSA9IHdpbmRvdy5wYXJzZUZsb2F0KHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSRUNUSU9OJykpIHx8IDA7XG4gICAgICByZXR1cm4gYFR1cnRsZS5wb2ludFRvKCR7dmFsdWV9LCAnYmxvY2tfaWRfJHt0aGlzLmlkfScpO1xcbmA7XG4gICAgfTtcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MucG9pbnRfdG9fcGFyYW0gPSBjcmVhdGVQb2ludFRvQmxvY2tzKGZ1bmN0aW9uIChibG9jaykge1xuICAgIC8vIEJsb2NrIGZvciBwb2ludGluZyB0byBhIHNwZWNpZmllZCBkaXJlY3Rpb25cbiAgICAgIGJsb2NrLmFwcGVuZFZhbHVlSW5wdXQoJ1ZBTFVFJylcbiAgICAgICAgICAuc2V0Q2hlY2soYmxvY2tseS5CbG9ja1ZhbHVlVHlwZS5OVU1CRVIpXG4gICAgICAgICAgLmFkZEZpZWxkSGVscGVyKGJsb2NrbHkuQmxvY2tGaWVsZEhlbHBlci5BTkdMRV9IRUxQRVIsIHtcbiAgICAgICAgICAgIGJsb2NrLFxuICAgICAgICAgICAgZGlyZWN0aW9uOiAndHVyblJpZ2h0JyxcbiAgICAgICAgICB9KTtcbiAgICAgIGJsb2NrLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuZGVncmVlcygpKTtcbiAgICB9KTtcbiAgXG4gICAgZ2VuZXJhdG9yLnBvaW50X3RvX3BhcmFtID0gZnVuY3Rpb24gKCkge1xuICAgICAgbGV0IHZhbHVlID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKHRoaXMsICdWQUxVRScsXG4gICAgICAgICAgYmxvY2tseS5KYXZhU2NyaXB0Lk9SREVSX05PTkUpO1xuICAgICAgcmV0dXJuIGBUdXJ0bGUucG9pbnRUbygke3ZhbHVlfSwgJ2Jsb2NrX2lkXyR7dGhpcy5pZH0nKTtcXG5gO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnBvaW50X3RvX2J5X2NvbnN0YW50X3Jlc3RyaWN0ZWQgPVxuICAgICAgICBjcmVhdGVQb2ludFRvQmxvY2tzKGZ1bmN0aW9uIChibG9jaykge1xuICAgICAgYmxvY2suYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkQW5nbGVEcm9wZG93bih7XG4gICAgICAgICAgICBkaXJlY3Rpb246ICd0dXJuUmlnaHQnLFxuICAgICAgICAgICAgbWVudUdlbmVyYXRvcjogYmxvY2suVkFMVUVcbiAgICAgICAgICB9KSwgJ1ZBTFVFJylcbiAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmRlZ3JlZXMoKSk7XG4gICAgfSk7XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnBvaW50X3RvX2J5X2NvbnN0YW50X3Jlc3RyaWN0ZWQuVkFMVUUgPVxuICAgICAgICBbMCwgMzAsIDQ1LCA2MCwgOTAsIDEyMCwgMTM1LCAxNTAsIDE4MF0ubWFwKGZ1bmN0aW9uICh0KSB7XG4gICAgICAgICAgcmV0dXJuIFtTdHJpbmcodCksIFN0cmluZyh0KV07XG4gICAgICAgIH0pO1xuICBcbiAgICBnZW5lcmF0b3IucG9pbnRfdG9fYnlfY29uc3RhbnRfcmVzdHJpY3RlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGxldCB2YWx1ZSA9IHdpbmRvdy5wYXJzZUZsb2F0KHRoaXMuZ2V0VGl0bGVWYWx1ZSgnVkFMVUUnKSk7XG4gICAgICByZXR1cm4gYFR1cnRsZS5wb2ludFRvKCR7dmFsdWV9LCAnYmxvY2tfaWRfJHt0aGlzLmlkfScpO1xcbmA7XG4gICAgfTtcbiAgXG4gICAgZ2VuZXJhdG9yLmRyYXdfdHVybl9pbmxpbmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBHZW5lcmF0ZSBKYXZhU2NyaXB0IGZvciB0dXJuaW5nIGxlZnQgb3IgcmlnaHQuXG4gICAgICB2YXIgdmFsdWUgPSB3aW5kb3cucGFyc2VGbG9hdCh0aGlzLmdldFRpdGxlVmFsdWUoJ1ZBTFVFJykpO1xuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSJykgK1xuICAgICAgICAgICcoJyArIHZhbHVlICsgJywgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnZhcmlhYmxlc19nZXRfY291bnRlciA9IHtcbiAgICAgIC8vIFZhcmlhYmxlIGdldHRlci5cbiAgICAgIGNhdGVnb3J5OiBudWxsLCAgLy8gVmFyaWFibGVzIGFyZSBoYW5kbGVkIHNwZWNpYWxseS5cbiAgICAgIGhlbHBVcmw6IGJsb2NrbHkuTXNnLlZBUklBQkxFU19HRVRfSEVMUFVSTCxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMzEyLCAwLjMyLCAwLjYyKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShibG9ja2x5Lk1zZy5WQVJJQUJMRVNfR0VUX1RJVExFKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkTGFiZWwobXNnLmxvb3BWYXJpYWJsZSgpKSwgJ1ZBUicpO1xuICAgICAgICB0aGlzLnNldE91dHB1dCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKGJsb2NrbHkuTXNnLlZBUklBQkxFU19HRVRfVE9PTFRJUCk7XG4gICAgICB9LFxuICAgICAgZ2V0VmFyczogYmxvY2tseS5WYXJpYWJsZXMuZ2V0VmFycyxcbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IudmFyaWFibGVzX2dldF9jb3VudGVyID0gZ2VuZXJhdG9yLnZhcmlhYmxlc19nZXQ7XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnZhcmlhYmxlc19nZXRfbGVuZ3RoID0ge1xuICAgICAgLy8gVmFyaWFibGUgZ2V0dGVyLlxuICAgICAgY2F0ZWdvcnk6IG51bGwsICAvLyBWYXJpYWJsZXMgYXJlIGhhbmRsZWQgc3BlY2lhbGx5LlxuICAgICAgaGVscFVybDogYmxvY2tseS5Nc2cuVkFSSUFCTEVTX0dFVF9IRUxQVVJMLFxuICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldEhTVigzMTIsIDAuMzIsIDAuNjIpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKGJsb2NrbHkuTXNnLlZBUklBQkxFU19HRVRfVElUTEUpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGRMYWJlbChtc2cubGVuZ3RoUGFyYW1ldGVyKCkpLCAnVkFSJyk7XG4gICAgICAgIHRoaXMuc2V0T3V0cHV0KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAoYmxvY2tseS5Nc2cuVkFSSUFCTEVTX0dFVF9UT09MVElQKTtcbiAgICAgIH0sXG4gICAgICBnZXRWYXJzOiBibG9ja2x5LlZhcmlhYmxlcy5nZXRWYXJzLFxuICAgIH07XG4gIFxuICAgIGdlbmVyYXRvci52YXJpYWJsZXNfZ2V0X2xlbmd0aCA9IGdlbmVyYXRvci52YXJpYWJsZXNfZ2V0O1xuICBcbiAgICBibG9ja2x5LkJsb2Nrcy52YXJpYWJsZXNfZ2V0X3NpZGVzID0ge1xuICAgICAgLy8gVmFyaWFibGUgZ2V0dGVyLlxuICAgICAgY2F0ZWdvcnk6IG51bGwsICAvLyBWYXJpYWJsZXMgYXJlIGhhbmRsZWQgc3BlY2lhbGx5LlxuICAgICAgaGVscFVybDogYmxvY2tseS5Nc2cuVkFSSUFCTEVTX0dFVF9IRUxQVVJMLFxuICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldEhTVigzMTIsIDAuMzIsIDAuNjIpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKGJsb2NrbHkuTXNnLlZBUklBQkxFU19HRVRfVElUTEUpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGRMYWJlbCgnc2lkZXMnKSwgJ1ZBUicpO1xuICAgICAgICB0aGlzLnNldE91dHB1dCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKGJsb2NrbHkuTXNnLlZBUklBQkxFU19HRVRfVE9PTFRJUCk7XG4gICAgICB9LFxuICAgICAgZ2V0VmFyczogYmxvY2tseS5WYXJpYWJsZXMuZ2V0VmFycyxcbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IudmFyaWFibGVzX2dldF9zaWRlcyA9IGdlbmVyYXRvci52YXJpYWJsZXNfZ2V0O1xuICBcbiAgICAvLyBDcmVhdGUgYSBmYWtlIFwiZHJhdyBhIHNxdWFyZVwiIGZ1bmN0aW9uIHNvIGl0IGNhbiBiZSBtYWRlIGF2YWlsYWJsZSB0byB1c2Vyc1xuICAgIC8vIHdpdGhvdXQgYmVpbmcgc2hvd24gaW4gdGhlIHdvcmtzcGFjZS5cbiAgICBibG9ja2x5LkJsb2Nrcy5kcmF3X2Ffc3F1YXJlID0ge1xuICAgICAgLy8gRHJhdyBhIHNxdWFyZS5cbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoOTQsIDAuODQsIDAuNjApO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG1zZy5kcmF3QVNxdWFyZSgpKTtcbiAgICAgICAgdGhpcy5hcHBlbmRWYWx1ZUlucHV0KCdWQUxVRScpXG4gICAgICAgICAgICAuc2V0QWxpZ24oYmxvY2tseS5BTElHTl9SSUdIVClcbiAgICAgICAgICAgIC5zZXRDaGVjayhibG9ja2x5LkJsb2NrVmFsdWVUeXBlLk5VTUJFUilcbiAgICAgICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmxlbmd0aFBhcmFtZXRlcigpICsgJzonKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKCcnKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IuZHJhd19hX3NxdWFyZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEdlbmVyYXRlIEphdmFTY3JpcHQgZm9yIGRyYXdpbmcgYSBzcXVhcmUuXG4gICAgICB2YXIgdmFsdWVfbGVuZ3RoID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKFxuICAgICAgICAgIHRoaXMsICdWQUxVRScsIGdlbmVyYXRvci5PUkRFUl9BVE9NSUMpIHx8IDA7XG4gICAgICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcbiAgICAgIHJldHVybiBbXG4gICAgICAgICAgLy8gVGhlIGdlbmVyYXRlZCBjb21tZW50IGhlbHBzIGRldGVjdCByZXF1aXJlZCBibG9ja3MuXG4gICAgICAgICAgLy8gRG9uJ3QgY2hhbmdlIGl0IHdpdGhvdXQgY2hhbmdpbmcgcmVxdWlyZWRCbG9ja3NfLlxuICAgICAgICAgICcvLyBkcmF3X2Ffc3F1YXJlJyxcbiAgICAgICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIgKyAnID0gMDsgJyArIGxvb3BWYXIgKyAnIDwgNDsgJyArXG4gICAgICAgICAgICAgICAgbG9vcFZhciArICcrKykgeycsXG4gICAgICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgICAgICcgIFR1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAgICAgJ31cXG4nXS5qb2luKCdcXG4nKTtcbiAgICB9O1xuICBcbiAgICAvLyBDcmVhdGUgYSBmYWtlIFwiZHJhdyBhIHNub3dtYW5cIiBmdW5jdGlvbiBzbyBpdCBjYW4gYmUgbWFkZSBhdmFpbGFibGUgdG9cbiAgICAvLyB1c2VycyB3aXRob3V0IGJlaW5nIHNob3duIGluIHRoZSB3b3Jrc3BhY2UuXG4gICAgYmxvY2tseS5CbG9ja3MuZHJhd19hX3Nub3dtYW4gPSB7XG4gICAgICAvLyBEcmF3IGEgY2lyY2xlIGluIGZyb250IG9mIHRoZSB0dXJ0bGUsIGVuZGluZyB1cCBvbiB0aGUgb3Bwb3NpdGUgc2lkZS5cbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoOTQsIDAuODQsIDAuNjApO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG1zZy5kcmF3QVNub3dtYW4oKSk7XG4gICAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dCgnVkFMVUUnKVxuICAgICAgICAgICAgLnNldEFsaWduKGJsb2NrbHkuQUxJR05fUklHSFQpXG4gICAgICAgICAgICAuc2V0Q2hlY2soYmxvY2tseS5CbG9ja1ZhbHVlVHlwZS5OVU1CRVIpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmxlbmd0aFBhcmFtZXRlcigpICsgJzonKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAoJycpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGdlbmVyYXRvci5kcmF3X2Ffc25vd21hbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEdlbmVyYXRlIEphdmFTY3JpcHQgZm9yIGRyYXdpbmcgYSBzbm93bWFuIGluIGZyb250IG9mIHRoZSB0dXJ0bGUuXG4gICAgICB2YXIgdmFsdWUgPSBnZW5lcmF0b3IudmFsdWVUb0NvZGUoXG4gICAgICAgICAgdGhpcywgJ1ZBTFVFJywgZ2VuZXJhdG9yLk9SREVSX0FUT01JQyk7XG4gICAgICB2YXIgZGlzdGFuY2VzVmFyID0gZ2Vuc3ltKCdkaXN0YW5jZXMnKTtcbiAgICAgIHZhciBsb29wVmFyID0gZ2Vuc3ltKCdjb3VudGVyJyk7XG4gICAgICB2YXIgZGVncmVlVmFyID0gZ2Vuc3ltKCdkZWdyZWUnKTtcbiAgICAgIHZhciBkaXN0YW5jZVZhciA9IGdlbnN5bSgnZGlzdGFuY2UnKTtcbiAgICAgIHJldHVybiBbXG4gICAgICAgIC8vIFRoZSBnZW5lcmF0ZWQgY29tbWVudCBoZWxwcyBkZXRlY3QgcmVxdWlyZWQgYmxvY2tzLlxuICAgICAgICAvLyBEb24ndCBjaGFuZ2UgaXQgd2l0aG91dCBjaGFuZ2luZyByZXF1aXJlZEJsb2Nrc18uXG4gICAgICAgICcvLyBkcmF3X2Ffc25vd21hbicsXG4gICAgICAgICdUdXJ0bGUudHVybkxlZnQoOTApOycsXG4gICAgICAgICd2YXIgJyArIGRpc3RhbmNlc1ZhciArICcgPSBbJyArIHZhbHVlICsgJyAqIDAuNSwgJyArIHZhbHVlICsgJyAqIDAuMywnICtcbiAgICAgICAgICAgIHZhbHVlICsgJyAqIDAuMl07JyxcbiAgICAgICAgJ2ZvciAodmFyICcgKyBsb29wVmFyICsgJyA9IDA7ICcgKyBsb29wVmFyICsgJyA8IDY7ICcgK1xuICAgICAgICAgICAgbG9vcFZhciArICcrKykge1xcbicsXG4gICAgICAgICcgIHZhciAnICsgZGlzdGFuY2VWYXIgKyAnID0gJyArIGRpc3RhbmNlc1ZhciArICdbJyArIGxvb3BWYXIgK1xuICAgICAgICAgICAgJyA8IDMgPyAnICsgbG9vcFZhciArICc6IDUgLSAnICsgbG9vcFZhciArICddIC8gNTcuNTsnLFxuICAgICAgICAnICBmb3IgKHZhciAnICsgZGVncmVlVmFyICsgJyA9IDA7ICcgKyBkZWdyZWVWYXIgKyAnIDwgOTA7ICcgK1xuICAgICAgICAgICAgZGVncmVlVmFyICsgJysrKSB7JyxcbiAgICAgICAgJyAgICBUdXJ0bGUubW92ZUZvcndhcmQoJyArIGRpc3RhbmNlVmFyICsgJyk7JyxcbiAgICAgICAgJyAgICBUdXJ0bGUudHVyblJpZ2h0KDIpOycsXG4gICAgICAgICcgIH0nLFxuICAgICAgICAnICBpZiAoJyArIGxvb3BWYXIgKyAnICE9PSAyKSB7JyxcbiAgICAgICAgJyAgICBUdXJ0bGUudHVybkxlZnQoMTgwKTsnLFxuICAgICAgICAnICB9JyxcbiAgICAgICAgJ30nLFxuICAgICAgICAnVHVydGxlLnR1cm5MZWZ0KDkwKTtcXG4nXS5qb2luKCdcXG4nKTtcbiAgICB9O1xuICBcbiAgICAvLyBUaGlzIGlzIGEgbW9kaWZpZWQgY29weSBvZiBibG9ja2x5LkJsb2Nrcy5jb250cm9sc19mb3Igd2l0aCB0aGVcbiAgICAvLyB2YXJpYWJsZSBuYW1lZCBcImNvdW50ZXJcIiBoYXJkY29kZWQuXG4gICAgYmxvY2tseS5CbG9ja3MuY29udHJvbHNfZm9yX2NvdW50ZXIgPSB7XG4gICAgICAvLyBGb3IgbG9vcCB3aXRoIGhhcmRjb2RlZCBsb29wIHZhcmlhYmxlLlxuICAgICAgaGVscFVybDogYmxvY2tseS5Nc2cuQ09OVFJPTFNfRk9SX0hFTFBVUkwsXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0SFNWKDMyMiwgMC45MCwgMC45NSk7XG4gICAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUoYmxvY2tseS5Nc2cuQ09OVFJPTFNfRk9SX0lOUFVUX1dJVEgpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGRMYWJlbChtc2cubG9vcFZhcmlhYmxlKCkpLFxuICAgICAgICAgICAgICAgICAgICAgICAgICdWQVInKTtcbiAgICAgICAgdGhpcy5pbnRlcnBvbGF0ZU1zZyhibG9ja2x5Lk1zZy5DT05UUk9MU19GT1JfSU5QVVRfRlJPTV9UT19CWSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgWydGUk9NJywgJ051bWJlcicsIGJsb2NrbHkuQUxJR05fUklHSFRdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBbJ1RPJywgJ051bWJlcicsIGJsb2NrbHkuQUxJR05fUklHSFRdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBbJ0JZJywgJ051bWJlcicsIGJsb2NrbHkuQUxJR05fUklHSFRdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICBibG9ja2x5LkFMSUdOX1JJR0hUKTtcbiAgICAgICAgdGhpcy5hcHBlbmRTdGF0ZW1lbnRJbnB1dCgnRE8nKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKGJsb2NrbHkuTXNnLkNPTlRST0xTX0ZPUl9JTlBVVF9ETyk7XG4gICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0VG9vbHRpcChibG9ja2x5Lk1zZy5DT05UUk9MU19GT1JfVE9PTFRJUC5yZXBsYWNlKFxuICAgICAgICAgICAgJyUxJywgdGhpcy5nZXRUaXRsZVZhbHVlKCdWQVInKSkpO1xuICAgICAgfSxcbiAgICAgIGdldFZhcnM6IGJsb2NrbHkuVmFyaWFibGVzLmdldFZhcnMsXG4gICAgICAvLyBzZXJpYWxpemUgdGhlIGNvdW50ZXIgdmFyaWFibGUgbmFtZSB0byB4bWwgc28gdGhhdCBpdCBjYW4gYmUgdXNlZCBhY3Jvc3NcbiAgICAgIC8vIGRpZmZlcmVudCBsb2NhbGVzXG4gICAgICBtdXRhdGlvblRvRG9tOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBjb250YWluZXIgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdtdXRhdGlvbicpO1xuICAgICAgICB2YXIgY291bnRlciA9IHRoaXMuZ2V0VGl0bGVWYWx1ZSgnVkFSJyk7XG4gICAgICAgIGNvbnRhaW5lci5zZXRBdHRyaWJ1dGUoJ2NvdW50ZXInLCBjb3VudGVyKTtcbiAgICAgICAgcmV0dXJuIGNvbnRhaW5lcjtcbiAgICAgIH0sXG4gICAgICAvLyBkZXNlcmlhbGl6ZSB0aGUgY291bnRlciB2YXJpYWJsZSBuYW1lXG4gICAgICBkb21Ub011dGF0aW9uOiBmdW5jdGlvbiAoeG1sRWxlbWVudCkge1xuICAgICAgICB2YXIgY291bnRlciA9IHhtbEVsZW1lbnQuZ2V0QXR0cmlidXRlKCdjb3VudGVyJyk7XG4gICAgICAgIHRoaXMuc2V0VGl0bGVWYWx1ZShjb3VudGVyLCAnVkFSJyk7XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgZ2VuZXJhdG9yLmNvbnRyb2xzX2Zvcl9jb3VudGVyID0gZ2VuZXJhdG9yLmNvbnRyb2xzX2ZvcjtcbiAgXG4gICAgLy8gRGVsZXRlIHRoZXNlIHN0YW5kYXJkIGJsb2Nrcy5cbiAgICBkZWxldGUgYmxvY2tseS5CbG9ja3MucHJvY2VkdXJlc19kZWZyZXR1cm47XG4gICAgZGVsZXRlIGJsb2NrbHkuQmxvY2tzLnByb2NlZHVyZXNfaWZyZXR1cm47XG4gIFxuICAgIC8vIEdlbmVyYWwgYmxvY2tzLlxuICBcbiAgICBibG9ja2x5LkJsb2Nrcy5kcmF3X21vdmUgPSB7XG4gICAgICAvLyBCbG9jayBmb3IgbW92aW5nIGZvcndhcmQgb3IgYmFja3dhcmRzLlxuICAgICAgaGVscFVybDogJycsXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dCgnVkFMVUUnKVxuICAgICAgICAgICAgLnNldENoZWNrKGJsb2NrbHkuQmxvY2tWYWx1ZVR5cGUuTlVNQkVSKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkRHJvcGRvd24oXG4gICAgICAgICAgICAgICAgYmxvY2tseS5CbG9ja3MuZHJhd19tb3ZlLkRJUkVDVElPTlMpLCAnRElSJyk7XG4gICAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmRvdHMoKSk7XG4gICAgICAgIHRoaXMuc2V0SW5wdXRzSW5saW5lKHRydWUpO1xuICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0VG9vbHRpcChtc2cubW92ZVRvb2x0aXAoKSk7XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MuZHJhd19tb3ZlLkRJUkVDVElPTlMgPVxuICAgICAgICBbW21zZy5tb3ZlRm9yd2FyZCgpLCAnbW92ZUZvcndhcmQnXSxcbiAgICAgICAgIFttc2cubW92ZUJhY2t3YXJkKCksICdtb3ZlQmFja3dhcmQnXV07XG4gIFxuICAgIGdlbmVyYXRvci5kcmF3X21vdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBHZW5lcmF0ZSBKYXZhU2NyaXB0IGZvciBtb3ZpbmcgZm9yd2FyZCBvciBiYWNrd2FyZHMuXG4gICAgICB2YXIgdmFsdWUgPSBnZW5lcmF0b3IudmFsdWVUb0NvZGUodGhpcywgJ1ZBTFVFJyxcbiAgICAgICAgICBnZW5lcmF0b3IuT1JERVJfTk9ORSkgfHwgJzAnO1xuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSJykgK1xuICAgICAgICAgICcoJyArIHZhbHVlICsgJywgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmp1bXAgPSB7XG4gICAgICAvLyBCbG9jayBmb3IgbW92aW5nIGZvcndhcmQgb3IgYmFja3dhcmRzLlxuICAgICAgaGVscFVybDogJycsXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dCgnVkFMVUUnKVxuICAgICAgICAgICAgLnNldENoZWNrKGJsb2NrbHkuQmxvY2tWYWx1ZVR5cGUuTlVNQkVSKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkRHJvcGRvd24oXG4gICAgICAgICAgICAgICAgYmxvY2tseS5CbG9ja3MuanVtcC5ESVJFQ1RJT05TKSwgJ0RJUicpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG1zZy5kb3RzKCkpO1xuICAgICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLmp1bXBUb29sdGlwKCkpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIHZhciBsb25nTW92ZUxlbmd0aERyb3Bkb3duVmFsdWUgPSBcIkxPTkdfTU9WRV9MRU5HVEhcIjtcbiAgICB2YXIgc2hvcnRNb3ZlTGVuZ3RoRHJvcGRvd25WYWx1ZSA9IFwiU0hPUlRfTU9WRV9MRU5HVEhcIjtcbiAgICB2YXIgbG9uZ0RpYWdvbmFsTW92ZUxlbmd0aERyb3Bkb3duVmFsdWUgPSBcIkxPTkdfRElBR09OQUxfTU9WRV9MRU5HVEhcIjtcbiAgICB2YXIgc2hvcnREaWFnb25hbE1vdmVMZW5ndGhEcm9wZG93blZhbHVlID0gXCJTSE9SVF9ESUFHT05BTF9NT1ZFX0xFTkdUSFwiO1xuICAgIHZhciBkZWZhdWx0TW92ZUxlbmd0aCA9IDUwO1xuICAgIHZhciBkZWZhdWx0RGlhZ29uYWxNb3ZlTGVuZ3RoID0gZGVmYXVsdE1vdmVMZW5ndGggKiBNYXRoLnNxcnQoMik7XG4gICAgdmFyIHNpbXBsZUxlbmd0aENob2ljZXMgPSBbXG4gICAgICBbc2tpbi5sb25nTGluZURyYXcsIGxvbmdNb3ZlTGVuZ3RoRHJvcGRvd25WYWx1ZV0sXG4gICAgICBbc2tpbi5zaG9ydExpbmVEcmF3LCBzaG9ydE1vdmVMZW5ndGhEcm9wZG93blZhbHVlXVxuICAgIF07XG4gICAgdmFyIHNpbXBsZURpYWdvbmFsTGVuZ3RoQ2hvaWNlcyA9IFtcbiAgICAgIFtza2luLmxvbmdMaW5lRHJhdywgbG9uZ0RpYWdvbmFsTW92ZUxlbmd0aERyb3Bkb3duVmFsdWVdLFxuICAgICAgW3NraW4uc2hvcnRMaW5lRHJhdywgc2hvcnREaWFnb25hbE1vdmVMZW5ndGhEcm9wZG93blZhbHVlXVxuICAgIF07XG4gICAgdmFyIHNpbXBsZUxlbmd0aFJpZ2h0Q2hvaWNlcyA9IFtcbiAgICAgIFtza2luLmxvbmdMaW5lRHJhd1JpZ2h0LCBsb25nTW92ZUxlbmd0aERyb3Bkb3duVmFsdWVdLFxuICAgICAgW3NraW4uc2hvcnRMaW5lRHJhd1JpZ2h0LCBzaG9ydE1vdmVMZW5ndGhEcm9wZG93blZhbHVlXVxuICAgIF07XG4gIFxuICAgIHZhciBTaW1wbGVNb3ZlID0ge1xuICAgICAgU0hPUlRfTU9WRV9MRU5HVEg6IDUwLFxuICAgICAgTE9OR19NT1ZFX0xFTkdUSDogMTAwLFxuICAgICAgU0hPUlRfRElBR09OQUxfTU9WRV9MRU5HVEg6IDUwICogTWF0aC5zcXJ0KDIpLFxuICAgICAgTE9OR19ESUFHT05BTF9NT1ZFX0xFTkdUSDogMTAwICogTWF0aC5zcXJ0KDIpLFxuICAgICAgRElSRUNUSU9OX0NPTkZJR1M6IHtcbiAgICAgICAgbGVmdDoge1xuICAgICAgICAgIHRpdGxlOiBjb21tb25Nc2cuZGlyZWN0aW9uV2VzdExldHRlcigpLFxuICAgICAgICAgIG1vdmVGdW5jdGlvbjogJ21vdmVMZWZ0JyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cubW92ZVdlc3RUb29sdGlwKCksXG4gICAgICAgICAgaW1hZ2U6IHNraW4ud2VzdExpbmVEcmF3LFxuICAgICAgICAgIGltYWdlRGltZW5zaW9uczoge3dpZHRoOiA3MiwgaGVpZ2h0OiA1Nn0sXG4gICAgICAgICAgbGVuZ3Roczogc2ltcGxlTGVuZ3RoQ2hvaWNlcyxcbiAgICAgICAgICBkZWZhdWx0TGVuZ3RoOiBkZWZhdWx0TW92ZUxlbmd0aCxcbiAgICAgICAgICBkZWZhdWx0RHJvcGRvd25WYWx1ZTogbG9uZ01vdmVMZW5ndGhEcm9wZG93blZhbHVlXG4gICAgICAgIH0sXG4gICAgICAgIHJpZ2h0OiB7XG4gICAgICAgICAgdGl0bGU6IGNvbW1vbk1zZy5kaXJlY3Rpb25FYXN0TGV0dGVyKCksXG4gICAgICAgICAgbW92ZUZ1bmN0aW9uOiAnbW92ZVJpZ2h0JyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cubW92ZUVhc3RUb29sdGlwKCksXG4gICAgICAgICAgaW1hZ2U6IHNraW4uZWFzdExpbmVEcmF3LFxuICAgICAgICAgIGltYWdlRGltZW5zaW9uczoge3dpZHRoOiA3MiwgaGVpZ2h0OiA1Nn0sXG4gICAgICAgICAgbGVuZ3Roczogc2ltcGxlTGVuZ3RoUmlnaHRDaG9pY2VzLFxuICAgICAgICAgIGRlZmF1bHRMZW5ndGg6IGRlZmF1bHRNb3ZlTGVuZ3RoLFxuICAgICAgICAgIGRlZmF1bHREcm9wZG93blZhbHVlOiBsb25nTW92ZUxlbmd0aERyb3Bkb3duVmFsdWVcbiAgICAgICAgfSxcbiAgICAgICAgdXA6IHtcbiAgICAgICAgICB0aXRsZTogY29tbW9uTXNnLmRpcmVjdGlvbk5vcnRoTGV0dGVyKCksXG4gICAgICAgICAgbW92ZUZ1bmN0aW9uOiAnbW92ZVVwJyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cubW92ZU5vcnRoVG9vbHRpcCgpLFxuICAgICAgICAgIGltYWdlOiBza2luLm5vcnRoTGluZURyYXcsXG4gICAgICAgICAgaW1hZ2VEaW1lbnNpb25zOiB7d2lkdGg6IDcyLCBoZWlnaHQ6IDU2fSxcbiAgICAgICAgICBsZW5ndGhzOiBzaW1wbGVMZW5ndGhDaG9pY2VzLFxuICAgICAgICAgIGRlZmF1bHRMZW5ndGg6IGRlZmF1bHRNb3ZlTGVuZ3RoLFxuICAgICAgICAgIGRlZmF1bHREcm9wZG93blZhbHVlOiBsb25nTW92ZUxlbmd0aERyb3Bkb3duVmFsdWVcbiAgICAgICAgfSxcbiAgICAgICAgZG93bjoge1xuICAgICAgICAgIHRpdGxlOiBjb21tb25Nc2cuZGlyZWN0aW9uU291dGhMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdtb3ZlRG93bicsXG4gICAgICAgICAgdG9vbHRpcDogbXNnLm1vdmVTb3V0aFRvb2x0aXAoKSxcbiAgICAgICAgICBpbWFnZTogc2tpbi5zb3V0aExpbmVEcmF3LFxuICAgICAgICAgIGltYWdlRGltZW5zaW9uczoge3dpZHRoOiA3MiwgaGVpZ2h0OiA1Nn0sXG4gICAgICAgICAgbGVuZ3Roczogc2ltcGxlTGVuZ3RoQ2hvaWNlcyxcbiAgICAgICAgICBkZWZhdWx0TGVuZ3RoOiBkZWZhdWx0TW92ZUxlbmd0aCxcbiAgICAgICAgICBkZWZhdWx0RHJvcGRvd25WYWx1ZTogbG9uZ01vdmVMZW5ndGhEcm9wZG93blZhbHVlXG4gICAgICAgIH0sXG4gICAgICAgIHVwX2xlZnQ6IHtcbiAgICAgICAgICB0aXRsZTogY29tbW9uTXNnLmRpcmVjdGlvbk5vcnRod2VzdExldHRlcigpLFxuICAgICAgICAgIG1vdmVGdW5jdGlvbjogJ21vdmVVcExlZnQnLFxuICAgICAgICAgIHRvb2x0aXA6IG1zZy5tb3ZlTm9ydGh3ZXN0VG9vbHRpcCgpLFxuICAgICAgICAgIGltYWdlOiBza2luLm5vcnRod2VzdExpbmVEcmF3LFxuICAgICAgICAgIGltYWdlRGltZW5zaW9uczoge3dpZHRoOiA3MiwgaGVpZ2h0OiA1Nn0sXG4gICAgICAgICAgbGVuZ3Roczogc2ltcGxlRGlhZ29uYWxMZW5ndGhDaG9pY2VzLFxuICAgICAgICAgIGRlZmF1bHRMZW5ndGg6IGRlZmF1bHREaWFnb25hbE1vdmVMZW5ndGgsXG4gICAgICAgICAgZGVmYXVsdERyb3Bkb3duVmFsdWU6IGxvbmdEaWFnb25hbE1vdmVMZW5ndGhEcm9wZG93blZhbHVlXG4gICAgICAgIH0sXG4gICAgICAgIHVwX3JpZ2h0OiB7XG4gICAgICAgICAgdGl0bGU6IGNvbW1vbk1zZy5kaXJlY3Rpb25Ob3J0aGVhc3RMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdtb3ZlVXBSaWdodCcsXG4gICAgICAgICAgdG9vbHRpcDogbXNnLm1vdmVOb3J0aGVhc3RUb29sdGlwKCksXG4gICAgICAgICAgaW1hZ2U6IHNraW4ubm9ydGhlYXN0TGluZURyYXcsXG4gICAgICAgICAgaW1hZ2VEaW1lbnNpb25zOiB7d2lkdGg6IDcyLCBoZWlnaHQ6IDU2fSxcbiAgICAgICAgICBsZW5ndGhzOiBzaW1wbGVEaWFnb25hbExlbmd0aENob2ljZXMsXG4gICAgICAgICAgZGVmYXVsdExlbmd0aDogZGVmYXVsdERpYWdvbmFsTW92ZUxlbmd0aCxcbiAgICAgICAgICBkZWZhdWx0RHJvcGRvd25WYWx1ZTogbG9uZ0RpYWdvbmFsTW92ZUxlbmd0aERyb3Bkb3duVmFsdWVcbiAgICAgICAgfSxcbiAgICAgICAgZG93bl9sZWZ0OiB7XG4gICAgICAgICAgdGl0bGU6IGNvbW1vbk1zZy5kaXJlY3Rpb25Tb3V0aHdlc3RMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdtb3ZlRG93bkxlZnQnLFxuICAgICAgICAgIHRvb2x0aXA6IG1zZy5tb3ZlU291dGh3ZXN0VG9vbHRpcCgpLFxuICAgICAgICAgIGltYWdlOiBza2luLnNvdXRod2VzdExpbmVEcmF3LFxuICAgICAgICAgIGltYWdlRGltZW5zaW9uczoge3dpZHRoOiA3MiwgaGVpZ2h0OiA1Nn0sXG4gICAgICAgICAgbGVuZ3Roczogc2ltcGxlRGlhZ29uYWxMZW5ndGhDaG9pY2VzLFxuICAgICAgICAgIGRlZmF1bHRMZW5ndGg6IGRlZmF1bHREaWFnb25hbE1vdmVMZW5ndGgsXG4gICAgICAgICAgZGVmYXVsdERyb3Bkb3duVmFsdWU6IGxvbmdEaWFnb25hbE1vdmVMZW5ndGhEcm9wZG93blZhbHVlXG4gICAgICAgIH0sXG4gICAgICAgIGRvd25fcmlnaHQ6IHtcbiAgICAgICAgICB0aXRsZTogY29tbW9uTXNnLmRpcmVjdGlvblNvdXRoZWFzdExldHRlcigpLFxuICAgICAgICAgIG1vdmVGdW5jdGlvbjogJ21vdmVEb3duUmlnaHQnLFxuICAgICAgICAgIHRvb2x0aXA6IG1zZy5tb3ZlU291dGhlYXN0VG9vbHRpcCgpLFxuICAgICAgICAgIGltYWdlOiBza2luLnNvdXRoZWFzdExpbmVEcmF3LFxuICAgICAgICAgIGltYWdlRGltZW5zaW9uczoge3dpZHRoOiA3MiwgaGVpZ2h0OiA1Nn0sXG4gICAgICAgICAgbGVuZ3Roczogc2ltcGxlRGlhZ29uYWxMZW5ndGhDaG9pY2VzLFxuICAgICAgICAgIGRlZmF1bHRMZW5ndGg6IGRlZmF1bHREaWFnb25hbE1vdmVMZW5ndGgsXG4gICAgICAgICAgZGVmYXVsdERyb3Bkb3duVmFsdWU6IGxvbmdEaWFnb25hbE1vdmVMZW5ndGhEcm9wZG93blZhbHVlXG4gICAgICAgIH0sXG4gICAgICAgIGp1bXBfbGVmdDoge1xuICAgICAgICAgIGlzSnVtcDogdHJ1ZSxcbiAgICAgICAgICB0aXRsZTogY29tbW9uTXNnLmRpcmVjdGlvbldlc3RMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdqdW1wTGVmdCcsXG4gICAgICAgICAgaW1hZ2U6IHNraW4ubGVmdEp1bXBBcnJvdyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cuanVtcFdlc3RUb29sdGlwKCksXG4gICAgICAgICAgZGVmYXVsdExlbmd0aDogZGVmYXVsdE1vdmVMZW5ndGhcbiAgICAgICAgfSxcbiAgICAgICAganVtcF9yaWdodDoge1xuICAgICAgICAgIGlzSnVtcDogdHJ1ZSxcbiAgICAgICAgICB0aXRsZTogY29tbW9uTXNnLmRpcmVjdGlvbkVhc3RMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdqdW1wUmlnaHQnLFxuICAgICAgICAgIGltYWdlOiBza2luLnJpZ2h0SnVtcEFycm93LFxuICAgICAgICAgIHRvb2x0aXA6IG1zZy5qdW1wRWFzdFRvb2x0aXAoKSxcbiAgICAgICAgICBkZWZhdWx0TGVuZ3RoOiBkZWZhdWx0TW92ZUxlbmd0aFxuICAgICAgICB9LFxuICAgICAgICBqdW1wX3VwOiB7XG4gICAgICAgICAgaXNKdW1wOiB0cnVlLFxuICAgICAgICAgIHRpdGxlOiBjb21tb25Nc2cuZGlyZWN0aW9uTm9ydGhMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdqdW1wVXAnLFxuICAgICAgICAgIGltYWdlOiBza2luLnVwSnVtcEFycm93LFxuICAgICAgICAgIHRvb2x0aXA6IG1zZy5qdW1wTm9ydGhUb29sdGlwKCksXG4gICAgICAgICAgZGVmYXVsdExlbmd0aDogZGVmYXVsdE1vdmVMZW5ndGhcbiAgICAgICAgfSxcbiAgICAgICAganVtcF9kb3duOiB7XG4gICAgICAgICAgaXNKdW1wOiB0cnVlLFxuICAgICAgICAgIHRpdGxlOiBjb21tb25Nc2cuZGlyZWN0aW9uU291dGhMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdqdW1wRG93bicsXG4gICAgICAgICAgaW1hZ2U6IHNraW4uZG93bkp1bXBBcnJvdyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cuanVtcFNvdXRoVG9vbHRpcCgpLFxuICAgICAgICAgIGRlZmF1bHRMZW5ndGg6IGRlZmF1bHRNb3ZlTGVuZ3RoXG4gICAgICAgIH0sXG4gICAgICAgIGp1bXBfdXBfbGVmdDoge1xuICAgICAgICAgIGlzSnVtcDogdHJ1ZSxcbiAgICAgICAgICB0aXRsZTogY29tbW9uTXNnLmRpcmVjdGlvbk5vcnRod2VzdExldHRlcigpLFxuICAgICAgICAgIG1vdmVGdW5jdGlvbjogJ2p1bXBVcExlZnQnLFxuICAgICAgICAgIGltYWdlOiBza2luLnVwTGVmdEp1bXBBcnJvdyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cuanVtcE5vcnRod2VzdFRvb2x0aXAoKSxcbiAgICAgICAgICBkZWZhdWx0TGVuZ3RoOiBkZWZhdWx0RGlhZ29uYWxNb3ZlTGVuZ3RoXG4gICAgICAgIH0sXG4gICAgICAgIGp1bXBfdXBfcmlnaHQ6IHtcbiAgICAgICAgICBpc0p1bXA6IHRydWUsXG4gICAgICAgICAgdGl0bGU6IGNvbW1vbk1zZy5kaXJlY3Rpb25Ob3J0aGVhc3RMZXR0ZXIoKSxcbiAgICAgICAgICBtb3ZlRnVuY3Rpb246ICdqdW1wVXBSaWdodCcsXG4gICAgICAgICAgaW1hZ2U6IHNraW4udXBSaWdodEp1bXBBcnJvdyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cuanVtcE5vcnRoZWFzdFRvb2x0aXAoKSxcbiAgICAgICAgICBkZWZhdWx0TGVuZ3RoOiBkZWZhdWx0RGlhZ29uYWxNb3ZlTGVuZ3RoXG4gICAgICAgIH0sXG4gICAgICAgIGp1bXBfZG93bl9sZWZ0OiB7XG4gICAgICAgICAgaXNKdW1wOiB0cnVlLFxuICAgICAgICAgIHRpdGxlOiBjb21tb25Nc2cuZGlyZWN0aW9uU291dGh3ZXN0TGV0dGVyKCksXG4gICAgICAgICAgbW92ZUZ1bmN0aW9uOiAnanVtcERvd25MZWZ0JyxcbiAgICAgICAgICBpbWFnZTogc2tpbi5kb3duTGVmdEp1bXBBcnJvdyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cuanVtcFNvdXRod2VzdFRvb2x0aXAoKSxcbiAgICAgICAgICBkZWZhdWx0TGVuZ3RoOiBkZWZhdWx0RGlhZ29uYWxNb3ZlTGVuZ3RoXG4gICAgICAgIH0sXG4gICAgICAgIGp1bXBfZG93bl9yaWdodDoge1xuICAgICAgICAgIGlzSnVtcDogdHJ1ZSxcbiAgICAgICAgICB0aXRsZTogY29tbW9uTXNnLmRpcmVjdGlvblNvdXRoZWFzdExldHRlcigpLFxuICAgICAgICAgIG1vdmVGdW5jdGlvbjogJ2p1bXBEb3duUmlnaHQnLFxuICAgICAgICAgIGltYWdlOiBza2luLmRvd25SaWdodEp1bXBBcnJvdyxcbiAgICAgICAgICB0b29sdGlwOiBtc2cuanVtcFNvdXRoZWFzdFRvb2x0aXAoKSxcbiAgICAgICAgICBkZWZhdWx0TGVuZ3RoOiBkZWZhdWx0RGlhZ29uYWxNb3ZlTGVuZ3RoXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBnZW5lcmF0ZUJsb2Nrc0ZvckFsbERpcmVjdGlvbnM6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgU2ltcGxlTW92ZS5nZW5lcmF0ZUJsb2Nrc0ZvckRpcmVjdGlvbigndXAnKTtcbiAgICAgICAgU2ltcGxlTW92ZS5nZW5lcmF0ZUJsb2Nrc0ZvckRpcmVjdGlvbignZG93bicpO1xuICAgICAgICBTaW1wbGVNb3ZlLmdlbmVyYXRlQmxvY2tzRm9yRGlyZWN0aW9uKCdsZWZ0Jyk7XG4gICAgICAgIFNpbXBsZU1vdmUuZ2VuZXJhdGVCbG9ja3NGb3JEaXJlY3Rpb24oJ3JpZ2h0Jyk7XG4gICAgICAgIFNpbXBsZU1vdmUuZ2VuZXJhdGVCbG9ja3NGb3JEaXJlY3Rpb24oJ3VwX2xlZnQnKTtcbiAgICAgICAgU2ltcGxlTW92ZS5nZW5lcmF0ZUJsb2Nrc0ZvckRpcmVjdGlvbigndXBfcmlnaHQnKTtcbiAgICAgICAgU2ltcGxlTW92ZS5nZW5lcmF0ZUJsb2Nrc0ZvckRpcmVjdGlvbignZG93bl9sZWZ0Jyk7XG4gICAgICAgIFNpbXBsZU1vdmUuZ2VuZXJhdGVCbG9ja3NGb3JEaXJlY3Rpb24oJ2Rvd25fcmlnaHQnKTtcbiAgICAgIH0sXG4gICAgICBnZW5lcmF0ZUJsb2Nrc0ZvckRpcmVjdGlvbjogZnVuY3Rpb24gKGRpcmVjdGlvbikge1xuICAgICAgICBnZW5lcmF0b3JbJ3NpbXBsZV9tb3ZlXycgKyBkaXJlY3Rpb25dID0gU2ltcGxlTW92ZS5nZW5lcmF0ZUNvZGVHZW5lcmF0b3IoZGlyZWN0aW9uKTtcbiAgICAgICAgZ2VuZXJhdG9yWydzaW1wbGVfanVtcF8nICsgZGlyZWN0aW9uXSA9IFNpbXBsZU1vdmUuZ2VuZXJhdGVDb2RlR2VuZXJhdG9yKCdqdW1wXycgKyBkaXJlY3Rpb24pO1xuICAgICAgICBnZW5lcmF0b3JbJ3NpbXBsZV9tb3ZlXycgKyBkaXJlY3Rpb24gKyAnX2xlbmd0aCddID0gU2ltcGxlTW92ZS5nZW5lcmF0ZUNvZGVHZW5lcmF0b3IoZGlyZWN0aW9uLCB0cnVlKTtcbiAgICAgICAgYmxvY2tseS5CbG9ja3NbJ3NpbXBsZV9tb3ZlXycgKyBkaXJlY3Rpb24gKyAnX2xlbmd0aCddID0gU2ltcGxlTW92ZS5nZW5lcmF0ZU1vdmVCbG9jayhkaXJlY3Rpb24sIHRydWUpO1xuICAgICAgICBibG9ja2x5LkJsb2Nrc1snc2ltcGxlX21vdmVfJyArIGRpcmVjdGlvbl0gPSBTaW1wbGVNb3ZlLmdlbmVyYXRlTW92ZUJsb2NrKGRpcmVjdGlvbik7XG4gICAgICAgIGJsb2NrbHkuQmxvY2tzWydzaW1wbGVfanVtcF8nICsgZGlyZWN0aW9uXSA9IFNpbXBsZU1vdmUuZ2VuZXJhdGVNb3ZlQmxvY2soJ2p1bXBfJyArIGRpcmVjdGlvbik7XG4gICAgICB9LFxuICAgICAgZ2VuZXJhdGVNb3ZlQmxvY2s6IGZ1bmN0aW9uIChkaXJlY3Rpb24sIGhhc0xlbmd0aElucHV0KSB7XG4gICAgICAgIHZhciBkaXJlY3Rpb25Db25maWcgPSBTaW1wbGVNb3ZlLkRJUkVDVElPTl9DT05GSUdTW2RpcmVjdGlvbl07XG4gICAgICAgIHZhciBkaXJlY3Rpb25MZXR0ZXJXaWR0aCA9IDEyO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIGhlbHBVcmw6ICcnLFxuICAgICAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgICAgICB2YXIgaW5wdXQgPSB0aGlzLmFwcGVuZER1bW15SW5wdXQoKTtcbiAgICAgICAgICAgIGlmIChkaXJlY3Rpb25Db25maWcuaXNKdW1wKSB7XG4gICAgICAgICAgICAgIGlucHV0LmFwcGVuZFRpdGxlKGNvbW1vbk1zZy5qdW1wKCkpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaW5wdXQuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGRMYWJlbChkaXJlY3Rpb25Db25maWcudGl0bGUsIHtmaXhlZFNpemU6IHt3aWR0aDogZGlyZWN0aW9uTGV0dGVyV2lkdGgsIGhlaWdodDogMTh9fSkpO1xuICBcbiAgICAgICAgICAgIGlmIChkaXJlY3Rpb25Db25maWcuaW1hZ2VEaW1lbnNpb25zKSB7XG4gICAgICAgICAgICAgIGlucHV0LmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkSW1hZ2UoZGlyZWN0aW9uQ29uZmlnLmltYWdlLFxuICAgICAgICAgICAgICAgIGRpcmVjdGlvbkNvbmZpZy5pbWFnZURpbWVuc2lvbnMud2lkdGgsXG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uQ29uZmlnLmltYWdlRGltZW5zaW9ucy5oZWlnaHQpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgIGlucHV0LmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkSW1hZ2UoZGlyZWN0aW9uQ29uZmlnLmltYWdlKSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICAgICAgdGhpcy5zZXRUb29sdGlwKGRpcmVjdGlvbkNvbmZpZy50b29sdGlwKTtcbiAgICAgICAgICAgIGlmIChoYXNMZW5ndGhJbnB1dCkge1xuICAgICAgICAgICAgICB2YXIgZHJvcGRvd24gPSBuZXcgYmxvY2tseS5GaWVsZEltYWdlRHJvcGRvd24oZGlyZWN0aW9uQ29uZmlnLmxlbmd0aHMpO1xuICAgICAgICAgICAgICBkcm9wZG93bi5zZXRWYWx1ZShkaXJlY3Rpb25Db25maWcuZGVmYXVsdERyb3Bkb3duVmFsdWUpO1xuICAgICAgICAgICAgICBpbnB1dC5hcHBlbmRUaXRsZShkcm9wZG93biwgJ2xlbmd0aCcpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgIH0sXG4gICAgICBnZW5lcmF0ZUNvZGVHZW5lcmF0b3I6IGZ1bmN0aW9uIChkaXJlY3Rpb24sIGhhc0xlbmd0aElucHV0KSB7XG4gICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgdmFyIGRpcmVjdGlvbkNvbmZpZyA9IFNpbXBsZU1vdmUuRElSRUNUSU9OX0NPTkZJR1NbZGlyZWN0aW9uXTtcbiAgICAgICAgICB2YXIgbGVuZ3RoID0gZGlyZWN0aW9uQ29uZmlnLmRlZmF1bHRMZW5ndGg7XG4gIFxuICAgICAgICAgIGlmIChoYXNMZW5ndGhJbnB1dCkge1xuICAgICAgICAgICAgbGVuZ3RoID0gU2ltcGxlTW92ZVt0aGlzLmdldFRpdGxlVmFsdWUoXCJsZW5ndGhcIildO1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gJ1R1cnRsZS4nICsgZGlyZWN0aW9uQ29uZmlnLm1vdmVGdW5jdGlvbiArICcoJyArIGxlbmd0aCArICcsJyArICdcXCdibG9ja19pZF8nICsgdGhpcy5pZCArICdcXCcpO1xcbic7XG4gICAgICAgIH07XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgU2ltcGxlTW92ZS5nZW5lcmF0ZUJsb2Nrc0ZvckFsbERpcmVjdGlvbnMoKTtcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MuanVtcC5ESVJFQ1RJT05TID1cbiAgICAgICAgW1ttc2cuanVtcEZvcndhcmQoKSwgJ2p1bXBGb3J3YXJkJ10sXG4gICAgICAgICBbbXNnLmp1bXBCYWNrd2FyZCgpLCAnanVtcEJhY2t3YXJkJ11dO1xuICBcbiAgICBnZW5lcmF0b3IuanVtcCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEdlbmVyYXRlIEphdmFTY3JpcHQgZm9yIGp1bXBpbmcgZm9yd2FyZCBvciBiYWNrd2FyZHMuXG4gICAgICB2YXIgdmFsdWUgPSBnZW5lcmF0b3IudmFsdWVUb0NvZGUodGhpcywgJ1ZBTFVFJyxcbiAgICAgICAgICBnZW5lcmF0b3IuT1JERVJfTk9ORSkgfHwgJzAnO1xuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSJykgK1xuICAgICAgICAgICcoJyArIHZhbHVlICsgJywgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmp1bXBfYnlfY29uc3RhbnQgPSB7XG4gICAgICAvLyBCbG9jayBmb3IgbW92aW5nIGZvcndhcmQgb3IgYmFja3dhcmQgdGhlIGludGVybmFsIG51bWJlciBvZiBwaXhlbHNcbiAgICAgIC8vIHdpdGhvdXQgZHJhd2luZy5cbiAgICAgIGhlbHBVcmw6ICcnLFxuICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldEhTVigxODQsIDEuMDAsIDAuNzQpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkRHJvcGRvd24oXG4gICAgICAgICAgICAgICAgYmxvY2tseS5CbG9ja3MuanVtcC5ESVJFQ1RJT05TKSwgJ0RJUicpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkVGV4dElucHV0KCcxMDAnLFxuICAgICAgICAgICAgICAgIGJsb2NrbHkuRmllbGRUZXh0SW5wdXQubnVtYmVyVmFsaWRhdG9yKSwgJ1ZBTFVFJylcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuZG90cygpKTtcbiAgICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKG1zZy5qdW1wVG9vbHRpcCgpKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBibG9ja2x5LkJsb2Nrcy5qdW1wX2J5X2NvbnN0YW50X2Ryb3Bkb3duID0ge1xuICAgICAgLy8gQmxvY2sgZm9yIG1vdmluZyBmb3J3YXJkIG9yIGJhY2t3YXJkIHRoZSBpbnRlcm5hbCBudW1iZXIgb2YgcGl4ZWxzXG4gICAgICAvLyB3aXRob3V0IGRyYXdpbmcuXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKFxuICAgICAgICAgICAgICAgIGJsb2NrbHkuQmxvY2tzLmp1bXAuRElSRUNUSU9OUyksICdESVInKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKCksICdWQUxVRScpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmRvdHMoKSk7XG4gICAgICAgIHRoaXMuc2V0SW5wdXRzSW5saW5lKHRydWUpO1xuICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0VG9vbHRpcChtc2cuanVtcFRvb2x0aXAoKSk7XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgZ2VuZXJhdG9yLmp1bXBfYnlfY29uc3RhbnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBHZW5lcmF0ZSBKYXZhU2NyaXB0IGZvciBtb3ZpbmcgZm9yd2FyZCBvciBiYWNrd2FyZCB0aGUgaW50ZXJuYWwgbnVtYmVyXG4gICAgICAvLyBvZiBwaXhlbHMgd2l0aG91dCBkcmF3aW5nLlxuICAgICAgdmFyIHZhbHVlID0gd2luZG93LnBhcnNlRmxvYXQodGhpcy5nZXRUaXRsZVZhbHVlKCdWQUxVRScpKSB8fCAwO1xuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnRElSJykgK1xuICAgICAgICAgICcoJyArIHZhbHVlICsgJywgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gICAgZ2VuZXJhdG9yLmp1bXBfYnlfY29uc3RhbnRfZHJvcGRvd24gPSBnZW5lcmF0b3IuanVtcF9ieV9jb25zdGFudDtcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MuanVtcF90byA9IHtcbiAgICAgIC8vIEJsb2NrIGZvciBqdW1waW5nIHRvIGEgc3BlY2lmaWVkIHBvc2l0aW9uXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGRyb3Bkb3duID0gbmV3IGJsb2NrbHkuRmllbGREcm9wZG93bih0aGlzLlZBTFVFUyk7XG4gICAgICAgIGRyb3Bkb3duLnNldFZhbHVlKHRoaXMuVkFMVUVTWzFdWzFdKTsgLy8gZGVmYXVsdCB0byB0b3AtbGVmdFxuICAgICAgICB0aGlzLnNldEhTVigxODQsIDEuMDAsIDAuNzQpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuanVtcCgpKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAuYXBwZW5kVGl0bGUoZHJvcGRvd24sICdWQUxVRScpO1xuICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLmp1bXBUb29sdGlwKCkpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGdlbmVyYXRvci5qdW1wX3RvID0gZnVuY3Rpb24gKCkge1xuICAgICAgbGV0IHZhbHVlID0gdGhpcy5nZXRUaXRsZVZhbHVlKCdWQUxVRScpO1xuICAgICAgaWYgKHZhbHVlID09PSBSQU5ET01fVkFMVUUpIHtcbiAgICAgICAgbGV0IHBvc3NpYmxlVmFsdWVzID0gdGhpcy5WQUxVRVMubWFwKGl0ZW0gPT4gaXRlbVsxXSlcbiAgICAgICAgICAgIC5maWx0ZXIoaXRlbSA9PiBpdGVtICE9PSBSQU5ET01fVkFMVUUpO1xuICAgICAgICB2YWx1ZSA9IGBUdXJ0bGUucmFuZG9tKFske3Bvc3NpYmxlVmFsdWVzfV0pYDtcbiAgICAgIH1cbiAgICAgIHJldHVybiBgVHVydGxlLmp1bXBUbygke3ZhbHVlfSwgJ2Jsb2NrX2lkXyR7dGhpcy5pZH0nKTtcXG5gO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmp1bXBfdG9feHkgPSB7XG4gICAgICAvLyBCbG9jayBmb3IganVtcGluZyB0byBzcGVjaWZpZWQgWFkgbG9jYXRpb24uXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLmp1bXBUbygpKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZFRleHRJbnB1dCgnMCcsXG4gICAgICAgICAgICAgIGJsb2NrbHkuRmllbGRUZXh0SW5wdXQubnVtYmVyVmFsaWRhdG9yKSwgJ1hQT1MnKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKGNvbW1vbk1zZy5wb3NpdGlvbkFic29sdXRlT3ZlcigpKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZFRleHRJbnB1dCgnMCcsXG4gICAgICAgICAgICAgIGJsb2NrbHkuRmllbGRUZXh0SW5wdXQubnVtYmVyVmFsaWRhdG9yKSwgJ1lQT1MnKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKGNvbW1vbk1zZy5wb3NpdGlvbkFic29sdXRlRG93bigpKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKG1zZy5qdW1wVG9vbHRpcCgpKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IuanVtcF90b194eSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIGNvbnN0IHhQYXJhbSA9IHdpbmRvdy5wYXJzZUZsb2F0KHRoaXMuZ2V0VGl0bGVWYWx1ZSgnWFBPUycpKSB8fCAwO1xuICAgICAgY29uc3QgeVBhcmFtID0gd2luZG93LnBhcnNlRmxvYXQodGhpcy5nZXRUaXRsZVZhbHVlKCdZUE9TJykpIHx8IDA7XG4gICAgICByZXR1cm4gYFR1cnRsZS5qdW1wVG9YWSgke3hQYXJhbX0sICR7eVBhcmFtfSwgJ2Jsb2NrX2lkXyR7dGhpcy5pZH0nKTtcXG5gO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybiA9IHtcbiAgICAgIC8vIEJsb2NrIGZvciB0dXJuaW5nIGxlZnQgb3IgcmlnaHQuXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5hcHBlbmRWYWx1ZUlucHV0KCdWQUxVRScpXG4gICAgICAgICAgICAuc2V0Q2hlY2soYmxvY2tseS5CbG9ja1ZhbHVlVHlwZS5OVU1CRVIpXG4gICAgICAgICAgICAuYWRkRmllbGRIZWxwZXIoYmxvY2tseS5CbG9ja0ZpZWxkSGVscGVyLkFOR0xFX0hFTFBFUiwge1xuICAgICAgICAgICAgICBibG9jazogdGhpcyxcbiAgICAgICAgICAgICAgZGlyZWN0aW9uVGl0bGU6ICdESVInLFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKFxuICAgICAgICAgICAgICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybi5ESVJFQ1RJT05TKSwgJ0RJUicpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG1zZy5kZWdyZWVzKCkpO1xuICAgICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLnR1cm5Ub29sdGlwKCkpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfdHVybi5ESVJFQ1RJT05TID1cbiAgICAgICAgW1ttc2cudHVyblJpZ2h0KCksICd0dXJuUmlnaHQnXSxcbiAgICAgICAgIFttc2cudHVybkxlZnQoKSwgJ3R1cm5MZWZ0J11dO1xuICBcbiAgICBnZW5lcmF0b3IuZHJhd190dXJuID0gZnVuY3Rpb24gKCkge1xuICAgICAgLy8gR2VuZXJhdGUgSmF2YVNjcmlwdCBmb3IgdHVybmluZyBsZWZ0IG9yIHJpZ2h0LlxuICAgICAgdmFyIHZhbHVlID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKHRoaXMsICdWQUxVRScsXG4gICAgICAgICAgZ2VuZXJhdG9yLk9SREVSX05PTkUpIHx8ICcwJztcbiAgICAgIHJldHVybiAnVHVydGxlLicgKyB0aGlzLmdldFRpdGxlVmFsdWUoJ0RJUicpICtcbiAgICAgICAgICAnKCcgKyB2YWx1ZSArICcsIFxcJ2Jsb2NrX2lkXycgKyB0aGlzLmlkICsgJ1xcJyk7XFxuJztcbiAgICB9O1xuICBcbiAgICAvLyB0aGlzIGlzIHRoZSBvbGQgdmVyc2lvbiBvZiB0aGlzIGJsb2NrLCB0aGF0IHNob3VsZCBvbmx5IHN0aWxsIGJlIHVzZWQgaW5cbiAgICAvLyBvbGQgc2hhcmVkIGxldmVsc1xuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfd2lkdGggPSB7XG4gICAgICAvLyBCbG9jayBmb3Igc2V0dGluZyB0aGUgcGVuIHdpZHRoLlxuICAgICAgaGVscFVybDogJycsXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dCgnV0lEVEgnKVxuICAgICAgICAgICAgLnNldENoZWNrKGJsb2NrbHkuQmxvY2tWYWx1ZVR5cGUuTlVNQkVSKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG1zZy5zZXRXaWR0aCgpKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLndpZHRoVG9vbHRpcCgpKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IuZHJhd193aWR0aCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEdlbmVyYXRlIEphdmFTY3JpcHQgZm9yIHNldHRpbmcgdGhlIHBlbiB3aWR0aC5cbiAgICAgIHZhciB3aWR0aCA9IGdlbmVyYXRvci52YWx1ZVRvQ29kZSh0aGlzLCAnV0lEVEgnLFxuICAgICAgICAgIGdlbmVyYXRvci5PUkRFUl9OT05FKSB8fCAnMSc7XG4gICAgICByZXR1cm4gJ1R1cnRsZS5wZW5XaWR0aCgnICsgd2lkdGggKyAnLCBcXCdibG9ja19pZF8nICsgdGhpcy5pZCArICdcXCcpO1xcbic7XG4gICAgfTtcbiAgXG4gICAgLy8gaW5saW5lZCB2ZXJzaW9uIG9mIGRyYXdfd2lkdGhcbiAgICBibG9ja2x5LkJsb2Nrcy5kcmF3X3dpZHRoX2lubGluZSA9IHtcbiAgICAgIC8vIEJsb2NrIGZvciBzZXR0aW5nIHRoZSBwZW4gd2lkdGguXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5zZXRJbnB1dHNJbmxpbmUodHJ1ZSk7XG4gICAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLnNldFdpZHRoKCkpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkVGV4dElucHV0KCcxJyxcbiAgICAgICAgICAgICAgYmxvY2tseS5GaWVsZFRleHRJbnB1dC5udW1iZXJWYWxpZGF0b3IpLCAnV0lEVEgnKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLndpZHRoVG9vbHRpcCgpKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IuZHJhd193aWR0aF9pbmxpbmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBHZW5lcmF0ZSBKYXZhU2NyaXB0IGZvciBzZXR0aW5nIHRoZSBwZW4gd2lkdGguXG4gICAgICB2YXIgd2lkdGggPSB0aGlzLmdldFRpdGxlVmFsdWUoJ1dJRFRIJyk7XG4gICAgICByZXR1cm4gJ1R1cnRsZS5wZW5XaWR0aCgnICsgd2lkdGggKyAnLCBcXCdibG9ja19pZF8nICsgdGhpcy5pZCArICdcXCcpO1xcbic7XG4gICAgfTtcbiAgXG4gICAgYmxvY2tseS5CbG9ja3MuZHJhd19wZW4gPSB7XG4gICAgICAvLyBCbG9jayBmb3IgcGVuIHVwL2Rvd24uXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKHRoaXMuU1RBVEUpLCAnUEVOJyk7XG4gICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXRUb29sdGlwKG1zZy5wZW5Ub29sdGlwKCkpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfcGVuLlNUQVRFID1cbiAgICAgICAgW1ttc2cucGVuVXAoKSwgJ3BlblVwJ10sXG4gICAgICAgICBbbXNnLnBlbkRvd24oKSwgJ3BlbkRvd24nXV07XG4gIFxuICAgIGdlbmVyYXRvci5kcmF3X3BlbiA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEdlbmVyYXRlIEphdmFTY3JpcHQgZm9yIHBlbiB1cC9kb3duLlxuICAgICAgcmV0dXJuICdUdXJ0bGUuJyArIHRoaXMuZ2V0VGl0bGVWYWx1ZSgnUEVOJykgK1xuICAgICAgICAgICcoXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfY29sb3VyID0ge1xuICAgICAgLy8gQmxvY2sgZm9yIHNldHRpbmcgdGhlIGNvbG91ci5cbiAgICAgIGhlbHBVcmw6ICcnLFxuICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldEhTVigxOTYsIDEuMCwgMC43OSk7XG4gICAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dCgnQ09MT1VSJylcbiAgICAgICAgICAgIC5zZXRDaGVjayhibG9ja2x5LkJsb2NrVmFsdWVUeXBlLkNPTE9VUilcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuc2V0Q29sb3VyKCkpO1xuICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICAgIHRoaXMuc2V0VG9vbHRpcChtc2cuY29sb3VyVG9vbHRpcCgpKTtcbiAgICAgIH1cbiAgICB9O1xuICBcbiAgICBibG9ja2x5LkJsb2Nrcy5hbHBoYSA9IHtcbiAgICAgIC8vIFRPRE86XG4gICAgICAvLyAtIEFkZCBhbHBoYSB0byBhIGdyb3VwXG4gICAgICAvLyAtIE1ha2Ugc3VyZSBpdCBkb2Vzbid0IGNvdW50IGFnYWluc3QgY29ycmVjdCBzb2x1dGlvbnNcbiAgICAgIC8vXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dChcIlZBTFVFXCIpXG4gICAgICAgICAgICAuc2V0Q2hlY2soXCJOdW1iZXJcIilcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuc2V0QWxwaGEoKSk7XG4gICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTk2LCAxLjAsIDAuNzkpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAoJycpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGdlbmVyYXRvci5hbHBoYSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIHZhciBhbHBoYSA9IGdlbmVyYXRvci52YWx1ZVRvQ29kZSh0aGlzLCAnVkFMVUUnLCBibG9ja2x5LkphdmFTY3JpcHQuT1JERVJfTk9ORSk7XG4gICAgICByZXR1cm4gJ1R1cnRsZS5nbG9iYWxBbHBoYSgnICsgYWxwaGEgKyAnLCBcXCdibG9ja19pZF8nICtcbiAgICAgICAgICB0aGlzLmlkICsgJ1xcJyk7XFxuJztcbiAgICB9O1xuICBcbiAgICBnZW5lcmF0b3IuZHJhd19jb2xvdXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBHZW5lcmF0ZSBKYXZhU2NyaXB0IGZvciBzZXR0aW5nIHRoZSBjb2xvdXIuXG4gICAgICB2YXIgY29sb3VyID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKHRoaXMsICdDT0xPVVInLFxuICAgICAgICAgIGdlbmVyYXRvci5PUkRFUl9OT05FKSB8fCAnXFwnIzAwMDAwMFxcJyc7XG4gICAgICByZXR1cm4gJ1R1cnRsZS5wZW5Db2xvdXIoJyArIGNvbG91ciArICcsIFxcJ2Jsb2NrX2lkXycgK1xuICAgICAgICAgIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfY29sb3VyX3NpbXBsZSA9IHtcbiAgICAgIC8vIFNpbXBsaWZpZWQgZHJvcGRvd24gYmxvY2sgZm9yIHNldHRpbmcgdGhlIGNvbG91ci5cbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIGNvbG91cnMgPSBbQ29sb3Vycy5SRUQsIENvbG91cnMuQkxBQ0ssIENvbG91cnMuUElOSywgQ29sb3Vycy5PUkFOR0UsXG4gICAgICAgICAgQ29sb3Vycy5ZRUxMT1csIENvbG91cnMuR1JFRU4sIENvbG91cnMuQkxVRSwgQ29sb3Vycy5BUVVBTUFSSU5FLCBDb2xvdXJzLlBMVU1dO1xuICAgICAgICB0aGlzLnNldEhTVigxOTYsIDEuMCwgMC43OSk7XG4gICAgICAgIHZhciBjb2xvdXJGaWVsZCA9IG5ldyBibG9ja2x5LkZpZWxkQ29sb3VyRHJvcGRvd24oY29sb3VycywgNDUsIDM1KTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuc2V0Q29sb3VyKCkpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUoY29sb3VyRmllbGQsICdDT0xPVVInKTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLmNvbG91clRvb2x0aXAoKSk7XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgZ2VuZXJhdG9yLmRyYXdfY29sb3VyX3NpbXBsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEdlbmVyYXRlIEphdmFTY3JpcHQgZm9yIHNldHRpbmcgdGhlIGNvbG91ci5cbiAgICAgIHZhciBjb2xvdXIgPSB0aGlzLmdldFRpdGxlVmFsdWUoJ0NPTE9VUicpIHx8ICdcXCcjMDAwMDAwXFwnJztcbiAgICAgIHJldHVybiAnVHVydGxlLnBlbkNvbG91cihcIicgKyBjb2xvdXIgKyAnXCIsIFxcJ2Jsb2NrX2lkXycgK1xuICAgICAgICAgIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLmRyYXdfbGluZV9zdHlsZV9wYXR0ZXJuID0ge1xuICAgICAgLy8gQmxvY2sgdG8gaGFuZGxlIGV2ZW50IHdoZW4gYW4gYXJyb3cgYnV0dG9uIGlzIHByZXNzZWQuXG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUsIG51bGwpO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgICAgIC5hcHBlbmRUaXRsZShtc2cuc2V0UGF0dGVybigpKVxuICAgICAgICAgICAgIC5hcHBlbmRUaXRsZSggbmV3IGJsb2NrbHkuRmllbGRJbWFnZURyb3Bkb3duKFxuICAgICAgICAgICAgICAgIHNraW4ubGluZVN0eWxlUGF0dGVybk9wdGlvbnMsIDE1MCwgMjAgKSwgJ1ZBTFVFJyApO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLnNldFBhdHRlcm4oKSk7XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgZ2VuZXJhdG9yLmRyYXdfbGluZV9zdHlsZV9wYXR0ZXJuID0gZnVuY3Rpb24gKCkge1xuICAgICAgLy8gR2VuZXJhdGUgSmF2YVNjcmlwdCBmb3Igc2V0dGluZyB0aGUgaW1hZ2UgZm9yIGEgcGF0dGVybmVkIGxpbmUuXG4gICAgICB2YXIgcGF0dGVybiA9IHRoaXMuZ2V0VGl0bGVWYWx1ZSgnVkFMVUUnKSB8fCAnXFwnREVGQVVMVFxcJyc7XG4gICAgICByZXR1cm4gJ1R1cnRsZS5wZW5QYXR0ZXJuKFwiJyArIHBhdHRlcm4gKyAnXCIsIFxcJ2Jsb2NrX2lkXycgK1xuICAgICAgICAgIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnVwX2JpZyA9IHtcbiAgICAgIGhlbHBVcmw6ICcnLFxuICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLnNldEhTVigxODQsIDEuMDAsIDAuNzQpO1xuICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUsIG51bGwpO1xuICAgICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgLmFwcGVuZFRpdGxlKG5ldyBibG9ja2x5LkZpZWxkRHJvcGRvd24odGhpcy5TVEFURSksICdWSVNJQklMSVRZJyk7XG4gICAgICAgIHRoaXMuc2V0VG9vbHRpcChtc2cudHVydGxlVmlzaWJpbGl0eVRvb2x0aXAoKSk7XG4gICAgICB9XG4gICAgfTtcbiAgXG4gICAgZ2VuZXJhdG9yLnVwX2JpZyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgIC8vIEdlbmVyYXRlIEphdmFTY3JpcHQgZm9yIHNldHRpbmcgdGhlIGNvbG91ci5cbiAgICAgIHZhciBjb2xvdXIgPSBnZW5lcmF0b3IudmFsdWVUb0NvZGUodGhpcywgJ0NPTE9VUicsXG4gICAgICAgIGdlbmVyYXRvci5PUkRFUl9OT05FKSB8fCAnXFwnIzAwMDAwMFxcJyc7XG4gICAgICByZXR1cm4gJ1R1cnRsZS5wZW5Db2xvdXIoJyArIGNvbG91ciArICcsIFxcJ2Jsb2NrX2lkXycgK1xuICAgICAgICB0aGlzLmlkICsgJ1xcJyk7XFxuJztcbiAgICB9O1xuICBcbiAgICBibG9ja2x5LkJsb2Nrcy50dXJ0bGVfdmlzaWJpbGl0eSA9IHtcbiAgICAgIC8vIEJsb2NrIGZvciBjaGFuZ2luZyB0dXJ0bGUgdmlzaWJsaXR5LlxuICAgICAgaGVscFVybDogJycsXG4gICAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgIHRoaXMuc2V0UHJldmlvdXNTdGF0ZW1lbnQodHJ1ZSwgbnVsbCk7XG4gICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlLCBudWxsKTtcbiAgICAgICAgdGhpcy5hcHBlbmREdW1teUlucHV0KClcbiAgICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKHRoaXMuU1RBVEUpLCAnVklTSUJJTElUWScpO1xuICAgICAgICB0aGlzLnNldFRvb2x0aXAobXNnLnR1cnRsZVZpc2liaWxpdHlUb29sdGlwKCkpO1xuICAgICAgfVxuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnR1cnRsZV92aXNpYmlsaXR5LlNUQVRFID1cbiAgICAgICAgW1ttc2cuaGlkZVR1cnRsZSgpLCAnaGlkZVR1cnRsZSddLFxuICAgICAgICAgW21zZy5zaG93VHVydGxlKCksICdzaG93VHVydGxlJ11dO1xuICBcbiAgICBnZW5lcmF0b3IudHVydGxlX3Zpc2liaWxpdHkgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyBHZW5lcmF0ZSBKYXZhU2NyaXB0IGZvciBjaGFuZ2luZyB0dXJ0bGUgdmlzaWJpbGl0eS5cbiAgICAgIHJldHVybiAnVHVydGxlLicgKyB0aGlzLmdldFRpdGxlVmFsdWUoJ1ZJU0lCSUxJVFknKSArXG4gICAgICAgICAgJyhcXCdibG9ja19pZF8nICsgdGhpcy5pZCArICdcXCcpO1xcbic7XG4gICAgfTtcbiAgXG4gICAgZnVuY3Rpb24gY3JlYXRlRHJhd1N0aWNrZXJCbG9jayhibG9ja05hbWUpICB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICBoZWxwVXJsOiAnJyxcbiAgICAgICAgaW5pdDogZnVuY3Rpb24gKCkge1xuICAgICAgICAgIHRoaXMuc2V0SFNWKDE4NCwgMS4wMCwgMC43NCk7XG4gICAgICAgICAgdmFyIGRyb3Bkb3duO1xuICAgICAgICAgIHZhciBpbnB1dCA9IHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpO1xuICAgICAgICAgIGlucHV0LmFwcGVuZFRpdGxlKG1zZy5kcmF3U3RpY2tlcigpKTtcbiAgICAgICAgICB0aGlzLnNldElucHV0c0lubGluZSh0cnVlKTtcbiAgICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgXG4gICAgICAgICAgLy8gR2VuZXJhdGVzIGEgbGlzdCBvZiBwYWlycyBvZiB0aGUgZm9ybSBbW3VybCwgbmFtZV1dXG4gICAgICAgICAgdmFyIHZhbHVlcyA9IFtdO1xuICAgICAgICAgIGZvciAodmFyIG5hbWUgaW4gc2tpbi5zdGlja2Vycykge1xuICAgICAgICAgICAgdmFyIHVybCA9IHNraW4uc3RpY2tlcnNbbmFtZV07XG4gICAgICAgICAgICB2YWx1ZXMucHVzaChbdXJsLCBuYW1lXSk7XG4gICAgICAgICAgfVxuICAgICAgICAgIGRyb3Bkb3duID0gbmV3IGJsb2NrbHkuRmllbGRJbWFnZURyb3Bkb3duKHZhbHVlcywgNDAsIDQwKTtcbiAgXG4gICAgICAgICAgaW5wdXQuYXBwZW5kVGl0bGUoZHJvcGRvd24sICdWQUxVRScpO1xuICBcbiAgICAgICAgICBhcHBlbmRUb0RyYXdTdGlja2VyQmxvY2soYmxvY2tOYW1lLCB0aGlzKTtcbiAgICAgICAgfVxuICAgICAgfTtcbiAgICB9XG4gIFxuICAgIC8vIEFkZCBzaXplIGlucHV0IHRvIHRoZSBkcmF3IHN0aWNrZXIgYmxvY2sgKHRleHQgaW5wdXQgJiBzb2NrZXQpXG4gICAgZnVuY3Rpb24gYXBwZW5kVG9EcmF3U3RpY2tlckJsb2NrKGJsb2NrTmFtZSwgYmxvY2spIHtcbiAgICAgIGlmIChibG9ja05hbWUgPT09ICd0dXJ0bGVfc3RpY2tlcl93aXRoX3NpemUnKSB7XG4gICAgICAgIGJsb2NrLmFwcGVuZER1bW15SW5wdXQoKS5hcHBlbmRUaXRsZShtc2cud2l0aFNpemUoKSk7XG4gICAgICAgIGJsb2NrLmFwcGVuZFZhbHVlSW5wdXQoJ1NJWkUnKVxuICAgICAgICAgICAgLnNldENoZWNrKGJsb2NrbHkuQmxvY2tWYWx1ZVR5cGUuTlVNQkVSKTtcbiAgICAgICAgYmxvY2suYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLnBpeGVscygpKTtcbiAgICAgICAgYmxvY2suc2V0VG9vbHRpcChtc2cuZHJhd1N0aWNrZXJXaXRoU2l6ZSgpKTtcbiAgICAgIH0gZWxzZSBpZiAoYmxvY2tOYW1lID09PSAndHVydGxlX3N0aWNrZXJfd2l0aF9zaXplX25vbl9wYXJhbScpIHtcbiAgICAgICAgYmxvY2suYXBwZW5kRHVtbXlJbnB1dCgpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLndpdGhTaXplKCkpO1xuICAgICAgICBibG9jay5hcHBlbmREdW1teUlucHV0KCkuYXBwZW5kVGl0bGUobmV3IGJsb2NrbHkuRmllbGRUZXh0SW5wdXQoJzAnLFxuICAgICAgICAgICAgYmxvY2tseS5GaWVsZFRleHRJbnB1dC5udW1iZXJWYWxpZGF0b3IpLCAnU0laRScpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUobXNnLnBpeGVscygpKTtcbiAgICAgICAgYmxvY2suc2V0VG9vbHRpcChtc2cuZHJhd1N0aWNrZXJXaXRoU2l6ZSgpKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGJsb2NrLnNldFRvb2x0aXAobXNnLmRyYXdTdGlja2VyKCkpO1xuICAgICAgfVxuICAgIH1cbiAgXG4gICAgLy8gV2UgYWxpYXMgJ3R1cnRsZV9zdGFtcCcgdG8gYmUgdGhlIHNhbWUgYXMgdGhlICdzdGlja2VyJyBibG9jayBmb3JcbiAgICAvLyBiYWNrd2FyZHMgY29tcGF0aWJpbGl0eS5cbiAgICBibG9ja2x5LkJsb2Nrcy5zdGlja2VyID0gYmxvY2tseS5CbG9ja3MudHVydGxlX3N0YW1wID1cbiAgICAgICAgY3JlYXRlRHJhd1N0aWNrZXJCbG9jaygpO1xuICBcbiAgICBnZW5lcmF0b3Iuc3RpY2tlciA9IGdlbmVyYXRvci50dXJ0bGVfc3RhbXAgPSBmdW5jdGlvbiAoKSB7XG4gICAgICByZXR1cm4gJ1R1cnRsZS5kcmF3U3RpY2tlcihcIicgKyB0aGlzLmdldFRpdGxlVmFsdWUoJ1ZBTFVFJykgK1xuICAgICAgICAgICdcIiwgbnVsbCwgXFwnYmxvY2tfaWRfJyArIHRoaXMuaWQgKyAnXFwnKTtcXG4nO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnR1cnRsZV9zdGlja2VyX3dpdGhfc2l6ZSA9XG4gICAgICAgIGNyZWF0ZURyYXdTdGlja2VyQmxvY2soJ3R1cnRsZV9zdGlja2VyX3dpdGhfc2l6ZScpO1xuICBcbiAgICBnZW5lcmF0b3IudHVydGxlX3N0aWNrZXJfd2l0aF9zaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgbGV0IHNpemUgPSBnZW5lcmF0b3IudmFsdWVUb0NvZGUodGhpcywgJ1NJWkUnLFxuICAgICAgICAgIGJsb2NrbHkuSmF2YVNjcmlwdC5PUkRFUl9OT05FKTtcbiAgICAgIHJldHVybiBgVHVydGxlLmRyYXdTdGlja2VyKCcke3RoaXMuZ2V0VGl0bGVWYWx1ZSgnVkFMVUUnKX0nLCR7c2l6ZX0sXG4gICAgICAgICAgJ2Jsb2NrX2lkXyR7dGhpcy5pZH0nKTtcXG5gO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnR1cnRsZV9zdGlja2VyX3dpdGhfc2l6ZV9ub25fcGFyYW0gPVxuICAgICAgICBjcmVhdGVEcmF3U3RpY2tlckJsb2NrKCd0dXJ0bGVfc3RpY2tlcl93aXRoX3NpemVfbm9uX3BhcmFtJyk7XG4gIFxuICAgIGdlbmVyYXRvci50dXJ0bGVfc3RpY2tlcl93aXRoX3NpemVfbm9uX3BhcmFtID0gZnVuY3Rpb24gKCkge1xuICAgICAgbGV0IHNpemUgPSB3aW5kb3cucGFyc2VGbG9hdCh0aGlzLmdldFRpdGxlVmFsdWUoJ1NJWkUnKSkgfHwgMDtcbiAgICAgIHJldHVybiBgVHVydGxlLmRyYXdTdGlja2VyKCcke3RoaXMuZ2V0VGl0bGVWYWx1ZSgnVkFMVUUnKX0nLCR7c2l6ZX0sXG4gICAgICAgICAgJ2Jsb2NrX2lkXyR7dGhpcy5pZH0nKTtcXG5gO1xuICAgIH07XG4gIFxuICAgIGJsb2NrbHkuQmxvY2tzLnR1cnRsZV9zZXRBcnRpc3QgPSB7XG4gICAgICBoZWxwVXJsOiAnJyxcbiAgICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdGhpcy5zZXRIU1YoMTg0LCAxLjAwLCAwLjc0KTtcbiAgICAgICAgdmFyIHZhbHVlcyA9IChza2luLmFydGlzdE9wdGlvbnMgfHwgWydkZWZhdWx0J10pXG4gICAgICAgICAgLm1hcChhcnRpc3QgPT4gW1xuICAgICAgICAgICAgbXNnLnNldENoYXJhY3Rlcih7Y2hhcmFjdGVyOiBhcnRpc3QuY2hhckF0KDApLnRvVXBwZXJDYXNlKCkgKyBhcnRpc3Quc2xpY2UoMSl9KSxcbiAgICAgICAgICAgIGFydGlzdFxuICAgICAgICAgIF0pO1xuICAgICAgICB0aGlzLmFwcGVuZER1bW15SW5wdXQoKVxuICAgICAgICAgIC5hcHBlbmRUaXRsZShuZXcgYmxvY2tseS5GaWVsZERyb3Bkb3duKHZhbHVlcyksICdWQUxVRScpO1xuICAgICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgICB0aGlzLnNldE5leHRTdGF0ZW1lbnQodHJ1ZSk7XG4gICAgICB9LFxuICAgIH07XG4gIFxuICAgIGdlbmVyYXRvci50dXJ0bGVfc2V0QXJ0aXN0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgcmV0dXJuIGBUdXJ0bGUuc2V0QXJ0aXN0KCcke3RoaXMuZ2V0VGl0bGVWYWx1ZSgnVkFMVUUnKX0nLFxuICAgICAgICAnYmxvY2tfaWRfJHt0aGlzLmlkfScpO1xcbmA7XG4gICAgfTtcbiAgXG4gICAgLy9jdXN0b21MZXZlbEJsb2Nrcy5pbnN0YWxsKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKTtcbiAgICAqL1xufTsiXX0=