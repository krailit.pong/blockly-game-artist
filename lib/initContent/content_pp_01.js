// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="text" x="70" y="30"><field name="TEXT"></field></block></xml>';
const blocks = require('./blocks');
const Blockly = require('./locale');

blocks.install(Blockly);

const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" x="70" y="30"></block></xml>';
const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">\n' + '  <category name="เดินไปข้างหน้า" colour="#5C81A6" visible="false">\n' + '    <block type="draw_move_by_constant" ><field name="FD">100</field></block>\n' + '  </category>\n' + '  <category name="หมุนตัวละคร" colour="#5C68A6">\n' + '    <block type="take_turn" ><field name="degrees">90</field></block>\n' + '  </category>\n' + '  <sep></sep>\n' + '</xml>';
const INITIAL_TOOLBOX_CATEGORIES = [];

const ConfigFiles = {
  INITIAL_XML,
  INITIAL_TOOLBOX_XML,
  INITIAL_TOOLBOX_CATEGORIES
};

export default ConfigFiles;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9pbml0Q29udGVudC9jb250ZW50X3BwXzAxLmpzeCJdLCJuYW1lcyI6WyJibG9ja3MiLCJyZXF1aXJlIiwiQmxvY2tseSIsImluc3RhbGwiLCJJTklUSUFMX1hNTCIsIklOSVRJQUxfVE9PTEJPWF9YTUwiLCJJTklUSUFMX1RPT0xCT1hfQ0FURUdPUklFUyIsIkNvbmZpZ0ZpbGVzIl0sIm1hcHBpbmdzIjoiQUFBQTtBQUNBLE1BQU1BLFNBQVNDLFFBQVEsVUFBUixDQUFmO0FBQ0EsTUFBTUMsVUFBVUQsUUFBUSxVQUFSLENBQWhCOztBQUVBRCxPQUFPRyxPQUFQLENBQWVELE9BQWY7O0FBRUEsTUFBTUUsY0FBYyw4RkFBcEI7QUFDQSxNQUFNQyxzQkFBc0IscUZBQzFCLHVFQUQwQixHQUUxQixpRkFGMEIsR0FHMUIsaUJBSDBCLEdBSTFCLG9EQUowQixHQUsxQix5RUFMMEIsR0FNMUIsaUJBTjBCLEdBTzFCLGlCQVAwQixHQVExQixRQVJGO0FBU0EsTUFBTUMsNkJBQTZCLEVBQW5DOztBQUVBLE1BQU1DLGNBQWM7QUFDbEJILGFBRGtCO0FBRWxCQyxxQkFGa0I7QUFHbEJDO0FBSGtCLENBQXBCOztBQU1BLGVBQWVDLFdBQWYiLCJmaWxlIjoiY29udGVudF9wcF8wMS5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIGNvbnN0IElOSVRJQUxfWE1MID0gJzx4bWwgeG1sbnM9XCJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sXCI+PGJsb2NrIHR5cGU9XCJ0ZXh0XCIgeD1cIjcwXCIgeT1cIjMwXCI+PGZpZWxkIG5hbWU9XCJURVhUXCI+PC9maWVsZD48L2Jsb2NrPjwveG1sPic7XG5jb25zdCBibG9ja3MgPSByZXF1aXJlKCcuL2Jsb2NrcycpO1xuY29uc3QgQmxvY2tseSA9IHJlcXVpcmUoJy4vbG9jYWxlJyk7XG5cbmJsb2Nrcy5pbnN0YWxsKEJsb2NrbHkpO1xuXG5jb25zdCBJTklUSUFMX1hNTCA9ICc8eG1sIHhtbG5zPVwiaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbFwiPjxibG9jayB0eXBlPVwic3RhcnRlclwiIHg9XCI3MFwiIHk9XCIzMFwiPjwvYmxvY2s+PC94bWw+JztcbmNvbnN0IElOSVRJQUxfVE9PTEJPWF9YTUwgPSAnPHhtbCB4bWxucz1cImh0dHA6Ly93d3cudzMub3JnLzE5OTkveGh0bWxcIiBpZD1cInRvb2xib3hcIiBzdHlsZT1cImRpc3BsYXk6IG5vbmU7XCI+XFxuJyArXG4gICcgIDxjYXRlZ29yeSBuYW1lPVwi4LmA4LiU4Li04LiZ4LmE4Lib4LiC4LmJ4Liy4LiH4Lir4LiZ4LmJ4LiyXCIgY29sb3VyPVwiIzVDODFBNlwiIHZpc2libGU9XCJmYWxzZVwiPlxcbicgK1xuICAnICAgIDxibG9jayB0eXBlPVwiZHJhd19tb3ZlX2J5X2NvbnN0YW50XCIgPjxmaWVsZCBuYW1lPVwiRkRcIj4xMDA8L2ZpZWxkPjwvYmxvY2s+XFxuJyArXG4gICcgIDwvY2F0ZWdvcnk+XFxuJyArXG4gICcgIDxjYXRlZ29yeSBuYW1lPVwi4Lir4Lih4Li44LiZ4LiV4Lix4Lin4Lil4Liw4LiE4LijXCIgY29sb3VyPVwiIzVDNjhBNlwiPlxcbicgK1xuICAnICAgIDxibG9jayB0eXBlPVwidGFrZV90dXJuXCIgPjxmaWVsZCBuYW1lPVwiZGVncmVlc1wiPjkwPC9maWVsZD48L2Jsb2NrPlxcbicgK1xuICAnICA8L2NhdGVnb3J5PlxcbicgK1xuICAnICA8c2VwPjwvc2VwPlxcbicgK1xuICAnPC94bWw+JztcbmNvbnN0IElOSVRJQUxfVE9PTEJPWF9DQVRFR09SSUVTID0gW107XG5cbmNvbnN0IENvbmZpZ0ZpbGVzID0ge1xuICBJTklUSUFMX1hNTCxcbiAgSU5JVElBTF9UT09MQk9YX1hNTCxcbiAgSU5JVElBTF9UT09MQk9YX0NBVEVHT1JJRVMsXG59O1xuXG5leHBvcnQgZGVmYXVsdCBDb25maWdGaWxlcztcbiJdfQ==