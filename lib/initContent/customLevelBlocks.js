/**
 * A set of blocks used by some of our custom levels (i.e. built by level builder)
 */

var msg = require('./locale');

exports.install = function (blockly, generator, gensym) {
  installDrawASquare(blockly, generator, gensym);
  installCreateACircle(blockly, generator, gensym);
  installCreateASnowflakeBranch(blockly, generator, gensym);
  installDrawATriangle(blockly, generator, gensym);
  installDrawAHouse(blockly, generator, gensym);
  installDrawAFlower(blockly, generator, gensym);
  installDrawASnowflake(blockly, generator, gensym);
  installDrawAHexagon(blockly, generator, gensym);
  installDrawAStar(blockly, generator, gensym);
  installDrawARobot(blockly, generator, gensym);
  installDrawARocket(blockly, generator, gensym);
  installDrawAPlanet(blockly, generator, gensym);
  installDrawARhombus(blockly, generator, gensym);
  installDrawUpperWave(blockly, generator, gensym);
  installDrawLowerWave(blockly, generator, gensym);

  installCreateASnowflakeDropdown(blockly, generator, gensym);
};

var LENGTH_PARAM = msg.lengthParameter();

function createACircleCode(size, gensym, indent) {
  var loopVar = gensym('count');
  indent = indent || '';
  return [indent + '// create_a_circle', indent + 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 36; ' + indent + loopVar + '++) {', indent + '  Turtle.moveForward(' + size + ');', indent + '  Turtle.turnRight(10);', indent + '}\n'].join('\n');
}

/**
 * Returns an initialization object that sets up blockly attributes;
 *
 * @param title - The title of the block that will be visible to the user.
 * @param [parameter] - Optional parameter for blocks that accept a value
 *    parameter. This is the title of the parameter.
 * @return the initialization object
 */
function makeBlockInitializer(title, parameter) {
  return {
    init: function () {
      this.setHSV(94, 0.84, 0.60);

      this.appendDummyInput().appendTitle(title);

      if (parameter !== undefined) {
        this.appendValueInput('VALUE').setAlign(Blockly.ALIGN_RIGHT).setCheck(Blockly.BlockValueType.NUMBER).appendTitle(parameter + ':');
      }

      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };
}

/**
 * Same as draw_a_square, except inputs are not inlined
 */
function installDrawASquare(blockly, generator, gensym) {
  // Create a fake "draw a square" function so it can be made available to users
  // without being shown in the workspace.
  var title = msg.drawASquare();

  blockly.Blocks.draw_a_square_custom = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_square_custom = function () {
    // Generate JavaScript for drawing a square.
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');
    return ['// draw_a_square', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 4; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnRight(90);', '}\n'].join('\n');
  };
}

/**
 * create_a_circle and create_a_circle_size
 * first defaults to size 10, second provides a size param
 */
function installCreateACircle(blockly, generator, gensym) {

  var title = msg.createACircle();
  var param = msg.sizeParameter();

  blockly.Blocks.create_a_circle = makeBlockInitializer(title);

  blockly.Blocks.create_a_circle_size = makeBlockInitializer(title, param);

  generator.create_a_circle = function () {
    return createACircleCode(10, gensym);
  };

  generator.create_a_circle_size = function () {
    var size = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    return createACircleCode(size, gensym);
  };
}

/**
 * create_a_snowflower
 */
function installCreateASnowflakeBranch(blockly, generator, gensym) {

  var title = msg.createASnowflakeBranch();

  blockly.Blocks.create_a_snowflake_branch = makeBlockInitializer(title);

  generator.create_a_snowflake_branch = function () {
    var loopVar = gensym('count');
    var loopVar2 = gensym('count');
    return ['// create_a_snowflake_branch', 'Turtle.jumpForward(90);', 'Turtle.turnLeft(45);', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 3; ' + loopVar + '++) {', '  for (var ' + loopVar2 + ' = 0; ' + loopVar2 + ' < 3; ' + loopVar2 + '++) {', '    Turtle.moveForward(30);', '    Turtle.moveBackward(30);', '    Turtle.turnRight(45);', '  }', '  Turtle.turnLeft(90);', '  Turtle.moveBackward(30);', '  Turtle.turnLeft(45);', '}', 'Turtle.turnRight(45);\n'].join('\n');
  };
}

/**
 * Draw a rhombus function call block
 */
function installDrawARhombus(blockly, generator, gensym) {

  var title = msg.drawARhombus();

  blockly.Blocks.draw_a_rhombus = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_rhombus = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');
    return ['for (var ' + loopVar + ' = 0; ' + loopVar + ' < 2; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnRight(60);', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnRight(120);', '}\n'].join('\n');
  };
}

/**
 * Draw a triangle function call block
 */
function installDrawATriangle(blockly, generator, gensym) {

  var title = msg.drawATriangle();

  blockly.Blocks.draw_a_triangle = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_triangle = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');
    return ['// draw_a_triangle', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 3; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnLeft(120);', '}\n'].join('\n');
  };
}

/**
 * Draw a triangle function call block
 */
function installDrawAHexagon(blockly, generator, gensym) {

  var title = msg.drawAHexagon();

  blockly.Blocks.draw_a_hexagon = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_hexagon = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');
    return ['// draw_a_triangle', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 6; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnLeft(60);', '}\n'].join('\n');
  };
}

/**
 * Draw a house function call block
 */
function installDrawAHouse(blockly, generator, gensym) {

  var title = msg.drawAHouse();

  blockly.Blocks.draw_a_house = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_house = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');
    return ['for (var ' + loopVar + ' = 0; ' + loopVar + ' < 4; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnLeft(90);', '}', 'Turtle.turnLeft(90);', 'Turtle.moveForward(' + value_length + ');', 'Turtle.turnRight(90);', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 3; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnLeft(120);', '}', 'Turtle.turnRight(90);', 'Turtle.moveForward(' + value_length + ');', 'Turtle.turnLeft(90);\n'].join('\n');
  };
}

/**
 * Draw a flower function call block
 */
function installDrawAFlower(blockly, generator, gensym) {

  var title = msg.drawAFlower();

  blockly.Blocks.draw_a_flower = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_flower = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');

    var color_random = generator.colour_random()[0];
    return ['Turtle.penColour("#228b22");', 'Turtle.moveForward(' + value_length + ');', 'Turtle.turnLeft(18);', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 10; ' + loopVar + '++) {', '  Turtle.penColour(' + color_random + ');', '  Turtle.turnLeft(36);', '  Turtle.moveForward(' + value_length + ' / 2);', '  Turtle.moveBackward(' + value_length + '/ 2);', '}', 'Turtle.turnRight(198);', 'Turtle.jumpForward(' + value_length + ');', 'Turtle.turnRight(180);\n'].join('\n');
  };
}

/**
 * Draw a snowflake function call block
 */
function installDrawASnowflake(blockly, generator, gensym) {

  var title = msg.drawASnowflake();

  blockly.Blocks.draw_a_snowflake = makeBlockInitializer(title);

  generator.draw_a_snowflake = function () {
    var loopVar = gensym('count');

    return ['for (var ' + loopVar + ' = 0; ' + loopVar + ' < 8; ' + loopVar + '++) {', '  Turtle.penColour("#7fffd4");', '  Turtle.moveForward(30);', '  Turtle.turnRight(90);', '  Turtle.moveForward(15);', '  Turtle.turnRight(90);', '  Turtle.penColour("#0000cd");', '  Turtle.moveForward(15);', '  Turtle.turnRight(90);', '  Turtle.moveForward(30);', '  Turtle.turnRight(45);', '}\n'].join('\n');
  };
}

/**
 * Draw a star function call block
 */
function installDrawAStar(blockly, generator, gensym) {

  var title = msg.drawAStar();

  blockly.Blocks.draw_a_star = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_star = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');

    return ['Turtle.turnRight(18);', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 5; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnRight(144);', '}', 'Turtle.turnLeft(18);\n'].join('\n');
  };
}

/**
 * Draw a robot function call block
 */
function installDrawARobot(blockly, generator, gensym) {

  var title = msg.drawARobot();

  blockly.Blocks.draw_a_robot = makeBlockInitializer(title);

  generator.draw_a_robot = function () {
    var loopVar = gensym('count');

    return ['Turtle.turnLeft(90);', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 4; ' + loopVar + '++) {', '  Turtle.moveForward(20);', '  Turtle.turnRight(90);', '}', 'Turtle.turnRight(90);', 'Turtle.moveBackward(10);', 'Turtle.moveForward(40);', 'Turtle.turnRight(90);', 'Turtle.moveForward(80);', 'Turtle.turnRight(90);', 'Turtle.moveForward(40);', 'Turtle.turnRight(90);', 'Turtle.moveForward(80);', 'Turtle.moveBackward(15);', 'Turtle.turnLeft(120);', 'Turtle.moveForward(40);', 'Turtle.moveBackward(40);', 'Turtle.turnRight(30);', 'Turtle.moveBackward(40);', 'Turtle.turnRight(210);', 'Turtle.moveForward(40);', 'Turtle.moveBackward(40);', 'Turtle.turnRight(60);', 'Turtle.moveForward(115);', 'Turtle.moveBackward(50);', 'Turtle.turnRight(90);', 'Turtle.moveForward(40);', 'Turtle.turnLeft(90);', 'Turtle.moveForward(50);\n'].join('\n');
  };
}

/**
 * Draw a robot function call block
 */
function installDrawARocket(blockly, generator, gensym) {

  var title = msg.drawARocket();

  blockly.Blocks.draw_a_rocket = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_rocket = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');
    var loopVar2 = gensym('count');

    return ['Turtle.penColour("#ff0000");', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 3; ' + loopVar + '++) {', '  Turtle.moveForward(20);', '  Turtle.turnLeft(120);', '}', 'Turtle.penColour("#000000");', 'Turtle.turnLeft(90);', 'Turtle.jumpForward(20);', 'Turtle.moveForward(' + value_length + ');', 'Turtle.turnRight(90);', 'Turtle.moveForward(20);', 'Turtle.turnRight(90);', 'Turtle.moveForward(' + value_length + ');', 'Turtle.turnRight(90);', 'Turtle.moveForward(20);', 'Turtle.turnRight(90);', 'Turtle.moveForward(' + value_length + ');', 'Turtle.turnRight(90);', 'for (var ' + loopVar2 + ' = 0; ' + loopVar2 + ' < 3; ' + loopVar2 + '++) {', '  Turtle.moveForward(20);', '  Turtle.turnLeft(120);', '}\n'].join('\n');
  };
}

/**
 * Draw a planet function call block
 */
function installDrawAPlanet(blockly, generator, gensym) {

  var title = msg.drawAPlanet();

  blockly.Blocks.draw_a_planet = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_a_planet = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');

    return ['Turtle.penColour("#808080");', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 360; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.moveBackward(' + value_length + ');', '  Turtle.turnRight(1);', '}\n'].join('\n');
  };
}

/**
 * Draw upper wave function call block
 */
function installDrawUpperWave(blockly, generator, gensym) {

  var title = msg.drawUpperWave();

  blockly.Blocks.draw_upper_wave = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_upper_wave = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');

    return ['Turtle.penColour("#0000cd");', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 10; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnRight(18);', '}\n'].join('\n');
  };
}

/**
 * Draw lower wave function call block
 */
function installDrawLowerWave(blockly, generator, gensym) {

  var title = msg.drawLowerWave();

  blockly.Blocks.draw_lower_wave = makeBlockInitializer(title, LENGTH_PARAM);

  generator.draw_lower_wave = function () {
    var value_length = generator.valueToCode(this, 'VALUE', generator.ORDER_ATOMIC);
    var loopVar = gensym('count');

    return ['Turtle.penColour("#0000cd");', 'for (var ' + loopVar + ' = 0; ' + loopVar + ' < 10; ' + loopVar + '++) {', '  Turtle.moveForward(' + value_length + ');', '  Turtle.turnLeft(18);', '}\n'].join('\n');
  };
}

function installCreateASnowflakeDropdown(blockly, generator, gensym) {
  var snowflakes = [[msg.createSnowflakeSquare(), 'square'], [msg.createSnowflakeParallelogram(), 'parallelogram'], [msg.createSnowflakeLine(), 'line'], [msg.createSnowflakeSpiral(), 'spiral'], [msg.createSnowflakeFlower(), 'flower'], [msg.createSnowflakeFractal(), 'fractal'], [msg.createSnowflakeRandom(), 'random']];

  blockly.Blocks.create_snowflake_dropdown = {
    // We use custom initialization (instead of makeBlockInitializer) here
    // because each initialization needs a new instance of the FieldDropdown.
    init: function () {
      this.setHSV(94, 0.84, 0.60);

      var title = new blockly.FieldDropdown(snowflakes);
      this.appendDummyInput().appendTitle(title, 'TYPE');

      this.setPreviousStatement(true);
      this.setNextStatement(true);
      this.setTooltip('');
    }
  };

  generator.create_snowflake_dropdown = function () {
    var type = this.getTitleValue('TYPE');
    return "Turtle.drawSnowflake('" + type + "', 'block_id_" + this.id + "');";
  };
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9pbml0Q29udGVudC9jdXN0b21MZXZlbEJsb2Nrcy5qcyJdLCJuYW1lcyI6WyJtc2ciLCJyZXF1aXJlIiwiZXhwb3J0cyIsImluc3RhbGwiLCJibG9ja2x5IiwiZ2VuZXJhdG9yIiwiZ2Vuc3ltIiwiaW5zdGFsbERyYXdBU3F1YXJlIiwiaW5zdGFsbENyZWF0ZUFDaXJjbGUiLCJpbnN0YWxsQ3JlYXRlQVNub3dmbGFrZUJyYW5jaCIsImluc3RhbGxEcmF3QVRyaWFuZ2xlIiwiaW5zdGFsbERyYXdBSG91c2UiLCJpbnN0YWxsRHJhd0FGbG93ZXIiLCJpbnN0YWxsRHJhd0FTbm93Zmxha2UiLCJpbnN0YWxsRHJhd0FIZXhhZ29uIiwiaW5zdGFsbERyYXdBU3RhciIsImluc3RhbGxEcmF3QVJvYm90IiwiaW5zdGFsbERyYXdBUm9ja2V0IiwiaW5zdGFsbERyYXdBUGxhbmV0IiwiaW5zdGFsbERyYXdBUmhvbWJ1cyIsImluc3RhbGxEcmF3VXBwZXJXYXZlIiwiaW5zdGFsbERyYXdMb3dlcldhdmUiLCJpbnN0YWxsQ3JlYXRlQVNub3dmbGFrZURyb3Bkb3duIiwiTEVOR1RIX1BBUkFNIiwibGVuZ3RoUGFyYW1ldGVyIiwiY3JlYXRlQUNpcmNsZUNvZGUiLCJzaXplIiwiaW5kZW50IiwibG9vcFZhciIsImpvaW4iLCJtYWtlQmxvY2tJbml0aWFsaXplciIsInRpdGxlIiwicGFyYW1ldGVyIiwiaW5pdCIsInNldEhTViIsImFwcGVuZER1bW15SW5wdXQiLCJhcHBlbmRUaXRsZSIsInVuZGVmaW5lZCIsImFwcGVuZFZhbHVlSW5wdXQiLCJzZXRBbGlnbiIsIkJsb2NrbHkiLCJBTElHTl9SSUdIVCIsInNldENoZWNrIiwiQmxvY2tWYWx1ZVR5cGUiLCJOVU1CRVIiLCJzZXRQcmV2aW91c1N0YXRlbWVudCIsInNldE5leHRTdGF0ZW1lbnQiLCJzZXRUb29sdGlwIiwiZHJhd0FTcXVhcmUiLCJCbG9ja3MiLCJkcmF3X2Ffc3F1YXJlX2N1c3RvbSIsInZhbHVlX2xlbmd0aCIsInZhbHVlVG9Db2RlIiwiT1JERVJfQVRPTUlDIiwiY3JlYXRlQUNpcmNsZSIsInBhcmFtIiwic2l6ZVBhcmFtZXRlciIsImNyZWF0ZV9hX2NpcmNsZSIsImNyZWF0ZV9hX2NpcmNsZV9zaXplIiwiY3JlYXRlQVNub3dmbGFrZUJyYW5jaCIsImNyZWF0ZV9hX3Nub3dmbGFrZV9icmFuY2giLCJsb29wVmFyMiIsImRyYXdBUmhvbWJ1cyIsImRyYXdfYV9yaG9tYnVzIiwiZHJhd0FUcmlhbmdsZSIsImRyYXdfYV90cmlhbmdsZSIsImRyYXdBSGV4YWdvbiIsImRyYXdfYV9oZXhhZ29uIiwiZHJhd0FIb3VzZSIsImRyYXdfYV9ob3VzZSIsImRyYXdBRmxvd2VyIiwiZHJhd19hX2Zsb3dlciIsImNvbG9yX3JhbmRvbSIsImNvbG91cl9yYW5kb20iLCJkcmF3QVNub3dmbGFrZSIsImRyYXdfYV9zbm93Zmxha2UiLCJkcmF3QVN0YXIiLCJkcmF3X2Ffc3RhciIsImRyYXdBUm9ib3QiLCJkcmF3X2Ffcm9ib3QiLCJkcmF3QVJvY2tldCIsImRyYXdfYV9yb2NrZXQiLCJkcmF3QVBsYW5ldCIsImRyYXdfYV9wbGFuZXQiLCJkcmF3VXBwZXJXYXZlIiwiZHJhd191cHBlcl93YXZlIiwiZHJhd0xvd2VyV2F2ZSIsImRyYXdfbG93ZXJfd2F2ZSIsInNub3dmbGFrZXMiLCJjcmVhdGVTbm93Zmxha2VTcXVhcmUiLCJjcmVhdGVTbm93Zmxha2VQYXJhbGxlbG9ncmFtIiwiY3JlYXRlU25vd2ZsYWtlTGluZSIsImNyZWF0ZVNub3dmbGFrZVNwaXJhbCIsImNyZWF0ZVNub3dmbGFrZUZsb3dlciIsImNyZWF0ZVNub3dmbGFrZUZyYWN0YWwiLCJjcmVhdGVTbm93Zmxha2VSYW5kb20iLCJjcmVhdGVfc25vd2ZsYWtlX2Ryb3Bkb3duIiwiRmllbGREcm9wZG93biIsInR5cGUiLCJnZXRUaXRsZVZhbHVlIiwiaWQiXSwibWFwcGluZ3MiOiJBQUFBOzs7O0FBSUEsSUFBSUEsTUFBTUMsUUFBUSxVQUFSLENBQVY7O0FBR0FDLFFBQVFDLE9BQVIsR0FBa0IsVUFBVUMsT0FBVixFQUFtQkMsU0FBbkIsRUFBOEJDLE1BQTlCLEVBQXNDO0FBQ3ZEQyxxQkFBbUJILE9BQW5CLEVBQTRCQyxTQUE1QixFQUF1Q0MsTUFBdkM7QUFDQUUsdUJBQXFCSixPQUFyQixFQUE4QkMsU0FBOUIsRUFBeUNDLE1BQXpDO0FBQ0FHLGdDQUE4QkwsT0FBOUIsRUFBdUNDLFNBQXZDLEVBQWtEQyxNQUFsRDtBQUNBSSx1QkFBcUJOLE9BQXJCLEVBQThCQyxTQUE5QixFQUF5Q0MsTUFBekM7QUFDQUssb0JBQWtCUCxPQUFsQixFQUEyQkMsU0FBM0IsRUFBc0NDLE1BQXRDO0FBQ0FNLHFCQUFtQlIsT0FBbkIsRUFBNEJDLFNBQTVCLEVBQXVDQyxNQUF2QztBQUNBTyx3QkFBc0JULE9BQXRCLEVBQStCQyxTQUEvQixFQUEwQ0MsTUFBMUM7QUFDQVEsc0JBQW9CVixPQUFwQixFQUE2QkMsU0FBN0IsRUFBd0NDLE1BQXhDO0FBQ0FTLG1CQUFpQlgsT0FBakIsRUFBMEJDLFNBQTFCLEVBQXFDQyxNQUFyQztBQUNBVSxvQkFBa0JaLE9BQWxCLEVBQTJCQyxTQUEzQixFQUFzQ0MsTUFBdEM7QUFDQVcscUJBQW1CYixPQUFuQixFQUE0QkMsU0FBNUIsRUFBdUNDLE1BQXZDO0FBQ0FZLHFCQUFtQmQsT0FBbkIsRUFBNEJDLFNBQTVCLEVBQXVDQyxNQUF2QztBQUNBYSxzQkFBb0JmLE9BQXBCLEVBQTZCQyxTQUE3QixFQUF3Q0MsTUFBeEM7QUFDQWMsdUJBQXFCaEIsT0FBckIsRUFBOEJDLFNBQTlCLEVBQXlDQyxNQUF6QztBQUNBZSx1QkFBcUJqQixPQUFyQixFQUE4QkMsU0FBOUIsRUFBeUNDLE1BQXpDOztBQUVBZ0Isa0NBQWdDbEIsT0FBaEMsRUFBeUNDLFNBQXpDLEVBQW9EQyxNQUFwRDtBQUNBLENBbEJEOztBQW9CQSxJQUFJaUIsZUFBZXZCLElBQUl3QixlQUFKLEVBQW5COztBQUVBLFNBQVNDLGlCQUFULENBQTJCQyxJQUEzQixFQUFpQ3BCLE1BQWpDLEVBQXlDcUIsTUFBekMsRUFBaUQ7QUFDL0MsTUFBSUMsVUFBVXRCLE9BQU8sT0FBUCxDQUFkO0FBQ0FxQixXQUFTQSxVQUFVLEVBQW5CO0FBQ0EsU0FBTyxDQUNMQSxTQUFTLG9CQURKLEVBRUxBLFNBQVMsV0FBVCxHQUF1QkMsT0FBdkIsR0FBaUMsUUFBakMsR0FBNENBLE9BQTVDLEdBQXNELFNBQXRELEdBQ0FELE1BREEsR0FDZUMsT0FEZixHQUN5QixPQUhwQixFQUlMRCxTQUFTLHVCQUFULEdBQW1DRCxJQUFuQyxHQUEwQyxJQUpyQyxFQUtMQyxTQUFTLHlCQUxKLEVBTUxBLFNBQVMsS0FOSixFQU1XRSxJQU5YLENBTWdCLElBTmhCLENBQVA7QUFPRDs7QUFFRDs7Ozs7Ozs7QUFRQSxTQUFTQyxvQkFBVCxDQUE4QkMsS0FBOUIsRUFBcUNDLFNBQXJDLEVBQWdEO0FBQzlDLFNBQU87QUFDTEMsVUFBTSxZQUFZO0FBQ2hCLFdBQUtDLE1BQUwsQ0FBWSxFQUFaLEVBQWdCLElBQWhCLEVBQXNCLElBQXRCOztBQUVBLFdBQUtDLGdCQUFMLEdBQXdCQyxXQUF4QixDQUFvQ0wsS0FBcEM7O0FBRUEsVUFBSUMsY0FBY0ssU0FBbEIsRUFBNkI7QUFDM0IsYUFBS0MsZ0JBQUwsQ0FBc0IsT0FBdEIsRUFDS0MsUUFETCxDQUNjQyxRQUFRQyxXQUR0QixFQUVLQyxRQUZMLENBRWNGLFFBQVFHLGNBQVIsQ0FBdUJDLE1BRnJDLEVBR0tSLFdBSEwsQ0FHaUJKLFlBQVksR0FIN0I7QUFJRDs7QUFFRCxXQUFLYSxvQkFBTCxDQUEwQixJQUExQjtBQUNBLFdBQUtDLGdCQUFMLENBQXNCLElBQXRCO0FBQ0EsV0FBS0MsVUFBTCxDQUFnQixFQUFoQjtBQUNEO0FBaEJJLEdBQVA7QUFrQkQ7O0FBRUQ7OztBQUdBLFNBQVN4QyxrQkFBVCxDQUE0QkgsT0FBNUIsRUFBcUNDLFNBQXJDLEVBQWdEQyxNQUFoRCxFQUF3RDtBQUN0RDtBQUNBO0FBQ0EsTUFBSXlCLFFBQVEvQixJQUFJZ0QsV0FBSixFQUFaOztBQUVBNUMsVUFBUTZDLE1BQVIsQ0FBZUMsb0JBQWYsR0FBc0NwQixxQkFBcUJDLEtBQXJCLEVBQTRCUixZQUE1QixDQUF0Qzs7QUFFQWxCLFlBQVU2QyxvQkFBVixHQUFpQyxZQUFZO0FBQzNDO0FBQ0EsUUFBSUMsZUFBZTlDLFVBQVUrQyxXQUFWLENBQ2YsSUFEZSxFQUNULE9BRFMsRUFDQS9DLFVBQVVnRCxZQURWLENBQW5CO0FBRUEsUUFBSXpCLFVBQVV0QixPQUFPLE9BQVAsQ0FBZDtBQUNBLFdBQU8sQ0FDSCxrQkFERyxFQUVILGNBQWNzQixPQUFkLEdBQXdCLFFBQXhCLEdBQW1DQSxPQUFuQyxHQUE2QyxRQUE3QyxHQUNNQSxPQUROLEdBQ2dCLE9BSGIsRUFJSCwwQkFBMEJ1QixZQUExQixHQUF5QyxJQUp0QyxFQUtILHlCQUxHLEVBTUgsS0FORyxFQU1JdEIsSUFOSixDQU1TLElBTlQsQ0FBUDtBQU9ELEdBWkQ7QUFhRDs7QUFFRDs7OztBQUlBLFNBQVNyQixvQkFBVCxDQUE4QkosT0FBOUIsRUFBdUNDLFNBQXZDLEVBQWtEQyxNQUFsRCxFQUEwRDs7QUFFeEQsTUFBSXlCLFFBQVEvQixJQUFJc0QsYUFBSixFQUFaO0FBQ0EsTUFBSUMsUUFBUXZELElBQUl3RCxhQUFKLEVBQVo7O0FBRUFwRCxVQUFRNkMsTUFBUixDQUFlUSxlQUFmLEdBQWlDM0IscUJBQXFCQyxLQUFyQixDQUFqQzs7QUFFQTNCLFVBQVE2QyxNQUFSLENBQWVTLG9CQUFmLEdBQXNDNUIscUJBQXFCQyxLQUFyQixFQUE0QndCLEtBQTVCLENBQXRDOztBQUVBbEQsWUFBVW9ELGVBQVYsR0FBNEIsWUFBWTtBQUN0QyxXQUFPaEMsa0JBQWtCLEVBQWxCLEVBQXNCbkIsTUFBdEIsQ0FBUDtBQUNELEdBRkQ7O0FBSUFELFlBQVVxRCxvQkFBVixHQUFpQyxZQUFZO0FBQzNDLFFBQUloQyxPQUFPckIsVUFBVStDLFdBQVYsQ0FBc0IsSUFBdEIsRUFBNEIsT0FBNUIsRUFBcUMvQyxVQUFVZ0QsWUFBL0MsQ0FBWDtBQUNBLFdBQU81QixrQkFBa0JDLElBQWxCLEVBQXdCcEIsTUFBeEIsQ0FBUDtBQUNELEdBSEQ7QUFJRDs7QUFFRDs7O0FBR0EsU0FBU0csNkJBQVQsQ0FBdUNMLE9BQXZDLEVBQWdEQyxTQUFoRCxFQUEyREMsTUFBM0QsRUFBbUU7O0FBRWpFLE1BQUl5QixRQUFRL0IsSUFBSTJELHNCQUFKLEVBQVo7O0FBRUF2RCxVQUFRNkMsTUFBUixDQUFlVyx5QkFBZixHQUEyQzlCLHFCQUFxQkMsS0FBckIsQ0FBM0M7O0FBRUExQixZQUFVdUQseUJBQVYsR0FBc0MsWUFBWTtBQUNoRCxRQUFJaEMsVUFBVXRCLE9BQU8sT0FBUCxDQUFkO0FBQ0EsUUFBSXVELFdBQVd2RCxPQUFPLE9BQVAsQ0FBZjtBQUNBLFdBQU8sQ0FDTCw4QkFESyxFQUVMLHlCQUZLLEVBR0wsc0JBSEssRUFJTCxjQUFjc0IsT0FBZCxHQUF3QixRQUF4QixHQUFtQ0EsT0FBbkMsR0FBNkMsUUFBN0MsR0FBd0RBLE9BQXhELEdBQWtFLE9BSjdELEVBS0wsZ0JBQWdCaUMsUUFBaEIsR0FBMkIsUUFBM0IsR0FBc0NBLFFBQXRDLEdBQWlELFFBQWpELEdBQTREQSxRQUE1RCxHQUF1RSxPQUxsRSxFQU1MLDZCQU5LLEVBT0wsOEJBUEssRUFRTCwyQkFSSyxFQVNMLEtBVEssRUFVTCx3QkFWSyxFQVdMLDRCQVhLLEVBWUwsd0JBWkssRUFhTCxHQWJLLEVBY0wseUJBZEssRUFjc0JoQyxJQWR0QixDQWMyQixJQWQzQixDQUFQO0FBZUQsR0FsQkQ7QUFtQkQ7O0FBR0Q7OztBQUdBLFNBQVNWLG1CQUFULENBQTZCZixPQUE3QixFQUFzQ0MsU0FBdEMsRUFBaURDLE1BQWpELEVBQXlEOztBQUV2RCxNQUFJeUIsUUFBUS9CLElBQUk4RCxZQUFKLEVBQVo7O0FBRUExRCxVQUFRNkMsTUFBUixDQUFlYyxjQUFmLEdBQWdDakMscUJBQXFCQyxLQUFyQixFQUE0QlIsWUFBNUIsQ0FBaEM7O0FBRUFsQixZQUFVMEQsY0FBVixHQUEyQixZQUFZO0FBQ3JDLFFBQUlaLGVBQWU5QyxVQUFVK0MsV0FBVixDQUNmLElBRGUsRUFDVCxPQURTLEVBQ0EvQyxVQUFVZ0QsWUFEVixDQUFuQjtBQUVBLFFBQUl6QixVQUFVdEIsT0FBTyxPQUFQLENBQWQ7QUFDQSxXQUFPLENBQ0wsY0FBY3NCLE9BQWQsR0FBd0IsUUFBeEIsR0FBbUNBLE9BQW5DLEdBQTZDLFFBQTdDLEdBQ01BLE9BRE4sR0FDZ0IsT0FGWCxFQUdMLDBCQUEwQnVCLFlBQTFCLEdBQXlDLElBSHBDLEVBSUwseUJBSkssRUFLTCwwQkFBMEJBLFlBQTFCLEdBQXlDLElBTHBDLEVBTUwsMEJBTkssRUFPTCxLQVBLLEVBT0V0QixJQVBGLENBT08sSUFQUCxDQUFQO0FBUUQsR0FaRDtBQWFEOztBQUVEOzs7QUFHQSxTQUFTbkIsb0JBQVQsQ0FBOEJOLE9BQTlCLEVBQXVDQyxTQUF2QyxFQUFrREMsTUFBbEQsRUFBMEQ7O0FBRXhELE1BQUl5QixRQUFRL0IsSUFBSWdFLGFBQUosRUFBWjs7QUFFQTVELFVBQVE2QyxNQUFSLENBQWVnQixlQUFmLEdBQWlDbkMscUJBQXFCQyxLQUFyQixFQUE0QlIsWUFBNUIsQ0FBakM7O0FBRUFsQixZQUFVNEQsZUFBVixHQUE0QixZQUFZO0FBQ3RDLFFBQUlkLGVBQWU5QyxVQUFVK0MsV0FBVixDQUNmLElBRGUsRUFDVCxPQURTLEVBQ0EvQyxVQUFVZ0QsWUFEVixDQUFuQjtBQUVBLFFBQUl6QixVQUFVdEIsT0FBTyxPQUFQLENBQWQ7QUFDQSxXQUFPLENBQ0gsb0JBREcsRUFFSCxjQUFjc0IsT0FBZCxHQUF3QixRQUF4QixHQUFtQ0EsT0FBbkMsR0FBNkMsUUFBN0MsR0FDTUEsT0FETixHQUNnQixPQUhiLEVBSUgsMEJBQTBCdUIsWUFBMUIsR0FBeUMsSUFKdEMsRUFLSCx5QkFMRyxFQU1ILEtBTkcsRUFNSXRCLElBTkosQ0FNUyxJQU5ULENBQVA7QUFPRCxHQVhEO0FBWUQ7O0FBRUQ7OztBQUdBLFNBQVNmLG1CQUFULENBQTZCVixPQUE3QixFQUFzQ0MsU0FBdEMsRUFBaURDLE1BQWpELEVBQXlEOztBQUV2RCxNQUFJeUIsUUFBUS9CLElBQUlrRSxZQUFKLEVBQVo7O0FBRUE5RCxVQUFRNkMsTUFBUixDQUFla0IsY0FBZixHQUFnQ3JDLHFCQUFxQkMsS0FBckIsRUFBNEJSLFlBQTVCLENBQWhDOztBQUVBbEIsWUFBVThELGNBQVYsR0FBMkIsWUFBWTtBQUNyQyxRQUFJaEIsZUFBZTlDLFVBQVUrQyxXQUFWLENBQ2YsSUFEZSxFQUNULE9BRFMsRUFDQS9DLFVBQVVnRCxZQURWLENBQW5CO0FBRUEsUUFBSXpCLFVBQVV0QixPQUFPLE9BQVAsQ0FBZDtBQUNBLFdBQU8sQ0FDSCxvQkFERyxFQUVILGNBQWNzQixPQUFkLEdBQXdCLFFBQXhCLEdBQW1DQSxPQUFuQyxHQUE2QyxRQUE3QyxHQUNNQSxPQUROLEdBQ2dCLE9BSGIsRUFJSCwwQkFBMEJ1QixZQUExQixHQUF5QyxJQUp0QyxFQUtILHdCQUxHLEVBTUgsS0FORyxFQU1JdEIsSUFOSixDQU1TLElBTlQsQ0FBUDtBQU9ELEdBWEQ7QUFZRDs7QUFFRDs7O0FBR0EsU0FBU2xCLGlCQUFULENBQTJCUCxPQUEzQixFQUFvQ0MsU0FBcEMsRUFBK0NDLE1BQS9DLEVBQXVEOztBQUVyRCxNQUFJeUIsUUFBUS9CLElBQUlvRSxVQUFKLEVBQVo7O0FBRUFoRSxVQUFRNkMsTUFBUixDQUFlb0IsWUFBZixHQUE4QnZDLHFCQUFxQkMsS0FBckIsRUFBNEJSLFlBQTVCLENBQTlCOztBQUVBbEIsWUFBVWdFLFlBQVYsR0FBeUIsWUFBWTtBQUNuQyxRQUFJbEIsZUFBZTlDLFVBQVUrQyxXQUFWLENBQ2YsSUFEZSxFQUNULE9BRFMsRUFDQS9DLFVBQVVnRCxZQURWLENBQW5CO0FBRUEsUUFBSXpCLFVBQVV0QixPQUFPLE9BQVAsQ0FBZDtBQUNBLFdBQU8sQ0FDTCxjQUFjc0IsT0FBZCxHQUF3QixRQUF4QixHQUFtQ0EsT0FBbkMsR0FBNkMsUUFBN0MsR0FBd0RBLE9BQXhELEdBQWtFLE9BRDdELEVBRUwsMEJBQTBCdUIsWUFBMUIsR0FBeUMsSUFGcEMsRUFHTCx3QkFISyxFQUlMLEdBSkssRUFLTCxzQkFMSyxFQU1MLHdCQUF3QkEsWUFBeEIsR0FBdUMsSUFObEMsRUFPTCx1QkFQSyxFQVFMLGNBQWN2QixPQUFkLEdBQXdCLFFBQXhCLEdBQW1DQSxPQUFuQyxHQUE2QyxRQUE3QyxHQUF3REEsT0FBeEQsR0FBa0UsT0FSN0QsRUFTTCwwQkFBMEJ1QixZQUExQixHQUF5QyxJQVRwQyxFQVVMLHlCQVZLLEVBV0wsR0FYSyxFQVlMLHVCQVpLLEVBYUwsd0JBQXdCQSxZQUF4QixHQUF1QyxJQWJsQyxFQWNMLHdCQWRLLEVBY3FCdEIsSUFkckIsQ0FjMEIsSUFkMUIsQ0FBUDtBQWVELEdBbkJEO0FBb0JEOztBQUVEOzs7QUFHQSxTQUFTakIsa0JBQVQsQ0FBNEJSLE9BQTVCLEVBQXFDQyxTQUFyQyxFQUFnREMsTUFBaEQsRUFBd0Q7O0FBRXRELE1BQUl5QixRQUFRL0IsSUFBSXNFLFdBQUosRUFBWjs7QUFFQWxFLFVBQVE2QyxNQUFSLENBQWVzQixhQUFmLEdBQStCekMscUJBQXFCQyxLQUFyQixFQUE0QlIsWUFBNUIsQ0FBL0I7O0FBRUFsQixZQUFVa0UsYUFBVixHQUEwQixZQUFZO0FBQ3BDLFFBQUlwQixlQUFlOUMsVUFBVStDLFdBQVYsQ0FDZixJQURlLEVBQ1QsT0FEUyxFQUNBL0MsVUFBVWdELFlBRFYsQ0FBbkI7QUFFQSxRQUFJekIsVUFBVXRCLE9BQU8sT0FBUCxDQUFkOztBQUVBLFFBQUlrRSxlQUFlbkUsVUFBVW9FLGFBQVYsR0FBMEIsQ0FBMUIsQ0FBbkI7QUFDQSxXQUFPLENBQ0wsOEJBREssRUFFTCx3QkFBd0J0QixZQUF4QixHQUF1QyxJQUZsQyxFQUdMLHNCQUhLLEVBSUwsY0FBY3ZCLE9BQWQsR0FBd0IsUUFBeEIsR0FBbUNBLE9BQW5DLEdBQTZDLFNBQTdDLEdBQXlEQSxPQUF6RCxHQUFtRSxPQUo5RCxFQUtMLHdCQUF3QjRDLFlBQXhCLEdBQXVDLElBTGxDLEVBTUwsd0JBTkssRUFPTCwwQkFBMEJyQixZQUExQixHQUF5QyxRQVBwQyxFQVFMLDJCQUEyQkEsWUFBM0IsR0FBMEMsT0FSckMsRUFTTCxHQVRLLEVBVUwsd0JBVkssRUFXTCx3QkFBd0JBLFlBQXhCLEdBQXVDLElBWGxDLEVBWUwsMEJBWkssRUFZdUJ0QixJQVp2QixDQVk0QixJQVo1QixDQUFQO0FBYUQsR0FuQkQ7QUFvQkQ7O0FBRUQ7OztBQUdBLFNBQVNoQixxQkFBVCxDQUErQlQsT0FBL0IsRUFBd0NDLFNBQXhDLEVBQW1EQyxNQUFuRCxFQUEyRDs7QUFFekQsTUFBSXlCLFFBQVEvQixJQUFJMEUsY0FBSixFQUFaOztBQUVBdEUsVUFBUTZDLE1BQVIsQ0FBZTBCLGdCQUFmLEdBQWtDN0MscUJBQXFCQyxLQUFyQixDQUFsQzs7QUFFQTFCLFlBQVVzRSxnQkFBVixHQUE2QixZQUFZO0FBQ3ZDLFFBQUkvQyxVQUFVdEIsT0FBTyxPQUFQLENBQWQ7O0FBRUEsV0FBTyxDQUNMLGNBQWNzQixPQUFkLEdBQXdCLFFBQXhCLEdBQW1DQSxPQUFuQyxHQUE2QyxRQUE3QyxHQUF3REEsT0FBeEQsR0FBa0UsT0FEN0QsRUFFTCxnQ0FGSyxFQUdMLDJCQUhLLEVBSUwseUJBSkssRUFLTCwyQkFMSyxFQU1MLHlCQU5LLEVBT0wsZ0NBUEssRUFRTCwyQkFSSyxFQVNMLHlCQVRLLEVBVUwsMkJBVkssRUFXTCx5QkFYSyxFQVlMLEtBWkssRUFZRUMsSUFaRixDQVlPLElBWlAsQ0FBUDtBQWFELEdBaEJEO0FBaUJEOztBQUVEOzs7QUFHQSxTQUFTZCxnQkFBVCxDQUEwQlgsT0FBMUIsRUFBbUNDLFNBQW5DLEVBQThDQyxNQUE5QyxFQUFzRDs7QUFFcEQsTUFBSXlCLFFBQVEvQixJQUFJNEUsU0FBSixFQUFaOztBQUVBeEUsVUFBUTZDLE1BQVIsQ0FBZTRCLFdBQWYsR0FBNkIvQyxxQkFBcUJDLEtBQXJCLEVBQTRCUixZQUE1QixDQUE3Qjs7QUFFQWxCLFlBQVV3RSxXQUFWLEdBQXdCLFlBQVk7QUFDbEMsUUFBSTFCLGVBQWU5QyxVQUFVK0MsV0FBVixDQUNmLElBRGUsRUFDVCxPQURTLEVBQ0EvQyxVQUFVZ0QsWUFEVixDQUFuQjtBQUVBLFFBQUl6QixVQUFVdEIsT0FBTyxPQUFQLENBQWQ7O0FBRUEsV0FBTyxDQUNMLHVCQURLLEVBRUwsY0FBY3NCLE9BQWQsR0FBd0IsUUFBeEIsR0FBbUNBLE9BQW5DLEdBQTZDLFFBQTdDLEdBQXdEQSxPQUF4RCxHQUFrRSxPQUY3RCxFQUdMLDBCQUEwQnVCLFlBQTFCLEdBQXlDLElBSHBDLEVBSUwsMEJBSkssRUFLTCxHQUxLLEVBTUwsd0JBTkssRUFNcUJ0QixJQU5yQixDQU0wQixJQU4xQixDQUFQO0FBT0QsR0FaRDtBQWFEOztBQUVEOzs7QUFHQSxTQUFTYixpQkFBVCxDQUEyQlosT0FBM0IsRUFBb0NDLFNBQXBDLEVBQStDQyxNQUEvQyxFQUF1RDs7QUFFckQsTUFBSXlCLFFBQVEvQixJQUFJOEUsVUFBSixFQUFaOztBQUVBMUUsVUFBUTZDLE1BQVIsQ0FBZThCLFlBQWYsR0FBOEJqRCxxQkFBcUJDLEtBQXJCLENBQTlCOztBQUVBMUIsWUFBVTBFLFlBQVYsR0FBeUIsWUFBWTtBQUNuQyxRQUFJbkQsVUFBVXRCLE9BQU8sT0FBUCxDQUFkOztBQUVBLFdBQU8sQ0FDTCxzQkFESyxFQUVMLGNBQWNzQixPQUFkLEdBQXdCLFFBQXhCLEdBQW1DQSxPQUFuQyxHQUE2QyxRQUE3QyxHQUF3REEsT0FBeEQsR0FBa0UsT0FGN0QsRUFHTCwyQkFISyxFQUlMLHlCQUpLLEVBS0wsR0FMSyxFQU1MLHVCQU5LLEVBT0wsMEJBUEssRUFRTCx5QkFSSyxFQVNMLHVCQVRLLEVBVUwseUJBVkssRUFXTCx1QkFYSyxFQVlMLHlCQVpLLEVBYUwsdUJBYkssRUFjTCx5QkFkSyxFQWVMLDBCQWZLLEVBZ0JMLHVCQWhCSyxFQWlCTCx5QkFqQkssRUFrQkwsMEJBbEJLLEVBbUJMLHVCQW5CSyxFQW9CTCwwQkFwQkssRUFxQkwsd0JBckJLLEVBc0JMLHlCQXRCSyxFQXVCTCwwQkF2QkssRUF3QkwsdUJBeEJLLEVBeUJMLDBCQXpCSyxFQTBCTCwwQkExQkssRUEyQkwsdUJBM0JLLEVBNEJMLHlCQTVCSyxFQTZCTCxzQkE3QkssRUE4QkwsMkJBOUJLLEVBOEJ3QkMsSUE5QnhCLENBOEI2QixJQTlCN0IsQ0FBUDtBQStCRCxHQWxDRDtBQW1DRDs7QUFHRDs7O0FBR0EsU0FBU1osa0JBQVQsQ0FBNEJiLE9BQTVCLEVBQXFDQyxTQUFyQyxFQUFnREMsTUFBaEQsRUFBd0Q7O0FBRXRELE1BQUl5QixRQUFRL0IsSUFBSWdGLFdBQUosRUFBWjs7QUFFQTVFLFVBQVE2QyxNQUFSLENBQWVnQyxhQUFmLEdBQStCbkQscUJBQXFCQyxLQUFyQixFQUE0QlIsWUFBNUIsQ0FBL0I7O0FBRUFsQixZQUFVNEUsYUFBVixHQUEwQixZQUFZO0FBQ3BDLFFBQUk5QixlQUFlOUMsVUFBVStDLFdBQVYsQ0FDZixJQURlLEVBQ1QsT0FEUyxFQUNBL0MsVUFBVWdELFlBRFYsQ0FBbkI7QUFFQSxRQUFJekIsVUFBVXRCLE9BQU8sT0FBUCxDQUFkO0FBQ0EsUUFBSXVELFdBQVd2RCxPQUFPLE9BQVAsQ0FBZjs7QUFFQSxXQUFPLENBQ0wsOEJBREssRUFFTCxjQUFjc0IsT0FBZCxHQUF3QixRQUF4QixHQUFtQ0EsT0FBbkMsR0FBNkMsUUFBN0MsR0FBd0RBLE9BQXhELEdBQWtFLE9BRjdELEVBR0wsMkJBSEssRUFJTCx5QkFKSyxFQUtMLEdBTEssRUFNTCw4QkFOSyxFQU9MLHNCQVBLLEVBUUwseUJBUkssRUFTTCx3QkFBd0J1QixZQUF4QixHQUF1QyxJQVRsQyxFQVVMLHVCQVZLLEVBV0wseUJBWEssRUFZTCx1QkFaSyxFQWFMLHdCQUF3QkEsWUFBeEIsR0FBdUMsSUFibEMsRUFjTCx1QkFkSyxFQWVMLHlCQWZLLEVBZ0JMLHVCQWhCSyxFQWlCTCx3QkFBd0JBLFlBQXhCLEdBQXVDLElBakJsQyxFQWtCTCx1QkFsQkssRUFtQkwsY0FBY1UsUUFBZCxHQUF5QixRQUF6QixHQUFvQ0EsUUFBcEMsR0FBK0MsUUFBL0MsR0FBMERBLFFBQTFELEdBQXFFLE9BbkJoRSxFQW9CTCwyQkFwQkssRUFxQkwseUJBckJLLEVBc0JMLEtBdEJLLEVBc0JFaEMsSUF0QkYsQ0FzQk8sSUF0QlAsQ0FBUDtBQXVCRCxHQTdCRDtBQThCRDs7QUFFRDs7O0FBR0EsU0FBU1gsa0JBQVQsQ0FBNEJkLE9BQTVCLEVBQXFDQyxTQUFyQyxFQUFnREMsTUFBaEQsRUFBd0Q7O0FBRXRELE1BQUl5QixRQUFRL0IsSUFBSWtGLFdBQUosRUFBWjs7QUFFQTlFLFVBQVE2QyxNQUFSLENBQWVrQyxhQUFmLEdBQStCckQscUJBQXFCQyxLQUFyQixFQUE0QlIsWUFBNUIsQ0FBL0I7O0FBRUFsQixZQUFVOEUsYUFBVixHQUEwQixZQUFZO0FBQ3BDLFFBQUloQyxlQUFlOUMsVUFBVStDLFdBQVYsQ0FDZixJQURlLEVBQ1QsT0FEUyxFQUNBL0MsVUFBVWdELFlBRFYsQ0FBbkI7QUFFQSxRQUFJekIsVUFBVXRCLE9BQU8sT0FBUCxDQUFkOztBQUdBLFdBQU8sQ0FDTCw4QkFESyxFQUVMLGNBQWNzQixPQUFkLEdBQXdCLFFBQXhCLEdBQW1DQSxPQUFuQyxHQUE2QyxVQUE3QyxHQUEwREEsT0FBMUQsR0FBb0UsT0FGL0QsRUFHTCwwQkFBMEJ1QixZQUExQixHQUF5QyxJQUhwQyxFQUlMLDJCQUEyQkEsWUFBM0IsR0FBMEMsSUFKckMsRUFLTCx3QkFMSyxFQU1MLEtBTkssRUFNRXRCLElBTkYsQ0FNTyxJQU5QLENBQVA7QUFPRCxHQWJEO0FBY0Q7O0FBRUQ7OztBQUdBLFNBQVNULG9CQUFULENBQThCaEIsT0FBOUIsRUFBdUNDLFNBQXZDLEVBQWtEQyxNQUFsRCxFQUEwRDs7QUFFeEQsTUFBSXlCLFFBQVEvQixJQUFJb0YsYUFBSixFQUFaOztBQUVBaEYsVUFBUTZDLE1BQVIsQ0FBZW9DLGVBQWYsR0FBaUN2RCxxQkFBcUJDLEtBQXJCLEVBQTRCUixZQUE1QixDQUFqQzs7QUFFQWxCLFlBQVVnRixlQUFWLEdBQTRCLFlBQVk7QUFDdEMsUUFBSWxDLGVBQWU5QyxVQUFVK0MsV0FBVixDQUNmLElBRGUsRUFDVCxPQURTLEVBQ0EvQyxVQUFVZ0QsWUFEVixDQUFuQjtBQUVBLFFBQUl6QixVQUFVdEIsT0FBTyxPQUFQLENBQWQ7O0FBRUEsV0FBTyxDQUNMLDhCQURLLEVBRUwsY0FBY3NCLE9BQWQsR0FBd0IsUUFBeEIsR0FBbUNBLE9BQW5DLEdBQTZDLFNBQTdDLEdBQXlEQSxPQUF6RCxHQUFtRSxPQUY5RCxFQUdMLDBCQUEwQnVCLFlBQTFCLEdBQXlDLElBSHBDLEVBSUwseUJBSkssRUFLTCxLQUxLLEVBS0V0QixJQUxGLENBS08sSUFMUCxDQUFQO0FBTUQsR0FYRDtBQVlEOztBQUVEOzs7QUFHQSxTQUFTUixvQkFBVCxDQUE4QmpCLE9BQTlCLEVBQXVDQyxTQUF2QyxFQUFrREMsTUFBbEQsRUFBMEQ7O0FBRXhELE1BQUl5QixRQUFRL0IsSUFBSXNGLGFBQUosRUFBWjs7QUFFQWxGLFVBQVE2QyxNQUFSLENBQWVzQyxlQUFmLEdBQWlDekQscUJBQXFCQyxLQUFyQixFQUE0QlIsWUFBNUIsQ0FBakM7O0FBRUFsQixZQUFVa0YsZUFBVixHQUE0QixZQUFZO0FBQ3RDLFFBQUlwQyxlQUFlOUMsVUFBVStDLFdBQVYsQ0FDZixJQURlLEVBQ1QsT0FEUyxFQUNBL0MsVUFBVWdELFlBRFYsQ0FBbkI7QUFFQSxRQUFJekIsVUFBVXRCLE9BQU8sT0FBUCxDQUFkOztBQUVBLFdBQU8sQ0FDTCw4QkFESyxFQUVMLGNBQWNzQixPQUFkLEdBQXdCLFFBQXhCLEdBQW1DQSxPQUFuQyxHQUE2QyxTQUE3QyxHQUF5REEsT0FBekQsR0FBbUUsT0FGOUQsRUFHTCwwQkFBMEJ1QixZQUExQixHQUF5QyxJQUhwQyxFQUlMLHdCQUpLLEVBS0wsS0FMSyxFQUtFdEIsSUFMRixDQUtPLElBTFAsQ0FBUDtBQU1ELEdBWEQ7QUFZRDs7QUFFRCxTQUFTUCwrQkFBVCxDQUF5Q2xCLE9BQXpDLEVBQWtEQyxTQUFsRCxFQUE2REMsTUFBN0QsRUFBcUU7QUFDbkUsTUFBSWtGLGFBQWEsQ0FDZixDQUFDeEYsSUFBSXlGLHFCQUFKLEVBQUQsRUFBOEIsUUFBOUIsQ0FEZSxFQUVmLENBQUN6RixJQUFJMEYsNEJBQUosRUFBRCxFQUFxQyxlQUFyQyxDQUZlLEVBR2YsQ0FBQzFGLElBQUkyRixtQkFBSixFQUFELEVBQTRCLE1BQTVCLENBSGUsRUFJZixDQUFDM0YsSUFBSTRGLHFCQUFKLEVBQUQsRUFBOEIsUUFBOUIsQ0FKZSxFQUtmLENBQUM1RixJQUFJNkYscUJBQUosRUFBRCxFQUE4QixRQUE5QixDQUxlLEVBTWYsQ0FBQzdGLElBQUk4RixzQkFBSixFQUFELEVBQStCLFNBQS9CLENBTmUsRUFPZixDQUFDOUYsSUFBSStGLHFCQUFKLEVBQUQsRUFBOEIsUUFBOUIsQ0FQZSxDQUFqQjs7QUFXQTNGLFVBQVE2QyxNQUFSLENBQWUrQyx5QkFBZixHQUEyQztBQUN6QztBQUNBO0FBQ0EvRCxVQUFNLFlBQVk7QUFDaEIsV0FBS0MsTUFBTCxDQUFZLEVBQVosRUFBZ0IsSUFBaEIsRUFBc0IsSUFBdEI7O0FBRUEsVUFBSUgsUUFBUSxJQUFJM0IsUUFBUTZGLGFBQVosQ0FBMEJULFVBQTFCLENBQVo7QUFDQSxXQUFLckQsZ0JBQUwsR0FBd0JDLFdBQXhCLENBQW9DTCxLQUFwQyxFQUEyQyxNQUEzQzs7QUFFQSxXQUFLYyxvQkFBTCxDQUEwQixJQUExQjtBQUNBLFdBQUtDLGdCQUFMLENBQXNCLElBQXRCO0FBQ0EsV0FBS0MsVUFBTCxDQUFnQixFQUFoQjtBQUNEO0FBWndDLEdBQTNDOztBQWVBMUMsWUFBVTJGLHlCQUFWLEdBQXNDLFlBQVk7QUFDaEQsUUFBSUUsT0FBTyxLQUFLQyxhQUFMLENBQW1CLE1BQW5CLENBQVg7QUFDQSxXQUFPLDJCQUEyQkQsSUFBM0IsR0FBa0MsZUFBbEMsR0FBb0QsS0FBS0UsRUFBekQsR0FBOEQsS0FBckU7QUFDRCxHQUhEO0FBSUQiLCJmaWxlIjoiY3VzdG9tTGV2ZWxCbG9ja3MuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIEEgc2V0IG9mIGJsb2NrcyB1c2VkIGJ5IHNvbWUgb2Ygb3VyIGN1c3RvbSBsZXZlbHMgKGkuZS4gYnVpbHQgYnkgbGV2ZWwgYnVpbGRlcilcbiAqL1xuXG52YXIgbXNnID0gcmVxdWlyZSgnLi9sb2NhbGUnKTtcblxuXG5leHBvcnRzLmluc3RhbGwgPSBmdW5jdGlvbiAoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pIHtcbiBpbnN0YWxsRHJhd0FTcXVhcmUoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pO1xuIGluc3RhbGxDcmVhdGVBQ2lyY2xlKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKTtcbiBpbnN0YWxsQ3JlYXRlQVNub3dmbGFrZUJyYW5jaChibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSk7XG4gaW5zdGFsbERyYXdBVHJpYW5nbGUoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pO1xuIGluc3RhbGxEcmF3QUhvdXNlKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKTtcbiBpbnN0YWxsRHJhd0FGbG93ZXIoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pO1xuIGluc3RhbGxEcmF3QVNub3dmbGFrZShibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSk7XG4gaW5zdGFsbERyYXdBSGV4YWdvbihibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSk7XG4gaW5zdGFsbERyYXdBU3RhcihibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSk7XG4gaW5zdGFsbERyYXdBUm9ib3QoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pO1xuIGluc3RhbGxEcmF3QVJvY2tldChibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSk7XG4gaW5zdGFsbERyYXdBUGxhbmV0KGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKTtcbiBpbnN0YWxsRHJhd0FSaG9tYnVzKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKTtcbiBpbnN0YWxsRHJhd1VwcGVyV2F2ZShibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSk7XG4gaW5zdGFsbERyYXdMb3dlcldhdmUoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pO1xuXG4gaW5zdGFsbENyZWF0ZUFTbm93Zmxha2VEcm9wZG93bihibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSk7XG59O1xuXG52YXIgTEVOR1RIX1BBUkFNID0gbXNnLmxlbmd0aFBhcmFtZXRlcigpO1xuXG5mdW5jdGlvbiBjcmVhdGVBQ2lyY2xlQ29kZShzaXplLCBnZW5zeW0sIGluZGVudCkge1xuICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcbiAgaW5kZW50ID0gaW5kZW50IHx8ICcnO1xuICByZXR1cm4gW1xuICAgIGluZGVudCArICcvLyBjcmVhdGVfYV9jaXJjbGUnLFxuICAgIGluZGVudCArICdmb3IgKHZhciAnICsgbG9vcFZhciArICcgPSAwOyAnICsgbG9vcFZhciArICcgPCAzNjsgJyArXG4gICAgaW5kZW50ICsgICAgICAgbG9vcFZhciArICcrKykgeycsXG4gICAgaW5kZW50ICsgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyBzaXplICsgJyk7JyxcbiAgICBpbmRlbnQgKyAnICBUdXJ0bGUudHVyblJpZ2h0KDEwKTsnLFxuICAgIGluZGVudCArICd9XFxuJ10uam9pbignXFxuJyk7XG59XG5cbi8qKlxuICogUmV0dXJucyBhbiBpbml0aWFsaXphdGlvbiBvYmplY3QgdGhhdCBzZXRzIHVwIGJsb2NrbHkgYXR0cmlidXRlcztcbiAqXG4gKiBAcGFyYW0gdGl0bGUgLSBUaGUgdGl0bGUgb2YgdGhlIGJsb2NrIHRoYXQgd2lsbCBiZSB2aXNpYmxlIHRvIHRoZSB1c2VyLlxuICogQHBhcmFtIFtwYXJhbWV0ZXJdIC0gT3B0aW9uYWwgcGFyYW1ldGVyIGZvciBibG9ja3MgdGhhdCBhY2NlcHQgYSB2YWx1ZVxuICogICAgcGFyYW1ldGVyLiBUaGlzIGlzIHRoZSB0aXRsZSBvZiB0aGUgcGFyYW1ldGVyLlxuICogQHJldHVybiB0aGUgaW5pdGlhbGl6YXRpb24gb2JqZWN0XG4gKi9cbmZ1bmN0aW9uIG1ha2VCbG9ja0luaXRpYWxpemVyKHRpdGxlLCBwYXJhbWV0ZXIpIHtcbiAgcmV0dXJuIHtcbiAgICBpbml0OiBmdW5jdGlvbiAoKSB7XG4gICAgICB0aGlzLnNldEhTVig5NCwgMC44NCwgMC42MCk7XG5cbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpLmFwcGVuZFRpdGxlKHRpdGxlKTtcblxuICAgICAgaWYgKHBhcmFtZXRlciAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIHRoaXMuYXBwZW5kVmFsdWVJbnB1dCgnVkFMVUUnKVxuICAgICAgICAgICAgLnNldEFsaWduKEJsb2NrbHkuQUxJR05fUklHSFQpXG4gICAgICAgICAgICAuc2V0Q2hlY2soQmxvY2tseS5CbG9ja1ZhbHVlVHlwZS5OVU1CRVIpXG4gICAgICAgICAgICAuYXBwZW5kVGl0bGUocGFyYW1ldGVyICsgJzonKTtcbiAgICAgIH1cblxuICAgICAgdGhpcy5zZXRQcmV2aW91c1N0YXRlbWVudCh0cnVlKTtcbiAgICAgIHRoaXMuc2V0TmV4dFN0YXRlbWVudCh0cnVlKTtcbiAgICAgIHRoaXMuc2V0VG9vbHRpcCgnJyk7XG4gICAgfVxuICB9O1xufVxuXG4vKipcbiAqIFNhbWUgYXMgZHJhd19hX3NxdWFyZSwgZXhjZXB0IGlucHV0cyBhcmUgbm90IGlubGluZWRcbiAqL1xuZnVuY3Rpb24gaW5zdGFsbERyYXdBU3F1YXJlKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKSB7XG4gIC8vIENyZWF0ZSBhIGZha2UgXCJkcmF3IGEgc3F1YXJlXCIgZnVuY3Rpb24gc28gaXQgY2FuIGJlIG1hZGUgYXZhaWxhYmxlIHRvIHVzZXJzXG4gIC8vIHdpdGhvdXQgYmVpbmcgc2hvd24gaW4gdGhlIHdvcmtzcGFjZS5cbiAgdmFyIHRpdGxlID0gbXNnLmRyYXdBU3F1YXJlKCk7XG5cbiAgYmxvY2tseS5CbG9ja3MuZHJhd19hX3NxdWFyZV9jdXN0b20gPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX3NxdWFyZV9jdXN0b20gPSBmdW5jdGlvbiAoKSB7XG4gICAgLy8gR2VuZXJhdGUgSmF2YVNjcmlwdCBmb3IgZHJhd2luZyBhIHNxdWFyZS5cbiAgICB2YXIgdmFsdWVfbGVuZ3RoID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKFxuICAgICAgICB0aGlzLCAnVkFMVUUnLCBnZW5lcmF0b3IuT1JERVJfQVRPTUlDKTtcbiAgICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcbiAgICByZXR1cm4gW1xuICAgICAgICAnLy8gZHJhd19hX3NxdWFyZScsXG4gICAgICAgICdmb3IgKHZhciAnICsgbG9vcFZhciArICcgPSAwOyAnICsgbG9vcFZhciArICcgPCA0OyAnICtcbiAgICAgICAgICAgICAgbG9vcFZhciArICcrKykgeycsXG4gICAgICAgICcgIFR1cnRsZS5tb3ZlRm9yd2FyZCgnICsgdmFsdWVfbGVuZ3RoICsgJyk7JyxcbiAgICAgICAgJyAgVHVydGxlLnR1cm5SaWdodCg5MCk7JyxcbiAgICAgICAgJ31cXG4nXS5qb2luKCdcXG4nKTtcbiAgfTtcbn1cblxuLyoqXG4gKiBjcmVhdGVfYV9jaXJjbGUgYW5kIGNyZWF0ZV9hX2NpcmNsZV9zaXplXG4gKiBmaXJzdCBkZWZhdWx0cyB0byBzaXplIDEwLCBzZWNvbmQgcHJvdmlkZXMgYSBzaXplIHBhcmFtXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxDcmVhdGVBQ2lyY2xlKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKSB7XG5cbiAgdmFyIHRpdGxlID0gbXNnLmNyZWF0ZUFDaXJjbGUoKTtcbiAgdmFyIHBhcmFtID0gbXNnLnNpemVQYXJhbWV0ZXIoKTtcblxuICBibG9ja2x5LkJsb2Nrcy5jcmVhdGVfYV9jaXJjbGUgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSk7XG5cbiAgYmxvY2tseS5CbG9ja3MuY3JlYXRlX2FfY2lyY2xlX3NpemUgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgcGFyYW0pO1xuXG4gIGdlbmVyYXRvci5jcmVhdGVfYV9jaXJjbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIGNyZWF0ZUFDaXJjbGVDb2RlKDEwLCBnZW5zeW0pO1xuICB9O1xuXG4gIGdlbmVyYXRvci5jcmVhdGVfYV9jaXJjbGVfc2l6ZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgc2l6ZSA9IGdlbmVyYXRvci52YWx1ZVRvQ29kZSh0aGlzLCAnVkFMVUUnLCBnZW5lcmF0b3IuT1JERVJfQVRPTUlDKTtcbiAgICByZXR1cm4gY3JlYXRlQUNpcmNsZUNvZGUoc2l6ZSwgZ2Vuc3ltKTtcbiAgfTtcbn1cblxuLyoqXG4gKiBjcmVhdGVfYV9zbm93Zmxvd2VyXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxDcmVhdGVBU25vd2ZsYWtlQnJhbmNoKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKSB7XG5cbiAgdmFyIHRpdGxlID0gbXNnLmNyZWF0ZUFTbm93Zmxha2VCcmFuY2goKTtcblxuICBibG9ja2x5LkJsb2Nrcy5jcmVhdGVfYV9zbm93Zmxha2VfYnJhbmNoID0gbWFrZUJsb2NrSW5pdGlhbGl6ZXIodGl0bGUpO1xuXG4gIGdlbmVyYXRvci5jcmVhdGVfYV9zbm93Zmxha2VfYnJhbmNoID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciBsb29wVmFyID0gZ2Vuc3ltKCdjb3VudCcpO1xuICAgIHZhciBsb29wVmFyMiA9IGdlbnN5bSgnY291bnQnKTtcbiAgICByZXR1cm4gW1xuICAgICAgJy8vIGNyZWF0ZV9hX3Nub3dmbGFrZV9icmFuY2gnLFxuICAgICAgJ1R1cnRsZS5qdW1wRm9yd2FyZCg5MCk7JyxcbiAgICAgICdUdXJ0bGUudHVybkxlZnQoNDUpOycsXG4gICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIgKyAnID0gMDsgJyArIGxvb3BWYXIgKyAnIDwgMzsgJyArIGxvb3BWYXIgKyAnKyspIHsnLFxuICAgICAgJyAgZm9yICh2YXIgJyArIGxvb3BWYXIyICsgJyA9IDA7ICcgKyBsb29wVmFyMiArICcgPCAzOyAnICsgbG9vcFZhcjIgKyAnKyspIHsnLFxuICAgICAgJyAgICBUdXJ0bGUubW92ZUZvcndhcmQoMzApOycsXG4gICAgICAnICAgIFR1cnRsZS5tb3ZlQmFja3dhcmQoMzApOycsXG4gICAgICAnICAgIFR1cnRsZS50dXJuUmlnaHQoNDUpOycsXG4gICAgICAnICB9JyxcbiAgICAgICcgIFR1cnRsZS50dXJuTGVmdCg5MCk7JyxcbiAgICAgICcgIFR1cnRsZS5tb3ZlQmFja3dhcmQoMzApOycsXG4gICAgICAnICBUdXJ0bGUudHVybkxlZnQoNDUpOycsXG4gICAgICAnfScsXG4gICAgICAnVHVydGxlLnR1cm5SaWdodCg0NSk7XFxuJ10uam9pbignXFxuJyk7XG4gIH07XG59XG5cblxuLyoqXG4gKiBEcmF3IGEgcmhvbWJ1cyBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3QVJob21idXMoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pIHtcblxuICB2YXIgdGl0bGUgPSBtc2cuZHJhd0FSaG9tYnVzKCk7XG5cbiAgYmxvY2tseS5CbG9ja3MuZHJhd19hX3Job21idXMgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX3Job21idXMgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHZhbHVlX2xlbmd0aCA9IGdlbmVyYXRvci52YWx1ZVRvQ29kZShcbiAgICAgICAgdGhpcywgJ1ZBTFVFJywgZ2VuZXJhdG9yLk9SREVSX0FUT01JQyk7XG4gICAgdmFyIGxvb3BWYXIgPSBnZW5zeW0oJ2NvdW50Jyk7XG4gICAgcmV0dXJuIFtcbiAgICAgICdmb3IgKHZhciAnICsgbG9vcFZhciArICcgPSAwOyAnICsgbG9vcFZhciArICcgPCAyOyAnICtcbiAgICAgICAgICAgIGxvb3BWYXIgKyAnKyspIHsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJyAgVHVydGxlLnR1cm5SaWdodCg2MCk7JyxcbiAgICAgICcgIFR1cnRsZS5tb3ZlRm9yd2FyZCgnICsgdmFsdWVfbGVuZ3RoICsgJyk7JyxcbiAgICAgICcgIFR1cnRsZS50dXJuUmlnaHQoMTIwKTsnLFxuICAgICAgJ31cXG4nXS5qb2luKCdcXG4nKTtcbiAgfTtcbn1cblxuLyoqXG4gKiBEcmF3IGEgdHJpYW5nbGUgZnVuY3Rpb24gY2FsbCBibG9ja1xuICovXG5mdW5jdGlvbiBpbnN0YWxsRHJhd0FUcmlhbmdsZShibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSkge1xuXG4gIHZhciB0aXRsZSA9IG1zZy5kcmF3QVRyaWFuZ2xlKCk7XG5cbiAgYmxvY2tseS5CbG9ja3MuZHJhd19hX3RyaWFuZ2xlID0gbWFrZUJsb2NrSW5pdGlhbGl6ZXIodGl0bGUsIExFTkdUSF9QQVJBTSk7XG5cbiAgZ2VuZXJhdG9yLmRyYXdfYV90cmlhbmdsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdmFsdWVfbGVuZ3RoID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKFxuICAgICAgICB0aGlzLCAnVkFMVUUnLCBnZW5lcmF0b3IuT1JERVJfQVRPTUlDKTtcbiAgICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcbiAgICByZXR1cm4gW1xuICAgICAgICAnLy8gZHJhd19hX3RyaWFuZ2xlJyxcbiAgICAgICAgJ2ZvciAodmFyICcgKyBsb29wVmFyICsgJyA9IDA7ICcgKyBsb29wVmFyICsgJyA8IDM7ICcgK1xuICAgICAgICAgICAgICBsb29wVmFyICsgJysrKSB7JyxcbiAgICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgICAnICBUdXJ0bGUudHVybkxlZnQoMTIwKTsnLFxuICAgICAgICAnfVxcbiddLmpvaW4oJ1xcbicpO1xuICB9O1xufVxuXG4vKipcbiAqIERyYXcgYSB0cmlhbmdsZSBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3QUhleGFnb24oYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pIHtcblxuICB2YXIgdGl0bGUgPSBtc2cuZHJhd0FIZXhhZ29uKCk7XG5cbiAgYmxvY2tseS5CbG9ja3MuZHJhd19hX2hleGFnb24gPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX2hleGFnb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHZhbHVlX2xlbmd0aCA9IGdlbmVyYXRvci52YWx1ZVRvQ29kZShcbiAgICAgICAgdGhpcywgJ1ZBTFVFJywgZ2VuZXJhdG9yLk9SREVSX0FUT01JQyk7XG4gICAgdmFyIGxvb3BWYXIgPSBnZW5zeW0oJ2NvdW50Jyk7XG4gICAgcmV0dXJuIFtcbiAgICAgICAgJy8vIGRyYXdfYV90cmlhbmdsZScsXG4gICAgICAgICdmb3IgKHZhciAnICsgbG9vcFZhciArICcgPSAwOyAnICsgbG9vcFZhciArICcgPCA2OyAnICtcbiAgICAgICAgICAgICAgbG9vcFZhciArICcrKykgeycsXG4gICAgICAgICcgIFR1cnRsZS5tb3ZlRm9yd2FyZCgnICsgdmFsdWVfbGVuZ3RoICsgJyk7JyxcbiAgICAgICAgJyAgVHVydGxlLnR1cm5MZWZ0KDYwKTsnLFxuICAgICAgICAnfVxcbiddLmpvaW4oJ1xcbicpO1xuICB9O1xufVxuXG4vKipcbiAqIERyYXcgYSBob3VzZSBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3QUhvdXNlKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKSB7XG5cbiAgdmFyIHRpdGxlID0gbXNnLmRyYXdBSG91c2UoKTtcblxuICBibG9ja2x5LkJsb2Nrcy5kcmF3X2FfaG91c2UgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX2hvdXNlID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciB2YWx1ZV9sZW5ndGggPSBnZW5lcmF0b3IudmFsdWVUb0NvZGUoXG4gICAgICAgIHRoaXMsICdWQUxVRScsIGdlbmVyYXRvci5PUkRFUl9BVE9NSUMpO1xuICAgIHZhciBsb29wVmFyID0gZ2Vuc3ltKCdjb3VudCcpO1xuICAgIHJldHVybiBbXG4gICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIgKyAnID0gMDsgJyArIGxvb3BWYXIgKyAnIDwgNDsgJyArIGxvb3BWYXIgKyAnKyspIHsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJyAgVHVydGxlLnR1cm5MZWZ0KDkwKTsnLFxuICAgICAgJ30nLFxuICAgICAgJ1R1cnRsZS50dXJuTGVmdCg5MCk7JyxcbiAgICAgICdUdXJ0bGUubW92ZUZvcndhcmQoJyArIHZhbHVlX2xlbmd0aCArICcpOycsXG4gICAgICAnVHVydGxlLnR1cm5SaWdodCg5MCk7JyxcbiAgICAgICdmb3IgKHZhciAnICsgbG9vcFZhciArICcgPSAwOyAnICsgbG9vcFZhciArICcgPCAzOyAnICsgbG9vcFZhciArICcrKykgeycsXG4gICAgICAnICBUdXJ0bGUubW92ZUZvcndhcmQoJyArIHZhbHVlX2xlbmd0aCArICcpOycsXG4gICAgICAnICBUdXJ0bGUudHVybkxlZnQoMTIwKTsnLFxuICAgICAgJ30nLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuTGVmdCg5MCk7XFxuJ10uam9pbignXFxuJyk7XG4gIH07XG59XG5cbi8qKlxuICogRHJhdyBhIGZsb3dlciBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3QUZsb3dlcihibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSkge1xuXG4gIHZhciB0aXRsZSA9IG1zZy5kcmF3QUZsb3dlcigpO1xuXG4gIGJsb2NrbHkuQmxvY2tzLmRyYXdfYV9mbG93ZXIgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX2Zsb3dlciA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdmFsdWVfbGVuZ3RoID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKFxuICAgICAgICB0aGlzLCAnVkFMVUUnLCBnZW5lcmF0b3IuT1JERVJfQVRPTUlDKTtcbiAgICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcblxuICAgIHZhciBjb2xvcl9yYW5kb20gPSBnZW5lcmF0b3IuY29sb3VyX3JhbmRvbSgpWzBdO1xuICAgIHJldHVybiBbXG4gICAgICAnVHVydGxlLnBlbkNvbG91cihcIiMyMjhiMjJcIik7JyxcbiAgICAgICdUdXJ0bGUubW92ZUZvcndhcmQoJyArIHZhbHVlX2xlbmd0aCArICcpOycsXG4gICAgICAnVHVydGxlLnR1cm5MZWZ0KDE4KTsnLFxuICAgICAgJ2ZvciAodmFyICcgKyBsb29wVmFyICsgJyA9IDA7ICcgKyBsb29wVmFyICsgJyA8IDEwOyAnICsgbG9vcFZhciArICcrKykgeycsXG4gICAgICAnICBUdXJ0bGUucGVuQ29sb3VyKCcgKyBjb2xvcl9yYW5kb20gKyAnKTsnLFxuICAgICAgJyAgVHVydGxlLnR1cm5MZWZ0KDM2KTsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnIC8gMik7JyxcbiAgICAgICcgIFR1cnRsZS5tb3ZlQmFja3dhcmQoJyArIHZhbHVlX2xlbmd0aCArICcvIDIpOycsXG4gICAgICAnfScsXG4gICAgICAnVHVydGxlLnR1cm5SaWdodCgxOTgpOycsXG4gICAgICAnVHVydGxlLmp1bXBGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoMTgwKTtcXG4nXS5qb2luKCdcXG4nKTtcbiAgfTtcbn1cblxuLyoqXG4gKiBEcmF3IGEgc25vd2ZsYWtlIGZ1bmN0aW9uIGNhbGwgYmxvY2tcbiAqL1xuZnVuY3Rpb24gaW5zdGFsbERyYXdBU25vd2ZsYWtlKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKSB7XG5cbiAgdmFyIHRpdGxlID0gbXNnLmRyYXdBU25vd2ZsYWtlKCk7XG5cbiAgYmxvY2tseS5CbG9ja3MuZHJhd19hX3Nub3dmbGFrZSA9IG1ha2VCbG9ja0luaXRpYWxpemVyKHRpdGxlKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX3Nub3dmbGFrZSA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcblxuICAgIHJldHVybiBbXG4gICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIgKyAnID0gMDsgJyArIGxvb3BWYXIgKyAnIDwgODsgJyArIGxvb3BWYXIgKyAnKyspIHsnLFxuICAgICAgJyAgVHVydGxlLnBlbkNvbG91cihcIiM3ZmZmZDRcIik7JyxcbiAgICAgICcgIFR1cnRsZS5tb3ZlRm9yd2FyZCgzMCk7JyxcbiAgICAgICcgIFR1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnICBUdXJ0bGUubW92ZUZvcndhcmQoMTUpOycsXG4gICAgICAnICBUdXJ0bGUudHVyblJpZ2h0KDkwKTsnLFxuICAgICAgJyAgVHVydGxlLnBlbkNvbG91cihcIiMwMDAwY2RcIik7JyxcbiAgICAgICcgIFR1cnRsZS5tb3ZlRm9yd2FyZCgxNSk7JyxcbiAgICAgICcgIFR1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnICBUdXJ0bGUubW92ZUZvcndhcmQoMzApOycsXG4gICAgICAnICBUdXJ0bGUudHVyblJpZ2h0KDQ1KTsnLFxuICAgICAgJ31cXG4nXS5qb2luKCdcXG4nKTtcbiAgfTtcbn1cblxuLyoqXG4gKiBEcmF3IGEgc3RhciBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3QVN0YXIoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pIHtcblxuICB2YXIgdGl0bGUgPSBtc2cuZHJhd0FTdGFyKCk7XG5cbiAgYmxvY2tseS5CbG9ja3MuZHJhd19hX3N0YXIgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX3N0YXIgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHZhbHVlX2xlbmd0aCA9IGdlbmVyYXRvci52YWx1ZVRvQ29kZShcbiAgICAgICAgdGhpcywgJ1ZBTFVFJywgZ2VuZXJhdG9yLk9SREVSX0FUT01JQyk7XG4gICAgdmFyIGxvb3BWYXIgPSBnZW5zeW0oJ2NvdW50Jyk7XG5cbiAgICByZXR1cm4gW1xuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoMTgpOycsXG4gICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIgKyAnID0gMDsgJyArIGxvb3BWYXIgKyAnIDwgNTsgJyArIGxvb3BWYXIgKyAnKyspIHsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJyAgVHVydGxlLnR1cm5SaWdodCgxNDQpOycsXG4gICAgICAnfScsXG4gICAgICAnVHVydGxlLnR1cm5MZWZ0KDE4KTtcXG4nXS5qb2luKCdcXG4nKTtcbiAgfTtcbn1cblxuLyoqXG4gKiBEcmF3IGEgcm9ib3QgZnVuY3Rpb24gY2FsbCBibG9ja1xuICovXG5mdW5jdGlvbiBpbnN0YWxsRHJhd0FSb2JvdChibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSkge1xuXG4gIHZhciB0aXRsZSA9IG1zZy5kcmF3QVJvYm90KCk7XG5cbiAgYmxvY2tseS5CbG9ja3MuZHJhd19hX3JvYm90ID0gbWFrZUJsb2NrSW5pdGlhbGl6ZXIodGl0bGUpO1xuXG4gIGdlbmVyYXRvci5kcmF3X2Ffcm9ib3QgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGxvb3BWYXIgPSBnZW5zeW0oJ2NvdW50Jyk7XG5cbiAgICByZXR1cm4gW1xuICAgICAgJ1R1cnRsZS50dXJuTGVmdCg5MCk7JyxcbiAgICAgICdmb3IgKHZhciAnICsgbG9vcFZhciArICcgPSAwOyAnICsgbG9vcFZhciArICcgPCA0OyAnICsgbG9vcFZhciArICcrKykgeycsXG4gICAgICAnICBUdXJ0bGUubW92ZUZvcndhcmQoMjApOycsXG4gICAgICAnICBUdXJ0bGUudHVyblJpZ2h0KDkwKTsnLFxuICAgICAgJ30nLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnVHVydGxlLm1vdmVCYWNrd2FyZCgxMCk7JyxcbiAgICAgICdUdXJ0bGUubW92ZUZvcndhcmQoNDApOycsXG4gICAgICAnVHVydGxlLnR1cm5SaWdodCg5MCk7JyxcbiAgICAgICdUdXJ0bGUubW92ZUZvcndhcmQoODApOycsXG4gICAgICAnVHVydGxlLnR1cm5SaWdodCg5MCk7JyxcbiAgICAgICdUdXJ0bGUubW92ZUZvcndhcmQoNDApOycsXG4gICAgICAnVHVydGxlLnR1cm5SaWdodCg5MCk7JyxcbiAgICAgICdUdXJ0bGUubW92ZUZvcndhcmQoODApOycsXG4gICAgICAnVHVydGxlLm1vdmVCYWNrd2FyZCgxNSk7JyxcbiAgICAgICdUdXJ0bGUudHVybkxlZnQoMTIwKTsnLFxuICAgICAgJ1R1cnRsZS5tb3ZlRm9yd2FyZCg0MCk7JyxcbiAgICAgICdUdXJ0bGUubW92ZUJhY2t3YXJkKDQwKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoMzApOycsXG4gICAgICAnVHVydGxlLm1vdmVCYWNrd2FyZCg0MCk7JyxcbiAgICAgICdUdXJ0bGUudHVyblJpZ2h0KDIxMCk7JyxcbiAgICAgICdUdXJ0bGUubW92ZUZvcndhcmQoNDApOycsXG4gICAgICAnVHVydGxlLm1vdmVCYWNrd2FyZCg0MCk7JyxcbiAgICAgICdUdXJ0bGUudHVyblJpZ2h0KDYwKTsnLFxuICAgICAgJ1R1cnRsZS5tb3ZlRm9yd2FyZCgxMTUpOycsXG4gICAgICAnVHVydGxlLm1vdmVCYWNrd2FyZCg1MCk7JyxcbiAgICAgICdUdXJ0bGUudHVyblJpZ2h0KDkwKTsnLFxuICAgICAgJ1R1cnRsZS5tb3ZlRm9yd2FyZCg0MCk7JyxcbiAgICAgICdUdXJ0bGUudHVybkxlZnQoOTApOycsXG4gICAgICAnVHVydGxlLm1vdmVGb3J3YXJkKDUwKTtcXG4nXS5qb2luKCdcXG4nKTtcbiAgfTtcbn1cblxuXG4vKipcbiAqIERyYXcgYSByb2JvdCBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3QVJvY2tldChibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSkge1xuXG4gIHZhciB0aXRsZSA9IG1zZy5kcmF3QVJvY2tldCgpO1xuXG4gIGJsb2NrbHkuQmxvY2tzLmRyYXdfYV9yb2NrZXQgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX3JvY2tldCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdmFsdWVfbGVuZ3RoID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKFxuICAgICAgICB0aGlzLCAnVkFMVUUnLCBnZW5lcmF0b3IuT1JERVJfQVRPTUlDKTtcbiAgICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcbiAgICB2YXIgbG9vcFZhcjIgPSBnZW5zeW0oJ2NvdW50Jyk7XG5cbiAgICByZXR1cm4gW1xuICAgICAgJ1R1cnRsZS5wZW5Db2xvdXIoXCIjZmYwMDAwXCIpOycsXG4gICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIgKyAnID0gMDsgJyArIGxvb3BWYXIgKyAnIDwgMzsgJyArIGxvb3BWYXIgKyAnKyspIHsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKDIwKTsnLFxuICAgICAgJyAgVHVydGxlLnR1cm5MZWZ0KDEyMCk7JyxcbiAgICAgICd9JyxcbiAgICAgICdUdXJ0bGUucGVuQ29sb3VyKFwiIzAwMDAwMFwiKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuTGVmdCg5MCk7JyxcbiAgICAgICdUdXJ0bGUuanVtcEZvcndhcmQoMjApOycsXG4gICAgICAnVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnVHVydGxlLm1vdmVGb3J3YXJkKDIwKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnVHVydGxlLm1vdmVGb3J3YXJkKDIwKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJ1R1cnRsZS50dXJuUmlnaHQoOTApOycsXG4gICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIyICsgJyA9IDA7ICcgKyBsb29wVmFyMiArICcgPCAzOyAnICsgbG9vcFZhcjIgKyAnKyspIHsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKDIwKTsnLFxuICAgICAgJyAgVHVydGxlLnR1cm5MZWZ0KDEyMCk7JyxcbiAgICAgICd9XFxuJ10uam9pbignXFxuJyk7XG4gIH07XG59XG5cbi8qKlxuICogRHJhdyBhIHBsYW5ldCBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3QVBsYW5ldChibG9ja2x5LCBnZW5lcmF0b3IsIGdlbnN5bSkge1xuXG4gIHZhciB0aXRsZSA9IG1zZy5kcmF3QVBsYW5ldCgpO1xuXG4gIGJsb2NrbHkuQmxvY2tzLmRyYXdfYV9wbGFuZXQgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19hX3BsYW5ldCA9IGZ1bmN0aW9uICgpIHtcbiAgICB2YXIgdmFsdWVfbGVuZ3RoID0gZ2VuZXJhdG9yLnZhbHVlVG9Db2RlKFxuICAgICAgICB0aGlzLCAnVkFMVUUnLCBnZW5lcmF0b3IuT1JERVJfQVRPTUlDKTtcbiAgICB2YXIgbG9vcFZhciA9IGdlbnN5bSgnY291bnQnKTtcblxuXG4gICAgcmV0dXJuIFtcbiAgICAgICdUdXJ0bGUucGVuQ29sb3VyKFwiIzgwODA4MFwiKTsnLFxuICAgICAgJ2ZvciAodmFyICcgKyBsb29wVmFyICsgJyA9IDA7ICcgKyBsb29wVmFyICsgJyA8IDM2MDsgJyArIGxvb3BWYXIgKyAnKyspIHsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVGb3J3YXJkKCcgKyB2YWx1ZV9sZW5ndGggKyAnKTsnLFxuICAgICAgJyAgVHVydGxlLm1vdmVCYWNrd2FyZCgnICsgdmFsdWVfbGVuZ3RoICsgJyk7JyxcbiAgICAgICcgIFR1cnRsZS50dXJuUmlnaHQoMSk7JyxcbiAgICAgICd9XFxuJ10uam9pbignXFxuJyk7XG4gIH07XG59XG5cbi8qKlxuICogRHJhdyB1cHBlciB3YXZlIGZ1bmN0aW9uIGNhbGwgYmxvY2tcbiAqL1xuZnVuY3Rpb24gaW5zdGFsbERyYXdVcHBlcldhdmUoYmxvY2tseSwgZ2VuZXJhdG9yLCBnZW5zeW0pIHtcblxuICB2YXIgdGl0bGUgPSBtc2cuZHJhd1VwcGVyV2F2ZSgpO1xuXG4gIGJsb2NrbHkuQmxvY2tzLmRyYXdfdXBwZXJfd2F2ZSA9IG1ha2VCbG9ja0luaXRpYWxpemVyKHRpdGxlLCBMRU5HVEhfUEFSQU0pO1xuXG4gIGdlbmVyYXRvci5kcmF3X3VwcGVyX3dhdmUgPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHZhbHVlX2xlbmd0aCA9IGdlbmVyYXRvci52YWx1ZVRvQ29kZShcbiAgICAgICAgdGhpcywgJ1ZBTFVFJywgZ2VuZXJhdG9yLk9SREVSX0FUT01JQyk7XG4gICAgdmFyIGxvb3BWYXIgPSBnZW5zeW0oJ2NvdW50Jyk7XG5cbiAgICByZXR1cm4gW1xuICAgICAgJ1R1cnRsZS5wZW5Db2xvdXIoXCIjMDAwMGNkXCIpOycsXG4gICAgICAnZm9yICh2YXIgJyArIGxvb3BWYXIgKyAnID0gMDsgJyArIGxvb3BWYXIgKyAnIDwgMTA7ICcgKyBsb29wVmFyICsgJysrKSB7JyxcbiAgICAgICcgIFR1cnRsZS5tb3ZlRm9yd2FyZCgnICsgdmFsdWVfbGVuZ3RoICsgJyk7JyxcbiAgICAgICcgIFR1cnRsZS50dXJuUmlnaHQoMTgpOycsXG4gICAgICAnfVxcbiddLmpvaW4oJ1xcbicpO1xuICB9O1xufVxuXG4vKipcbiAqIERyYXcgbG93ZXIgd2F2ZSBmdW5jdGlvbiBjYWxsIGJsb2NrXG4gKi9cbmZ1bmN0aW9uIGluc3RhbGxEcmF3TG93ZXJXYXZlKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKSB7XG5cbiAgdmFyIHRpdGxlID0gbXNnLmRyYXdMb3dlcldhdmUoKTtcblxuICBibG9ja2x5LkJsb2Nrcy5kcmF3X2xvd2VyX3dhdmUgPSBtYWtlQmxvY2tJbml0aWFsaXplcih0aXRsZSwgTEVOR1RIX1BBUkFNKTtcblxuICBnZW5lcmF0b3IuZHJhd19sb3dlcl93YXZlID0gZnVuY3Rpb24gKCkge1xuICAgIHZhciB2YWx1ZV9sZW5ndGggPSBnZW5lcmF0b3IudmFsdWVUb0NvZGUoXG4gICAgICAgIHRoaXMsICdWQUxVRScsIGdlbmVyYXRvci5PUkRFUl9BVE9NSUMpO1xuICAgIHZhciBsb29wVmFyID0gZ2Vuc3ltKCdjb3VudCcpO1xuXG4gICAgcmV0dXJuIFtcbiAgICAgICdUdXJ0bGUucGVuQ29sb3VyKFwiIzAwMDBjZFwiKTsnLFxuICAgICAgJ2ZvciAodmFyICcgKyBsb29wVmFyICsgJyA9IDA7ICcgKyBsb29wVmFyICsgJyA8IDEwOyAnICsgbG9vcFZhciArICcrKykgeycsXG4gICAgICAnICBUdXJ0bGUubW92ZUZvcndhcmQoJyArIHZhbHVlX2xlbmd0aCArICcpOycsXG4gICAgICAnICBUdXJ0bGUudHVybkxlZnQoMTgpOycsXG4gICAgICAnfVxcbiddLmpvaW4oJ1xcbicpO1xuICB9O1xufVxuXG5mdW5jdGlvbiBpbnN0YWxsQ3JlYXRlQVNub3dmbGFrZURyb3Bkb3duKGJsb2NrbHksIGdlbmVyYXRvciwgZ2Vuc3ltKSB7XG4gIHZhciBzbm93Zmxha2VzID0gW1xuICAgIFttc2cuY3JlYXRlU25vd2ZsYWtlU3F1YXJlKCksICdzcXVhcmUnXSxcbiAgICBbbXNnLmNyZWF0ZVNub3dmbGFrZVBhcmFsbGVsb2dyYW0oKSwgJ3BhcmFsbGVsb2dyYW0nXSxcbiAgICBbbXNnLmNyZWF0ZVNub3dmbGFrZUxpbmUoKSwgJ2xpbmUnXSxcbiAgICBbbXNnLmNyZWF0ZVNub3dmbGFrZVNwaXJhbCgpLCAnc3BpcmFsJ10sXG4gICAgW21zZy5jcmVhdGVTbm93Zmxha2VGbG93ZXIoKSwgJ2Zsb3dlciddLFxuICAgIFttc2cuY3JlYXRlU25vd2ZsYWtlRnJhY3RhbCgpLCAnZnJhY3RhbCddLFxuICAgIFttc2cuY3JlYXRlU25vd2ZsYWtlUmFuZG9tKCksICdyYW5kb20nXVxuICBdO1xuXG5cbiAgYmxvY2tseS5CbG9ja3MuY3JlYXRlX3Nub3dmbGFrZV9kcm9wZG93biA9IHtcbiAgICAvLyBXZSB1c2UgY3VzdG9tIGluaXRpYWxpemF0aW9uIChpbnN0ZWFkIG9mIG1ha2VCbG9ja0luaXRpYWxpemVyKSBoZXJlXG4gICAgLy8gYmVjYXVzZSBlYWNoIGluaXRpYWxpemF0aW9uIG5lZWRzIGEgbmV3IGluc3RhbmNlIG9mIHRoZSBGaWVsZERyb3Bkb3duLlxuICAgIGluaXQ6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHRoaXMuc2V0SFNWKDk0LCAwLjg0LCAwLjYwKTtcblxuICAgICAgdmFyIHRpdGxlID0gbmV3IGJsb2NrbHkuRmllbGREcm9wZG93bihzbm93Zmxha2VzKTtcbiAgICAgIHRoaXMuYXBwZW5kRHVtbXlJbnB1dCgpLmFwcGVuZFRpdGxlKHRpdGxlLCAnVFlQRScpO1xuXG4gICAgICB0aGlzLnNldFByZXZpb3VzU3RhdGVtZW50KHRydWUpO1xuICAgICAgdGhpcy5zZXROZXh0U3RhdGVtZW50KHRydWUpO1xuICAgICAgdGhpcy5zZXRUb29sdGlwKCcnKTtcbiAgICB9XG4gIH07XG5cbiAgZ2VuZXJhdG9yLmNyZWF0ZV9zbm93Zmxha2VfZHJvcGRvd24gPSBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIHR5cGUgPSB0aGlzLmdldFRpdGxlVmFsdWUoJ1RZUEUnKTtcbiAgICByZXR1cm4gXCJUdXJ0bGUuZHJhd1Nub3dmbGFrZSgnXCIgKyB0eXBlICsgXCInLCAnYmxvY2tfaWRfXCIgKyB0aGlzLmlkICsgXCInKTtcIjtcbiAgfTtcbn1cbiJdfQ==