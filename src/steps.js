var ArtistAPI = require('./utils/api');
var apiJavascript = require('./utils/apiJavascript');
import { blockAsXmlNode, cleanBlocks } from './utils/block_utils';


const CANVAS_HEIGHT = 400;
const CANVAS_WIDTH = 400;

const DEFAULT_X = CANVAS_WIDTH / 2;
const DEFAULT_Y = CANVAS_HEIGHT / 2;
const DEFAULT_DIRECTION = 90;

const MAX_STICKER_SIZE = 100;

const SMOOTH_ANIMATE_STEP_SIZE = 5;
const FAST_SMOOTH_ANIMATE_STEP_SIZE = 15;

const REMIX_PROPS = [
  {
    defaultValues: {
      initialX: DEFAULT_X,
      initialY: DEFAULT_Y,
    },
    generateBlock: args => blockAsXmlNode('jump_to_xy', {
      titles: {
        'XPOS': args.initialX,
        'YPOS': args.initialY,
      }
    }),
  }, {
    defaultValues: {
      startDirection: DEFAULT_DIRECTION
    },
    generateBlock: args => blockAsXmlNode('draw_turn', {
      titles: {
        'DIR': 'turnRight',
      },
      values: {
        'VALUE': {
          type: 'math_number',
          titleName: 'NUM',
          titleValue: args.startDirection - DEFAULT_DIRECTION,
        },
      },
    }),
  },
];

const FROZEN_REMIX_PROPS = [
  {
    defaultValues: {
      initialX: DEFAULT_X,
      initialY: DEFAULT_Y,
    },
    generateBlock: args => blockAsXmlNode('jump_to_xy', {
      titles: {
        'XPOS': args.initialX,
        'YPOS': args.initialY,
      }
    }),
  }, {
    defaultValues: {
      startDirection: 180
    },
    generateBlock: args => blockAsXmlNode('draw_turn', {
      titles: {
        'DIR': 'turnRight',
      },
      values: {
        'VALUE': {
          type: 'math_number',
          titleName: 'NUM',
          titleValue: args.startDirection - 180,
        },
      },
    }),
  }, {
    defaultValues: {
      skin: "elsa",
    },
    generateBlock: args => blockAsXmlNode('turtle_setArtist', {
      titles: {
        'VALUE': args.skin
      },
    }),
  }
];

const REMIX_PROPS_BY_SKIN = {
  artist: REMIX_PROPS,
  anna: FROZEN_REMIX_PROPS,
  elsa: FROZEN_REMIX_PROPS,
};

const PUBLISHABLE_SKINS = ['artist', 'artist_zombie', 'anna', 'elsa'];

var StepsArtist = function () {

  this.api = new ArtistAPI();
  apiJavascript.injectArtistAPI(this.api);
}
StepsArtist.prototype.step = function (command, values, options) {
  var tupleDone = true;
  var result;
  var distance;
  var heading;

  switch (command) {
    case 'FD':  // Forward
      distance = values[0];
      result = this.calculateSmoothAnimate(options, distance);
      tupleDone = result.tupleDone;
      this.visualization.moveForward(result.distance);
      break;
    case 'JF':  // Jump forward
      distance = values[0];
      result = this.calculateSmoothAnimate(options, distance);
      tupleDone = result.tupleDone;
      this.visualization.jumpForward(result.distance);
      break;
    case 'MV':  // Move (direction)
      distance = values[0];
      heading = values[1];
      result = this.calculateSmoothAnimate(options, distance);
      tupleDone = result.tupleDone;
      this.visualization.setHeading(heading);
      this.visualization.moveForward(result.distance);
      break;
    case 'JT':  // Jump To Location
      if (Array.isArray(values[0])) {
        this.visualization.jumpTo(values[0]);
      } else {
        this.visualization.jumpTo([
          utils.xFromPosition(values[0], CANVAS_WIDTH),
          utils.yFromPosition(values[0], CANVAS_HEIGHT),
        ]);
      }
      break;
    case 'MD':  // Move diagonally (use longer steps if showing joints)
      distance = values[0];
      heading = values[1];
      result = this.calculateSmoothAnimate(options, distance);
      tupleDone = result.tupleDone;
      this.visualization.setHeading(heading);
      this.visualization.moveForward(result.distance, true);
      break;
    case 'JD':  // Jump (direction)
      distance = values[0];
      heading = values[1];
      result = this.calculateSmoothAnimate(options, distance);
      tupleDone = result.tupleDone;
      this.visualization.setHeading(heading);
      this.visualization.jumpForward(result.distance);
      break;
    case 'RT':  // Right Turn
      distance = values[0];
      result = this.calculateSmoothAnimate(options, distance);
      tupleDone = result.tupleDone;
      this.visualization.turnByDegrees(result.distance);
      break;
    case 'PT': // Point To
      this.visualization.pointTo(values[0]);
      break;
    case 'GA':  // Global Alpha
      var alpha = values[0];
      alpha = Math.max(0, alpha);
      alpha = Math.min(100, alpha);
      this.visualization.ctxScratch.globalAlpha = alpha / 100;
      break;
    case 'PU':  // Pen Up
      this.visualization.penDownValue = false;
      break;
    case 'PD':  // Pen Down
      this.visualization.penDownValue = true;
      break;
    case 'PW':  // Pen Width
      this.visualization.ctxScratch.lineWidth = values[0];
      break;
    case 'PC':  // Pen Colour
      this.visualization.ctxScratch.strokeStyle = values[0];
      this.visualization.ctxScratch.fillStyle = values[0];
      if (!this.isFrozenSkin()) {
        this.visualization.isDrawingWithPattern = false;
      }
      break;
    case 'PS':  // Pen style with image
      if (!values[0] || values[0] === 'DEFAULT') {
        this.setPattern(null);
      } else {
        this.setPattern(values[0]);
      }
      break;
    case 'HT':  // Hide Turtle
      this.visualization.avatar.visible = false;
      break;
    case 'ST':  // Show Turtle
      this.visualization.avatar.visible = true;
      break;
    case 'sticker': {
      let size = MAX_STICKER_SIZE;

      if (typeof values[1] === 'number') {
        size = values[1];
      }

      if (this.visualization.shouldDrawNormalized_) {
        values = Object.keys(this.stickers);
      }

      var img = this.stickers[values[0]];

      var dimensions = scaleToBoundingBox(size, img.width, img.height);
      var width = dimensions.width;
      var height = dimensions.height;

      // Rotate the image such the the turtle is at the center of the bottom of
      // the image and the image is pointing (from bottom to top) in the same
      // direction as the turtle.
      this.visualization.ctxScratch.save();
      this.visualization.ctxScratch.translate(this.visualization.x, this.visualization.y);
      this.visualization.ctxScratch.rotate(this.visualization.degreesToRadians_(this.visualization.heading));
      this.visualization.ctxScratch.drawImage(img, 0, 0, img.width, img.height, -width / 2, -height, width, height);

      this.visualization.ctxScratch.restore();

      break;
    }
    case 'setArtist':
      if (this.skin.id !== values[0]) {
        this.skin = ArtistSkins.load(this.studioApp_.assetUrl, values[0]);
        this.visualization.avatar = this.skin.avatarSettings;
        this.linePatterns = this.skin.linePatterns;
        this.loadTurtle(false /* initializing */);
        this.preloadAllPatternImages().then(() => this.selectPattern());
      }
      break;
  }

  return tupleDone;
};


StepsArtist.prototype.calculateSmoothAnimate = function (options, distance) {
  var tupleDone = true;
  var stepDistanceCovered = this.visualization.stepDistanceCovered;

  if (options && options.smoothAnimate) {
    var fullDistance = distance;
    var smoothAnimateStepSize = this.visualization.smoothAnimateStepSize;

    if (fullDistance < 0) {
      // Going backward.
      if (stepDistanceCovered - smoothAnimateStepSize <= fullDistance) {
        // clamp at maximum
        distance = fullDistance - stepDistanceCovered;
        stepDistanceCovered = fullDistance;
      } else {
        distance = -smoothAnimateStepSize;
        stepDistanceCovered -= smoothAnimateStepSize;
        tupleDone = false;
      }

    } else {
      // Going forward.
      if (stepDistanceCovered + smoothAnimateStepSize >= fullDistance) {
        // clamp at maximum
        distance = fullDistance - stepDistanceCovered;
        stepDistanceCovered = fullDistance;
      } else {
        distance = smoothAnimateStepSize;
        stepDistanceCovered += smoothAnimateStepSize;
        tupleDone = false;
      }
    }
  }

  this.visualization.stepDistanceCovered = stepDistanceCovered;

  return { tupleDone: tupleDone, distance: distance };
};