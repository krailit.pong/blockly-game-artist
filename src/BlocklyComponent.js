import BlocklyEditor from '../lib/react-blockly-tie/BlocklyEditor';
import BlocklyToolbox from '../lib/react-blockly-tie/BlocklyToolbox';
import BlocklyToolboxBlock from '../lib/react-blockly-tie/BlocklyToolboxBlock';
import BlocklyToolboxCategory from '../lib/react-blockly-tie/BlocklyToolboxCategory';
import BlocklyWorkspace from '../lib/react-blockly-tie/BlocklyWorkspace';

const ReactBlocklyComponent = {
  BlocklyEditor,
  BlocklyToolbox,
  BlocklyToolboxBlock,
  BlocklyToolboxCategory,
  BlocklyWorkspace,
};

export default ReactBlocklyComponent;
