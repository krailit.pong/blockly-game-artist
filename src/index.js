import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './App';
import Album from './Album';
import Artist from './games/Artist';
import Stages_Elsa from './games/Stages_Elsa';
import PP_01 from './games/PP_01';
import PP_02 from './games/PP_02';

// import { settingBlocks } from './initContent/blocks';
const blocks = require('./initContent/blocks');
const Blockly = require('./initContent/locale');
// var generator = Blockly.Generator.get('JavaScript');
// Blockly.JavaScript = generator;
// settingBlocks(Blockly);
// window.setTimeout(() => {
//     ReactDOM.render(<App />, document.getElementById('root'));
// }, 500);
import { BrowserRouter as Router, Route } from "react-router-dom";
import { isMobile } from 'react-device-detect';

// window.setTimeout(() => {
//     blocks.install(Blockly);
// }, 1);
import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#757ce8',
            main: '#3f50b5',
            dark: '#002884',
            contrastText: '#fff',
        },
        secondary: {
            light: '#ff7961',
            main: '#f44336',
            dark: '#ba000d',
            contrastText: '#000',
        },
    },
});

let holder = document.getElementById('root');
if (!isMobile) {
    ReactDOM.render(
        <Router>
            <div>
                <Route exact path="/" component={Stages_Elsa} />
                <Route path="/pp_game" component={Stages_Elsa} />
                <Route path="/Artist" component={Artist} />
                <Route path="/probe" component={Album} />
            </div>
        </Router>,
        holder
    );
} else {
    ReactDOM.render(
        <Router>
            <div style={{ margin: 20 }}>
                <h1>Please use desktop browser instead.</h1>
            </div>
        </Router>,
        holder
    );
}



function Home() {
    return (
        <div>
            <h2>Home</h2>
        </div>
    );
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
