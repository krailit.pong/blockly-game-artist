// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="walk" x="70" y="30"></block></xml>';
const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" x="70" y="50"></block></xml>';
const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">\n' +
  '  <category name="ขีดเส้น" colour="#5C81A6">\n' +
  '     <block type="starter"><field name="x">default</field></block>\n' +
  '     <block type="go_option"><field name="x">default</field></block>\n' +
  '     <block type="draw_circle"><field name="x">default</field></block>\n' +
  '     <block type="turn_option"><field name="x">default</field></block>\n' +
  '     <block type="jump_option"><field name="x">default</field></block>\n' +
  '     <block type="jump_axis"><field name="x">default</field></block>\n' +
  '     <block type="point_to"><field name="x">default</field></block>\n' +
  '  </category>\n' +
  '  <category name="เปลี่ยนสี" colour="#5C68A6">\n' +
  '     <block type="dng_cl_static"><field name="x">default</field></block>\n' +
  '     <block type="dgn_cl_random"><field name="x">default</field></block>\n' +
  '     <block type="alpha"><field name="x">default</field></block>\n' +
  '     <block type="dng_line_height"><field name="x">default</field></block>\n' +
  '  </category>\n' +
  '  <category name="ทำซ้ำ" colour="#5C68A6">\n' +
  '     <block type="loop_for"><field name="x">default</field></block>\n' +
  '     <block type="loop_while"><field name="x">default</field></block>\n' +
  '  </category>\n' +
  // '  <category name="คำนวณ" colour="#5C68A6">\n' +
  // '     <block type="cal_variable"><field name="x">default</field></block>\n' +
  // '     <block type="cal_function"><field name="x">default</field></block>\n' +
  // '  </category>\n' +
  // '  <category name="ตรรกะ" colour="#5C68A6">\n' +
  // '     <block type="starter"><field name="x">default</field></block>\n' +
  // '  </category>\n' +
  // '  <category name="ฟังก์ชั่น" colour="#5C68A6">\n' +
  // '     <block type="starter"><field name="x">default</field></block>\n' +
  // '  </category>\n' +
  // '  <category name="ตัวแปร" colour="#5C68A6">\n' +
  // '     <block type="variable_first"><field name="x">default</field></block>\n' +
  // '     <block type="variable_second"><field name="x">default</field></block>\n' +
  // '  </category>\n' +
  '  <sep></sep>\n' +
  '</xml>';
const INITIAL_TOOLBOX_CATEGORIES = [];

const ConfigFiles = {
  INITIAL_XML,
  INITIAL_TOOLBOX_XML,
  INITIAL_TOOLBOX_CATEGORIES,
};

export default ConfigFiles;
