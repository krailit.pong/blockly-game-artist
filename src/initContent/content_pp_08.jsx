// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="text" x="70" y="30"><field name="TEXT"></field></block></xml>';
const blocks = require('./blocks');
const Blockly = require('./locale');

blocks.install(Blockly);

const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" x="70" y="30"></block></xml>';
const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">\n' +
  '  <category name="ทำซ้ำ" colour="#5C81A6" visible="false">\n' +
  '     <block type="math_number"><field name="x">default</field></block>\n' +
  '     <block type="take_turn"><field name="x">default</field></block>\n' +
  '  </category>\n' +
  '  <category name="กล่องเครื่องมือ" colour="#5C68A6">\n' +
  '    <block type="draw_move_by_constant" ><field name="FD">100</field></block>\n' +
  '    <block type="take_turn" ><field name="degrees">90</field></block>\n' +
  '  </category>\n' +
  '  <sep></sep>\n' +
  '</xml>';
const INITIAL_TOOLBOX_CATEGORIES = [];

const ConfigFiles = {
  INITIAL_XML,
  INITIAL_TOOLBOX_XML,
  INITIAL_TOOLBOX_CATEGORIES,
};

export default ConfigFiles;
