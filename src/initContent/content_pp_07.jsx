// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="text" x="70" y="30"><field name="TEXT"></field></block></xml>';
const blocks = require('./blocks');
const Blockly = require('./locale');

blocks.install(Blockly);

const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" id="N8f.$k4-dO#.sDc.nUmd" deletable="false" movable="false" x="70" y="30"><next><block type="loop_while_static" id="ukQ,0|!vE7?7g7fGT-Y]" deletable="false" movable="false"><field name="start">10</field><statement name="while"><block type="draw_move_by_constant" id="SdQ(!Bh{2DcHr,IZx^aP"><field name="FD">100</field><next><block type="take_turn" id="A?A,%F_w9KN|1SS50,Fs"><field name="turn_choice">r</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id="vzN[]mf.DH-LNa(_9$zj"><field name="FD">100</field><next><block type="take_turn" id="6WK382103+a6FXQ:cyNw"><field name="turn_choice">r</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id=".q90tO7l_]=IpFOjk*b1"><field name="FD">100</field><next><block type="take_turn" id="-/o;+pOn0aH{9{N;|(Hu"><field name="turn_choice">r</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id="@*Kb?,1W7VUy*uZB5(,A"><field name="FD">100</field><next><block type="take_turn" id="Jg#BWVHV}rn1fL~QH1y6"><field name="turn_choice">r</field><field name="degrees">45</field></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></statement></block></next></block></xml>';
// const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">\n' +
//   '  <category name="ทำซ้ำ" colour="#5C81A6" visible="false">\n' +
//   '     <block type="loop_while_static"><field name="0">default</field></block>\n' +
//   '  </category>\n' +
//   '  <category name="กล่องเครื่องมือ" colour="#5C68A6">\n' +
//   '     <block type="draw_move_by_constant"><field name="FD">100</field></block>\n' +
//   '     <block type="take_turn"><field name="degrees">90</field></block>\n' +
//   // '    <block type="draw_move_by_constant" ><field name="FD">100</field></block>\n' +
//   // '    <block type="take_turn" ><field name="degrees">90</field></block>\n' +
//   '  </category>\n' +
//   '  <sep></sep>\n' +
//   '</xml>';
const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">\n' +
  '  <category name="เดินไปข้างหน้า" colour="#5C81A6" visible="false">\n' +
  '    <block type="draw_move_by_constant" ><field name="FD">100</field></block>\n' +
  '  </category>\n' +
  '  <category name="หมุนตัวละคร" colour="#5C68A6">\n' +
  '    <block type="take_turn" ><field name="degrees">90</field></block>\n' +
  '  </category>\n' +
  '  <sep></sep>\n' +
  '</xml>';
const INITIAL_TOOLBOX_CATEGORIES = [];

const ConfigFiles = {
  INITIAL_XML,
  INITIAL_TOOLBOX_XML,
  INITIAL_TOOLBOX_CATEGORIES,
};

export default ConfigFiles;
