// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="text" x="70" y="30"><field name="TEXT"></field></block></xml>';
const blocks = require('./blocks');
const Blockly = require('./locale');

blocks.install(Blockly);

// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" x="70" y="30"></block></xml>';
const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" id="VA|-Nb6d=|J;*6cbxKYI" x="70" y="30"><next><block type="draw_move_by_constant" id="k4Z+`mf!`FBNy;gqk!_@"><field name="FD">50</field><next><block type="take_turn" id="D8LYF9*Tt+*fo-yR(zO0"><field name="turn_choice">r</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id="yyqiR.pi7A6i(/QA8{T0"><field name="FD">150</field><next><block type="take_turn" id="A2,-alSdz-V@$]/)M/?L"><field name="turn_choice">l</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id="9fd3NQ}O6!JeOpLi=wt0"><field name="FD">50</field><next><block type="take_turn" id="~$TAgrE+?H,LikY!C7PQ"><field name="turn_choice">l</field><field name="degrees">45</field><next><block type="draw_move_by_constant" id="QYgJo)aWKS=ZYC~_TDCF"><field name="FD">100</field><next><block type="take_turn" id="ME?5y*vW|n#KS`=OWj5x"><field name="turn_choice">l</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id="_qb`7j9tDD!;IL.{4y2("><field name="FD">100</field></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></xml>';
const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">\n' +
  '  <category name="เดินไปข้างหน้า" colour="#5C81A6" visible="false">\n' +
  '    <block type="draw_move_by_constant" ><field name="FD">100</field></block>\n' +
  '  </category>\n' +
  '  <category name="หมุนตัวละคร" colour="#5C68A6">\n' +
  '    <block type="take_turn" ><field name="degrees">90</field></block>\n' +
  '  </category>\n' +
  '  <sep></sep>\n' +
  '</xml>';
const INITIAL_TOOLBOX_CATEGORIES = [];

const ConfigFiles = {
  INITIAL_XML,
  INITIAL_TOOLBOX_XML,
  INITIAL_TOOLBOX_CATEGORIES,
};

export default ConfigFiles;
