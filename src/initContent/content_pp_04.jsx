// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="text" x="70" y="30"><field name="TEXT"></field></block></xml>';
const blocks = require('./blocks');
const Blockly = require('./locale');

blocks.install(Blockly);

const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" id="iTX4{q_u^p}77+G4`6x]" x="70" y="30"><next><block type="draw_move_by_constant" id="v_zTa_?oR)9fFh84CbqC"><field name="FD">50</field><next><block type="take_turn" id=":3yGDlHPV9vQ8zJHI%`("><field name="turn_choice">r</field><field name="degrees">45</field><next><block type="draw_move_by_constant" id="8Tnu9IHN@LcXov{-c-JE"><field name="FD">50</field><next><block type="take_turn" id="ek;)N[+Prpus5QLKX`l|"><field name="turn_choice">l</field><field name="degrees">180</field><next><block type="draw_move_by_constant" id="9{gpCtOD-(0`s3#?jA4*"><field name="FD">50</field><next><block type="take_turn" id="Vu`THjsP#J=f_fP|R;iM"><field name="turn_choice">l</field><field name="degrees">45</field><next><block type="draw_move_by_constant" id="B;#4458TK%AnfC0NW]9P"><field name="FD">100</field><next><block type="take_turn" id="!e4p~/sZkNR@J(4|39J*"><field name="turn_choice">r</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id="GIGi!A(0NiiO^K%pxv+B"><field name="FD">100</field></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></xml>';
const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="display: none;">\n' +
  '  <category name="เดินไปข้างหน้า" colour="#5C81A6" visible="false">\n' +
  '    <block type="draw_move_by_constant" ><field name="FD">100</field></block>\n' +
  '  </category>\n' +
  '  <category name="หมุนตัวละคร" colour="#5C68A6">\n' +
  '    <block type="take_turn" ><field name="degrees">90</field></block>\n' +
  '  </category>\n' +
  '  <sep></sep>\n' +
  '</xml>';
const INITIAL_TOOLBOX_CATEGORIES = [];

const ConfigFiles = {
  INITIAL_XML,
  INITIAL_TOOLBOX_XML,
  INITIAL_TOOLBOX_CATEGORIES,
};

export default ConfigFiles;
