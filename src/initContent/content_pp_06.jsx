// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="text" x="70" y="30"><field name="TEXT"></field></block></xml>';
const blocks = require('./blocks');
const Blockly = require('./locale');

blocks.install(Blockly);

// const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" x="70" y="30"></block></xml>';
const INITIAL_XML = '<xml xmlns="http://www.w3.org/1999/xhtml"><block type="starter" id="N8f.$k4-dO#.sDc.nUmd" x="70" y="30"><next><block type="draw_move_by_constant" id="oq%W5EOFE+mnW_pV)`sR"><field name="FD">50</field><next><block type="take_turn" id="gRS8AC!=sUxn=PA475ZB"><field name="turn_choice">r</field><field name="degrees">90</field><next><block type="draw_move_by_constant" id="WAaH_Ov]jDo_mW+:!HP~"><field name="FD">50</field><next><block type="take_turn" id="jh]GA:Pt(T,mMHO6nf`Z"><field name="turn_choice">l</field><field name="degrees">45</field><next><block type="draw_move_by_constant" id="%;rL%W[6/!D$5x$(o2%y"><field name="FD">100</field><next><block type="take_turn" id="0}~*r)SAqTxY+YkVY.]M"><field name="turn_choice">l</field><field name="degrees">180</field><next><block type="draw_move_by_constant" id="*8%b+0yA(O!qaClad:Y+"><field name="FD">100</field></block></next></block></next></block></next></block></next></block></next></block></next></block></next></block></xml>';
const INITIAL_TOOLBOX_XML = '<xml xmlns="http://www.w3.org/1999/xhtml" id="toolbox" style="visibility: hidden;">\n' +
  '  <category name="เดินไปข้างหน้า" colour="#5C81A6" >\n' +
  '    <block type="draw_move_by_constant" ><field name="FD">100</field></block>\n' +
  '  </category>\n' +
  '  <category name="หมุนตัวละคร" colour="#5C68A6">\n' +
  '    <block type="take_turn" ><field name="degrees">90</field></block>\n' +
  '  </category>\n' +
  '  <sep></sep>\n' +
  '</xml>';
const INITIAL_TOOLBOX_CATEGORIES = [];

const ConfigFiles = {
  INITIAL_XML,
  INITIAL_TOOLBOX_XML,
  INITIAL_TOOLBOX_CATEGORIES,
};

export default ConfigFiles;
