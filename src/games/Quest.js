export const questByStage = function (stages) {
    switch (stages) {
        case 0: {
            return [
                { command: 'setLineColor', args: ['#303f9f'] },
                { command: 'setLineHeight', args: ['2'] },
                { command: 'moveForward', args: ['2'] },
            ]
            break
        }
        default: break
    }

}