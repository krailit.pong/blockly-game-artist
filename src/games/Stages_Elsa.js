import React, { Component, forwardRef, useRef, useImperativeMethods } from 'react';
import ReactDOM from 'react-dom';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Avatar from '@material-ui/core/Avatar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

import ModalElsa from './ModalElsa';
import Stepper from './Stepper'

import PP_01 from './PP_01'
import PP_02 from './PP_02'
import PP_03 from './PP_03'
import PP_04 from './PP_04'
import PP_05 from './PP_05'
import PP_06 from './PP_06'
import PP_07 from './PP_07'
import PP_08 from './PP_08'


function generateRandomSteps() {
  const n = Math.floor(Math.random() * 3) + 3;
  const arr = [];
  for (let i = 0; i < n; i++) {
    arr.push({
      title: 'Step = ${(i + 1)}',
    });
  }
  return arr;
}
const steps = generateRandomSteps();

class Stages_Elsa extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentStep: 1,
      disabled: false,
      displayModal: false,
      isLoggedIn: false,
      html: <PP_01 goToNext={this.onClickNext} />,
    }

    // this.goToNext = this.goToNext.bind(this)
    // this.func_currentState = this.func_currentState.bind(this)
    // this.onClickNext = this.onClickNext.bind(this)
  }

  func_currentState = (step) => {
    this.setState({
      currentStep: step
    })
    step = step + 1;
    this.determined(step)
  }

  onClickNext = (index) => {
    console.log('onClickNext index = ' + index)
    // this.myRef.current.getAlert();
    // node.handleStep();

    this.func_currentState(index)
    this.virtualClickStepper()
  }

  determined(step) {
    if (step == 1) {
      this.setState({ html: <PP_01 goToNext={this.onClickNext} /> });
    } else if (step == 2) {
      this.setState({ html: <PP_02 goToNext={this.onClickNext} /> });
    } else if (step == 3) {
      this.setState({ html: <PP_03 goToNext={this.onClickNext} /> });
    } else if (step == 4) {
      this.setState({ html: <PP_04 goToNext={this.onClickNext} /> });
    } else if (step == 5) {
      this.setState({ html: <PP_05 goToNext={this.onClickNext} /> });
    } else if (step == 6) {
      this.setState({ html: <PP_06 goToNext={this.onClickNext} /> });
    } else if (step == 7) {
      this.setState({ html: <PP_07 goToNext={this.onClickNext} /> });
    } else if (step == 8) {
      this.setState({ html: <PP_08 goToNext={this.onClickNext} /> });
    }
  }

  render() {
    return (
      <div className="App">
        <div className="">
          <Grid container spacing={24}>
            <Grid item xs={12} md={12} style={{ padding: 4 }}>
              <Paper className="">
                <AppBar position="static" color="secondary">
                  <Toolbar variant="dense">
                    {/* <img alt="Betimes Solutions" src={require('../assets/betime.png')} style={{ margin: 5 }} /> */}
                    <Typography variant="h6" color="inherit" style={{ margin: 5 }}>
                      {/* {React.version} */}
                    </Typography>
                    <Stepper setClick={click => this.virtualClickStepper = click} nowClick={this.func_currentState} />
                  </Toolbar>
                </AppBar>
              </Paper>
            </Grid>
            <Grid item xs={3} md={3}></Grid>
            <Grid item xs={6} md={6}></Grid>
            <Grid item xs={3} md={3}></Grid>
          </Grid>
          {this.state.html}
        </div>
      </div>
    );
  }
}

export default Stages_Elsa;