import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactBlocklyComponent from '../BlocklyComponent';
import ConfigFiles from '../initContent/content_elsa_01';
import parseWorkspaceXml from '../../lib/react-blockly-tie/BlocklyHelper';
import { Stage, Layer, Shape, Sprite } from 'react-konva';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import './Elsa.css';
import { resolve, parse } from 'path';
import ModalElsa from './ModalElsa';
const Blockly = require('../initContent/locale');
const Answer = require('./Answer');
var ArtistAPI = require('../utils/api');
var apiJavascript = require('../utils/apiJavascript');
const Visualization = require("../../visualization_pp.js");

var shape = null
var imagePath = require('../assets/elsa.png');
var petternPath = require('../assets/elsaline.png');
// var petternPath = require('../assets/patterns/rainbow.png');
var vizOption = {
  isFrozenSkin: true,
  avatar: {
    src: imagePath,
    width: 73,
    height: 100,
    numHeadings: 18,
    numFrames: 20,
    visible: true,
  },
  smoothAnimate: true,
  consolidateTurnAndMove: true,
  artistOptions: ['elsa'],
  avatarAllowedScripts: ['frozen'],
  showDecoration: () => "elsa",
  decorationAnimationImage: petternPath,
  // blankAvatar: skin.assetUrl('blank.png'),
};
var viz = new Visualization(vizOption);


class Elsa_02 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      api: new ArtistAPI(),
      toolboxCategories: parseWorkspaceXml(ConfigFiles.INITIAL_TOOLBOX_XML),
      listOfAction: "",
      stroke: "black",
      image: null,
      imageSrc: imagePath,
      code: [],
      disabled: false,
      displayModal: false,
    }

    this.workspaceDidChange = this.workspaceDidChange.bind(this);
    this.hideModal = this.hideModal.bind(this);
  }

  componentDidMount() {
    apiJavascript.injectArtistAPI(this.state.api);

    window.setTimeout(() => {
      let cv = shape.getCanvas();
      viz.ctxDisplay = cv.getContext();
      viz.isDrawingWithPattern = true;
      viz.display();
    }, 1000);

    window.setTimeout(() => {
      var image = new Image();
      image.src = this.state.imageSrc;
      image.onload = () => {
        this.setState({
          image: image
        });
        viz.avatar.image = image;
      };

      var image2 = new Image();
      image2.src = petternPath
      image2.onload = () => {
        viz.decorationAnimationImage = image2;
      };
    }, 200);
  }

  async executeAction(arrayCode) {
    //TODO: for-in is react special async loop
    for (const index in arrayCode) {
      console.log('index = ' + index + ' of ' + arrayCode.length)
      let commandString = arrayCode[index];
      if (commandString.search('\n') < 0) {
        //TODO: \n block is unattach block
        //TODO: \n block is unattach block
        if (commandString.search('FD') >= 0) {
          let value = commandString.replace("FD", "");
          await this.moveFoward(parseInt(value));
        } else if (commandString.search('TURN') >= 0) {
          let value = commandString.replace("TURN", "");
          await this.rotateDrawer(parseInt(value));
        }
      }

      //TODO on ending animate all action.
      if (parseInt(index) >= arrayCode.length - 1) {
        this.setState({
          disabled: false,
        })

        const arrayAnswer = Answer.getAnswer(1);
        // console.log('== ' + Answer.equals(arrayCode, arrayAnswer))
        if (Answer.equals(arrayCode, arrayAnswer)) {
          // this.setState({
          //   code: []
          // })
          this.setState({ displayModal: true });
        }
      }
    }
  }


  async moveFoward(values) {
    var result;
    var distance = values;

    result = calculateSmoothAnimate(vizOption, Math.abs(distance));

    if (result.distance < 0)
      return resolve();

    var currentDistance = 0;
    var overAllDistane = distance;
    var safeTCut = 0;

    var executeSecondTuple: boolean = true;
    // const ThredInterval = setInterval(() => {
    return new Promise(resolve => {
      let ThredInterval = setInterval(() => {
        if (!executeSecondTuple) {
          console.log('end moveFoward')
          resolve();
          clearInterval(ThredInterval);
        }

        currentDistance = currentDistance + result.distance;
        // console.log(currentDistance + ' == ? ' + overAllDistane)
        if (currentDistance > overAllDistane) {
          executeSecondTuple = false;
        } else {
          viz.moveForward(result.distance);
          viz.display();
        }

        safeTCut++;
        if (safeTCut >= 10000) {
          executeSecondTuple = false;
        }

      }, 50)
    });
  }

  async rotateDrawer(values) {
    // console.log('rotateDrawer ' + values)
    // var tupleDone = true;
    var result;
    var distance = values;

    result = calculateSmoothAnimate(vizOption, Math.abs(distance));

    // console.log('rotate = ' + result.distance)

    if (result.distance < 0)
      return resolve();

    var currentDistance = 0;
    var overAllDistane = distance;
    var safeTCut = 0;

    var executeSecondTuple: boolean = true;
    return new Promise(resolve => {
      let ThredInterval = setInterval(() => {
        if (!executeSecondTuple) {
          // console.log('end turnByDegrees for ' + distance)
          resolve();
          clearInterval(ThredInterval);
        }

        currentDistance = currentDistance + result.distance;
        if (currentDistance > overAllDistane) {
          executeSecondTuple = false;
        } else {
          var log = { command: 'turnByDegrees', args: [Math.abs(result.distance)] };
          // console.log('log.args = ' + log.args)

          if (Math.abs(result.distance) > 0) {
            viz[log.command].apply(viz, log.args);
            viz.display();
          } else {
            //TODO force end on 0 degree
            executeSecondTuple = false;
          }

        }

        safeTCut++;
        if (safeTCut >= 365) {
          executeSecondTuple = false;
        }
      }, 50)
    });
  }

  async clickHandler(e) {
    var arrayCode = this.state.code.split("|");
    arrayCode = Answer.removeA(arrayCode, "");
    if (arrayCode.length > 0) {
      // console.log(JSON.stringify(arrayCode, null, 2))
      viz.reset()
      this.executeAction(arrayCode);
      // if (arrayCode[0] != "") {
      //   this.setState({
      //     disabled: true,
      //     code: arrayCode,
      //   })
      // }

    }
  }

  clickResetHandler(e) {
    viz.reset()
    // resetStepInfo_();
  }

  workspaceDidChange(workspace) {
    const newXml = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(workspace));
    // document.getElementById('generated-xml').innerText = newXml;

    const code = Blockly.JavaScript.workspaceToCode(workspace);
    document.getElementById('generated-xml').innerText = code;

    this.setState({
      code: code,
    });

  }


  async hideModal(e) {
    console.log('this.props.hideModal')
    this.setState({ displayModal: false });
  }

  render() {
    return (
      <div className="App">
        <ModalElsa displayModal={this.state.displayModal} hideModal={this.hideModal} imageUrl="https://studio.code.org/blockly/media/skins/elsa/win_avatar.png" nextPageUrl=""/>
  
        <div className="App-header">

          <Stage className="canvas-elsa" width={400} height={400}>
            <Layer>
              <Shape
                ref={node => {
                  shape = node;
                }}
                fill="#00D2FF"
                stroke={this.state.stroke}
                strokeWidth={4}
              />
            </Layer>
          </Stage>

          <Button variant="contained" color="primary" style={{ margin: 10 }} onClick={this.clickHandler.bind(this)} disabled={this.state.disabled}>
            Play
          </Button>
        </div>

        <pre id="generated-xml"></pre>

        <div style={{ height: 600, width: 1000 }} id="blockly">
          <ReactBlocklyComponent.BlocklyEditor
            toolboxCategories={this.state.toolboxCategories}
            workspaceConfiguration={{
              grid: {
                spacing: 20,
                length: 3,
                colour: '#ccc',
                snap: true,
              },
              collapse: false,
            }}
            initialXml={ConfigFiles.INITIAL_XML}
            wrapperDivClassName="fill-height"
            workspaceDidChange={this.workspaceDidChange}
          />
        </div>
        {/* <textarea id="code" style={{ height: 200, width: 400 }} value=""></textarea> */}
      </div>
    );
  }
}


const CANVAS_HEIGHT = 400;
const CANVAS_WIDTH = 400;

const DEFAULT_X = CANVAS_WIDTH / 2;
const DEFAULT_Y = CANVAS_HEIGHT / 2;
const DEFAULT_DIRECTION = 90;

const MAX_STICKER_SIZE = 100;

const SMOOTH_ANIMATE_STEP_SIZE = 5;
const FAST_SMOOTH_ANIMATE_STEP_SIZE = 15;

function calculateSmoothAnimate(options, distance) {
  var tupleDone = true;
  var stepDistanceCovered = viz.stepDistanceCovered;

  if (options && options.smoothAnimate) {
    var fullDistance = distance;
    var smoothAnimateStepSize = viz.smoothAnimateStepSize;

    if (fullDistance < 0) {
      // Going backward.
      if (stepDistanceCovered - smoothAnimateStepSize <= fullDistance) {
        // clamp at maximum
        distance = fullDistance - stepDistanceCovered;
        stepDistanceCovered = fullDistance;
      } else {
        distance = -smoothAnimateStepSize;
        stepDistanceCovered -= smoothAnimateStepSize;
        tupleDone = false;
      }

    } else {
      // Going forward.
      if (stepDistanceCovered + smoothAnimateStepSize >= fullDistance) {
        // clamp at maximum
        distance = fullDistance - stepDistanceCovered;
        stepDistanceCovered = fullDistance;
      } else {
        distance = smoothAnimateStepSize;
        stepDistanceCovered += smoothAnimateStepSize;
        tupleDone = false;
      }
    }
  }

  viz.stepDistanceCovered = stepDistanceCovered;

  return { tupleDone: tupleDone, distance: distance };
}

function resetStepInfo_() {
  viz.stepStartX = viz.x;
  viz.stepStartY = viz.y;
  viz.stepDistanceCovered = 0;
};

export default Elsa_02;