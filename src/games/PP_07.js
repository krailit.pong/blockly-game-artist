import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactBlocklyComponent from '../BlocklyComponent';
import ConfigFiles from '../initContent/content_pp_07';
import parseWorkspaceXml from '../../lib/react-blockly-tie/BlocklyHelper';
import { Stage, Layer, Shape, Sprite } from 'react-konva';

import './Elsa.css';
import { resolve, parse } from 'path';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Replay';
import GuideComponent from './components/GuideComponent';
import ModalElsa from './ModalElsa';
import ModalFailed from './ModalFailed';

const Blockly = require('../initContent/locale');
const Answer = require('./Answer');
var ArtistAPI = require('../utils/api');
const Visualization = require("../../visualization_pp.js");
const VisualizationDraw = require("../../visualization.js");
const AnimateSkin = require('./AnimateSkin.js')

var shape = null
var shapePattern = null
var imagePath = require('../assets/PP_Sprite.png');
var petternPath = require('../assets/PPline.png');
// var petternPath = require('../assets/elsaline.png');
// var petternPath = require('../assets/patterns/rainbow.png');
var ppimg  = require('../assets/win.png');
var ppguide  = require('../assets/small.png');
var vizOption = {
  isFrozenSkin: true,
  isK1: true,
  avatar: {
    src: imagePath,
    width: 73,
    height: 100,
    numHeadings: 18,
    numFrames: 20,
    visible: true,
  },
  smoothAnimate: true,
  consolidateTurnAndMove: true,
  artistOptions: ['elsa'],
  avatarAllowedScripts: ['frozen'],
  showDecoration: () => "elsa",
};

var vizOption2 = {
  isFrozenSkin: false,
  avatar: {
    src: '',
    width: 70,
    height: 51,
    numHeadings: 180,
    numFrames: 1,
    visible: true,
  },
  lineStylePatternOptions: [],
  linePatterns: {
    rainbowLine: require("../assets/patterns/rainbow.png"),
    ropeLine: require("../assets/patterns/rope.png"),
    squigglyLine: require("../assets/patterns/squiggly.png"),
    swirlyLine: require("../assets/patterns/swirly.png"),
  },
  ctxDisplay: null,
  showDecoration: () => null,
}
var viz = new Visualization(vizOption);
var vizDraw = new VisualizationDraw(vizOption2);


class PP_07 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      api: new ArtistAPI(),
      toolboxCategories: parseWorkspaceXml(ConfigFiles.INITIAL_TOOLBOX_XML),
      listOfAction: "",
      stroke: "#303f9f",
      image: null,
      imageSrc: imagePath,
      code: [],
      isPlaying: false,
      displayModal: false,
      displayModalFailed: false,
      currentBlock: 0,
      maxBlock: 9
    }
    this.workspaceDidChange = this.workspaceDidChange.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.onClickNextPage = this.onClickNextPage.bind(this);
  }

  componentDidMount() {
    // apiJavascript.injectArtistAPI(this.state.api);

    // window.setTimeout(() => {
    //   this.drawHint();
    // }, 800);

    window.setTimeout(() => {
      let cv = shape.getCanvas();
      viz.ctxDisplay = cv.getContext();
      viz.ctxDisplay.width = 400;
      viz.ctxDisplay.height = 400;
      viz.isDrawingWithPattern = true;
      viz.display();


      let cv2 = shapePattern.getCanvas();
      vizDraw.ctxDisplay = cv2.getContext();
      vizDraw.ctxDisplay.width = 400;
      vizDraw.ctxDisplay.height = 400;
      this.createHint();
    }, 1000);

    window.setTimeout(() => {
      var image = new Image();
      image.src = this.state.imageSrc;
      image.onload = () => {
        this.setState({
          image: image
        });
        viz.avatar.image = image;
      };

      var image2 = new Image();
      image2.src = petternPath
      image2.onload = () => {
        viz.currentPathPattern = image2;
        // viz.decorationAnimationImage = image2;
      };
    }, 10);
  }


  createHint() {
    // vizDraw.jumpTo([200, 50])
    vizDraw['setLineColor'].apply(vizDraw, ['#303f9f']);
    vizDraw['setLineHeight'].apply(vizDraw, [2]);

    let i = 0;
    while (i < 10) {
      let x = 0;
      // while (x < 4) {
      //   vizDraw['moveForward'].apply(vizDraw, [100]);
      //   vizDraw['turnByDegrees'].apply(vizDraw, [90]);
      //   x++;
      // }
      vizDraw['moveForward'].apply(vizDraw, [100]);
      vizDraw['turnByDegrees'].apply(vizDraw, [90]);
      vizDraw['moveForward'].apply(vizDraw, [100]);
      vizDraw['turnByDegrees'].apply(vizDraw, [90]);
      vizDraw['moveForward'].apply(vizDraw, [100]);
      vizDraw['turnByDegrees'].apply(vizDraw, [90]);
      vizDraw['moveForward'].apply(vizDraw, [100]);
      vizDraw['turnByDegrees'].apply(vizDraw, [45]);
      i++;
    }

    vizDraw.display();
  }

  async executeAction(arrayCode) {
    //TODO: for-in is react special async loop
    console.log('arrayCode count = ' + arrayCode.length)

    for (const index in arrayCode) {
      let commandString = arrayCode[index];
      commandString = commandString.replace("/**/", '')
      console.log('index = ' + index + ' of ' + commandString)

      await this.evaluateCommand(commandString)

      //TODO on ending animate all action.
      if (parseInt(index) >= arrayCode.length - 1) {
        this.setState({
          isPlaying: false,
        })
        const arrayAnswer = Answer.getAnswer(6);
        if (this.state.currentBlock == this.state.maxBlock) {
          if (Answer.equals(arrayCode, arrayAnswer)) {
            // alert('ths.state.currentBlock == this.state.maxBlock ' + this.state.currentBlock == this.state.maxBlock)
            this.setState({ displayModal: true });
          }
        } else if (this.state.currentBlock > this.state.maxBlock){
          this.setState({
            displayModalFailed: true,
          })
        }
      }
    }
  }

  async evaluateCommand(commandString) {
    if (commandString.search('\n') < 0) {
      //TODO: \n block is unattach block
      if (commandString.search('LOOP_WHILE') >= 0) {
        let value = commandString.replace("LOOP_WHILE,", "");
        let arrayInfo = value.split(',')
        let repeatTimes = arrayInfo[0]
        let statement = arrayInfo[1]

        let arrayStatement = statement.split('^');
        // console.log('arrayStatement = ' + arrayStatement)

        for (let i = 0; i < repeatTimes; i++) {
          for(let j = 0; j < arrayStatement.length; j++) { 
            console.log('EVAL = ' + arrayStatement[j])
            if (arrayStatement[j] != '')
              await this.evaluateCommand(arrayStatement[j])
          }
        }
        // await this.rotateDrawer(parseInt(value));
      } else if (commandString.search('FD') >= 0) {
        let value = commandString.replace("FD", "");
        await this.moveFoward(parseInt(value));
      } else if (commandString.search('TURN') >= 0) {
        let value = commandString.replace("TURN", "");
        await this.rotateDrawer(parseInt(value));
      }
    }
  }

  async moveFoward(values) {
    return AnimateSkin.animMoveForward(viz, vizOption, values)
  }

  async rotateDrawer(values) {
    console.log('rotateDrawer ' + values)
    return AnimateSkin.animRotate(viz, vizOption, values)
  }

  clickHandler(e) {
    console.log('onClick Code = ' + this.state.code)

    if (this.state.isPlaying) {
      this.clickStopHandler()
      return
    }

    if (this.state.code == null) {
      return
    }

    AnimateSkin.resetStepInfo_(viz)
    viz.reset()

    let code = this.state.code
    var arrayCode = this.state.code.split("|");
    arrayCode = Answer.removeA(arrayCode, "");
    if (arrayCode.length > 0) {
      // console.log(JSON.stringify(arrayCode, null, 2))
      this.executeAction(arrayCode)
      if (arrayCode[0] != "") {
        this.setState({
          isPlaying: true,
          arrayCode: arrayCode,
        })
      }
    }
  }

  async clickStopHandler(e) {
    AnimateSkin.clearAllInterval_()
    AnimateSkin.resetStepInfo_(viz)
    viz.reset()

    this.setState({
      isPlaying: false,
    })
  }


  clickResetHandler(e) {
    viz.reset()
    // resetStepInfo_();
  }

  workspaceDidChange(workspace) {
    const newXml = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(workspace));
    // document.getElementById('generated-xml').innerText = newXml;

    const code = Blockly.JavaScript.workspaceToCode(workspace);
    // document.getElementById('generated-xml').innerText = code;

    let newCode = AnimateSkin.calculateCodeForPangpond(code)
    this.setState({
      code: newCode,
      currentBlock: AnimateSkin.countCode_(newCode)
    })

  }

  async hideModal(e) {
    this.setState({ displayModal: false });
  }

  onClickNextPage(e) {
    this.setState({ displayModal: false });
    this.props.goToNext(0)
  }

  render() {
    return (
      <div className="">
        <ModalElsa displayModal={this.state.displayModal} hideModal={this.hideModal} imageUrl={ppimg} onClickNextPage={this.onClickNextPage} />
        <ModalFailed displayModal={this.state.displayModalFailed} hideModal={() => {
          this.setState({
            displayModalFailed: false
          })
        }} />
        <Grid container>
          <Grid item xs={6} md={4} className="canvas-bg">
            <div className="elsa-container">

              <Stage className="pattern" width={400} height={400} >
                <Layer>
                  <Shape
                    ref={node => {
                      shapePattern = node;
                    }}
                    fill="#00D2FF"
                    stroke={this.state.stroke}
                    strokeWidth={2}
                  />
                </Layer>
              </Stage>

              <Stage className="canvas-elsa" width={400} height={400}>
                <Layer>
                  <Shape
                    ref={node => {
                      shape = node;
                    }}
                  />
                </Layer>
              </Stage>

            </div>
            <Button className="button-play" variant="contained" color={!this.state.isPlaying ? 'primary' : 'light'} style={{ margin: 10 }} onClick={this.clickHandler.bind(this)}>
              {!this.state.isPlaying ? <PlayArrowIcon /> : <StopIcon />}
              {!this.state.isPlaying ? 'เริ่มเดิน' : 'เริ่มใหม่'}
            </Button>
          </Grid>
          <Grid item xs={6} md={8} className="canvas-bg">
            <GuideComponent currentBlock={this.state.currentBlock} maxBlock={this.state.maxBlock} messages={"สวัสดีครับ! ผมชื่อปังปอนด์ ช่วยให้ผม"}></GuideComponent>
            <div style={{ height: 600, width: 1000 }} id="blockly">
              <ReactBlocklyComponent.BlocklyEditor
                toolboxCategories={this.state.toolboxCategories}
                workspaceConfiguration={{
                  grid: {
                    spacing: 20,
                    length: 3,
                    colour: '#ccc',
                    snap: true,
                  },
                  collapse: false,
                }}
                initialXml={ConfigFiles.INITIAL_XML}
                wrapperDivClassName="fill-height"
                workspaceDidChange={this.workspaceDidChange}
              />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default PP_07;