import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactBlocklyComponent from '../BlocklyComponent';
import ConfigFiles from '../initContent/content_pp_01';
import parseWorkspaceXml from '../../lib/react-blockly-tie/BlocklyHelper';
import { Stage, Layer, Shape, Sprite } from 'react-konva';

import './Elsa.css';
import { resolve, parse } from 'path';

import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { withStyles } from '@material-ui/core/styles';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Replay';
import GuideComponent from './components/GuideComponent';
import ModalElsa from './ModalElsa';
import ModalFailed from './ModalFailed';

const Blockly = require('../initContent/locale');
const Answer = require('./Answer');
var ArtistAPI = require('../utils/api');
var apiJavascript = require('../utils/apiJavascript');
const Visualization = require("../../visualization_pp.js");
const VisualizationHint = require("../../visualization.js");
const AnimateSkin = require('./AnimateSkin.js')

var shape = null
var shapePattern = null
var imagePath = require('../assets/PP_Sprite.png');
var petternPath = require('../assets/PPline.png');
// var petternPath = require('../assets/elsaline.png');
// var petternPath = require('../assets/patterns/rainbow.png');
var ppimg  = require('../assets/win.png');
var ppguide  = require('../assets/small.png');
var vizOption = {
  isFrozenSkin: true,
  isK1: true,
  // avatar: {
  //   src: imagePath,
  //   width: 74.5,
  //   height: 137.55,
  //   numHeadings: 18,
  //   numFrames: 20,
  //   visible: true,
  // },
  // 1314 × 2000 pixels
  // 1341 × 2751 pixels
  avatar: {
    src: imagePath,
    width: 73,
    height: 100,
    numHeadings: 18,
    numFrames: 20,
    visible: true,
  },
  smoothAnimate: true,
  consolidateTurnAndMove: true,
  artistOptions: ['elsa'],
  avatarAllowedScripts: ['frozen'],
  showDecoration: () => "elsa",
  // shouldDrawNormalized_: true,
  // isPredrawing_: true,
  // isDrawingWithPattern: true,
  // decorationAnimationImage: petternPath,
  // blankAvatar: skin.assetUrl('blank.png'),
};

var vizHintoption = {
  isFrozenSkin: false,
  avatar: {
    src: '',
    width: 70,
    height: 51,
    numHeadings: 180,
    numFrames: 1,
    visible: true,
  },
  lineStylePatternOptions: [],
  linePatterns: {
    rainbowLine: require("../assets/patterns/rainbow.png"),
    ropeLine: require("../assets/patterns/rope.png"),
    squigglyLine: require("../assets/patterns/squiggly.png"),
    swirlyLine: require("../assets/patterns/swirly.png"),
  },
  ctxDisplay: null,
  showDecoration: () => null,
}

var viz = new Visualization(vizOption);
var vizHint = new VisualizationHint(vizHintoption);


class PP_03 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      api: new ArtistAPI(),
      toolboxCategories: parseWorkspaceXml(ConfigFiles.INITIAL_TOOLBOX_XML),
      listOfAction: "",
      stroke: "#303f9f",
      image: null,
      imageSrc: imagePath,
      code: [],
      isPlaying: false,
      displayModal: false,
      displayModalFailed: false,
      currentBlock: 0,
      maxBlock: 7
    }
    this.workspaceDidChange = this.workspaceDidChange.bind(this);
    this.hideModal = this.hideModal.bind(this);
    this.onClickNextPage = this.onClickNextPage.bind(this);
  }

  componentDidMount() {
    apiJavascript.injectArtistAPI(this.state.api);

    // window.setTimeout(() => {
    //   this.drawHint();
    // }, 800);

    window.setTimeout(() => {
      let cv = shape.getCanvas();
      viz.ctxDisplay = cv.getContext();
      viz.ctxDisplay.width = 400;
      viz.ctxDisplay.height = 400;
      viz.isDrawingWithPattern = true;

      // let cp = shapePattern.getCanvas()
      // viz.ctxPattern = cp.getContext();
      viz.reset();
      viz.display();

      let cvhint = shapePattern.getCanvas();
      vizHint.ctxDisplay = cvhint.getContext();
      vizHint.ctxDisplay.width = 400;
      vizHint.ctxDisplay.height = 400;
      vizHint.isDrawingWithPattern = false;

      vizHint.reset();
      this.createHint();
    }, 1000);

    window.setTimeout(() => {
      var image = new Image();
      image.src = this.state.imageSrc;
      image.onload = () => {
        this.setState({
          image: image
        });
        viz.avatar.image = image;
      };

      var image2 = new Image();
      image2.src = petternPath
      image2.onload = () => {
        viz.currentPathPattern = image2;
        // viz.decorationAnimationImage = image2;
      };
    }, 10);
  }

  createHint() {
    vizHint['setLineColor'].apply(vizHint, ['#303f9f']);
    vizHint['setLineHeight'].apply(vizHint, [2])
    vizHint['moveForward'].apply(vizHint, [100]);
    vizHint['turnByDegrees'].apply(vizHint, [90]);
    vizHint['moveForward'].apply(vizHint, [100]);
    vizHint['turnByDegrees'].apply(vizHint, [90]);
    vizHint['moveForward'].apply(vizHint, [100]);
    vizHint['turnByDegrees'].apply(vizHint, [90]);
    vizHint['moveForward'].apply(vizHint, [100]);
    vizHint.display();
  }

  async executeAction(arrayCode) {
    //TODO: for-in is react special async loop
    for (const index in arrayCode) {
      // console.log('index = ' + index + ' of ' + arrayCode.length)
      let commandString = arrayCode[index];
      if (commandString.search('\n') < 0) {
        //TODO: \n block is unattach block
        if (commandString.search('FD') >= 0) {
          let value = commandString.replace("FD", "");
          await this.moveFoward(parseInt(value));
        } else if (commandString.search('TURN') >= 0) {
          let value = commandString.replace("TURN", "");
          await this.rotateDrawer(parseInt(value));
        }
      }

      //TODO on ending animate all action.
      if (parseInt(index) >= arrayCode.length - 1) {
        this.setState({
          isPlaying: false,
        })
        const arrayAnswer = Answer.getAnswer(2);
        if (this.state.currentBlock == this.state.maxBlock) {
          if (Answer.equals(arrayCode, arrayAnswer)) {
            // alert('ths.state.currentBlock == this.state.maxBlock ' + this.state.currentBlock == this.state.maxBlock)
            this.setState({ displayModal: true });
          }
        } else if (this.state.currentBlock > this.state.maxBlock){
          this.setState({
            displayModalFailed: true,
          })
        }
      }
    }
  }

  async moveFoward(values) {
    return AnimateSkin.animMoveForward(viz, vizOption, values)
  }

  async rotateDrawer(values) {
    return AnimateSkin.animRotate(viz, vizOption, values)
  }

  async clickHandler(e) {
    console.log('this.state.code ' + this.state.code)
    if (this.state.isPlaying) {
      this.clickStopHandler()
      return
    }

    if (this.state.code == null) {
      return
    }
    var arrayCode = this.state.code.split("|");
    arrayCode = Answer.removeA(arrayCode, "");
    if (arrayCode.length > 0) {
      // console.log(JSON.stringify(arrayCode, null, 2))
      AnimateSkin.resetStepInfo_(viz)
      viz.reset()
      this.executeAction(arrayCode);
      if (arrayCode[0] != "") {
        this.setState({
          isPlaying: true,
          arrayCode: arrayCode,
        })
      }
    }
  }

  async clickStopHandler(e) {
    AnimateSkin.clearAllInterval_()
    AnimateSkin.resetStepInfo_(viz)
    viz.reset()

    this.setState({
      isPlaying: false,
    })
  }

  clickResetHandler(e) {
    viz.reset()
    // resetStepInfo_();
  }

  workspaceDidChange(workspace) {
    const newXml = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(workspace));
    // document.getElementById('generated-xml').innerText = newXml;

    const code = Blockly.JavaScript.workspaceToCode(workspace);
    // document.getElementById('generated-xml').innerText = code;

    let newCode = AnimateSkin.calculateCodeForPangpond(code)
    this.setState({
      code: newCode,
      currentBlock: AnimateSkin.countCode_(newCode)
    })

  }

  async hideModal(e) {
    this.setState({ displayModal: false });
  }

  onClickNextPage(e) {
    this.setState({ displayModal: false });
    this.props.goToNext(3)
  }

  render() {
    return (
      <div className="">
        <ModalElsa displayModal={this.state.displayModal} hideModal={this.hideModal} imageUrl={ppimg} onClickNextPage={this.onClickNextPage} />
        <ModalFailed displayModal={this.state.displayModalFailed} hideModal={() => {
          this.setState({
            displayModalFailed: false
          })
        }} />
        <Grid container>
          <Grid item xs={6} md={4} className="canvas-bg">
   
            <div className="elsa-container">

              <Stage className="pattern" width={400} height={400} >
                <Layer>
                  <Shape
                    ref={node => {
                      shapePattern = node;
                    }}
                    fill="#00D2FF"
                    stroke={this.state.stroke}
                    strokeWidth={2}
                  />
                </Layer>
              </Stage>

              <Stage className="canvas-elsa" width={400} height={400}>
                <Layer>
                  <Shape
                    ref={node => {
                      shape = node;
                    }}
                  />
                </Layer>
              </Stage>

            </div>

            <Button className="button-play" variant="contained" color={!this.state.isPlaying ? 'primary' : 'light'} style={{ margin: 10 }} onClick={this.clickHandler.bind(this)}>
              {!this.state.isPlaying ? <PlayArrowIcon /> : <StopIcon />}
              {!this.state.isPlaying ? 'เริ่มเดิน' : 'เริ่มใหม่'}
            </Button>
          </Grid>
          <Grid item xs={6} md={8} className="canvas-bg">
          <GuideComponent currentBlock={this.state.currentBlock} maxBlock={this.state.maxBlock} messages={"สวัสดีครับ! ผมชื่อปังปอนด์ น้องๆช่วยผมเลี้ยงบอลเป็นรูป4เหลี่ยมหน่อยครับ"}></GuideComponent>
            <div style={{ height: 600, width: 1000 }} id="blockly">
              <ReactBlocklyComponent.BlocklyEditor
                toolboxCategories={this.state.toolboxCategories}
                workspaceConfiguration={{
                  grid: {
                    spacing: 20,
                    length: 3,
                    colour: '#ccc',
                    snap: true,
                  },
                  collapse: false,
                }}
                initialXml={ConfigFiles.INITIAL_XML}
                wrapperDivClassName="fill-height"
                workspaceDidChange={this.workspaceDidChange}
              />
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default PP_03;