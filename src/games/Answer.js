export const getAnswer = function (choice_no) {
    switch (choice_no) {
        case 0: {
            return [
                'FD100'
            ];
            break;
        }
        case 1: {
            return [
                'FD100',
                'TURN90',
                'FD100'
            ];
            break;
        }
        case 2: {
            return [
                'FD100',
                'TURN90',
                'FD100',
                'TURN90',
                'FD100',
                'TURN90',
                'FD100'
            ];
            break;
        }
        case 3: {
            return [
                'FD100',
                'TURN90',
                'FD100',
                'TURN-90',
                'FD50',
                'TURN-90',
                'FD100',
                'TURN90',
                'FD100'
            ];
            break;
        }
        case 4: {
            return [
                'FD100',
                'TURN45',
                'FD100',
                'TURN-90',
                'FD50',
                'TURN-90',
                'FD100',
                'TURN-45',
                'FD100'
            ];
            break;
        }
        case 5: {
            return [
                'FD200',
                'TURN180',
                'FD100',
                'TURN-90',
                'FD100',
                'TURN-180',
                'FD200'
            ];
            break;
        }
    }
}


export const removeA = function (arr) {
    // To remove inside array by value.
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}


// attach the .equals method to Array's prototype to call it on any array
export const equals = function (array1, array2) {
    // if the other array is a falsy value, return
    if (!array1 && !array2)
        return false;

    // compare lengths - can save a lot of time 
    if (array1.length != array2.length)
        return false;

    for (var i = 0, l=array1.length; i < l; i++) {
        // Check if we have nested arrays
        if (array1[i] instanceof Array && array2[i] instanceof Array) {
            // recurse into the nested arrays
            if (!array1[i].equals(array2[i]))
                return false;       
        }           
        else if (array1[i] != array2[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}