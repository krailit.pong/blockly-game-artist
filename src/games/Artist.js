import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import ReactBlocklyComponent from '../BlocklyComponent';
import ConfigFiles from '../initContent/content_artist';
import parseWorkspaceXml from '../../lib/react-blockly-tie/BlocklyHelper';
import { Stage, Layer, Shape, Sprite } from 'react-konva';
import Konva from 'konva';
import '../App.css';
import '../assets/artist/style.css';
import { timingSafeEqual } from 'crypto';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import SnackbarContent from '@material-ui/core/SnackbarContent';

const blocks = require('../initContent/blocks_artist');
const Blockly = require('../initContent/locale');
const Answer = require('./Answer');
var Colours = require('../initContent/colours');
var ArtistAPI = require('../utils/api');
var apiJavascript = require('../utils/apiJavascript');
const Visualization = require("../../visualization.js");

var shape = null
// var imagePath = require('../assets/avatar2.png');
var imagePath = require('../assets/pp_pen.png');
var ppguide = require('../assets/small.png');
var viz = new Visualization({
  isFrozenSkin: false,
  avatar: {
    src: null,
    width: 70,
    // width: 80,
    height: 51,
    numHeadings: 180,
    numFrames: 1,
    visible: true,
  },
  lineStylePatternOptions: [],
  linePatterns: {
    rainbowLine: require("../assets/patterns/rainbow.png"),
    ropeLine: require("../assets/patterns/rope.png"),
    squigglyLine: require("../assets/patterns/squiggly.png"),
    swirlyLine: require("../assets/patterns/swirly.png"),
  },
  ctxDisplay: null,
  showDecoration: () => null,
});

function preload(src, target) {
  return new Promise((resolve, reject) => {
    const img = new Image();

    img.onload = () => resolve();
    img.onerror = () => reject();

    img.src = src;
    target.image = img;
  });
}

class Artist extends Component {

  constructor(props) {
    super(props);
    this.state = {
      api: new ArtistAPI(),
      toolboxCategories: parseWorkspaceXml(ConfigFiles.INITIAL_TOOLBOX_XML),
      stroke: "black",
      brushSize: 1,
      image: null,
      imageSrc: imagePath,
    }

    this.workspaceDidChange = this.workspaceDidChange.bind(this);

  }

  componentDidMount() {
    apiJavascript.injectArtistAPI(this.state.api);
    window.setTimeout(() => {
      blocks.install(Blockly);
    }, 10);

    window.setTimeout(() => {
      let cv = shape.getCanvas();
      viz.ctxDisplay = cv.getContext();
      viz.display();
    }, 1000);

    var image = new Image();
    image.src = this.state.imageSrc;
    image.onload = () => {
      this.setState({
        image: image
      });

      viz.avatar.image = image;
    };
  }

  drawStar() {
    var log = [];
    for (let i = 0; i < 5; i++) {
      log.push({ command: 'moveForward', args: [100] });
      log.push({ command: 'turnByDegrees', args: [216] });
    }

    const drawLoop = setInterval(() => {
      const step = log.shift();
      if (!step) {
        clearInterval(drawLoop);
      } else {
        viz[step.command].apply(viz, step.args);
        viz.display();
      }
    }, 2000);
  }


  drawLine(value) {
    var log = { command: 'moveForward', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  rotateDrawerRight(value) {
    var log = { command: 'turnByDegrees', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  rotateDrawerLeft(value) {
    var log = { command: 'turnByDegreesLeft', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerJumpforward(value) {
    var log = { command: 'jumpForward', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerJumpbackward(value) {
    var log = { command: 'jumpBackward', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerPointTo(value) {
    var log = { command: 'pointTo', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerLineColor(value) {
    var log = { command: 'setLineColor', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerLineAlpha(value) {
    let currentColor = viz.ctxScratch.strokeStyle
    var rgbA = (Colours.hexToRgbA(currentColor)).replace(/[^,]+(?=\))/, value / 10)
    let newHex = Colours.rgba2hex(rgbA)
    var log = { command: 'setLineColor', args: ['#' + newHex] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerJumper(value) {
    var log = { command: 'jumpTo', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerLineHeight(value) {
    var log = { command: 'setLineHeight', args: [value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  drawerCircle(value) {
    var log = { command: 'drawJointAtTurtle_', args: [viz.x, viz.y, value] };
    viz[log.command].apply(viz, log.args);
    viz.display();
  }

  clickHandler() {

  }

  handleClick() {
    viz.reset()
  }

  workspaceDidChange(workspace) {
    const newXml = Blockly.Xml.domToText(Blockly.Xml.workspaceToDom(workspace));
    // document.getElementById('generated-xml').innerText = newXml;

    const code = Blockly.JavaScript.workspaceToCode(workspace);
    // document.getElementById('generated-xml').innerText = code;
    if (code != '') {
      viz.reset()
      let brIndex = code.search('\n');
      // console.log('Log Code. iIndex =' + iIndex)


      if (parseInt(brIndex) < 0) {
        // console.log(code)
        // var myInterpreter = new Interpreter(code);
        // myInterpreter.run()
        eval(code)
      } else {
        let newCode = code.substr(0, parseInt(brIndex));
        // console.log('newCode = ' + newCode)
        // var myInterpreter = new Interpreter(newCode);
        // myInterpreter.run()
        eval(newCode)
      }

    }
  }


  render() {
    return (
      <div className="App">
        <div className="">
          <Grid container spacing={24}>
            <Grid item xs={12} md={12} style={{ padding: 4 }}>
              <Paper className="">
                <AppBar position="static" color="secondary">
                  <Toolbar variant="dense">
                    {/* <div >
                      <img alt="Remy Sharp" src={require('../assets/betime.png')} style={{ margin: 5, height: 50, }} />
                    </div> */}
                    {/* <Typography variant="h6" color="inherit" style={{ margin: 5 }}>
                      Betimes Solution
                    </Typography> */}
                    {/* <Button variant="contained" className="" className="btn_menu">
                      Default
                    </Button>
                    <Button variant="contained" color="primary" className="btn_menu">
                      Primary
                    </Button>
                    <Button variant="contained" color="secondary" className="btn_menu">
                      Secondary
                    </Button>
                    <IconButton className="" color="inherit" aria-label="Menu" className="hamburger">
                      <MenuIcon />
                    </IconButton> */}
                  </Toolbar>
                </AppBar>
              </Paper>
            </Grid>
            <Grid item xs={6} md={3} style={{ backgroundColor: '#ddd' }}>
              <div className="">
                <div className=""></div>
                <Stage className="display" width={400} height={400}>
                  <Layer>
                    <Shape
                      ref={node => {
                        shape = node;
                      }}
                      fill="#00D2FF"
                      stroke={this.state.stroke}
                      strokeWidth={this.state.brushSize}
                    />
                  </Layer>
                </Stage>
                {/* <Button variant="contained" onClick={this.handleClick} className="" className="btn-play">
                  Default
                </Button> */}
              </div>
            </Grid>


            <Grid item xs={6} md={8} className="canvas-bg">
              <Grid item xs={12} md={12} className="gridsnak">
                <img className="teacherpp" src={ppguide}></img>
                <SnackbarContent
                  className="snakbar"
                  message={
                    "สวัสดีครับ! ผมชื่อปังปอนด์ น้องๆสามารถวาดรูปอิสระโดยใช้บล็อคที่อยู่ทางด้านล่างได้เลยครับ "
                  }
                />
              </Grid>
              <div className="col-a">กล่องเครื่องมือ</div>
              <div className="col-b">พื้นที่ทำงาน :
                <Button variant="contained" color="secondary" className="btn_setting">เริ่มเล่นใหม่</Button>
                <Button variant="contained" color="secondary" className="btn_setting">แสดงโค้ด</Button>
              </div>
              <div style={{ height: 600, width: 1000 }} id="blockly">
                <ReactBlocklyComponent.BlocklyEditor
                  toolboxCategories={this.state.toolboxCategories}
                  workspaceConfiguration={{
                    grid: {
                      spacing: 20,
                      length: 3,
                      colour: '#ccc',
                      snap: true,
                    },
                    collapse: false,
                  }}
                  initialXml={ConfigFiles.INITIAL_XML}
                  wrapperDivClassName="fill-height"
                  workspaceDidChange={this.workspaceDidChange}
                />
              </div>
            </Grid>
            <Grid item xs={12} md={12}>
              <Paper className="">
                <pre id="generated-xml"></pre>
              </Paper>
            </Grid>
          </Grid>
        </div>
      </div>
    );
  }
}

export default Artist;