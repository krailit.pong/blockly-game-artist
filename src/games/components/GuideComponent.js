import React, { Component } from 'react';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Grid from '@material-ui/core/Grid';
import '../Elsa.css';
var ppguide = require('../../assets/small.png');

class GuideComponent extends Component {
  render() {
    return (
      <Grid container>
        <Grid item xs={12} md={12} className="gridsnak">
          <img className="teacherpp" src={ppguide}></img>
          <SnackbarContent
            className="snakbar"
            message={this.props.messages}
          />
        </Grid>
        <Grid item xs={12} md={12} >
          <div className="bg-toolbar">
            <Grid container>
              <Grid item xs={12} md={3} >
                <span className="toolbar-text">กล่องเครื่องมือ</span>
              </Grid>
              <Grid item xs={12} md={9} >
                {(this.props.currentBlock <= this.props.maxBlock) && (
                  <div className="toolbar-text">พื้นที่ทำงาน : {this.props.currentBlock} / {this.props.maxBlock} </div>
                )}

                {(this.props.currentBlock > this.props.maxBlock) && (
                  <div className="toolbar-text" style={{ color: "red" }}>พื้นที่ทำงาน : {this.props.currentBlock} / {this.props.maxBlock} </div>
                )}
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    )
  }
}

export default GuideComponent;