var speedInt = 50;
var pid = 0;
const CANVAS_HEIGHT = 400;
const CANVAS_WIDTH = 400;

const DEFAULT_X = CANVAS_WIDTH / 2;
const DEFAULT_Y = CANVAS_HEIGHT / 2;
const DEFAULT_DIRECTION = 90;

const MAX_STICKER_SIZE = 100;

const SMOOTH_ANIMATE_STEP_SIZE = 5;
const FAST_SMOOTH_ANIMATE_STEP_SIZE = 15;

export const animMoveForward = function (viz, vizOption, values) {
  var result;
  var distance = values;

  result = calculateSmoothAnimate(viz, vizOption, Math.abs(distance));

  // console.log('animMoveForward = ' + result.distance)
  if (result.distance < 0)
    result.distance = result.distance * -1
  // return resolve();

  var currentDistance = 0;
  var overAllDistane = distance;
  var safeTCut = 0;

  var executeSecondTuple: boolean = true;
  return new Promise(resolve => {
    pid = setInterval(() => {
      // if (!executeSecondTuple) {
      //   resolve();
      //   clearInterval(pid);
      // }

      currentDistance = currentDistance + result.distance;
      // console.log(currentDistance + ' == ? ' + overAllDistane)
      if (currentDistance > overAllDistane) {
        // executeSecondTuple = false;
        resolve();
        clearInterval(pid);
      } else {
        viz.moveForward(result.distance);
        viz.display();
      }

      safeTCut++;
      if (safeTCut >= 10000) {
        resolve();
        clearInterval(pid);
        // executeSecondTuple = false;
      }

    }, speedInt)
  });
}

export const animRotate = function (viz, vizOption, values) {
  var result;
  var distance = values;

  result = calculateSmoothAnimate(viz, vizOption, Math.abs(distance));

  // console.log('rotate = ' + result.distance)

  if (result.distance < 0)
    result.distance = result.distance * -1

  var currentDistance = 0;
  var safeTCut = 0;

  var executeSecondTuple: boolean = true;
  return new Promise(resolve => {
    pid = setInterval(() => {
      if (!executeSecondTuple) {
        // console.log('end turnByDegrees for ' + distance)
        resolve();
        clearInterval(pid);
      }

      if (values < 0) {
        currentDistance = currentDistance + result.distance;
        if (currentDistance * -1 < distance) {
          executeSecondTuple = false;
          resolve();
          clearInterval(pid);
        } else {
          var log = { command: 'turnByDegrees', args: [-Math.abs(result.distance)] };
          // console.log('log.args = ' + log.args)

          if (Math.abs(result.distance) > 0) {
            viz[log.command].apply(viz, log.args);
            viz.display();
          } else {
            //TODO force end on 0 degree
            executeSecondTuple = false;
            resolve();
            clearInterval(pid);
          }
        }
      } else {
        currentDistance = currentDistance + result.distance;
        if (currentDistance > distance) {
          executeSecondTuple = false;
          resolve();
          clearInterval(pid);
        } else {
          var log = { command: 'turnByDegrees', args: [Math.abs(result.distance)] };
          // console.log('log.args = ' + log.args)

          if (Math.abs(result.distance) > 0) {
            viz[log.command].apply(viz, log.args);
            viz.display();
          } else {
            //TODO force end on 0 degree
            executeSecondTuple = false;
            resolve();
            clearInterval(pid);
          }
        }
      }

      safeTCut++;
      if (safeTCut >= 365) {
        executeSecondTuple = false;
      }
    }, speedInt / 2)
  });
}

export const calculateCodeForPangpond = function (code) {
  let starterWithBr = code.search('starter\n');
  // console.log('allCode = ' + code)
  if (parseInt(starterWithBr) < 0) {
    /* if not found "starter\n" */
    // console.log(' not found starterBr ')
    var newCode = code.replace('starter', '')
    let brIndex = newCode.search('\n');
    if (parseInt(brIndex) < 0) {
      /* if not found "\n" */
      // console.log(' not found brIndex ')
      return newCode
    } else {
      /* found "\n" then substring "\n" to the next*/
      // console.log(' found brIndex ')
      newCode = newCode.substr(0, parseInt(brIndex));
      // console.log('newCode = ' + newCode)
      if (newCode != '')
        return newCode
      else
        return ''
    }
  } else {
    return ''
  }
};

export const countCode_ = function (code) { 
  var arrayCode = code.split("|");
  const Answer = require('./Answer')
  // arrayCode = Answer.removeA(arrayCode, "");
  return arrayCode.length - 1
}

export const resetStepInfo_ = function (viz) {
  viz.stepStartX = viz.DEFAULT_X;
  viz.stepStartY = viz.DEFAULT_Y;
  viz.stepDistanceCovered = 0;
};

export const clearAllInterval_ = function () {
  // var interval_id = window.setInterval("", 9999); // Get a reference to the last
  // // interval +1
  // for (var i = 1; i < interval_id; i++)
    window.clearInterval(pid);
}

function calculateSmoothAnimate(viz, options, distance) {
  var tupleDone = true;
  var stepDistanceCovered = viz.stepDistanceCovered;

  if (options && options.smoothAnimate) {
    var fullDistance = distance;
    var smoothAnimateStepSize = viz.smoothAnimateStepSize;

    if (fullDistance < 0) {
      // Going backward.
      if (stepDistanceCovered - smoothAnimateStepSize <= fullDistance) {
        // clamp at maximum
        distance = fullDistance - stepDistanceCovered;
        stepDistanceCovered = fullDistance;
      } else {
        distance = -smoothAnimateStepSize;
        stepDistanceCovered -= smoothAnimateStepSize;
        tupleDone = false;
      }

    } else {
      // Going forward.
      if (stepDistanceCovered + smoothAnimateStepSize >= fullDistance) {
        // clamp at maximum
        distance = fullDistance - stepDistanceCovered;
        stepDistanceCovered = fullDistance;
      } else {
        distance = smoothAnimateStepSize;
        stepDistanceCovered += smoothAnimateStepSize;
        tupleDone = false;
      }
    }
  }

  viz.stepDistanceCovered = stepDistanceCovered;

  return { tupleDone: tupleDone, distance: distance };
}