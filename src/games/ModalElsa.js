import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Modal from '@material-ui/core/Modal';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import './Elsa.css';

class ModalElsa extends Component {
  render() {
    const imageUrl = this.props.imageUrl;
    const nextPageUrl = this.props.nextPageUrl;
    return (
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={this.props.displayModal}
        onClose={this.props.hideModal}
        disableAutoFocus={true} >
        <div className="modal-display">
          <Grid container spacing={16}>
            <Grid item xs={3}>
              <img className="modal-image" src={imageUrl}></img>
            </Grid>
            <Grid item xs={8}>
              <Typography variant="h6" id="modal-title">
                เยี่ยมมาก สำเร็จแล้ว!
              </Typography>
              <Typography variant="subtitle1" id="simple-modal-description">
                {/* กดเพื่อไปด่านต่อไป */}
              </Typography>
            </Grid>
            <Grid item xs={1}>
              <Button variant="fab" className="modal-close-btn" color="secondary" onClick={this.props.hideModal}>
                <CloseIcon />
              </Button>
            </Grid>
          </Grid>
          <Grid>
            <Button variant="contained" color="primary" style={{ margin: 10 }} onClick={this.props.onClickNextPage}>
              ไปด่านถัดไป
            </Button>
          </Grid>
        </div>
      </Modal>
    )
  }
}

export default ModalElsa;