const webpack = require('webpack');
const path = require('path');

module.exports = {
  entry: [
    // 'webpack-dev-server/client?http://0.0.0.0:81/node',
    // './src/PlaygroundEditor.jsx'
    './src/index.js'
  ],
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: "react-blockly-component.js",
    libraryTarget: "var",
    library: "ReactBlocklyComponent",
    publicPath: '/'
  },
  devtool: 'inline-source-map',
  module: {
    loaders: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015','stage-1']
                }
      },
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      {
        test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
        loader: 'url-loader?limit=100000'
      },
    ]
  },
  plugins: [
    new webpack.ProvidePlugin({
      React: 'react',
      ReactDOM: 'react-dom',
    }),
  ],
  resolve: {
    extensions: ['.js', '.jsx', '.css']
  },
  devServer: {
    contentBase: './public',
    filename: 'react-blockly-component.js',
    historyApiFallback: true,
    disableHostCheck: true,
  }
};
